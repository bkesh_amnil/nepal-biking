/* product filter starts */
var value;
var val;
$(document).on("change", "select#parent_product_category", function(e) {
    value = $(this).find("option:selected").val();

    $("select#child_product_category option").not("." + value).hide();
    $("select#child_product_category option." + value).show();
    $("select#child_product_category option." + value + ":first").prop('selected', 'selected');


    val = $("select#child_product_category option:selected").val();
    a.setOptions({params: {parent_id: value, child_id: val}});

    $.ajax({
        url : $("#baseUrl").val() + 'search/product/',
        type : "POST",
        dataType : "JSON",
        data : {parent_id : value, child_id : val},
        success : function(data) {
            $("div#product-listing").html("").html(data);
            return false;
        },
        error : function() {
            alert('Error occured');
            return false;
        }
    })
});

var a = $("input#product_name_search").autocomplete({
    serviceUrl: $("#baseUrl").val() + 'search/product/',
    minChars: 1,
    delimiter: /(,|;)\s*/,
    maxHeight: 400,
    width: 200,
    deferRequestBy: 0,
    onSelect: function(value, data) {
        window.location = data;
    },
})

$(document).on("change", "select#child_product_category", function(e) {
    value = $("select#parent_product_category option:selected").val();
    val = $(this).find("option:selected").val();
    a.setOptions({params: {parent_id: val, child_id: value}});

    $.ajax({
        url : $("#baseUrl").val() + 'search/product/',
        type : "POST",
        dataType : "JSON",
        data : {parent_id : value, child_id : val},
        success : function(data) {
            $("div#product-listing").html("").html(data);
            return false;
        },
        error : function() {
            alert('Error occured');
            return false;
        }
    })
});
/* product filter ends */