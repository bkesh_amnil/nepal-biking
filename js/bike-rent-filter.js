/* bike_rent filter starts */
var value;
var val;
$(document).on("change", "select#bike_category", function(e) {
    value = $(this).find("option:selected").val();
    val = $("select#charge_per_day option:selected").val();
    a.setOptions({params: {category_id: value, charge: val}});

    $.ajax({
        url : $("#baseUrl").val() + 'search/bike_rent/',
        type : "POST",
        dataType : "JSON",
        data : {category_id : value, charge : val},
        success : function(data) {
            $("div#product-listing").html("").html(data);
            return false;
        },
        error : function() {
            alert('Error occured');
            return false;
        }
    })
});

var a = $("input#bike_name_search").autocomplete({
    serviceUrl: $("#baseUrl").val() + 'search/bike_rent/',
    minChars: 1,
    delimiter: /(,|;)\s*/,
    maxHeight: 400,
    width: 200,
    deferRequestBy: 0,
    onSelect: function(value, data) {
        window.location = data;
    },
})

$(document).on("change", "select#charge_per_day", function(e) {
    value = $("select#bike_category option:selected").val();
    val = $(this).find("option:selected").val();
    a.setOptions({params: {category_id: value, charge: val}});

    $.ajax({
        url : $("#baseUrl").val() + 'search/bike_rent/',
        type : "POST",
        dataType : "JSON",
        data : {category_id : value, charge : val},
        success : function(data) {
            $("div#product-listing").html("").html(data);
            return false;
        },
        error : function() {
            alert('Error occured');
            return false;
        }
    })
});
/* bike_rent filter ends */