/* tour filter starts */
var value;
var val;
$(document).on("change", "select#tour_country", function(e) {
    value = $(this).find("option:selected").val();
    val = $("select#tour_difficulty option:selected").val();
    a.setOptions({params: {country_id: value, difficulty_id: val}});

	$.ajax({
		url : $("#baseUrl").val() + 'search/tour/',
		type : "POST",
		dataType : "JSON",
		data : {country_id : value, difficulty_id : val},
		success : function(data) {
			$("div#tour-listing").html("").html(data);
			return false;
		},
		error : function() {
			alert('Error occured');
			return false;
		}
	})
});

var a = $("input#tour_name_search").autocomplete({
		    serviceUrl: $("#baseUrl").val() + 'search/tour/',
		    minChars: 1,
		    delimiter: /(,|;)\s*/,
		    maxHeight: 400,
		    width: 200,
		    deferRequestBy: 0,
		    //params: {country_id : value, difficulty_id : val},
		    onSelect: function(value, data) {
		    	window.location = data;
		    },
		})

$(document).on("change", "select#tour_difficulty", function(e) {
	value = $("select#tour_country option:selected").val();
	val = $(this).find("option:selected").val();
	a.setOptions({params: {country_id: value, difficulty_id: val}});

	$.ajax({
		url : $("#baseUrl").val() + 'search/tour/',
		type : "POST",
		dataType : "JSON",
		data : {country_id : value, difficulty_id : val},
		success : function(data) {
			$("div#tour-listing").html("").html(data);
			return false;
		},
		error : function() {
			alert('Error occured');
			return false;
		}
	})
});
/* tour filter ends */