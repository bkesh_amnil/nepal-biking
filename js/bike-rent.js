var bike_id = $("input[name=hidden_bike_id]").val();
var availableDates = $("input#hidden_selected_dates").val();

if(availableDates != '0') {
    availableDates = availableDates.split(",");
}
$(document).ready(function() {
    if(availableDates != '0') {
        $('#rent_date_id').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            beforeShowDay: available
        });
    }
})
function available(date) {
    dmy = date.getFullYear() + "-" + (( '0' + (date.getMonth()+1) ).slice( -2 )) + "-" + (( '0' + (date.getDate()+1) ).slice( -2 ));
    if ($.inArray(dmy, availableDates) == -1){
        return {
            enabled : false
        };
    }
    return
}
/* bike rent starts */
$(document).on("change", "input#rent_date_id", function(e) {
    /*var sel_date_id = $(this).find("option:selected").val();
    var sel_date = $(this).find("option:selected").text();*/
    var sel_date = $("#rent_date_id").val();
    var nos_of_pax = $("select#number_of_pax option:selected").val();
    var rent_days = $("select#duration option:selected").val();

    if(sel_date != 'NULL') {
        $.ajax({
            url : $("#baseUrl").val() + 'booking/bike/',
            type : "POST",
            dataType : "JSON",
            data : {bike_id : bike_id, sel_date : sel_date, nos_of_pax : nos_of_pax, rent_days : rent_days},
            success : function(data) {
                if(data != '') {
                    appendData(data, sel_date, nos_of_pax);
                }
            },
            error : function() {
                alert('Error occured');
                return false;
            }
        })
    }
})

$(document).on("change", "select#duration", function(e) {
    //var nos_of_pax = $(this).find("option:selected").val();
    var nos_of_pax = $("select#number_of_pax option:selected").val();
    /*var sel_date_id = $("select#rent_date option:selected").val();
    var sel_date = $("select#rent_date option:selected").text();*/
    var sel_date = $("#rent_date_id").val();
    var rent_days = $("select#duration option:selected").val();

    if(sel_date != 'NULL') {
        $.ajax({
            url : $("#baseUrl").val() + 'booking/bike/',
            type : "POST",
            dataType : "JSON",
            data : {bike_id : bike_id/*, date_id : sel_date_id*/, sel_date : sel_date, nos_of_pax : nos_of_pax, rent_days : rent_days},
            success : function(data) {
                if(data != '') {
                    appendData(data, sel_date, nos_of_pax);
                }
            },
            error : function() {
                alert('Error occured');
                return false;
            }
        })
    }
})

$(document).on("change", "select#number_of_pax", function(){
    //var val = $(this).find("option:selected").val();
    //var nos_of_pax = $("select#number_of_pax option:selected").val();
    var val = $("select#duration option:selected").val();
    var nos_of_pax = $(this).find("option:selected").val();
    var unit_price = $("input[name=price_per_person]").val();
    var total_price = unit_price * nos_of_pax * val;
    $("input.total_cost").val(total_price);
})

function appendData(data, sel_date, nos_of_pax) {
    console.log(data);
    $("span#selected_rent_date").text(sel_date);
    $("span#total_booking_available").text(data.all_allocation);
    $("span#total_booked").text(data.booked);
    $("span#total_available").text(data.available);

    var duration = $("#duration").val();
    /*if(data.rent_duration_days_option != '') {
        $("select#duration")
            .find("option")
            .remove()
            .end()
            .append(data.rent_duration_days_option);
    } else {
        $("select#duration")
            .find("option")
            .remove()
            .end()
            .append('<option>Not Available</option>');
    }*/
                   
    unit_price = data.unit_price;

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price * duration;
        $("input.total_cost").val(total_cost);
    } else {
        $("input.total_cost").val('');
    }
    $("input[name=price_per_person]").val(data.unit_price);
}

$(document).ready(function() {
    $(".btn-send").css("display", "block");
})

$(document).off('click', '.btn-send');
$(document).on('click', '.btn-send', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form = $('form[name=bike_rent]');
    var form_name = $('form[name=bike_rent]').attr('name');
    var data = form.serialize();
    var action = form.attr('action');

    form.validate({
        rules: {
            captchaResult: {
                math: true
            }
        }
    });

    $.validator.addMethod("math", function(value, element, params) {
        var a = parseInt($("input[name=firstNumber]").val());
        var b = parseInt($("input[name=secondNumber]").val());
        var c = $("input[name=captchaResult]").val();
        value = parseInt(a + b);

        if(c == value) {
            return true;
        } else {
            return false;
        }
    }, $.validator.format("Wrong Sum"));


    if(form.valid()) {
        $('.processing').css('display','block');
        $('html, body').animate({
            scrollTop: $('.processing').offset().top - 600
        }, 700);
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                $('.processing').css('display','none');
                $("#messageModal .modal-body").html(data.msg);
                $("#messageModal").modal('show');
                $this.parents('form')[0].reset();
            }
        });
    }
});
/* bike rent ends */
/* dob datepicker starts */
$(document).ready(function() {
     $('#datepicker1').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        yearRange: '1920:1998'
    });
});
/* dob datepicker ends */
$("a.terms-condition-bike").on("click", function() {
    var bike_id = $("input[type=hidden][name=hidden_bike_id]").val();

    $.ajax({
        url : $("#baseUrl").val() + 'getConditionText',
        type: 'post',
        data : {bike_id : bike_id},
        dataType : "JSON",
        success : function(res) {
            /*
            $("#messageModal .modal-img img").attr("src", img);*/
            $("#messageModal1 .modal-title").html('Terms & Conditions');
            $("#messageModal1 .modal-body").html(res);
            $("#messageModal1").modal('show');
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})