//Package Banner
$('#package-banner').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    nav: true,
    loop:owlLoop('#package-banner'),
    smartSpeed: 500,
    autoplayHoverPause:true
});

//Hire Bike slider
$('#hire-bike-slider').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    nav: true,
    loop:owlLoop('#hire-bike-slider'),
    smartSpeed: 500,
});
//Bike Detail Slider
$('#bike-detail-slider').owlCarousel({
    autoplay: true,
    autoplayTimeout: 3000,
    items :1,
    nav: true,
    loop:owlLoop('#bike-detail-slider'),
    smartSpeed: 500
});
//Team Image Slider
$("ul#myTab li a").on("click", function() {

})
/*$('.team-image-slider').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    nav: true,
    smartSpeed: 500,
});*/
//customer review
$('#customer-review').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    loop:owlLoop('#customer-review'),
    dots: true,
    smartSpeed: 500
});
//tour detail slider
$(document).ready(function(){
  $(".tour-detail-slide-owl").owlCarousel({
    items: 1,
    loop: owlLoop('#tour-detail-slide-owl'),
    nav: true,
  });
});

function owlLoop(owlElementID) {
    if (!$(owlElementID).length){
        return false;
    }
    if ($(owlElementID).children().length <2) {
        return false;
    } else {
        return true;
    }
}

// the following to the end is whats needed for the thumbnails.
jQuery( document ).ready(function() {
	 
  
// 1) ASSIGN EACH 'DOT' A NUMBER
dotcount = 1;

jQuery('#tour-detail-slide .owl-dot').each(function() {
    jQuery( this ).addClass( 'dotnumber' + dotcount);
    jQuery( this ).attr('data-info', dotcount);
    dotcount=dotcount+1;
});

// 2) ASSIGN EACH 'SLIDE' A NUMBER
slidecount = 1;

jQuery('#tour-detail-slide .owl-item').not('.cloned').each(function() {
    jQuery( this ).addClass( 'slidenumber' + slidecount);
    slidecount=slidecount+1;
});

// SYNC THE SLIDE NUMBER IMG TO ITS DOT COUNTERPART (E.G SLIDE 1 IMG TO DOT 1 BACKGROUND-IMAGE)
jQuery('#tour-detail-slide .owl-dot').each(function() {

    grab = jQuery(this).data('info');

    slidegrab = jQuery('.slidenumber'+ grab +' img').attr('src');
//    console.log(slidegrab);

    jQuery(this).css("background-image", "url("+slidegrab+")");  

});

// THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
// TO MAKE IT ALL NEAT 
    amount = jQuery('#tour-detail-slide .owl-dot').length;
    gotowidth = 100/amount;

    jQuery('#tour-detail-slide .owl-dot').css("width", gotowidth+"%");
    newwidth = jQuery('#tour-detail-slide .owl-dot').width();
    jQuery('#tour-detail-slide .owl-dot').css("height", newwidth /1.5+"px");	
	
});