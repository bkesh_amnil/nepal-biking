var dept_year;
var dept_month;
var dept_tour_days;

$(document).ready(function() {
	dept_year = $("select#departure_years option:selected").val();
	dept_month = $("select#departure_months option:selected").val();
	dept_tour_days = $("select#departure_nos_of_days option:selected").val();

	callAjax();
})
$(document).on("change", "select#departure_years", function() {
	dept_year = $(this).find("option:selected").val();
	dept_month = $("select#departure_months option:selected").val();
	dept_tour_days = $("select#departure_nos_of_days option:selected").val();
	
	callAjax();
})
$(document).on("change", "select#departure_months", function() {
	dept_month = $(this).find("option:selected").val();
	dept_year = $("select#departure_years option:selected").val();
	dept_tour_days = $("select#departure_nos_of_days option:selected").val();

	callAjax();
})
$(document).on("change", "select#departure_nos_of_days", function() {
	dept_tour_days = $(this).find("option:selected").val();
	dept_year = $("select#departure_years option:selected").val();
	dept_month = $("select#departure_months option:selected").val();

	callAjax();
})

$(document).on("click", ".active-day", function() {
	var url = $(this).attr("rel");
	var year = $("select#departure_years option:selected").val();
	var month = $("select#departure_months option:selected").val();
	var day = $(this).text();
	var tour_departure_detail_id = $(this).data("date");

	$("input#date").val(year+'-'+month+'-'+ $.trim(day));
	$("input#tour_departure_detail_id").val(tour_departure_detail_id);
	$("#book_now_date").attr("action", url);
	$("input#url").val(url);
	$('#book_now_date').submit();

	/*window.location = url;*/
})

function callAjax() {
	$.ajax({
		url : $("#baseUrl").val() + 'search/tour_departure',
		type : "POST",
		dataType : "JSON",
		data : {dept_year : dept_year, dept_month : dept_month, dept_tour_days : dept_tour_days},
		success : function(data) {
			if(data != '') {
				$("p.info").css("display", "none");
				$("table.table").remove();
				$("p.info").after(data);
			} else {
				$("table.table").remove();
				$("p.info").css("display", "block").html("No result found");
			}
		},
		error : function() {
			alert('Error occured');
			return false;
		}
	})
}