//Header scroll and fixed
var windowWidth = $(window).width();
if(windowWidth > 1023) {
    $( window ).scroll(function() {
       var scroll = $(window).scrollTop();
       if(scroll >= 40) { 
            //adding class navbar-fixedtop
            $('nav.header-nav-wrap').addClass('navbar-fixed-top');
            
            //pushing fixed nav from top
            var mainnavHeight = $('.header-nav-wrap').height();
            $('.package-banner').css('top', mainnavHeight);
            
            //decreasing padding of  nav on scroll 
            $('nav.header-nav-wrap').css('padding', '10px 0 10px 0');
            
            //main logo scroll effect
            $('.header-logo img').css({
                'transform': 'scale(0.8)',            
                '-webkit-transform': 'scale(0.8)'            
            });
        }else {
            //removing class navbar-fixedtop
            $('nav.header-nav-wrap').removeClass('navbar-fixed-top');
            
            //main logo scroll effect remove
            $('.header-logo img').css({
                'transform': 'scale(1)',            
                '-webkit-transform': 'scale(1)'            
            });
            
            //setting padding to  default
            $('nav.header-nav-wrap').css('padding', '20px 0 20px 0');
        }
    });
}
//Tricker
/*$(document).ready(function() {
    $(".blog-review-title").each( function() {
        var count = $(this).text().length;
        if(count > 27){
            $(this).addClass("content");
        }
    });
});*/
// Subscribe form submission
$(document).off('click', '.btn-subscribe');
$(document).on('click', '.btn-subscribe', function (e) {
   e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form_name = $(this).parents('form').attr('name');
    var form = $(this).parents('form');
    var data = form.serialize();
    var action = form.attr('action');

    
    if(form.valid()) {
        $('.processing').css('display','block');
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                $("#messageModal .modal-body").html(data.msg);
                $("#messageModal").modal('show');
                $('.processing').css('display','none');
                $this.parents('form')[0].reset();
           }
        });
    }
});
// Autocomplete search
$("input#search").autocomplete({
    serviceUrl: $("#baseUrl").val() + 'search/',
    minChars: 1,
    delimiter: /(,|;)\s*/,
    maxHeight: 400,
    width: 200,
    deferRequestBy: 0,
    onSelect: function(value, data) {
        window.location = data;
    },
})
$("input#search, input[name=bike_search]").keydown(function(event){
    if(event.keyCode == 13) {
        event.preventDefault();
        return false;
    }
});
$("input[name=bike_search]").autocomplete({
    serviceUrl: $("#baseUrl").val() + 'search/bike_search/',
    minChars: 1,
    delimiter: /(,|;)\s*/,
    maxHeight: 400,
    width: 200,
    deferRequestBy: 0,
    onSelect: function(value, data) {
        window.location = data;
    },
})
//" Testimonial popup
$(document).on("click", ".btn-write-review, #testimonial_popup", function() {
    var modal = $('#exampleModal-testimonial').modal('show');
})
// Save Testimonial form
$(document).off('click', '#send_testimonial');
$(document).on('click', '#send_testimonial', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var that = $(this),
        form = $('form#testimonial_form');
        url = $('form#testimonial_form').attr('action');
    var forms = $("form#testimonial_form")[0];
    var data = new FormData(forms);

    if(form.valid()) {
        $('.testimonial-processing').css('display','block');
        $.ajax({
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            dataType: 'JSON',
            success: function (res) {
                $('#exampleModal').modal('hide');
                $("#messageModal .modal-body").html(res.message);
                $("#messageModal").modal('show');
                $('.testimonial-processing').css('display','none');
                //$('.loader').addClass(res.result).text(res.message);
                if(res.result != 'error') {
                    forms.reset();
                }
            }
        })
    }
});
// Banner Description
$(document).ready(function() {
    $(".btn-detail").click( function(e) {
        e.preventDefault();
        var parent = $(this).parents('.banner-title');
        parent.prev().toggleClass("active");
    });
});
/* to top starts */
$(document).ready(function($){
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
    //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }
    });
    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
                scrollTop: 0 ,
            }, scroll_top_duration
        );
    });
});
/* to top ends */