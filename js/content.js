var cur_url = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
$(document).ready(function() {
    var count = $("#hidden_count").val();
    if(count > 1) {
        var slug = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        var tab = $("ul#myTab li#" +slug + " a").attr("href").split("#");
        var img = $("ul#myTab li#" +slug + " a").data("img");
        var bckimg = $("ul#myTab li#" +slug + " a").data("background-img");
        var tabId = $("ul#myTab li#" +slug + " a").attr("href").split("-")[1];

        $("ul#myTab li").removeClass("active");
        $("ul#myTab li#" + slug).addClass("active");
        $("div#myTabContent > div").removeClass("active").removeClass("in");
        $("div#myTabContent div#" + tab[1]).addClass("active in");
        $("div.about-logo img").attr("src", img);
        $("section.inner-page-banner").css('background-image', 'url("' + bckimg +'")');

        if(cur_url == 'team') {
            setTimeout(function(){
                $("#team-image-slider-"+tabId).owlCarousel({
                    autoplay: true,
                    autoplayTimeout: 5000,
                    items: 1,
                    nav: true,
                    smartSpeed: 500,
                });
            }, 200);
        }
    } else {
        var cur_title = $("ul#myTab li.active a").text();
        var cur_desc = $("div#myTabContent div#active p").text();

        if(cur_url == 'team') {
            var tabId = $("ul#myTab a:first").attr("href").split("-")[1];
            $("#team-image-slider-"+tabId).owlCarousel({
                autoplay: true,
                autoplayTimeout: 5000,
                items: 1,
                nav: true,
                smartSpeed: 500,
            });
        }
    }
})
$(document).on("click", "ul#myTab li a", function() {
    var count = $("#hidden_count").val();
    var slug = $(this).attr("rel");
    var name = $(this).text();
    var img = $(this).data("img");
    var bckimg = $(this).data("background-img");
    var tabId = $(this).attr("href").split("-")[1];

    if(count > 1) {
        var url = $("#hidden_current_url").val();
        var current_url = url.substring(0,url.lastIndexOf("/"));
    } else {
        var current_url = $("#hidden_current_url").val();
    }

    $("div.about-logo img").attr("src", img);
    $("section.inner-page-banner").css('background-image', 'url("' + bckimg +'")');
    
    if(cur_url == 'team') {
        setTimeout(function(){
            $("#team-image-slider-"+tabId).owlCarousel({
                autoplay: true,
                autoplayTimeout: 5000,
                items: 1,
                nav: true,
                smartSpeed: 500,
            });
        }, 200);
    }

    history.pushState(null, null,  current_url + '/' + slug);
})