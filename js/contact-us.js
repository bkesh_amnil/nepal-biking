//google map
/*function initialize() {
    var mapCanvas = document.getElementById('map-canvas');
    var mapOptions = {
      center: new google.maps.LatLng(27.719022, 85.328752),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions)
  }
  google.maps.event.addDomListener(window, 'load', initialize);*/


jQuery(function($) {
    // Asynchronously Load the map API
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var data = $("div.contact-contents-wrap").html();
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    map.setTilt(45);

    // Multiple Markers
    var markers = [
        ['Dawn till Dusk', 27.719022, 85.328752]/*,
        ['Newroad, Kathmandu', 27.7035,85.3107],
        ['Chabahil, Kathmandu', 27.7132,85.3449],
        ['Durbarthok, Pokhara', 28.21,83.98]*/
    ];

    // Info Window Content
    var infoWindowContent = [
        [data]/*,
        ['<div class="info_content">' +
        '<h3>Newroad Branch Office</h3>' +
        '<p>Newroad, Kathmandu</p><p>Beside Nabil Bank, Sankata Marg</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Chabahil Branch Office</h3>' +
        '<p>Chabahil, Kathmandu</p><p>Mitrapark(Pipal Bot)</p>' +
        '</div>'],
        ['<div class="info_content">' +
        '<h3>Regional Office</h3>' +
        '<p>Durbarthok, Pokhara</p><p>Samsung Galli</p>' +
        '</div>']*/
    ];

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });

}