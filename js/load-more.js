var total_pages;
var track_click = 0;
var type = $("#hidden_type").val();
var ids;

/*if(type == '') {
    type = 'tour';
}*/
/* load more */
$(document).on("click", "button.btn-more", function() {
    ids = $("#hidden_ids").val();
    total_pages = $("#hidden_total_pages").val();
    $(this).hide(); //hide load more button on click
    $('.load-more-processing').show();
    if(track_click < total_pages) {
        $.ajax({
            url : $("#baseUrl").val() + 'content/load_more',
            type : "POST",
            dataType : "JSON",
            data : {type : type, page : track_click, ids : ids},
            success : function(msg) {
$(".btn-more").show(); //bring back load more button
                if(msg.data != '') {
                    
                    if(type == 'tour' || type == 'nepalbiketours') {
                        $("div#tour-listing").children().last().after(msg.data);
                    } else if(type == 'bike' || type == 'bike-rent' || type == 'bike-accessories') {
                        $("div#product-listing").children().last().after(msg.data);
                    } else {
                        $("div#news-events-listing").children().last().after(msg.data);
                    }
                    $("#hidden_ids").val(msg.ids);
                    //scroll page to button element
                    //$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 500);
                    //hide loading image
                    //hide loading image once data is received

                    track_click++; //user click increment on load button
                } else {
                   $(".btn-more").prop('disabled', false);
$(".btn-more").text('No more data');
                }
                $('.load-more-processing').hide();
            },
            error : function(xhr, ajaxOptions, thrownError) {
                alert(thrownError); //alert any HTTP error
                $(".btn-more").show(); //bring back load more button
                $('.load-more-processing').hide(); //hide loading image once data is received
            }
        });

        if(track_click >= total_pages-1) {
            //reached end of the page yet? disable load button
            $(".btn-more").attr("disabled", "disabled");
        }
    }
});