// Fixed height for tab content

//window width calculating
var windowwidth = $(window).width();
//window height calculating
var Windowheight = $(window).height(); 
//Header height calculating
var Headerheight = $('header').height(); 
//inner page banner height calculating
var Innerpageheight = $('.inner-page-banner').height();
//breadcrumb height Calculating
var Breadcrumbheight = $('.breadcrumb-wrap').height();     
//footer height calculating
var Footerheight = $('footer').height(); 
//calculating total height for tab content
var Totaltabcontentheight = Windowheight - (Headerheight + Breadcrumbheight + Footerheight);
//adding height to tab content
$('.tab-content').css('min-height', Totaltabcontentheight);

//On scroll fixed aside
if(windowwidth > 767) {
    $( window ).scroll(function() {
        var scroll = $(window).scrollTop();

        ///////////For about page///////////
        if(scroll > 350){
            $('#about').css({
                position: 'fixed',
                top : Headerheight - 32       
            });
        } else {
            $('#about').css({
                position: 'relative',
                top : 0        
            });
        }

        ///////////For FAQ page///////////
        if(scroll > 105){
            $('#faq').css({
                position: 'fixed',
                top : Headerheight - 32       
            });
        } else {
            $('#faq').css({
                position: 'relative',
                top : 0        
            });
        }
    });
}