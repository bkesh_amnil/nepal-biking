var url = window.location.href,
        parts = url.split('/'),
//        lastPart = parts.pop() == '' ? parts[parts.length - 1] : parts.pop();
        lastPart = 'book';

if (lastPart == 'book') {
    var availableDates = $("input#hidden_selected_dates").val();
    console.log(availableDates);

    if (availableDates != '0' || availableDates != 'NULL') {
        availableDates = availableDates.split(",");
    }
}
$(document).ready(function() {
    if (lastPart == 'book') {
        $('#departure_date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            beforeShowDay: enableAvailableDates
        });
    } else {
        $('#departure_date').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            beforeShowDay: disablePrevious
        });
    }
})
function enableAvailableDates(date) {
    dmy = date.getFullYear() + "-" + (('0' + (date.getMonth() + 1)).slice(-2)) + "-" + (('0' + (date.getDate())).slice(-2));

    if ($.inArray(dmy, availableDates) == -1) {
        return {
            enabled: false
        };
    }
    return
}

function disablePrevious(date) {
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    if (date.valueOf() < now.valueOf()) {
        return {
            enabled: false
        };
    }
    return
}
/* tour booking starts */
var tour_id = $("input[name=hidden_tour_id]").val();
$(document).on("change", "input#departure_date", function(e) {
    /*var sel_date_id = $(this).find("option:selected").val();
     var sel_date = $(this).find("option:selected").text();*/
    var sel_date = $(this).val();
    var nos_of_pax = $("select#nos_of_pax option:selected").val();
    if (sel_date != 'NULL' && sel_date != "") {
        $.ajax({
            url: $("#baseUrl").val() + 'booking/tour/',
            type: "POST",
            dataType: "JSON",
            data: {tour_id: tour_id, /* date_id : sel_date_id,*/ sel_date: sel_date, nos_of_pax: nos_of_pax},
            success: function(data) {
                if (data != '') {
                    appendData(data, sel_date, nos_of_pax);
                }
            },
            error: function() {
                alert('Error occured');
                return false;
            }
        })
    }
})

$(document).on("change", "select#nos_of_pax", function(e) {
    /*var sel_date_id = $("select#departure_date").find("option:selected").val();
     var sel_date = $("select#departure_date").find("option:selected").text();*/
    var sel_date = $("input#departure_date").val();
    var nos_of_pax = $(this).find("option:selected").val();
    if (sel_date != 'NULL') {
        $.ajax({
            url: $("#baseUrl").val() + 'booking/tour/',
            type: "POST",
            dataType: "JSON",
            data: {tour_id: tour_id, /* date_id : sel_date_id, */sel_date: sel_date, nos_of_pax: nos_of_pax},
            success: function(data) {
                if (data != '') {
                    appendData(data, sel_date, nos_of_pax);
                }
            },
            error: function() {
                alert('Error occured');
                return false;
            }
        })
    }
})

function appendData(data, sel_date, nos_of_pax) {
    $("span#selected_departure_date").text(sel_date);
    $("span#total_booking_available").text(data.tour_all_allocation);
    $("span#total_booked").text(data.tour_booked);
    $("span#total_available").text(data.tour_available);
    $("input.price_per_person").val(data.tour_unit_price);

    if (data.tour_unit_price != '') {
        var total_cost = nos_of_pax * data.tour_unit_price;
        $("input[name=total_cost]").val(total_cost);
    } else {
        $("input[name=total_cost]").val('');
    }
    /*$("input.btn-booking-submit").val(data.tour_is_fixed_departure);*/
    $("input.hidden_fixed_departure").val(data.tour_is_fixed_departure_id);
}

$(document).ready(function() {
    $(".btn-booking-submit").css("display", "block");
})

$.validator.setDefaults({ignore: ''});
$(document).off('click', '.btn-booking-submit');
$(document).on('click', '.btn-booking-submit', function(e) {

    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form_name = $(this).parents('form').attr('name');
    var form = $(this).parents('form');
    var data = form.serialize();
    var action = form.attr('action');

    if (form.valid()) {
        if ($("input.price_per_person").val() == '' || $("input.total_cost").val() == '') {
            alert('Price per Person/Total Cost is a must');
            return false;
        }
        $('.similar-processing').css('display', 'block');
        $('html, body').animate({
            scrollTop: $('.similar-processing').offset().top - 100
        }, 700);
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function(data) {
                $('.processing').css('display', 'none');
                $("#messageModal .success-msg").html(data.msg);
                $("#messageModal").modal('show');
                if (data.class != 'error') {
                    $this.parents('form')[0].reset();
                }
            }
        });
    }
});
/* tour booking ends */
/* tour enquiry starts */
$(document).ready(function() {
    $(".btn-enquire-submit").css("display", "block");
})

$.validator.setDefaults({ignore: ''});
$(document).off('click', '.btn-enquire-submit');
$(document).on('click', '.btn-enquire-submit', function(e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form_name = $(this).parents('form').attr('name');
    var form = $(this).parents('form');
    var data = form.serialize();
    var action = form.attr('action');

    if (form.valid()) {
        $('.similar-right-processing').css('display', 'block');
        $('html, body').animate({
            scrollTop: $('.similar-right-processing').offset().top - 100
        }, 700);
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function(data) {
                $('.processing').css('display', 'none');
                $("#messageModal .success-msg").html(data.msg);
                $("#messageModal").modal('show');
                if (data.class != 'error') {
                    $this.parents('form')[0].reset();
                }
            }
        });
    }
});
/* date picker*/
$(document).ready(function() {
    $('#datepicker1').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });


    /**
     * event for book now when clicked from itenery tabs
     */
    $(document).on("click", ".book-now", function() {
        var formID = $('[name="tour_booking_enquire"]');
        formID[0].reset();
        formID.validate({
            focusCleanup: true
        }).resetForm();
        formID.find('input').removeClass('error');
        //# load modal now
        $('#bookingModal').modal('show');
        $('html').addClass('modal-open');
    });

    /**
     * enquire pop up on click
     */
    $('body').on('click', '.btn-enquire', function() {
        var formID = $('[name="tour_enquire"]');
        formID[0].reset();
        formID.validate({
            focusCleanup: true
        }).resetForm();
        formID.find('input').removeClass('error');
        $('#departure_date_enquire').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            beforeShowDay: disablePrevious
        });
        $('#bookingModal').modal('hide');
        //# load modal now
        $('#enquireModal').modal('show');
    });

    $('#bookingModal').on('shown.bs.modal', function(e) {
        $('html').addClass('modal-open');
        var windowHeight = $(window).height();
        var popheight = $(".modal-dialog").height();
        console.log(windowHeight, popheight);
        if (windowHeight > popheight) {
            console.log('great');
            $(".modal-dialog").addClass("not-middle");
        }
    });
    $('#bookingModal').on('hidden.bs.modal', function(e) {
        $('html').removeClass('modal-open');
        $(".modal-dialog").removeClass("not-middle");
    });
    $('#enquireModal').on('shown.bs.modal', function(e) {
        var windowHeight = $(window).height();
        var popheight = $(".modal-dialog").height();
        console.log(windowHeight, popheight);
        if (windowHeight > popheight) {
            console.log('great');
            $(".modal-dialog").addClass("not-middle");
        }
        $('html').addClass('modal-open');
    });
    $('#enquireModal').on('hidden.bs.modal', function(e) {
        $('html').removeClass('modal-open');
        $(".modal-dialog").removeClass("not-middle");
    });
});