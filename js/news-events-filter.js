/* tour filter starts */
var value = 0;
var val = 'DESC';
var type = $("#hidden_type_page").val();

$(document).on("change", "select#news_events_year", function(e) {
    value = $(this).find("option:selected").val();
    val = $("select#news_events_order option:selected").val();
    a.setOptions({params: {type: type, year: value, order: val}});

	$.ajax({
		url : $("#baseUrl").val() + 'search/news_events/',
		type : "POST",
		dataType : "JSON",
		data : {type: type, year : value, order : val},
		success : function(data) {
			$("div#news-events-listing").html("").html(data);
			return false;
		},
		error : function() {
			alert('Error occured');
			return false;
		}
	})
});

var a = $("input#news_events_name_search").autocomplete({
		    serviceUrl: $("#baseUrl").val() + 'search/news_events/',
		    minChars: 1,
		    delimiter: /(,|;)\s*/,
		    maxHeight: 400,
		    width: 200,
		    deferRequestBy: 0,
		    params: {type :type},
		    onSelect: function(value, data) {
		    	window.location = data;
		    },
		})

$(document).on("change", "select#news_events_order", function(e) {
	val = $(this).find("option:selected").val();
    value = $("select#news_events_year option:selected").val();
    a.setOptions({params: {type: type, year: value, order: val}});

	$.ajax({
		url : $("#baseUrl").val() + 'search/news_events/',
		type : "POST",
		dataType : "JSON",
		data : {type: type, year : value, order : val},
		success : function(data) {
			$("div#news-events-listing").html("").html(data);
			return false;
		},
		error : function() {
			alert('Error occured');
			return false;
		}
	})
});
/* tour filter ends */