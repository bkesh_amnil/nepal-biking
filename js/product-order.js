$(document).on("change", "select#product_quantity", function(e) {
     var quan = $(this).find("option:selected").val();
     var unit_price = $("input.product_price").val();
     var total_price = quan * unit_price;
     $("input.total_price").val(total_price);
})

$(document).ready(function() {
    $(".btn-send").css("display", "block");
})

$(document).off('click', '.btn-send');
$(document).on('click', '.btn-send', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form_name = $(this).parents('form').attr('name');
    var form = $(this).parents('form');
    var data = form.serialize();
    var action = form.attr('action');


    form.validate({
        rules: {
            captchaResult: {
                math: true
            }
        }
    });

    $.validator.addMethod("math", function(value, element, params) {
        var a = parseInt($("input[name=firstNumber]").val());
        var b = parseInt($("input[name=secondNumber]").val());
        var c = $("input[name=captchaResult]").val();
        value = parseInt(a + b);

        if(c == value) {
            return true;
        } else {
            return false;
        }
    }, $.validator.format("Wrong Sum"));


    if(form.valid()) {
        $('.processing').css('display','block');
        $('html, body').animate({
            scrollTop: $('.processing').offset().top - 100
        }, 700);
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                $('.processing').css('display','none');
                $("#messageModal .modal-body").html(data.msg);
                $("#messageModal").modal('show');
                //$('.message-holder').css('display','block').html('<div class="alert alert-success"><a aria-hidden="true" href="#" data-dismiss="alert" class="close">×</a>' + data.msg + '</div>');
                $this.parents('form')[0].reset();
            }
        });
    }
});
/* product order ends */