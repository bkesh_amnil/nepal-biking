/* tour book form js starts */
var base_url = $("#base-url").val();
var backend_folder = $("#backend_folder").val();
var current_page = $("#current_page").val();

var tour_name;
var departure_date;
var unit_price;
var total_cost;

var tour_id;
var nos_of_pax;
var date_id;
var sel_date;
var type;

$(document).on("change", 'select#tour_category_id', function() {
    var val = $(this).find("option:selected").val();
    type = 'tour_category';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {tour_category_id : val, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#tour_id", function() {
    tour_id = $(this).find("option:selected").val();
    type = 'tour';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {tour_id : tour_id, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#departure_date_id", function() {
    date_id = $(this).find("option:selected").val();
    sel_date = $(this).find("option:selected").text();
    nos_of_pax = $("select#number_of_pax option:selected").val();
    tour_id = $("select#tour_id option:selected").val();
    type = 'departure_date';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {date_id : date_id, sel_date : sel_date, nos_of_pax : nos_of_pax, tour_id : tour_id, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#number_of_pax", function(e) {
    date_id = $("select#departure_date_id").find("option:selected").val();
    sel_date = $("select#departure_date_id").find("option:selected").text();
    nos_of_pax = $(this).find("option:selected").val();
    tour_id = $("select#tour_id option:selected").val();
    type = 'number_of_person';

    if(nos_of_pax != 'Select') {
        $.ajax({
            url: base_url + backend_folder + '/' + current_page + '/' + 'load_data',
            type: "POST",
            dataType: "JSON",
            data : {date_id : date_id, sel_date : sel_date, nos_of_pax : nos_of_pax, tour_id : tour_id, type : type},
            success: function (data) {
                appendData(data, type);
                return false;
            },
            error: function () {
                alert('error occured');
                return false;
            }
        })
    }
})

function appendData(data, type) {
    if(type == 'tour_category') {
        if(data.tour_html != '') {
            $("select#tour_id")
                .find('option')
                .remove()
                .end()
                .append(data.tour_html);
        } else {
            $("select#tour_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Tour Available</option>');
        }
    }

    if(type == 'tour_category' || type == 'tour') {
        $("select#number_of_pax").val('Select'); /* change Number of pax to default everytime tour category or tour changes */
        /* change in departure date */
        if (data.tour_dates != '') {
            $("select#departure_date_id")
                .find('option')
                .remove()
                .end()
                .append(data.tour_dates);
        } else {
            $("select#departure_date_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Date/Price Available</option>');
        }
        /* change in departure date */
    }
    /* Price Info */
    nos_of_pax = $("select#number_of_pax option:selected").val();
    unit_price = data.tour_unit_price;

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price;
        $("input[name=total_price]").val(total_cost);
    } else {
        $("input[name=total_price]").val('');
    }
    $("input[name=price_per_person]").val(data.tour_unit_price);
    /* Price Info */
    /* Booking Status Info */
    tour_name = $("select#tour_id option:selected").text();
    departure_date = $("select#departure_date_id option:selected").text();
    if(tour_name == 'No Tour Available') {
        tour_name = '';
    }
    if(departure_date == 'No Date/Price Available') {
        departure_date = '';
    }

    $("span#selected_tour_name").text(tour_name);
    $("span#selected_departure_date").text(departure_date);
    if(type != 'number_of_person') {
        $("span#selected_booking_available").text(data.tour_all_allocation);
        $("span#selected_total_booked").text(data.tour_booked);
        $("span#selected_total_available").text(data.tour_available);
    }
    /* Booking Status Info */
}
/* tour book form js ends */

/* tour book list js starts */
$(document).ready(function() {
    $("#export_filter_tour_category, #export_filter_tour, #export_filter_customer_name, #export_filter_booking_status, #export_filter_booking_date, #export_filter_departure_date").css("display", "none");
})
$(document).on("change", "select[name=export_filter_id]", function() {
    var sel_val = $(this).find("option:selected").val();

    $("#export_filter_" + sel_val).css("display", "block");
    $(".searchFields select").not("#export_filter_" + sel_val).css("display", "none");
    $(".searchFields input").not("#export_filter_" + sel_val).css("display", "none");
})
/* tour book list js ends */