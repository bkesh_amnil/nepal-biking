var base_url = $("#base-url").val();
var backend_folder = $("#backend_folder").val();
var current_page = $("#current_page").val();

var bike_name;
var rent_date;
var unit_price;
var total_cost;

var bike_id;
var nos_of_pax;
var date_id;
var sel_date;
var type;
var nos_of_days;

$(document).on("change", 'select#bike_category_id', function() {
    var val = $(this).find("option:selected").val();
    type = 'bike_category';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {bike_category_id : val, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#bike_id", function() {
    bike_id = $(this).find("option:selected").val();
    type = 'bike';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {bike_id : bike_id, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#rent_date_id", function() {
    date_id = $(this).find("option:selected").val();
    sel_date = $(this).find("option:selected").text();
    nos_of_pax = $("select#number_of_pax option:selected").val();
    bike_id = $("select#bike_id option:selected").val();
    nos_of_days = $("select#number_of_days_id option:selected").val();
    type = 'rent_date';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {date_id : date_id, sel_date : sel_date, nos_of_pax : nos_of_pax, bike_id : bike_id, type : type, nos_of_days : nos_of_days},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#number_of_days_id", function(e) {
    sel_date = $("select#rent_date_id option:selected").text();
    nos_of_days = $(this).find('option:selected').val();
    nos_of_pax = $('select#number_of_pax option:selected').val();
    bike_id = $("select#bike_id option:selected").val();

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'get_unit_price',
        type : "POST",
        dataType : "JSON",
        data : {sel_date : sel_date, nos_of_days : nos_of_days, bike_id : bike_id},
        success : function(data) {
            $("input[name=price_per_person]").val(data.unit_price);
            unit_price = data.unit_price;
            total_cost = nos_of_pax * unit_price * nos_of_days;
            $("input[name=total_price]").val(total_cost);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })/*
    unit_price = $("input[name=price_per_person]").val();

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price * nos_of_days;
        $("input[name=total_price]").val(total_cost);
    } else {
        $("input[name=total_price]").val('');
    }*/
})

$(document).on("change", "select#number_of_pax", function(e) {
    nos_of_days = $('select#number_of_days_id option:selected').val();
    nos_of_pax = $(this).find('option:selected').val();
    unit_price = $("input[name=price_per_person]").val();

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price * nos_of_days;
        $("input[name=total_price]").val(total_cost);
    } else {
        $("input[name=total_price]").val('');
    }
})

function appendData(data, type) {
    if(type == 'bike_category') {
        if(data.bike_html != '') {
            $("select#bike_id")
                .find('option')
                .remove()
                .end()
                .append(data.bike_html);
        } else {
            $("select#bike_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Bike Available</option>');
        }
    }
    if(type == 'bike_category' || type == 'bike' || type == 'rent_date') {
        /*if(data.rent_duration_days_option != '') {
            $("select#number_of_days_id")
                .find("option")
                .remove()
                .end()
                .append(data.rent_duration_days_option);
        } else {
            $("select#number_of_days_id")
                .find("option")
                .remove()
                .end()
                .append('<option>Not Available</option>');
        }*/
        $("select#number_of_pax").val('1'); /* change Number of pax to default everytime bike category or bike changes */
        /* change in rent date */
        if(type == 'bike_category' || type == 'bike') {
            if (data.bike_dates != '') {
                $("select#rent_date_id")
                    .find('option')
                    .remove()
                    .end()
                    .append(data.bike_dates);
            } else {
                $("select#rent_date_id")
                    .find('option')
                    .remove()
                    .end()
                    .append('<option>No Date/Price Available</option>');
            }
        }
        /* change in rent date */
    }
    /* Price Info */
    nos_of_days = $("select#number_of_days_id option:selected").val();
    nos_of_pax = $("select#number_of_pax option:selected").val();
    unit_price = data.bike_unit_price;

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price * nos_of_days;
        $("input.total_price").val(total_cost);
    } else {
        $("input.total_price").val('');
    }
    $("input[name=price_per_person]").val(data.bike_unit_price);
    /* Price Info */
    /*nos_of_pax = $("select#number_of_pax option:selected").val();
    unit_price = data.bike_unit_price;

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price;
        $("input[name=total_price]").val(total_cost);
    }
    $("input[name=price_per_person]").val(data.bike_unit_price);*/
    /* Price Info */
    /* Rent Status Info */
    bike_name = $("select#bike_id option:selected").text();
    rent_date = $("select#rent_date_id option:selected").text();

    if(bike_name == 'No Bike Available') {
        bike_name = '';
    }
    if(rent_date == 'No Date/Price Available') {
        rent_date = '';
    }

    $("span#selected_bike_name").text(bike_name);
    $("span#selected_rent_date").text(rent_date);
    $("span#selected_booking_available").text(data.all_allocation);
    $("span#selected_total_booked").text(data.booked);
    $("span#selected_total_available").text(data.available);
    /* Rent Status Info */
}
/* bike book list js starts */
$(document).ready(function() {
    $("#export_filter_bike_category, #export_filter_bike, #export_filter_customer_name, #export_filter_rent_status, #export_filter_rent_days, #export_filter_rent_date").css("display", "none");
})
$(document).on("change", "select[name=export_filter_id]", function() {
    var sel_val = $(this).find("option:selected").val();

    $("#export_filter_" + sel_val).css("display", "block");
    $(".searchFields select").not("#export_filter_" + sel_val).css("display", "none");
    $(".searchFields input").not("#export_filter_" + sel_val).css("display", "none");
})
/* bike book list js ends */