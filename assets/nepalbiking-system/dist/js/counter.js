var options = {
    'maxCharacterSize': 255,
    'originalStyle': 'originalDisplayInfo',
    'warningStyle': 'warningDisplayInfo',
    'warningNumber': 40,
    'displayFormat': '#input Characters | #left Characters Left | #words Words'
};
$('textarea[name=short_description]').textareaCount(options);