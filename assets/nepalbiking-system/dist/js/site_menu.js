$(document).ready(function (e) {
    jQuery('.enableAllModules').change(function () {
        var set = ".modulesEnabled";
        var checked = jQuery(this).is(":checked");
        jQuery(set).each(function () {
            if (checked) {
                $(this).attr("checked", true);
            } else {
                $(this).attr("checked", false);
            }
        });
        jQuery.uniform.update(set);
    });
    jQuery('.modulesEnabled').change(function () {
        var set = ".enableAllModules";
        var allchecked = true;
        $('.modulesEnabled').each(function () {
            if ($(this).is(":checked")) {
            } else {
                allchecked = false;
            }
        });
        if (allchecked == true) {
            $(set).attr("checked", true);
        } else {
            $(set).attr("checked", false);
        }
        jQuery.uniform.update(set);
    });
    $('#btn_nomOk').live("click", function () {
        var nums = parseInt($('#noofmenus').val());
        if (nums > 0) {
            var available = $('.addedMenus').length;
            //alert(available);
            if (available > nums) {
                //alert("remove");
                RemoveRows(nums);
            } else {
                //alert("add");
                AddRows((nums - available), $(this).parent('.site-menu').find(".typemenudetails"))
            }
        }
    });
    $('#btn_tonOk').live("click", function () {
        var nums = parseInt($('#noofnewstypes').val());
        if (nums > 0) {
            var available = $('.addedNewsTypes').length;
            //alert(available);
            if (available > nums) {
                //alert("remove");
                RemoveRowsNews(nums);
            } else {
                //alert("add");
                AddRowsNews((nums - available), $(this).parent('.controls'));
            }
        }
    });
});
function RemoveRows(nums) {
    $('.addedMenus').each(function (i) {
        if (i >= nums) {
            $(this).remove();
            resetAll();
        }
    })
}
function RemoveRowsNews(nums) {
    $('.addedNewsTypes').each(function (i) {
        if (i >= nums) {
            $(this).remove();
            resetAll();
        }
    })
}
function AddRows(nums, elm) {
    var available = $('.addedMenus').length;
    for (var i = 0; i < nums; i++) {
        var randNum = (Math.floor(Math.random() * (1 + (available + 20) - available)) ) + available;
        var addElm = '<div class="form-group addedMenus row">' +
            '<div class="col-lg-6"><span class="site-menu-block">Type Name :' +
            ' <input type="text" name="menuName[' + randNum + ']" class="form-control" /></span></div>' +
            '<div class="col-lg-6"><span class="site-menu-block">&nbsp;&nbsp;Sub Menus :<label> <input type="radio" name="submenus[' + randNum + ']" value="yes" checked="checked" /> Yes</label>' +
            '<label> <input type="radio" name="submenus[' + randNum + ']" value="no" /> No</label></span></div>' +
            '<div class="col-lg-6"><span class="site-menu-block">&nbsp;&nbsp;Depth: <input type="text" name="depth[' + randNum + ']" class="form-control" value="0" /></span></div></div>';
        elm.append(addElm);
    }
    resetAll();
}
function resetAll() {
    $('.addedMenus').each(function (i) {
        $(this).find('input').each(function () {
            var name = $(this).attr("name");
            name = name.split('[');
            var newName = name[0] + "[" + i + "]";
            $(this).attr("name", newName);
        });
    });
}
function AddRowsNews(nums, elm) {
    var available = $('.addedNewsTypes').length;
    for (var i = 0; i < nums; i++) {
        var randNum = (Math.floor(Math.random() * (1 + (available + 20) - available)) ) + available;
        var addElm = '<tr class="addedNewsTypes"><td></td><td><span class="block">News Type Name : <input type="text" name="newsName[' + randNum + ']" class="col-lg-12 inline" /></span></td></tr>';
        $('.logo_row').before(addElm);
    }
    resetAllNews();
}
function resetAllNews() {
    $('.addedNewsTypes').each(function (i) {
        $(this).find('input').each(function () {
            var name = $(this).attr("name");
            name = name.split('[');
            var newName = name[0] + "[" + i + "]";
            $(this).attr("name", newName);
        });
    });
}
