$("input[type=submit]").click(function() {
    var input_sort_vals = $(".sort_order_value").map(function() {
        return $(this).val();
    }).get();
    var input_sort_count = input_sort_vals.length;

    var input_sort_vals_distinct = $.unique(input_sort_vals);
    var input_sort_distinct_count = input_sort_vals_distinct.length;

    if(input_sort_count != input_sort_distinct_count) {
        alert('Order must be unique');
        return false;
    } else {
        $("form[name=map]").submit();
    }
})

var marker;
var infowindow;
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;
var markers = {};
var currentId = 0;
var uniqueId = function() {
    return ++currentId;
}
var edit = 0;
var direction = '';
var linecordinates = [];
var map;
var sortorder = 0;
var latest_marker;
var latest_latlng;
var lineSymbol;
var flightPath;
var line = [];

var saved_locations = $("#hidden_lat_long").val();

function initialize() {
    lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    };
    /* edit portion */
    if(saved_locations != '') {
        edit = 1;
        saved_locations = JSON.parse(saved_locations);
        currentId = labelIndex = sortorder = saved_locations.length;
    }
    /* edit portion */
    var latlng = new google.maps.LatLng(27.7238,85.3214);
    var options = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map"), options);

    google.maps.event.addListener(map, "click", function(event) {
        var id = uniqueId();
        marker = new google.maps.Marker({
            id: id,
            position: event.latLng,
            map: map,
            label: labels[labelIndex++ % labels.length],
        });

        markers[id] = marker;  // cache created marker to markers object with label as its key
        google.maps.event.addListener(marker, "click", function() {
            latest_marker = this.label;
            latest_latlng = this.getPosition();
            infowindow.open(map, this);
        });
    });
    /* edit portion */
    if(edit) {
        for( i = 0; i < saved_locations.length; i++ ) {
            var position = new google.maps.LatLng(saved_locations[i].lat, saved_locations[i].long);
            var id = saved_locations[i].id;
            var saved_label = saved_locations[i].label;
            marker = new google.maps.Marker({
                id: id,
                position: position,
                map: map,
                label: saved_label,
            });

            markers[id] = marker;

            linecordinates.push({
                id: parseInt(saved_locations[i].id),
                lat: parseFloat(saved_locations[i].lat),
                lng: parseFloat(saved_locations[i].long)
            });
        }

        flightPath = new google.maps.Polyline({
            path: linecordinates,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            icons: [{
                icon: lineSymbol,
                offset: '100%'
            }],
        });
        addline();
    }
    /* edit portion */
    var html = "<table class='infotable'>" +
        "<tr><td>Title:</td> <td><input type='text' class='title'/> </td> </tr>" +
        "<tr><td>Address:</td> <td><input type='text' class='address'/></td> </tr>" +
        "<tr><td>Description:</td> <td><input type='text' class='description'/></td></tr>" +
        "<tr><td></td><td><span class='saveandclose'>[ Save & Close ]</span></td></tr></table>";
    infowindow = new google.maps.InfoWindow({
        content: html
    });
}

$(document).on('click', 'span.saveandclose', function() {
    saveData();
})

$(document).on('click', 'span.closeandremove', function() {
    marker.setMap(null);
    if(currentId != '0') {
        --currentId;
    }
    if(labelIndex != '0') {
        --labelIndex;
    }
})

function saveData() {
    var title = $('input.title').val();
    var address = $('input.address').val();
    var description = $('input.description').val();

    if(title == '') {
        alert('Title is a must');
        return false;
    } else {
        ++sortorder;

        linecordinates.push({
            id: sortorder,
            lat: latest_latlng.lat(),
            lng: latest_latlng.lng()
        });

        $('input.title').val("");
        $('input.address').val("");
        $('input.description').val("");

        infowindow.close();

        var html = '';
        html += "<tr id="+marker.id+" class='trdesc'>";
        html += "<td><input type='text' name='sort_order[]' value='"+latest_marker+"' disabled /></td>";
        html += "<td><input type='text' name='title[]' value='"+title+"' /></td>";
        html += "<td><input type='text' name='address[]' value='"+address+"' /></td>";
        html += "<td><input type='text' name='description[]' value='"+description+"' /></td>";
        html += "<td><input type='text' value='"+latest_latlng.lat()+"' disabled /><input type='hidden' name='latitude[]' value='"+latest_latlng.lat()+"' /></td>";
        html += "<td><input type='text' value='"+latest_latlng.lng()+"' disabled /><input type='hidden' name='longitude[]' value='"+latest_latlng.lng()+"' /></td>";
        html += "<td><input type='text' name='order[]' value='"+sortorder+"' /></td>";
        html += "<td><span class='removeinfo'>Delete</span></td>"
        html += "</tr>";

        $("#HtmlTable").append(html);

        flightPath = new google.maps.Polyline({
            path: linecordinates,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            icons: [{
                icon: lineSymbol,
                offset: '100%'
            }],
        });

        addline();
    }
}

$(document).on('click', 'span.removeinfo', function() {
    var trid = $(this).parents('tr.trdesc').attr('id');
    deleteData(trid);
})

function deleteData(trid) {
    if (confirm("Are you sure want to delete this location?") == true) {
        removeline();

        $("tr#" + trid).remove();
        var marker = markers[trid]; // find the marker by given id
        marker.setMap(null);

        linecordinates = $.grep(linecordinates, function(data, index) {
            return data.id != trid
        });

        flightPath = new google.maps.Polyline({
            path: linecordinates,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            icons: [{
                icon: lineSymbol,
                offset: '100%'
            }],
        });

        addline();
    } else {
        return false;
    }
}

function addline() {
    line.push(flightPath);
    flightPath.setMap(map);
}

function removeline() {
    for (i=0; i<line.length; i++) {
        line[i].setMap(null); //or line[i].setVisible(false);
    }
    flightPath.setMap(null);
}