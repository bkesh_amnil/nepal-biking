$(document).on("change", "select#link_type", function() {
    var value = $(this).find("option:selected").val();
    if(value == 'menu'){
        $("#menuId").css("display", "block");
        $("#tourId, #bikeId, #accessoriesId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'tour') {
        $("#tourId").css("display", "block");
        $("#menuId, #bikeId, #accessoriesId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'bike') {
        $("#bikeId").css("display", "block");
        $("#tourId, #menuId, #accessoriesId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'accessories') {
        $("#accessoriesId").css("display", "block");
        $("#tourId, #bikeId, #menuId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'url') {
        $("#urlId").css("display", "block");
        $("#menuId, #tourId, #bikeId, #accessoriesId").css("display", "none");
        $("select#opens").css("display", "block");
    } else {
        $("#menuId, #tourId, #bikeId, #accessoriesId, #urlId").css("display", "none");
        $("select#opens").css("display", "none");
    }
})

/*$(".linkType").click(function(){
    var selvalue = $(this).val();
    if(selvalue == "none"){
        $('.opens, .linkIdMenu, .linkIdContent, .linkUrl').hide();
    }else if(selvalue == "menu"){
        $('.linkIdContent, .linkUrl').hide();
        $('.opens, .linkIdMenu').show();
    }else if(selvalue == "content"){
        $('.linkIdMenu, .linkUrl').hide();
        $('.opens, .linkIdContent').show();
    }else if(selvalue == "url"){
        $('.linkIdMenu, .linkIdContent').hide();
        $('.opens, .linkUrl').show();
    }else if(selvalue === "product"){
        $('.linkIdMenu, .linkUrl, .linkIdContent').hide();
        $('.opens, .linkIdProduct').show();
    }/!**
 * Created by samjhana on 12/18/2015.
 *!/*/
