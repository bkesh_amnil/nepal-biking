<?php

class Bike_price extends My_Controller
{
    var $table = 'tbl_bike_price';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bike_rent_model', 'bike_rent');
        $this->load->model('bike_price_model', 'bike_price');
        $this->data['module_name'] = 'Bike Price Manager';
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
        if($this->data['activeModulePermission']['view']) {
            $bike_id = $this->uri->segment(3);
            $bike_name = $this->bike_rent->get_single_data('tbl_bike', 'name', $bike_id, 'id');
            $this->data['show_add_link'] = true;
            $this->data['sub_module_name'] = 'Bike Price List for <b>' . $bike_name .'</b>';
            $this->data['rows'] = $this->bike_price->getBikePrice($bike_id);
            $this->data['body'] = BACKENDFOLDER.'/bike_price/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $bike_id = segment(4);
        $bike_name = $this->bike_rent->get_single_data('tbl_bike', 'name', $bike_id, 'id');
        $id = segment(5);
        $this->data['bike_id'] = $bike_id;
        $this->data['bike_rent_days'] = $this->bike_rent->getBikeRentDays();
        if($_POST) {
            $post = $_POST;

            $this->bike_price->id = $id;
            $this->form_validation->set_rules($this->bike_price->rules($id));
            if($this->form_validation->run()) {
                if ($id == 0) {
                    $bike_description_arr = $post['hidden_bike_description'];
                    $valid_from_arr = $post['hidden_rent_valid_from'];
                    $valid_to_arr = $post['hidden_rent_valid_to'];
                    $num_of_days = $post['hidden_rent_nos_of_days'];
                    $unit_price = $post['hidden_rent_unit_price'];
                    $valley_arr = $post['hidden_rent_outside_valley'];

                    foreach ($valid_from_arr as $ind => $val) {
                        $insert_batch[] = array(
                            'bike_id' => $bike_id,
                            'description' => $bike_description_arr[$ind],
                            'valid_from' => $val,
                            'valid_to' => $valid_to_arr[$ind],
                            'number_of_days_id' => $num_of_days[$ind],
                            'unit_price' => $unit_price[$ind],
                            'is_outside_valley' => $valley_arr[$ind],
                            'status' => $post['status'],
                            'created_by' => get_userdata('user_id'),
                            'created_date' => time()
                        );
                    }

                    if (isset($insert_batch) && !empty($insert_batch)) {
                        if ($this->db->insert_batch($this->table, $insert_batch)) {
                            set_flash('msg', 'Data saved');
                        } else {
                            set_flash('msg', 'Data could not be saved');
                        }
                    }

                } else {
                    $insert_data['description'] = $post['description'];
                    $insert_data['valid_from'] = $post['valid_from'];
                    $insert_data['valid_to'] = $post['valid_to'];
                    $insert_data['number_of_days_id'] = $post['number_of_days'];
                    $insert_data['unit_price'] = $post['unit_price'];
                    $insert_data['is_outside_valley'] = $post['is_outside_valley'];
                    $insert_data['status'] = $post['status'];
                    $insert_data['updated_by'] = get_userdata('user_id');
                    $insert_data['updated_date'] = time();
                    $this->db->where('id', $id);
                    $this->db->where('bike_id', $bike_id);
                    if($this->db->update($this->table, $insert_data)) {
                        set_flash('msg', 'Data saved');
                    } else {
                        set_flash('msg', 'Data could not be saved');
                    }
                }
                redirect(BACKENDFOLDER.'/bike_price/' . $bike_id);
            } else {
                $this->form($id, 'bike_price', $bike_name);
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js','assets/' . BACKENDFOLDER . '/dist/js/calendar.js', 'assets/' . BACKENDFOLDER . '/dist/js/dynamic_add.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'bike_price', $bike_name);
        }
    }

    public function delete()
    {
        $bike_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->bike_price->delete(array('id' => $selected_id, 'bike_id' => $bike_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->bike_price->delete(array('id' => $id, 'bike_id' => $bike_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/bike_price/' . $bike_id);
    }

    public function status()
    {
        $post = $_POST;
        $bike_id = segment(5);
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->bike_price->changeStatus('bike_price', $status, $selected_id, $bike_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(6);
            $res = $this->bike_price->changeStatus('bike_price', $status, $id, $bike_id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/bike_price/' . $bike_id);
    }

}