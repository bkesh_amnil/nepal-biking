<?php

class Image_Manager extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model', 'menu');
        $this->load->model('image_manager_model', 'image_manager');
        $this->data['module_name'] = 'Background Image Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Background Image List';
            $this->data['rows'] = $this->image_manager->getData();
            $this->data['body'] = BACKENDFOLDER.'/image_manager/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['menus'] = $this->menu->getBackgroundImageMenus();

        if($_POST) {
            $post = $_POST;
            $this->image_manager->id = $id;

            $this->form_validation->set_rules($this->image_manager->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->image_manager->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->image_manager->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/image_manager');
            } else {
                $this->form($id, 'image_manager');
            }
        } else {
            $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'/dist/js/image_manager.js');
            $this->form($id, 'image_manager');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->image_manager->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->image_manager->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/image_manager');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->image_manager->changeStatus('image_manager', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->image_manager->changeStatus('image_manager', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/image_manager');
    }

}