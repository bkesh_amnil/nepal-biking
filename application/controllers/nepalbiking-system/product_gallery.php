<?php

class Product_gallery extends My_Controller
{
    var $table = 'tbl_product_gallery';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_gallery_model', 'product_gallery');
        $this->data['module_name'] = 'Product Gallery Manager';
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {
        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
        if($this->data['activeModulePermission']['view']) {
            $product_id = $this->uri->segment(3);
            $product_name = $this->product_gallery->get_single_data('tbl_product', 'name', $product_id, 'id');
            $this->data['show_add_link'] = true;
            $this->data['sub_module_name'] = 'Product Gallery List <b>' . $product_name .'</b>';
            $this->data['rows'] = $this->product_gallery->getProductGallery($product_id);
            $this->data['body'] = BACKENDFOLDER.'/product_gallery/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $product_id = segment(4);
        $product_name = $this->product_gallery->get_single_data('tbl_product', 'name', $product_id, 'id');
        $id = segment(5);
        $this->data['product_id'] = $product_id;
        if($_POST) {
            $post = $_POST;

            $this->product_gallery->id = $id;
            $this->form_validation->set_rules($this->product_gallery->rules($id));
            if($this->form_validation->run()) {
                if ($id == 0) {
                    $media_arr = $post['media'];
                    if(isset($media_arr) && ! empty($media_arr)) {
                        foreach($media_arr as $ind => $val) {
                            $insert_batch[] = array(
                                'product_id' => $product_id,
                                'image' =>  $val,
                                'title' => $post['title'][$ind],
                                'caption' => $post['description'][$ind],
                                'status' => $post['status'],
                                'created_by' => get_userdata('user_id'),
                                'created_date' => time(),
                            );
                        }
                    }
                    if(isset($insert_batch) && !empty($insert_batch)) {
                        $res = $this->db->insert_batch($this->table, $insert_batch);
                    }
                } else {
                    $update_data['title'] = $post['title'][0];
                    $update_data['caption'] = $post['description'][0];
                    $update_data['status'] = $post['status'];
                    $this->db->where('id', $id);
                    $this->db->where('product_id', $product_id);
                    $res = $this->db->update($this->table, $update_data);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/product_gallery/' . $product_id);
            } else {
                $this->form($id, 'product_gallery', $product_name);
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/gallery.js');
            $this->form($id, 'product_gallery', $product_name);
        }
    }

    public function delete()
    {
        $product_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->product_gallery->delete(array('id' => $selected_id, 'product_id' => $product_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->product_gallery->delete(array('id' => $id, 'product_id' => $product_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/product_gallery/' . $product_id);
    }

    public function status()
    {
        $post = $_POST;
        $product_id = segment(5);
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->product_gallery->changeStatus('product_gallery', $status, $selected_id, $product_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(6);
            $res = $this->product_gallery->changeStatus('product_gallery', $status, $id, $product_id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/product_gallery/' . $product_id);
    }

}