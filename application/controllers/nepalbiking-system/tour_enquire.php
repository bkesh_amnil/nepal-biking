<?php

class Tour_Enquire extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('tour_category_model', 'tour_category');
        $this->load->model('tour_model', 'tour');
        $this->load->model('tour_enquire_model', 'tour_enquire');
        $this->data['module_name'] = 'Tour Enquire Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Tour Enquire List';
            $this->data['tour_booking_categories'] = $this->tour_category->getTourEnquireCategory();
            $this->data['tours'] = $this->tour->getTourEnquireNames();
            $this->data['customers'] = $this->tour_enquire->getTourEnquireCustomers();
            $this->data['rows'] = $this->tour_enquire->getTourEnquiries();
            $this->data['body'] = BACKENDFOLDER.'/tour_enquire/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/tour_enquire.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;

        $this->data['tour_categories'] = $this->tour_category->getTourCategories();
        $this->data['tours'] = $this->tour->getTours();

        if($_POST) {
            $post = $_POST;
            if($post['enquire_note'] != '') {
                $condition = array('id' => $id);
                $res = $this->tour_enquire->save($post, $condition);

                if($res) {
                    /* sending email code */
                    $footer_contact = $this->public_model->getContent('contact-address');
                    $email_admin = '';
                    $subject = 'Tour Enquire';

                    $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                    $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                    $email_admin .= '<tbody>';
                    $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                    $email_admin .= $subject;
                    $email_admin .= '</td></tr>';
                    $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                    $email_admin .= $post['enquire_note'];
                    $email_admin .= '</td></tr>';
                    $email_admin .= '</td></tr>';
                    $email_admin .= 'Regards<br/>';
                    $email_admin .= 'Nepal Biking<br/>';
                    $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                    $email_admin .= '</td>';
                    $email_admin .= '<td>';
                    $email_admin .= $footer_contact[0]->long_description;
                    $email_admin .= '</td></tr>';

                    //$message = $post['enquire_note'];
                    $message = $email_admin;
                    $res = $this->email_send->sendMail($subject, $post['customer_email_address'], $message);
                    /* sending email code */
                    if(isset($res) && ($res == 'success')) {
                        set_flash('msg', 'Data saved and mail sent to user.');
                    } else {
                        set_flash('msg', 'Data saved');
                    }
                } else {
                    set_flash('msg', 'Data not saved');
                }
            } else {
                set_flash('msg', 'No changes made.');
            }

            redirect(BACKENDFOLDER . '/tour_enquire');
        } else {
            $this->form($id, 'tour_enquire');
        }
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'tour_category') {
            $post['search_field'] = $_POST['export_filter_tour_category'];
        } else if($post['search_field_type'] == 'tour') {
            $post['search_field'] = $_POST['export_filter_tour'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'enquire_date') {
            $post['search_field'] = $_POST['export_filter_enquire_date'];
        }

        $result = $this->tour_enquire->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Tour-Enquire- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Tour Category');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Tour Name');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Tour Code');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Customer Contact Address');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Tour Departure Date');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Number of Pax');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Enquire Date');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Customer Enquiry');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->tour_category_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->tour_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->customer_email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->customer_contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->customer_contact_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->departure_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->number_of_pax, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, $v->customer_enquiry, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        } else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/tour_enquire');
        }
    }

}