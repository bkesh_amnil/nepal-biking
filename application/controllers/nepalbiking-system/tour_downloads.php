<?php

class Tour_Downloads extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tour_download_model', 'tour_download');
        $this->data['module_name'] = 'Tour Download Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Tour Download List';
            $this->data['rows'] = $this->tour_download->getTourDownloads();
            $this->data['body'] = BACKENDFOLDER.'/tour_download/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'tour_destination') {
            $post['search_field'] = $_POST['export_filter_tour_destination'];
        } else if($post['search_field_type'] == 'tour_experience') {
            $post['search_field'] = $_POST['export_filter_tour_experience'];
        } else if($post['search_field_type'] == 'tour') {
            $post['search_field'] = $_POST['export_filter_tour'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'download_date') {
            $post['search_field'] = $_POST['export_filter_download_date'];
        }

        $result = $this->tour_download->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Tour-Download- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Tour Destination');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Tour Experience');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Tour Name');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Tour Code');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Customer Contact Address');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Departure Date');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Number of Pax');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Enquiry Date');
            $excel->getActiveSheet()->setCellValue('M' . $k, 'Customer Enquiry');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->destination_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->experience_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->tour_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->contact_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->departure_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, $v->number_of_pax, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('M' . $count, $v->enquiry, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/tour_download');
        }
    }

}