<?php

class Race extends My_Controller
{
    var $race_media = 'tbl_race_media';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('racemedia_model', 'racemedia');
        $this->load->model('race_model', 'race');
        $this->data['module_name'] = 'Race Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Race List';
            $this->data['races'] = $this->race->get();
            $this->data['body'] = BACKENDFOLDER.'/race/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;
        if($_POST) {
            $post = $_POST;
            if(isset($post['media'])) {
                $racemedias = $post['media'];
                $titlemedia = $post['title'];
                $captionmedia = $post['caption'];
                unset($post['media'], $post['title'], $post['caption']);
            }
            $this->race->id = $id;
            $this->form_validation->set_rules($this->race->rules($id));
            if($this->form_validation->run()) {
                $insert_post['name'] = $post['name'];
                $insert_post['slug'] = $post['slug'];
                $insert_post['venue'] = $post['venue'];
                $insert_post['start_point'] = $post['start_point'];
                $insert_post['start_point_latitude'] = $post['start_point_latitude'];
                $insert_post['start_point_longitude'] = $post['start_point_longitude'];
                $insert_post['finish_point'] = $post['finish_point'];
                $insert_post['finish_point_latitude'] = $post['finish_point_latitude'];
                $insert_post['finish_point_longitude'] = $post['finish_point_longitude'];
                $insert_post['start_date'] = $post['start_date'];
                $insert_post['end_date'] = $post['end_date'];
                $insert_post['cover_image'] = $post['cover_image'];
                $insert_post['backgroud_image'] = $post['backgroud_image'];
                $insert_post['entry_fee'] = $post['entry_fee'];
                $insert_post['categories'] = $post['categories'];
                $insert_post['events'] = $post['events'];
                $insert_post['description'] = $post['description'];
                $insert_post['itinerary'] = $post['itinerary'];
                $insert_post['contact'] = $post['contact'];
                $insert_post['youtube_link'] = $post['youtube_link'];
                $insert_post['google_form_link'] = (isset($post['google_form_link']) && !empty($post['google_form_link'])) ? $post['google_form_link'] : $post['google_form_link_edit'];
                $insert_post['meta_keywords'] = $post['meta_keywords'];
                $insert_post['meta_description'] = $post['meta_description'];
                $insert_post['status'] = $post['status'];
                $this->db->trans_begin();

                if($id == '') {
                    $res = $this->race->save($insert_post, '', true);
                    $id = $res;
                    if($post['google_form_link'] != '') {
                        $insert_form_link['type'] = 'race';
                        $insert_form_link['type_id'] = $id;
                        $insert_form_link['google_form_link'] = $post['google_form_link'];
                        $this->db->insert('tbl_google_form_links', $insert_form_link);
                    }
                } else {
                    $condition = array('id' => $id);
                    $res = $this->race->save($insert_post, $condition);

                    if(isset($post['edit_google_form'])) {
                        $insert_form_link['type'] = 'race';
                        $insert_form_link['type_id'] = $id;
                        $insert_form_link['google_form_link'] = $post['google_form_link'];
                        $this->db->insert('tbl_google_form_links', $insert_form_link);
                    }
                }
                // saving race media
                $this->racemedia->delete(array('race_id' => $id));

                foreach($racemedias as $ind => $media) {
                    foreach($media as $index => $val) {
                        $insert_batch[] = array(
                            'race_id' => $id,
                            'type' => $ind,
                            'image' => $val,
                            'title' => $titlemedia[$ind][$index],
                            'caption' => $captionmedia[$ind][$index]
                        );
                    }
                }

                if(isset($insert_batch) && !empty($insert_batch)) {
                    $this->db->insert_batch($this->race_media, $insert_batch);
                }

                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    set_flash('msg', 'Data could not be saved');
                } else {
                    $this->db->trans_commit();
                    set_flash('msg', 'Data saved');
                }

                redirect(BACKENDFOLDER.'/race');
            } else {
                $this->form($id, 'race');
            }
        } else {
            if($id != '') {
                $this->data['savedMedia'] = $this->racemedia->getSavedMedia($id);
            }
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js', 'assets/' . BACKENDFOLDER . '/dist/js/calendar.js', 'assets/' . BACKENDFOLDER . '/dist/js/gallery.js', 'assets/'.BACKENDFOLDER.'/dist/js/google_form.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'race');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->race->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->race->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/race');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->race->changeStatus('race', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->race->changeStatus('race', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/race');
    }

    public function formdata() {

    }

}