<?php

class Tour_map extends My_Controller
{
    var $table = 'tbl_tour_map';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tour_map_model', 'tour_map');
        $this->data['module_name'] = 'Tour Map Manager';
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {
        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
        if($this->data['activeModulePermission']['view']) {
            $tour_id = $this->uri->segment(3);
            $tour_name = $this->tour_map->get_single_data('tbl_tour', 'name', $tour_id, 'id');
            $this->data['show_add_link'] = true;
            $this->data['sub_module_name'] = 'Tour Map List for <b>' . $tour_name .'</b>';
            $this->data['rows'] = $this->tour_map->getTourMaps($tour_id);
            $this->data['body'] = BACKENDFOLDER.'/tour_map/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $tour_id = segment(4);
        $tour_name = $this->tour_map->get_single_data('tbl_tour', 'name', $tour_id, 'id');
        $id = segment(5);
        $this->data['tour_id'] = $tour_id;
        $this->data['rows'] = $this->tour_map->getTourMaps($tour_id);
        if($_POST) {
            $post = $_POST;
            $alphas = range('A', 'Z');
            $title_arr = $post['title'];
            $address_arr = $post['address'];
            $description_arr = $post['description'];
            $latitude_arr = $post['latitude'];
            $longitude_arr = $post['longitude'];
            $order_arr = $post['order'];

            if(isset($title_arr) && !empty($title_arr)) {
                foreach($title_arr as $ind => $val) {
                    $array[] = array(
                        'title' => $val,
                        'address' => $address_arr[$ind],
                        'description' => $description_arr[$ind],
                        'latitude' => $latitude_arr[$ind],
                        'longitude' => $longitude_arr[$ind],
                        'order' => $order_arr[$ind]
                    );
                }
            }

            function sortByOrder($a, $b) {
                return $a['order'] - $b['order'];
            }

            usort($array, 'sortByOrder');

            if(isset($array) && !empty($array)) {
                foreach($array as $ind => $val) {
                    $order = $ind+1;
                    $insert_batch[] = array(
                        'tour_id' => $tour_id,
                        'latitude' => $val['latitude'],
                        'longitude' => $val['longitude'],
                        'marker_title' => $val['title'],
                        'marker_address' => $val['address'],
                        'marker_description' => $val['description'],
                        'marker_label' => $alphas[$ind],
                        'marker_sort_order' => $order,
                        'created_by' => get_userdata('user_id'),
                        'created_date' => time()
                    );
                }
            }

            if(isset($insert_batch) && !empty($insert_batch)) {
                $this->db->where('tour_id', $tour_id);
                $this->db->delete($this->table);

                if($this->db->insert_batch($this->table, $insert_batch)) {
                    set_flash('msg', 'Data saved');
                } else {
                    set_flash('msg', 'Data could not be saved');
                }
            }
            redirect(BACKENDFOLDER.'/tour_map/' . $tour_id);
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/map.js');
            $this->form($id, 'tour_map', $tour_name);
        }
    }

    public function delete()
    {
        $tour_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->tour_map->delete(array('id' => $selected_id, 'tour_id' => $tour_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->tour_map->delete(array('id' => $id, 'tour_id' => $tour_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/tour_map/' . $tour_id);
    }

}