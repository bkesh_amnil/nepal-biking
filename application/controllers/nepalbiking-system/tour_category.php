<?php

class Tour_Category extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tour_category_model', 'tour_category');
        $this->data['module_name'] = 'Tour Category Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Tour Category List';
            $this->data['tour_categories'] = $this->tour_category->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/tour_category/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->tour_category->id = $id;

            $this->form_validation->set_rules($this->tour_category->rules($id));
            if($this->form_validation->run()) {
                $post['slug'] = $this->tour_category->createSlug($post['name'], $id);
                if($id == '') {
                    $res = $this->tour_category->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->tour_category->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/tour_category');
            } else {
                $this->form($id, 'tour_category');
            }
        } else {
            $this->form($id, 'tour_category');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_tour.tour_category_id|tbl_tour_booking.tour_category_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->tour_category->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->tour_category->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/tour_category');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->tour_category->changeStatus('tour_category', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->tour_category->changeStatus('tour_category', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/tour_category');
    }

}