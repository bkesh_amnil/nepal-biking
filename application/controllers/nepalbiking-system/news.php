<?php

class News extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model', 'news');
        $this->data['module_name'] = 'News Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'News List';
            $this->data['news'] = $this->news->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER . '/news/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if ($id == '')
            $this->data['news'] = $this->news->get();
        else
            $this->data['news'] = $this->news->get('', array('id !=' => $id));
        if ($_POST) {
            $post = $_POST;
            $this->news->id = $id;
            $this->form_validation->set_rules($this->news->rules($id));
            if ($this->form_validation->run()) {
                if ($id == '') {
                    $res = $this->news->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->news->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER . '/news');
            } else {
                $this->form($id, 'news');
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js', 'assets/'.BACKENDFOLDER.'/dist/js/calendar.js', 'assets/' . BACKENDFOLDER . '/dist/js/jquery.textarea-counter.js', 'assets/' . BACKENDFOLDER . '/dist/js/news.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'news');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->news->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->news->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER . '/news');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->news->changeStatus('news', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->news->changeStatus('news', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER . '/news');
    }

    public function checkHighlightedNews()
    {
        $this->db->select('id, name');
        $this->db->where('is_highlighted', 1);
        $row = $this->db->get('tbl_news')->row();

        if (isset($row) && !empty($row)) {
            $data['action'] = 'error';
            $data['msg'] = 'news ' . $row->name . ' is highlighted. Do you want to remove it?';
            $data['id'] = $row->id;
        } else {
            $data['action'] = 'success';
        }

        echo json_encode($data);
    }

    public function removeHighlight()
    {
        $news_id = $_POST['id'];

        $edit_data['is_highlighted'] = '0';
        $this->db->where('id', $news_id);

        if ($this->db->update('tbl_news', $edit_data)) {
            $data['result'] = 'sucess';
            $data['msg'] = 'Highlight removed successfully';
        } else {
            $data['result'] = 'error';
            $data['msg'] = 'Error in updating news';
        }

        echo json_encode($data);
    }


}