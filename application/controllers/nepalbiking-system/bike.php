<?php

class Bike extends My_Controller
{
    var $table = 'tbl_bike';
    var $bike_rent_detail = 'tbl_bike_rent_detail';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bike_category_model', 'bike_category');
        $this->load->model('departure_type_model', 'departure');
        $this->load->model('bike_model', 'bike');
        $this->data['module_name'] = 'Bike Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Bike List';
            $this->data['bikes'] = $this->bike->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/bike/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['bike_categories'] = $this->bike_category->getBikeCategories();
        $this->data['rent_types'] = $this->departure->getTourDepartures();
        if($_POST) {
            $post = $_POST;
            $this->bike->id = $id;

            $this->form_validation->set_rules($this->bike->rules($id));
            if($this->form_validation->run()) {
                $insert_bike['bike_category_id'] = $post['bike_category_id'];
                $insert_bike['code'] = $post['code'];
                $insert_bike['name'] = $post['name'];
                $insert_bike['slug'] = $post['slug'];
                $insert_bike['cover_image'] = $post['cover_image'];
                $insert_bike['file'] = $post['file'];
                $insert_bike['bike_model'] = $post['bike_model'];
                $available_quantity = $insert_bike['available_quantity'] = $post['available_quantity'];
                $insert_bike['size'] = $post['size'];
                $insert_bike['cost']= $post['cost'];
                $insert_bike['charge_per_day'] = $post['charge_per_day'];
                $insert_bike['short_description'] = $post['short_description'];
                $insert_bike['description'] = $post['description'];
                $insert_bike['terms'] = $post['terms'];
                $rent_type = $insert_bike['rent_type'] = $post['rent_type'];
                if($id == '') {
                    $rent_years = $post['rent_year'];
                    $insert_bike['rent_year'] = implode(',', $post['rent_year']);
                }
                $insert_bike['meta_keywords'] = $post['meta_keywords'];
                $insert_bike['meta_description'] = $post['meta_description'];
                $insert_bike['status'] = $post['status'];

                $current_month = date('m');
                $current_year = date('Y');

                $this->db->trans_begin();

                if($id == '') {
                   /* $insert_bike['created_by'] = get_userdata('user_id');
                    $insert_bike['created_date'] = time();*/
                    $this->db->insert($this->table, $insert_bike);
                    $bike_id = $this->db->insert_id();
                    if( $rent_type != 'anyday') {
                        if( $rent_type != 'everyday') {
                            foreach($rent_years as $rent_year) {
                                $start_month = 01;
                                if($rent_year == $current_year) {
                                    $start_month = $current_month;
                                }
                                for($month = $start_month; $month <= 12; $month ++) {
                                    $days[] = $this->getAllSpecificDaysInAMonth($rent_year, $month,  $rent_type);
                                }
                            }
                            if(isset($days) && !empty($days)) {
                                foreach($days as $day) {
                                    foreach($day as $val) {
                                        $insert_batch[] = array(
                                            'bike_id' => $bike_id,
                                            'rent_date' => $val->format('Y-m-d'),
                                            'all_allocation' => $available_quantity,
                                            'available' => $available_quantity,
                                            'booked' => 0,
                                            'is_fixed_departure' => 1,
                                            'status' => 1
                                        );
                                    }
                                }

                                if(isset($insert_batch) && !empty($insert_batch)) {
                                    $this->db->insert_batch($this->bike_rent_detail, $insert_batch);
                                }
                            }
                        } else {
                            foreach($rent_years as $rent_year) {
                                $start_month = 01;
                                if($rent_year == $current_year) {
                                    $start_month = $current_month;
                                }
                                for($month = $start_month; $month <= 12; $month ++) {
                                    $days[] = $this->saveAllDaysInAMonth($rent_year, $month);
                                }
                            }
                            if(isset($days) && !empty($days)) {
                                foreach($days as $day) {
                                    foreach($day as $val) {
                                        $insert_batch[] = array(
                                            'bike_id' => $bike_id,
                                            'rent_date' => $val,
                                            'all_allocation' => $available_quantity,
                                            'available' => $available_quantity,
                                            'booked' => 0,
                                            'is_fixed_departure' => 1,
                                            'status' => 1
                                        );
                                    }
                                }
                                if(isset($insert_batch) && !empty($insert_batch)) {
                                    $this->db->insert_batch($this->bike_rent_detail, $insert_batch);
                                }
                            }
                        }
                    }
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        set_flash('msg', 'Data saved');
                    }
                } else {
                    $saved_rent_year = $post['saved_rent_year'];
                    $insert_bike['rent_year'] = $saved_rent_year;
                    if(isset($post['rent_year']) && !empty($post['rent_year'])) {
                        $rent_years = $post['rent_year'];
                        $insert_bike['rent_year'] = $saved_rent_year . ',' . implode(',', $post['rent_year']);
                    }
                    /*$insert_bike['updated_by']	 = get_userdata('user_id');
                    $insert_bike['updated_date'] = time();*/
                    $this->db->where('id', $id);
                    $this->db->update($this->table, $insert_bike);

                    if(isset($rent_years) && !empty($rent_years)) {
                         if( $rent_type != 'anyday') {
                            if( $rent_type != 'everyday') {
                                foreach($rent_years as $rent_year) {
                                    $start_month = 01;
                                    if($rent_year == $current_year) {
                                        $start_month = $current_month;
                                    }
                                    for($month = $start_month; $month <= 12; $month ++) {
                                        $days[] = $this->getAllSpecificDaysInAMonth($rent_year, $month, $rent_type);
                                    }
                                }
                                if(isset($days) && !empty($days)) {
                                    foreach($days as $day) {
                                        foreach($day as $val) {
                                            $insert_batch[] = array(
                                                'bike_id' => $id,
                                                'rent_date' => $val->format('Y-m-d'),
                                                'all_allocation' => $available_quantity,
                                                'available' => $available_quantity,
                                                'booked' => 0,
                                                'is_fixed_departure' => 1,
                                                'status' => 1
                                            );
                                        }
                                    }

                                    if(isset($insert_batch) && !empty($insert_batch)) {
                                        $this->db->insert_batch($this->bike_rent_detail, $insert_batch);
                                    }
                                }
                            } else {
                                foreach($rent_years as $rent_year) {
                                    $start_month = 01;
                                    if($rent_year == $current_year) {
                                        $start_month = $current_month;
                                    }
                                    for($month = $start_month; $month <= 12; $month ++) {
                                        $days[] = $this->saveAllDaysInAMonth($rent_year, $month);
                                    }
                                }
                                if(isset($days) && !empty($days)) {
                                    foreach($days as $day) {
                                        foreach($day as $val) {
                                            $insert_batch[] = array(
                                                'bike_id' => $id,
                                                'rent_date' => $val,
                                                'all_allocation' => $available_quantity,
                                                'available' => $available_quantity,
                                                'booked' => 0,
                                                'is_fixed_departure' => 1,
                                                'status' => 1
                                            );
                                        }
                                    }
                                    if(isset($insert_batch) && !empty($insert_batch)) {
                                        $this->db->insert_batch($this->bike_rent_detail, $insert_batch);
                                    }
                                }
                            }
                        }
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        set_flash('msg', 'Data saved');
                    }
                }

                //$res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/bike');
            } else {
                $this->form($id, 'bike');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/jquery.textarea-counter.js', 'assets/' . BACKENDFOLDER . '/dist/js/counter.js');
            $this->form($id, 'bike');
        }
    }

    public function getAllSpecificDaysInAMonth($year, $month, $day, $daysError = 3)
    {
        $dateString = 'first '.$day.' of '.$year.'-'.$month;
        if (!strtotime($dateString))
        {
            throw new \Exception('"'.$dateString.'" is not a valid strtotime');
        }
        $startDay = new \DateTime($dateString);
        $days = array();
        while ($startDay->format('Y-m') <= $year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT))
        {
            $days[] = clone($startDay);
            $startDay->modify('+ 7 days');
        }

        return $days;
    }

    public function saveAllDaysInAMonth($year, $month)
    {
        $days = array();
        $num_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for($d = 01; $d <= $num_of_days; $d++)
        {
            $time = mktime(12, 0, 0, $month, $d, $year);
            $monthName = date('F', mktime(0, 0, 0, $month, 10));
            if (date('m', $time) == $month) {
                $days[] = date('Y-m-d', $time);
            }
        }

        return $days;
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_bike_rent.bike_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->bike->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->bike->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/bike');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->bike->changeStatus('bike', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->bike->changeStatus('bike', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/bike');
    }

}