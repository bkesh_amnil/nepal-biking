<?php

class Bike_Book extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('booking_status_model', 'book_status');
        $this->load->model('bike_category_model', 'bike_category');
        $this->load->model('bike_model', 'bike');
        $this->load->model('bike_rent_model', 'bike_rent');
        $this->load->model('public_model', 'public');
        $this->load->model('bike_book_model', 'bike_book');
        $this->load->model('bike_book_status_model', 'bike_book_status');
        $this->load->model('bike_rent_detail_model', 'bike_rent_detail');
        $this->data['module_name'] = 'Bike Book Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Bike Book List';
            $this->data['bike_categories'] = $this->bike_category->getBikeRentCategory();
            $this->data['bikes'] = $this->bike->getBikeRentNames();
            $this->data['customers'] = $this->bike_book_status->getBikeRentCustomers();
            $this->data['rent_status'] = $this->book_status->getBookingStatus('bike_rent');
            $this->data['rent_days'] = $this->bike_rent->getRentDays();
            $this->data['bike_bookings'] = $this->bike_book->getBikeBookings();

            $this->data['body'] = BACKENDFOLDER.'/bike_book/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/bike_book.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;
        $this->data['bike_book_status'] = $this->_format_data($id);
        $this->data['book_status'] = $this->book_status->getBookStatus();
        $this->data['bike_categories'] = $this->bike_category->getBikeCategories();
        $this->data['nationalities'] = $this->public->getCountries();

        if($id != '') {
            $this->data['logs'] = $this->bike_book_status->getSavedData($id, 'logs');
            $bike_category_id = $this->bike_book->get_single_data('tbl_bike_rent', 'bike_category_id', $id, 'id');
            $this->data['bikes'] = $this->bike->getBikes($bike_category_id);
        } else {
            $this->data['bikes'] = $this->bike->getBikes($this->data['bike_categories'][0]->id);
        }

        //if (isset($this->data['bikes']) && !empty($this->data['bikes'])) {
            $this->data['rent_dates'] = $this->public->getBikeRentDates($this->data['bikes'][0]->id);
        //$this->data['rent_dates'] = $this->bike->getBikeRentDates($this->data['bikes'][0]->id);
        /*} else {
            $this->data['rent_dates'] = '';
        }*/

        $this->data['id'] = $id;

        if($_POST) {
            $post = $_POST;
            $this->bike_book->id = $id;
            $this->form_validation->set_rules($this->bike_book->rules($id));
            if($this->form_validation->run()) {
                $this->form_validation->set_rules($this->bike_book_status->rules($id));
                if($this->form_validation->run()) {
                    $insert_book['bike_category_id'] = $post['bike_category_id'];
                    $insert_book['bike_id'] = $post['bike_id'];
                    $insert_book['customer_remarks'] = '';
                    $insert_book['last_booking_status_id'] = $post['bike_booking_status_id'];

                    $insert_status['rent_date_id'] = $post['rent_date_id'];
                    $insert_status['bike_available'] = $post['bike_available'];
                    $insert_status['number_of_pax'] = $post['number_of_pax'];
                    $insert_status['number_of_days_id'] = $post['number_of_days_id'];
                    $insert_status['price_per_person'] = $post['price_per_person'];
                    $insert_status['total_price'] = $post['total_price'];
                    $insert_status['customer_full_name'] = $post['customer_full_name'];
                    $insert_status['customer_email_address'] = $post['customer_email_address'];
                    $insert_status['customer_contact_number'] = $post['customer_contact_number'];
                    $insert_status['customer_contact_address'] = $post['customer_contact_address'];
                    $insert_status['customer_nationality'] = $post['customer_nationality'];
                    $insert_status['customer_passport_number'] = $post['customer_passport_number'];
                    $insert_status['customer_dob'] = $post['customer_dob'];
                    $insert_status['customer_local_address'] = $post['customer_local_address'];
                    $insert_status['customer_room_number'] = $post['customer_room_number'];
                    $insert_status['bike_booking_status_id'] = $post['bike_booking_status_id'];
                    $insert_status['bike_note'] = $post['bike_note'];

                    $this->db->trans_begin();

                    if ($id == '') {
                        $res = $this->bike_book->save($insert_book, '', true);
                        $bike_rent_id = $res;

                        $insert_status['bike_rent_id'] = $bike_rent_id;

                        $this->bike_book_status->save($insert_status);
                        /* change bike booking count according to booking status starts */
                        if($post['bike_booking_status_id'] == '2') {
                            $this->changeAllocationBookedAvailable($post['number_of_pax'], $post['bike_id'], $id, '', '', $post['rent_date_id'], '');
                        }
                        /* change bike booking count according to booking status ends */
                    } else {
                        $condition = array('id' => $id);
                        $this->bike_book->save($insert_book, $condition);

                        $insert_status['bike_rent_id'] = $id;

                        $this->bike_book_status->save($insert_status);

                        $this->changeAllocationBookedAvailable($post['number_of_pax'], $post['bike_id'], $id, $post['previous_rent_date_id'], $post['previous_book_status'], $post['rent_date_id'], $post['bike_booking_status_id']);
                    }
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        /* sending mail code */
                        if(!empty($post['bike_note'])) {
                            $booking_status = $this->db->select('name')->where('id', $post['bike_booking_status_id'])->get('tbl_booking_status')->row();
                            $footer_contact = $this->public_model->getContent('contact-address');
                            $email_admin = '';
                            $subject = 'Bike Rent Booking (' . $booking_status->name . ')';

                            $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                            $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                            $email_admin .= '<tbody>';
                            $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                            $email_admin .= $subject;
                            $email_admin .= '</td></tr>';
                            $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                            $email_admin .= $post['bike_note'];
                            $email_admin .= '</td></tr>';
                            $email_admin .= '</td></tr>';
                            $email_admin .= 'Regards<br/>';
                            $email_admin .= 'Nepal Biking<br/>';
                            $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                            $email_admin .= '</td>';
                            $email_admin .= '<td>';
                            $email_admin .= $footer_contact[0]->long_description;
                            $email_admin .= '</td></tr>';
                            //$message = $post['bike_note'];
                            $message = $email_admin;

                            $res = $this->email_send->sendMail($subject, $post['customer_email_address'], $message);
                        }
                        /* sending mail code */
                        if(isset($res) && ($res == 'success')) {
                            set_flash('msg', 'Data saved and mail sent to user.');
                        } else {
                            set_flash('msg', 'Data saved');
                        }
                    }
                    redirect(BACKENDFOLDER . '/bike_book');
                }else {
                    $this->form($id, 'bike_book');
                }
            } else {
                $this->form($id, 'bike_book');
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js', 'assets/'.BACKENDFOLDER.'/dist/js/calendar.js', 'assets/' . BACKENDFOLDER . '/dist/js/bike_book.js', 'assets/' . BACKENDFOLDER . '/dist/js/calendar.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'bike_book');
        }
    }

    public function changeAllocationBookedAvailable($nos_of_pax, $bike_id, $id, $previous_rent_date_id, $previous_booking_status_id, $current_rent_date_id, $current_booking_status_id) {
        if($id == '') { /* create status only confirmed*/
            $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
            $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
        } else { /* edit */
            /* prev date = cur date, prev status = cur status ---> no change @ all*/
            if($previous_rent_date_id == $current_rent_date_id) {
                if($previous_booking_status_id != $current_booking_status_id) {
                    if($previous_booking_status_id == '1') { /* Pending */
                        if($current_booking_status_id == '2') { /* to Confirmed */
                            $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                            $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                        }
                        /* Pending to Cancelled ---> No change */
                    } else if($previous_booking_status_id == '2') { /* Confirmed */
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                    } else { /* Cancelled*/
                        if($current_booking_status_id == '2') { /* Confirmed */
                            $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                            $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                        }
                        /* Cancelled to Pending ---> No change */
                    }
                }
            } else {
                /* prev date !=  cur date */
                if($previous_booking_status_id != $current_booking_status_id) { /* prev status != curr status */
                    if(($previous_booking_status_id == '1' && $current_booking_status_id == '2') || ($previous_booking_status_id == '3' && $current_booking_status_id == '2')) { /* prev - pending; curr - confirmed OR prev - cancelled;curr - confirmed */
                        /* add booking to current date id */
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                    } elseif(($previous_booking_status_id == '2' && $current_booking_status_id == '1') || ($previous_booking_status_id == '2' && $current_booking_status_id == '3')) { /* prev - confirmed;curr - pending OR prev - confirmed; curr - cancelled*/
                        /* subtract booking from previous date id */
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $previous_rent_date_id);
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $previous_rent_date_id);
                    }
                } else { /* prev status == curr status */
                    /* Pending n Cancelled Status ---> no change */
                    if($previous_booking_status_id == '2') { /* Confirmed */
                        /* subtract booking from previous date id */
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $previous_rent_date_id);
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $previous_rent_date_id);
                        /* add booking to current date id */
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET booked = booked + " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                        $this->db->query("UPDATE tbl_bike_rent_detail
                                        SET available = available - " . $nos_of_pax . "
                                        WHERE bike_id = " . $bike_id . "
                                        AND id = " . $current_rent_date_id);
                    }
                }
            }
        }
    }

    public function _format_data($id) {
        if($id != '') {
            $bike_rent_status = $this->bike_book_status->getSavedData($id, 'edit');

            $data['rent_date_id'] = $bike_rent_status->rent_date_id;
            $data['available'] = $bike_rent_status->available;
            $data['number_of_days_id'] = $bike_rent_status->number_of_days_id;
            $data['number_of_pax'] = $bike_rent_status->number_of_pax;
            $data['price_per_person'] = $bike_rent_status->price_per_person;
            $data['total_price'] = $bike_rent_status->total_price;
            $data['customer_full_name'] = $bike_rent_status->customer_full_name;
            $data['customer_email_address'] = $bike_rent_status->customer_email_address;
            $data['customer_contact_number'] = $bike_rent_status->customer_contact_number;
            $data['customer_contact_address'] = $bike_rent_status->customer_contact_address;
            $data['customer_nationality'] = $bike_rent_status->customer_nationality;
            $data['customer_passport_number'] = $bike_rent_status->customer_passport_number;
            $data['customer_dob'] = $bike_rent_status->customer_dob;
            $data['customer_local_address'] = $bike_rent_status->customer_local_address;
            $data['customer_room_number'] = $bike_rent_status->customer_room_number;
            $data['bike_booking_status_id'] = $bike_rent_status->bike_booking_status_id;
            $data['bike_note'] = $bike_rent_status->bike_note;

            $data['bike_name'] = $bike_rent_status->bike_name;
            $data['rent_date'] = $bike_rent_status->rent_date;
            $data['allocation'] = $bike_rent_status->all_allocation;
            $data['available'] = $bike_rent_status->available;
            $data['booked'] = $bike_rent_status->booked;
        } else {
            $data['rent_date_id'] = '';
            $data['available'] = '';
            $data['number_of_days_id'] = '';
            $data['number_of_pax'] = '';
            $data['price_per_person'] = '';
            $data['total_price'] = '';
            $data['customer_full_name'] = '';
            $data['customer_email_address'] = '';
            $data['customer_contact_number'] = '';
            $data['customer_contact_address'] = '';
            $data['customer_nationality'] = '';
            $data['customer_passport_number'] = '';
            $data['customer_dob'] = '';
            $data['customer_local_address'] = '';
            $data['customer_room_number'] = '';
            $data['bike_booking_status_id'] = '';
            $data['bike_note'] = '';
        }

        $data = (object)$data;

        return $data;
    }

    public function load_data() {
        $post = $_POST;

        $data['bike_html'] = '';
        $data['bike_dates'] = '';
        $data['bike_pax'] = '';
        $data['bike_unit_price'] = '';
        $data['bike_name'] = '';
        $data['rent_date'] = '';
        $data['all_allocation'] = '';
        $data['booked'] = '';
        $data['available'] = '';
        $data['rent_duration_days_id'] = '';
        $data['rent_duration_days'] = '';
        $data['rent_duration_days_option'] = '';

        if($post['type'] == 'bike_category') { /* change in bike category */
            $bikes = $this->bike->getBikes($post['bike_category_id']);
        } else if($post['type'] == 'bike') { /* change in bike */
            $data['bike_name'] = $this->bike->get_single_data('tbl_bike', 'name', $post['bike_id'], 'id');
            $rent_dates = $this->public->getBikeRentDates($post['bike_id']);
        } else if($post['type'] == 'rent_date') { /* change in rent date */
            $date_detail = $this->public_model->getDateDetail($post['sel_date'], $post['bike_id'], 'bike');
            $price_detail = $this->public_model->getPriceDetail($post['nos_of_days'], $post['sel_date'], $post['bike_id'], 'bike');
        }

        if(isset($bikes) && !empty($bikes)) {
            $data['bike_name'] = $bikes[0]->name;
            $rent_dates = $this->public->getBikeRentDates($bikes[0]->id);

            foreach($bikes as $bike) {
                $data['bike_html'] .= '<option value="'.$bike->id.'">'.$bike->name.'</option>';
            }
        }

        if(isset($rent_dates) && !empty($rent_dates)) {
            $data['all_allocation'] = $rent_dates[0]->all_allocation;
            $data['booked'] = $rent_dates[0]->booked;
            $data['available'] = $rent_dates[0]->available;

            $data['bike_unit_price'] = $rent_dates[0]->unit_price;
            $data['rent_duration_days_id'] = $rent_dates[0]->number_of_days_id;
            $data['rent_duration_days'] = $rent_dates[0]->name;

            foreach($rent_dates as $val) {
                $data['bike_dates'] .= '<option value="'.$val->id.'">'.$val->rent_date.'</option>';
            }
            /*if($data['rent_duration_days_id'] != '') {
                $data['rent_duration_days_option'] .= '<option value="'.$data['rent_duration_days_id'].'">'.$data['rent_duration_days'].'</option>';
            }*/
        } else if($post['type'] == 'rent_date') {
            $data['all_allocation'] = $date_detail->all_allocation;
            $data['booked'] = $date_detail->booked;
            $data['available'] = $date_detail->available;
            $data['unit_price'] = $price_detail->unit_price;
        }

        if(isset($price_detail) && !empty($price_detail)) {
            $data['bike_unit_price'] = $price_detail->unit_price;
            $data['rent_duration_days_id'] = $price_detail->number_of_days_id;
            $data['rent_duration_days'] = $price_detail->name;
        }

        if($data['rent_duration_days_id'] != '') {
            $str = $data['rent_duration_days'];
            $str = preg_replace('/\D/', '', $str);
            $range = range(1, $str);
            //$data['rent_duration_days_option'] .= '<option value="'.$data['rent_duration_days_id'].'">'.$data['rent_duration_days'].'</option>';
            if(isset($range) && !empty($range)) {
                foreach($range as $val) {
                    $data['rent_duration_days_option'] .= '<option value="'.$val.'">'.$val.'</option>';
                }
            }
        }

        echo json_encode($data);
        die;
    }

    public function get_unit_price() {
        $data['unit_price'] = '';
        $post = $_POST;
        $number_of_days_id = '1';
        if($post['nos_of_days'] > 5 && $post['nos_of_days'] <= 10) {
            $number_of_days_id = '2';
        } elseif($post['nos_of_days'] > 10) {
            $number_of_days_id = '3';
        }

        $query = "SELECT unit_price
                        FROM tbl_bike_price
                        JOIN tbl_bike_rent_days ON tbl_bike_price.number_of_days_id = tbl_bike_rent_days.id
                        WHERE bike_id = " . $post['bike_id'] . "
                        AND '" . $post['sel_date'] . "' BETWEEN valid_from AND valid_to
                        AND number_of_days_id = " . $number_of_days_id;
        $row = $this->db->query($query)->row();

        if(isset($row) && !empty($row)) {
            $data['unit_price'] = $row->unit_price;
        }

        echo json_encode($data);
        die;
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'bike_category') {
            $post['search_field'] = $_POST['export_filter_bike_category'];
        } else if($post['search_field_type'] == 'bike') {
            $post['search_field'] = $_POST['export_filter_bike'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'rent_status') {
            $post['search_field'] = $_POST['export_filter_rent_status'];
        } /*else if($post['search_field_type'] == 'rent_days') {
            $post['search_field'] = $_POST['export_filter_rent_days'];
        }*/ else if($post['search_field_type'] == 'rent_date') {
            $post['search_field'] = $_POST['export_filter_rent_date'];
        }

        $result = $this->bike_book->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Bike-Booking- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Bike Category');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Bike Name');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Bike Code');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Customer Contact Address');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Customer Nationality');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Customer Passport Number');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Customer DOB');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Customer Local Address');
            $excel->getActiveSheet()->setCellValue('M' . $k, 'Customer Room Number');
            $excel->getActiveSheet()->setCellValue('N' . $k, 'Bike Rent Date');
            $excel->getActiveSheet()->setCellValue('O' . $k, 'Number of Pax');
            $excel->getActiveSheet()->setCellValue('P' . $k, 'Number of Days');
            $excel->getActiveSheet()->setCellValue('Q' . $k, 'Price Per Person($)');
            $excel->getActiveSheet()->setCellValue('R' . $k, 'Total Price($)');
            $excel->getActiveSheet()->setCellValue('S' . $k, 'Booking Status');
            $excel->getActiveSheet()->setCellValue('T' . $k, 'Booking Date');
            $excel->getActiveSheet()->setCellValue('U' . $k, 'Booked By');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->bike_category_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->bike_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->customer_email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->customer_contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->customer_contact_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->customer_nationality, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->customer_passport_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, $v->customer_dob, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, $v->customer_local_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('M' . $count, $v->customer_room_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('N' . $count, $v->rent_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('O' . $count, $v->number_of_pax, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('P' . $count, $v->number_of_days_id, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('Q' . $count, $v->price_per_person, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('R' . $count, $v->total_price, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('S' . $count, $v->booking_status, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('T' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('U' . $count, ($v->created_by != '0') ? get_userdata('name') : $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/bike_book');
        }
    }

}