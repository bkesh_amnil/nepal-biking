<?php

class Product_order extends My_Controller
{
    var $table = 'tbl_product_order';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('booking_status_model', 'book_status');
        $this->load->model('product_category_model', 'product_category');
        $this->load->model('product_model', 'product');
        $this->load->model('product_order_model', 'product_order');
        $this->load->model('product_order_status_model', 'product_order_status');
        $this->data['module_name'] = 'Product Order Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index($id = '')
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Product Order';
            $this->data['rows'] = $this->product_order->getProductOrder();
            $this->data['product_order_categories'] = $this->product_category->getProductOrderCategory();
            $this->data['product_orders'] = $this->product->getProductOrderNames();
            $this->data['customers'] = $this->product_order_status->getProductOrderCustomers();
            $this->data['booking_status'] = $this->book_status->getBookingStatus('product_order');
            $this->data['body'] = BACKENDFOLDER.'/product_order/_list';
            $this->data['body'] = BACKENDFOLDER.'/product_order/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/product_order.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;
        $this->data['product_order_status'] = $this->_format_data($id);
        $this->data['product_categories'] = $this->product_category->get_product_categories_list(0);

        if($id != '') {
            $this->data['logs'] = $this->product_order_status->getSavedData($id, 'logs');
            $product_category_id = $this->product_order->get_single_data('tbl_product_order', 'product_category_id', $id, 'id');
            $this->data['products'] = $this->product->getProductDetailfromCategory($product_category_id, 'product_category_id');
        }

        if($_POST) {
            $post = $_POST;

            $this->product_order->id = $id;
            $this->form_validation->set_rules($this->product_order->rules($id));
            if($this->form_validation->run()) {
                $this->form_validation->set_rules($this->product_order_status->rules($id));
                if($this->form_validation->run()) {
                    $insert_order['product_category_id'] = $post['product_category_id'];
                    $insert_order['product_id'] = $post['product_id'];
                    $insert_order['product_series'] = $post['product_series'];
                    $insert_order['product_model'] = $post['product_model'];
                    $insert_order['last_booking_status_id'] = $post['product_order_status_id'];

                    $insert_order_status['product_quantity'] = $post['product_quantity'];
                    $insert_order_status['product_price'] = $post['product_price'];
                    $insert_order_status['total_price'] = $post['total_price'];
                    $insert_order_status['customer_full_name'] = $post['customer_full_name'];
                    $insert_order_status['customer_email_address'] = $post['customer_email_address'];
                    $insert_order_status['customer_contact_number'] = $post['customer_contact_number'];
                    $insert_order_status['customer_special_instruction'] = $post['customer_special_instruction'];
                    $insert_order_status['product_order_status_id'] = $post['product_order_status_id'];
                    $insert_order_status['product_note'] = $post['product_note'];

                    $this->db->trans_begin();

                    if ($id == '') {
                        $res = $this->product_order->save($insert_order, '', true);
                        $product_order_id = $res;

                        $insert_order_status['product_order_id'] = $product_order_id;

                        $this->product_order_status->save($insert_order_status);
                    } else {
                        $condition = array('id' => $id);
                        $this->product_order->save($insert_order, $condition);

                        $insert_order_status['product_order_id'] = $id;

                        $this->product_order_status->save($insert_order_status);
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        /* sending mail code */
                        if(!empty($post['product_note'])) {
                            $booking_status = $this->db->select('name')->where('id', $post['product_order_status_id'])->get('tbl_booking_status')->row();
                            $footer_contact = $this->public_model->getContent('contact-address');
                            $email_admin = '';
                            $subject = 'Product Order (' . $booking_status->name . ')';

                            $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                            $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                            $email_admin .= '<tbody>';
                            $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                            $email_admin .= $subject;
                            $email_admin .= '</td></tr>';
                            $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                            $email_admin .= $post['product_note'];
                            $email_admin .= '</td></tr>';
                            $email_admin .= '</td></tr>';
                            $email_admin .= 'Regards<br/>';
                            $email_admin .= 'Nepal Biking<br/>';
                            $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                            $email_admin .= '</td>';
                            $email_admin .= '<td>';
                            $email_admin .= $footer_contact[0]->long_description;
                            $email_admin .= '</td></tr>';
                            //$message = $post['product_note'];
                            $message = $email_admin;

                            $res = $this->email_send->sendMail($subject, $post['customer_email_address'], $message);
                        }
                        /* sending mail code */
                        if(isset($res) && ($res == 'success')) {
                            set_flash('msg', 'Data saved and mail sent to user.');
                        } else {
                            set_flash('msg', 'Data saved');
                        }
                    }
                    redirect(BACKENDFOLDER . '/product_order');
                } else {
                    $this->form($id, 'product_order');
                }
            } else {
                $this->form($id, 'product_order');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/product_order.js');
            $this->form($id, 'product_order');
        }
    }

    public function _format_data($id) {
        if($id != '') {
            $product_order_status = $this->product_order_status->getSavedData($id, 'edit');

            $data['product_quantity'] = $product_order_status->product_quantity;
            $data['product_price'] = $product_order_status->product_price;
            $data['total_price'] = $product_order_status->total_price;
            $data['customer_full_name'] = $product_order_status->customer_full_name;
            $data['customer_email_address'] = $product_order_status->customer_email_address;
            $data['customer_contact_number'] = $product_order_status->customer_contact_number;
            $data['customer_special_instruction'] = $product_order_status->customer_special_instruction;
            $data['product_order_status_id'] = $product_order_status->product_order_status_id;
            $data['product_note'] = $product_order_status->product_note;

            $data['product_name'] = $product_order_status->product_name;
        } else {
            $data['product_quantity'] = '';
            $data['product_price'] = '';
            $data['total_price'] = '';
            $data['customer_full_name'] = '';
            $data['total_price'] = '';
            $data['customer_full_name'] = '';
            $data['customer_email_address'] = '';
            $data['customer_contact_number'] = '';
            $data['customer_special_instruction'] = '';
            $data['product_order_status_id'] = '';
            $data['product_note'] = '';
        }

        $data = (object)$data;

        return $data;
    }

    public function load_products()
    {
        $post = $_POST;
        $html['products'] = $html['model'] = $html['price'] = $html['product_name'] = '';

        if($post['type'] == 'product_category_id') {
            $result = $this->product->getProductDetailfromCategory($post['product_category_id'], $post['type']);
        } else {
            $row = $this->product->getProductDetailfromCategory($post['product_id'], $post['type']);
        }

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $html['products'] .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
            $html['model'] = $result[0]->product_model;
            $html['price'] = $result[0]->promotional_price;
            $html['product_name'] = $result[0]->name;
        }

        if(isset($row) && !empty($row)) {
            $html['model'] = $row->product_model;
            $html['price'] = $row->promotional_price;
            $html['product_name'] = $row->name;
        }

        echo json_encode($html);
        die;
    }

    public function delete()
    {
        $product_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->product_order->delete(array('id' => $selected_id, 'product_id' => $product_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->product_order->delete(array('id' => $id, 'product_id' => $product_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/product_order/' . $product_id);
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'product_category') {
            $post['search_field'] = $_POST['export_filter_product_category'];
        } else if($post['search_field_type'] == 'product') {
            $post['search_field'] = $_POST['export_filter_product'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'booking_status') {
            $post['search_field'] = $_POST['export_filter_booking_status'];
        } else if($post['search_field_type'] == 'booking_date') {
            $post['search_field'] = $_POST['export_filter_booking_status'];
        } else if($post['search_field_type'] == 'order_date') {
            $post['search_field'] = $_POST['export_filter_order_date'];
        }

        $result = $this->product_order->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Product-Order- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Product Category');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Product Name');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Product Code');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Product Order Date');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Quantity');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Price Per Unit($)');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Total Price($)');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Order Status');
            $excel->getActiveSheet()->setCellValue('M' . $k, 'Order Date');
            $excel->getActiveSheet()->setCellValue('N' . $k, 'Ordered By');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->product_category_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->product_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->customer_email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->customer_contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->order_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->product_quantity, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->product_price, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, $v->total_price, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, $v->booking_status, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('M' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('N' . $count, ($v->created_by != '0') ? get_userdata('name') : $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/product_order');
        }
    }

}