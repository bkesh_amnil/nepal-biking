<?php

class Tour extends My_Controller
{
    var $table = 'tbl_tour';
    var $tour_departure_detail = 'tbl_tour_departure_detail';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('tour_category_model', 'tour_category');
        $this->load->model('difficulty_model', 'difficulty');
        $this->load->model('departure_type_model', 'departure');
        $this->load->model('tour_model', 'tour');
        $this->data['module_name'] = 'Tour Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Tour List';
            $this->data['tours'] = $this->tour->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/tour/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);

        $this->data['tour_categories'] = $this->tour_category->getTourCategories();
        $this->data['tour_difficulty'] = $this->difficulty->getTourDifficulty();
        $this->data['tour_departure'] = $this->departure->getTourDepartures();
        if($_POST) {
            $post = $_POST;
            $this->tour->id = $id;

            $this->form_validation->set_rules($this->tour->rules($id));
            if($this->form_validation->run()) {
                $insert_tour['tour_category_id'] = $post['tour_category_id'];
                $insert_tour['code'] = $post['code'];
                $insert_tour['name'] = $post['name'];
                $insert_tour['slug'] = $post['slug'];
                $insert_tour['cover_image'] = $post['cover_image'];
                $insert_tour['file'] = $post['file'];
                $insert_tour['map_file'] = $post['map_file'];
                $insert_tour['duration'] = $post['duration'];
                $min_group_size = $insert_tour['minimum_group_size'] = $post['minimum_group_size'];
                $insert_tour['best_season'] = $post['best_season'];
                $insert_tour['altitude']= $post['altitude'];
                $insert_tour['difficulty_id'] = $post['difficulty_id'];
                $insert_tour['support'] = $post['support'];
                $insert_tour['is_recommended'] = $post['is_recommended'];
                $insert_tour['is_highlighted'] = $post['is_highlighted'];
                $insert_tour['short_description'] = $post['short_description'];
                $insert_tour['description'] = $post['description'];
                $insert_tour['itinerary'] = $post['itinerary'];
                $departure_type = $insert_tour['departure_type'] = $post['departure_type'];
                if($id == '') {
                    $departure_years = $post['departure_year'];
                    $insert_tour['departure_year'] = implode(',', $post['departure_year']);
                }/* else {
                    $departure_years = $post['saved_departure_year'];
                    $insert_tour['departure_year'] = $departure_years;
                }*/
                $insert_tour['meta_keywords'] = $post['meta_keywords'];
                $insert_tour['meta_description'] = $post['meta_description'];
                $insert_tour['status'] = $post['status'];

                $current_month = date('m');
                $current_year = date('Y');

                $this->db->trans_begin();

                if($id == '') {
                    $insert_tour['created_by'] = get_userdata('user_id');
                    $insert_tour['created_date'] = time();
                    $this->db->insert($this->table, $insert_tour);
                    $tour_id = $this->db->insert_id();
                    if( $departure_type != 'anyday') {
                        if( $departure_type != 'everyday') {
                            foreach($departure_years as $departure_year) {
                                $start_month = 01;
                                if($departure_year == $current_year) {
                                    $start_month = $current_month;
                                }
                                for($month = $start_month; $month <= 12; $month ++) {
                                    $days[] = $this->getAllSpecificDaysInAMonth($departure_year, $month,  $departure_type);
                                }
                            }
                            if(isset($days) && !empty($days)) {
                                foreach($days as $day) {
                                    foreach($day as $val) {
                                        $insert_batch[] = array(
                                            'tour_id' => $tour_id,
                                            'tour_departure_date' => $val->format('Y-m-d'),
                                            'tour_all_allocation' => $min_group_size,
                                            'tour_available' => $min_group_size,
                                            'tour_booked' => 0,
                                            'is_fixed_departure' => 1,
                                            'status' => 1,
                                            'created_by' => get_userdata('user_id'),
                                            'created_date' => time(),
                                        );
                                    }
                                }

                                if(isset($insert_batch) && !empty($insert_batch)) {
                                    $this->db->insert_batch($this->tour_departure_detail, $insert_batch);
                                }
                            }
                        } else {
                            foreach($departure_years as $departure_year) {
                                $start_month = 01;
                                if($departure_year == $current_year) {
                                    $start_month = $current_month;
                                }
                                for($month = $start_month; $month <= 12; $month ++) {
                                    $days[] = $this->saveAllDaysInAMonth($departure_year, $month);
                                }
                            }
                            if(isset($days) && !empty($days)) {
                                foreach($days as $day) {
                                    foreach($day as $val) {
                                        $insert_batch[] = array(
                                            'tour_id' => $tour_id,
                                            'tour_departure_date' => $val,
                                            'tour_all_allocation' => $min_group_size,
                                            'tour_available' => $min_group_size,
                                            'tour_booked' => 0,
                                            'is_fixed_departure' => 1,
                                            'status' => 1,
                                            'created_by' => get_userdata('user_id'),
                                            'created_date' => time(),
                                        );
                                    }
                                }
                                if(isset($insert_batch) && !empty($insert_batch)) {
                                    $this->db->insert_batch($this->tour_departure_detail, $insert_batch);
                                }
                            }
                        }
                    }
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        set_flash('msg', 'Data saved');
                    }
                } else {
                    $saved_departure_year = $post['saved_departure_year'];
                    $insert_tour['departure_year'] = $saved_departure_year;
                    if(isset($post['departure_year']) && !empty($post['departure_year'])) {
                        $departure_years = $post['departure_year'];
                        $insert_tour['departure_year'] = $saved_departure_year . ',' . implode(',', $post['departure_year']);
                    }
                    $insert_tour['updated_by']	 = get_userdata('user_id');
                    $insert_tour['updated_date'] = time();
                    $this->db->where('id', $id);
                    $this->db->update($this->table, $insert_tour);

                    if(isset($departure_years) && !empty($departure_years)) {
                        if( $departure_type != 'anyday') {
                            if( $departure_type != 'everyday') {
                                foreach($departure_years as $departure_year) {
                                    $start_month = 01;
                                    if($departure_year == $current_year) {
                                        $start_month = $current_month;
                                    }
                                    for($month = $start_month; $month <= 12; $month ++) {
                                        $days[] = $this->getAllSpecificDaysInAMonth($departure_year, $month, $departure_type);
                                    }
                                }
                                if(isset($days) && !empty($days)) {
                                    foreach($days as $day) {
                                        foreach($day as $val) {
                                            $insert_batch[] = array(
                                                'tour_id' => $id,
                                                'tour_departure_date' => $val->format('Y-m-d'),
                                                'tour_all_allocation' => $min_group_size,
                                                'tour_available' => $min_group_size,
                                                'tour_booked' => 0,
                                                'is_fixed_departure' => 1,
                                                'status' => 1,
                                                'created_by' => get_userdata('user_id'),
                                                'created_date' => time()
                                            );
                                        }
                                    }

                                    if(isset($insert_batch) && !empty($insert_batch)) {
                                        $this->db->insert_batch($this->tour_departure_detail, $insert_batch);
                                    }
                                }
                            } else {
                                foreach($departure_years as $departure_year) {
                                    $start_month = 01;
                                    if($departure_year == $current_year) {
                                        $start_month = $current_month;
                                    }
                                    for($month = $start_month; $month <= 12; $month ++) {
                                        $days[] = $this->saveAllDaysInAMonth($departure_year, $month);
                                    }
                                }
                                if(isset($days) && !empty($days)) {
                                    foreach($days as $day) {
                                        foreach($day as $val) {
                                            $insert_batch[] = array(
                                                'tour_id' => $id,
                                                'tour_departure_date' => $val,
                                                'tour_all_allocation' => $min_group_size,
                                                'tour_available' => $min_group_size,
                                                'tour_booked' => 0,
                                                'is_fixed_departure' => 1,
                                                'status' => 1,
                                                'created_by' => get_userdata('user_id'),
                                                'created_date' => time()
                                            );
                                        }
                                    }
                                    if(isset($insert_batch) && !empty($insert_batch)) {
                                        $this->db->insert_batch($this->tour_departure_detail, $insert_batch);
                                    }
                                }
                            }
                        }
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        set_flash('msg', 'Data saved');
                    }
                }

                //$res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/tour');
            } else {
                $this->form($id, 'tour');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/jquery.textarea-counter.js', 'assets/' . BACKENDFOLDER . '/dist/js/counter.js');
            $this->form($id, 'tour');
        }
    }

    public function getAllSpecificDaysInAMonth($year, $month, $day, $daysError = 3)
    {
        $dateString = 'first '.$day.' of '.$year.'-'.$month;
        if (!strtotime($dateString))
        {
            throw new \Exception('"'.$dateString.'" is not a valid strtotime');
        }
        $startDay = new \DateTime($dateString);
        $days = array();
        while ($startDay->format('Y-m') <= $year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT))
        {
            $days[] = clone($startDay);
            $startDay->modify('+ 7 days');
        }

        return $days;
    }

    public function saveAllDaysInAMonth($year, $month)
    {
        $days = array();
        $num_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for($d = 01; $d <= $num_of_days; $d++)
        {
            $time = mktime(12, 0, 0, $month, $d, $year);
            $monthName = date('F', mktime(0, 0, 0, $month, 10));
            if (date('m', $time) == $month) {
                $days[] = date('Y-m-d', $time);
            }
        }

        return $days;
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_tour_booking.tour_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->tour->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->tour->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            /*$success_msg ? set_flash('msg', $success_msg) : */set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/tour');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->tour->changeStatus('tour', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->tour->changeStatus('tour', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/tour');
    }

}