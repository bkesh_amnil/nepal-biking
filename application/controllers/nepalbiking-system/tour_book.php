<?php

class Tour_Book extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        require APPPATH.'/third_party/password.php';
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('booking_status_model', 'book_status');
        $this->load->model('tour_category_model', 'tour_category');
        $this->load->model('tour_model', 'tour');
        $this->load->model('tour_departure_model', 'tour_departure');
        $this->load->model('public_model', 'public');
        $this->load->model('tour_book_model', 'tour_book');
        $this->load->model('tour_book_status_model', 'tour_book_status');
        $this->data['module_name'] = 'Tour Book Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Tour Book List';
            $this->data['tour_bookings'] = $this->tour_book->getTourBookings();
            $this->data['tour_booking_categories'] = $this->tour_category->getTourBookingCategory();
            $this->data['tours'] = $this->tour->getTourBookingNames();
            $this->data['customers'] = $this->tour_book_status->getTourBookingCustomers();
            $this->data['booking_status'] = $this->book_status->getBookingStatus('tour_booking');
            $this->data['body'] = BACKENDFOLDER.'/tour_book/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/tour_book.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;
        $this->data['tour_book_status'] = $this->_format_data($id);
        $this->data['book_status'] = $this->book_status->getBookStatus();
        $this->data['tour_categories'] = $this->tour_category->getTourCategories();

        if($id != '') {
            $this->data['logs'] = $this->tour_book_status->getSavedData($id, 'logs');
            //$tour_category_id = $this->tour_book->get_single_data('tbl_tour_booking', 'tour_category_id', $id, 'id');
            //$this->data['tours'] = $this->tour->getTours($tour_category_id);
            $this->data['tours'] = $this->tour->getTours($this->data['logs'][0]->tour_category_id);
        } else {
            $this->data['tours'] = $this->tour->getTours($this->data['tour_categories'][0]->id);
        }

        if ($id != '') {
            $this->data['departure_dates'] = $this->public->getTourDepartureDates($this->data['logs'][0]->tour_id);
        } else {
            $this->data['departure_dates'] = $this->public->getTourDepartureDates($this->data['tours'][0]->id);
        }

        $this->data['id'] = $id;

        if($_POST) {
            $post = $_POST;
            $this->tour_book->id = $id;

            $this->form_validation->set_rules($this->tour_book->rules($id));
            if($this->form_validation->run()) {
                $this->form_validation->set_rules($this->tour_book_status->rules($id));
                if($this->form_validation->run()) {
                    $insert_book['form_type'] = 'book';
                    $insert_book['tour_category_id'] = $post['tour_category_id'];
                    $insert_book['tour_id'] = $post['tour_id'];
                    $insert_book['customer_remarks'] = '';
                    $insert_book['last_booking_status_id'] = $post['tour_booking_status_id'];

                    $insert_status['departure_date_id'] = $post['departure_date_id'];
                    $insert_status['number_of_pax'] = $post['number_of_pax'];
                    $insert_status['price_per_person'] = $post['price_per_person'];
                    $insert_status['total_price'] = $post['total_price'];
                    $insert_status['customer_full_name'] = $post['customer_full_name'];
                    $insert_status['customer_email_address'] = $post['customer_email_address'];
                    $insert_status['customer_contact_number'] = $post['customer_contact_number'];
                    $insert_status['customer_contact_address'] = $post['customer_contact_address'];
                    $insert_status['tour_booking_status_id'] = $post['tour_booking_status_id'];
                    $insert_status['tour_note'] = $post['tour_note'];

                    $this->db->trans_begin();

                    if ($id == '') {
                        $res = $this->tour_book->save($insert_book, '', true);
                        $tour_booking_id = $res;

                        $insert_status['tour_booking_id'] = $tour_booking_id;

                        $this->tour_book_status->save($insert_status);
                        /* change tour booking count according to booking status starts */
                        if($post['tour_booking_status_id'] == '2') {
                            $this->changeAllocationBookedAvailable($post['number_of_pax'], $post['tour_id'], /*$post['departure_date_id'],*/ $id, '', '', $post['departure_date_id'], '');
                        }
                        /* change tour booking count according to booking status ends */
                    } else {
                        $condition = array('id' => $id);
                        $this->tour_book->save($insert_book, $condition);

                        $insert_status['tour_booking_id'] = $id;

                        $this->tour_book_status->save($insert_status);

                        $this->changeAllocationBookedAvailable($post['number_of_pax'], $post['tour_id'], /*$post['departure_date_id'],*/ $id, $post['previous_departure_date_id'], $post['previous_book_status'], $post['departure_date_id'], $post['tour_booking_status_id']);
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        /* sending mail code */
                        if(!empty($post['tour_note'])) {
                            $booking_status = $this->db->select('name')->where('id', $post['tour_booking_status_id'])->get('tbl_booking_status')->row();
                            $footer_contact = $this->public_model->getContent('contact-address');
                            $email_admin = '';
                            $subject = 'Tour Booking (' . $booking_status->name . ')';

                            $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                            $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                            $email_admin .= '<tbody>';
                            $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                            $email_admin .= $subject;
                            $email_admin .= '</td></tr>';
                            $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                            $email_admin .= $post['tour_note'];
                            $email_admin .= '</td></tr>';
                            $email_admin .= 'Regards<br/>';
                            $email_admin .= 'Nepal Biking<br/>';
                            $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                            $email_admin .= '</td>';
                            $email_admin .= '<td>';
                            $email_admin .= $footer_contact[0]->long_description;
                            $email_admin .= '</td></tr>';

                            $message = $email_admin;
                            
                            $res = $this->email_send->sendMail($subject, $post['customer_email_address'], $message);
                        }
                        /* sending mail code */
                        if(isset($res) && ($res == 'success')) {
                            set_flash('msg', 'Data saved and mail sent to user.');
                        } else {
                            set_flash('msg', 'Data saved');
                        }
                    }

                    redirect(BACKENDFOLDER . '/tour_book');
                } else {
                    $this->form($id, 'tour_book');
                }
            } else {
                $this->form($id, 'tour_book');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/tour_book.js');
            $this->form($id, 'tour_book');
        }
    }

    public function changeAllocationBookedAvailable($nos_of_pax , $tour_id, /*$departure_date_id, */$id, $previous_departure_date_id, $previous_booking_status_id, $current_departure_date_id, $current_booking_status_id) {
        if($id == '') { /* create status only confirmed*/
            $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
            $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
        } else { /* edit */
            /* prev date = cur date, prev status = cur status ---> no change @ all*/
            if($previous_departure_date_id == $current_departure_date_id) {
                if($previous_booking_status_id != $current_booking_status_id) {
                    if($previous_booking_status_id == '1') { /* Pending */
                        if($current_booking_status_id == '2') { /* to Confirmed */
                            $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                            $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                        }
                        /* Pending to Cancelled ---> No change */
                    } else if($previous_booking_status_id == '2') { /* Confirmed */
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                    } else { /* Cancelled*/
                        if($current_booking_status_id == '2') { /* Confirmed */
                            $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                            $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                        }
                        /* Cancelled to Pending ---> No change */
                    }
                }
            } else {
                /* prev date !=  cur date */
                if($previous_booking_status_id != $current_booking_status_id) { /* prev status != curr status */
                    if(($previous_booking_status_id == '1' && $current_booking_status_id == '2') || ($previous_booking_status_id == '3' && $current_booking_status_id == '2')) { /* prev - pending; curr - confirmed OR prev - cancelled;curr - confirmed */
                        /* add booking to current date id */
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                    } elseif(($previous_booking_status_id == '2' && $current_booking_status_id == '1') || ($previous_booking_status_id == '2' && $current_booking_status_id == '3')) { /* prev - confirmed;curr - pending OR prev - confirmed; curr - cancelled*/
                        /* subtract booking from previous date id */
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $previous_departure_date_id);
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $previous_departure_date_id);
                    }
                } else { /* prev status == curr status */
                    /* Pending n Cancelled Status ---> no change */
                    if($previous_booking_status_id == '2') { /* Confirmed */
                        /* subtract booking from previous date id */
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $previous_departure_date_id);
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $previous_departure_date_id);
                        /* add booking to current date id */
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_booked = tour_booked + " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                        $this->db->query("UPDATE tbl_tour_departure_detail
                                        SET tour_available = tour_available - " . $nos_of_pax . "
                                        WHERE tour_id = " . $tour_id . "
                                        AND id = " . $current_departure_date_id);
                    }
                }
            }
        }
    }

    public function _format_data($id) {
        if($id != '') {
            $tour_booking_status = $this->tour_book_status->getSavedData($id, 'edit');

            $data['departure_date_id'] = $tour_booking_status->departure_date_id;
            $data['number_of_pax'] = $tour_booking_status->number_of_pax;
            $data['price_per_person'] = $tour_booking_status->price_per_person;
            $data['total_price'] = $tour_booking_status->total_price;
            $data['customer_full_name'] = $tour_booking_status->customer_full_name;
            $data['customer_email_address'] = $tour_booking_status->customer_email_address;
            $data['customer_contact_number'] = $tour_booking_status->customer_contact_number;
            $data['customer_contact_address'] = $tour_booking_status->customer_contact_address;
            $data['tour_booking_status_id'] = $tour_booking_status->tour_booking_status_id;
            $data['tour_note'] = $tour_booking_status->tour_note;

            $data['tour_name'] = $tour_booking_status->tour_name;
            $data['departure_date'] = $tour_booking_status->tour_departure_date;
            $data['allocation'] = $tour_booking_status->tour_all_allocation;
            $data['available'] = $tour_booking_status->tour_available;
            $data['booked'] = $tour_booking_status->tour_booked;
        } else {
            $data['departure_date_id'] = '';
            $data['number_of_pax'] = '';
            $data['price_per_person'] = '';
            $data['total_price'] = '';
            $data['customer_full_name'] = '';
            $data['customer_email_address'] = '';
            $data['customer_contact_number'] = '';
            $data['customer_contact_address'] = '';
            $data['tour_booking_status_id'] = '';
            $data['tour_note'] = '';
        }

        $data = (object)$data;

        return $data;
    }

    public function load_data() {
        $post = $_POST;
        $data['tour_html'] = '';
        $data['tour_dates'] = '';
        $data['tour_pax'] = '';
        $data['tour_unit_price'] = '';
        $data['tour_name'] = '';
        $data['departure_date'] = '';
        $data['tour_all_allocation'] = '';
        $data['tour_booked'] = '';
        $data['tour_available'] = '';

        if($post['type'] == 'tour_category') { /* change in tour_category */
            $tours = $this->tour->getTours($post['tour_category_id']);
        } else if($post['type'] == 'tour') { /* change in tour */
            $data['tour_name'] = $this->tour->get_single_data('tbl_tour', 'name', $post['tour_id'], 'id');
            $departure_dates = $this->public->getTourDepartureDates($post['tour_id']);
        } else if($post['type'] == 'departure_date') { /* change in departure date */
            $date_detail = $this->public_model->getDateDetail($post['date_id'], 'tour');
            if($post['nos_of_pax'] != 'Select') {
                $price_detail = $this->public_model->getPriceDetail($post['nos_of_pax'], $post['sel_date'], $post['tour_id'], 'tour');
            }
        } else { /* change in nos of pax */
            $price_detail = $this->public_model->getPriceDetail($post['nos_of_pax'], $post['sel_date'], $post['tour_id'], 'tour');
        }

        if(isset($tours) && !empty($tours)) { /* start from loading tours */
            $data['tour_name'] = $tours[0]->name;
            $departure_dates = $this->public->getTourDepartureDates($tours[0]->id);

            foreach($tours as $tour) {
                $data['tour_html'] .= '<option value="'.$tour->id.'">'.$tour->name.'</option>';
            }
        }

        if(isset($departure_dates) && !empty($departure_dates)) {
            $data['tour_all_allocation'] = $departure_dates[0]->tour_all_allocation;
            $data['tour_booked'] = $departure_dates[0]->tour_booked;
            $data['tour_available'] = $departure_dates[0]->tour_available;
            foreach($departure_dates as $val) {
                $data['tour_dates'] .= '<option value="'.$val->id.'">'.$val->tour_departure_date.'</option>';
            }
        } else if($post['type'] == 'departure_date') {
            $data['tour_all_allocation'] = $date_detail->tour_all_allocation;
            $data['tour_booked'] = $date_detail->tour_booked;
            $data['tour_available'] = $date_detail->tour_available;
        }

        if(isset($price_detail) && !empty($price_detail)) {
            $data['tour_unit_price'] = $price_detail->unit_price;
        }

        echo json_encode($data);
        die;
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'tour_category') {
            $post['search_field'] = $_POST['export_filter_tour_category'];
        } else if($post['search_field_type'] == 'tour') {
            $post['search_field'] = $_POST['export_filter_tour'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'booking_status') {
            $post['search_field'] = $_POST['export_filter_booking_status'];
        } else if($post['search_field_type'] == 'booking_date') {
            $post['search_field'] = $_POST['export_filter_booking_date'];
        } else if($post['search_field_type'] == 'departure_date') {
            $post['search_field'] = $_POST['export_filter_departure_date'];
        }

        $result = $this->tour_book->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Tour-Booking- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Tour Category');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Tour Name');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Tour Code');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Customer Contact Address');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Tour Departure Date');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Number of Pax');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Price Per Person($)');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Total Price($)');
            $excel->getActiveSheet()->setCellValue('M' . $k, 'Booking Status');
            $excel->getActiveSheet()->setCellValue('N' . $k, 'Booking Date');
            $excel->getActiveSheet()->setCellValue('O' . $k, 'Booked By');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->tour_category_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->tour_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->customer_email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->customer_contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->customer_contact_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->tour_departure_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->number_of_pax, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, $v->price_per_person, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, $v->total_price, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('M' . $count, $v->booking_status, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('N' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('O' . $count, ($v->created_by != '0') ? get_userdata('name') : $v->customer_full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/tour_book');
        }
    }

}