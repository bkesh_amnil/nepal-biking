<?php
    Class Home extends BASE_Controller{

        function __construct() {
            parent::__construct();
        }
        
        function index(){
            $data['active_menu'] = $data['module_name'] = 'home';

            $data['banners'] = $this->public_model->getBanner();
            $data['count_tours'] = $this->public_model->countRows('tour');
            $data['highlighted_tours'] = $this->public_model->getTours('highlight', 2);
            $data['latest_tours'] = $this->public_model->getTours('latest', 4);
            $data['latest_bike_tours'] = $this->public_model->getBikeHire();
            $data['latest_news'] = $this->public_model->getNews('latest', 2);
            $data['hot_news'] = $this->public_model->getNews('highlight', 1);
            $data['latest_events'] = $this->public_model->getEvents('latest', 2);
            $data['hot_events'] = $this->public_model->getEvents('highlight', 1);
            $data['testimonials'] = $this->public_model->getTestimonials();

            $this->template['content'] = $this->load->view('home', $data, TRUE);
        }

    }
?>