<?php 
class Image_show extends CI_Controller {
		
    function __Construct(){ 
        parent::__Construct();
    }
		
    function index($type = NULL, $file = NULL){
        if(!empty($file)){
            switch($type){
                case "logo":
                    $path = "./system-nepalbiking/uploaded_files/site_logo/";
                    break;
                case "banner":
                    $path = "./system-nepalbiking/uploaded_files/banner/";
                    break;
                case "tour":
                    $path = "./system-nepalbiking/uploaded_files/tour/";
                    break;
                case "bike":
                    $path = "./system-nepalbiking/uploaded_files/bike/";
                    break;
                case "news":
                    $path = "./system-nepalbiking/uploaded_files/news/";
                    break;
                case "event":
                    $path = "./system-nepalbiking/uploaded_files/events/";
                    break;
                case "content";
                    $path = "./system-nepalbiking/uploaded_files/content/";
                    break;
                case "popup_image":
                    $path = "./system-nepalbiking/uploaded_files/popup_image/";
                    break;
                case "gallery":
                    $path = "./system-nepalbiking/uploaded_files/gallery/";
                    break;
                case "temp":
                    $path = "./system-nepalbiking/uploaded_files/temp/";
                    break;
                case "default":
                    $path = "images/";
                    break;
                default:
                    //$path = "./system-nepalbiking/images/";
            }

            if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) {
                $img_path = file_get_contents($path.$file);
                echo $img_path;
            } else {

                if(empty($width)){
                    $width = 100;
                }
                $ratio = 1; 
                if(empty($height)){
                    if(file_exists($path.$file) && !is_dir($path.$file)){
                        list($origwidth, $origheight) = getimagesize($path.$file);
                        if($origheight && $origwidth){
                            $ratio = $origwidth/$origheight;
                        }
                    }
                    $height = intval($width / $ratio);
                }
                $imagePath = $path.$file;
                if(file_exists($imagePath)){
                    $this->output->set_content_type('JPEG');
                    require_once './system-nepalbiking/phpthumb/ThumbLib.inc.php';
                    $thumb = PhpThumbFactory::create($path.$file);
                    $thumb->adaptiveResize($origwidth,$origheight);
                    $thumb->show();
                }
            }
        }
    }
}