<?php 
    
    Class Booking extends BASE_Controller{
        
        function __construct() {
            parent::__construct();
        }

        function tour() {
            $post = $this->input->post();

            if(isset($post) && !empty($post)) {
                $data = '';
                $data['tour_number_of_pax'] = '';
                $data['tour_unit_price'] = '';
                $data['tour_all_allocation'] = '';
                $data['tour_available'] = '';
                $data['tour_booked'] = '';
                $data['tour_is_fixed_departure_id'] = '';
                $data['tour_is_fixed_departure'] = '';


                $date_detail = $this->public_model->getDateDetail(/*$post['date_id'],*/$post['sel_date'], $post['tour_id'], 'tour');
                if($post['nos_of_pax'] != 'Select') {
                    $price_detail = $this->public_model->getPriceDetail($post['nos_of_pax'], $post['sel_date'], $post['tour_id'], 'tour');
                }

                if(isset($date_detail) && !empty($date_detail)) {
                    $data['tour_all_allocation'] = $date_detail->tour_all_allocation;
                    $data['tour_available'] = $date_detail->tour_available;
                    $data['tour_booked'] = $date_detail->tour_booked;
                    $data['tour_is_fixed_departure_id'] = $date_detail->is_fixed_departure;
                    $data['tour_is_fixed_departure'] = ($date_detail->is_fixed_departure == '1') ? 'Book Now' : 'Enquire';
                }

                if(isset($price_detail) && !empty($price_detail)) {
                    $data['tour_unit_price'] = $price_detail->unit_price;
                }

                echo json_encode($data);
                die;
            }
        }

        function bike() {
            $post = $this->input->post();

            if(isset($post) && !empty($post)) {
                $data = '';
                $data['all_allocation'] = '';
                $data['available'] = '';
                $data['booked'] = '';
                $data['is_fixed_departure_id'] = '';
                $data['unit_price'] = '';
                $data['rent_duration_days_id'] = '';
                $data['rent_duration_days'] = '';
                $data['rent_duration_days_option'] = '';

                $date_detail = $this->public_model->getDateDetail(/*$post['date_id'],*/$post['sel_date'], $post['bike_id'], 'bike');
                $price_detail = $this->public_model->getPriceDetail($post['rent_days'], $post['sel_date'], $post['bike_id'], 'bike');

                if(isset($date_detail) && !empty($date_detail)) {
                    $data['all_allocation'] = $date_detail->all_allocation;
                    $data['available'] = $date_detail->available;
                    $data['booked'] = $date_detail->booked;
                    $data['is_fixed_departure_id'] = $date_detail->is_fixed_departure;
                }

                if(isset($price_detail) && !empty($price_detail)) {
                    $data['unit_price'] = $price_detail->unit_price;
                    $data['rent_duration_days_id'] = $price_detail->number_of_days_id;
                    $data['rent_duration_days'] = $price_detail->name;
                }

                if($data['rent_duration_days_id'] != '') {
                    $str = $data['rent_duration_days'];
                    $str = preg_replace('/\D/', '', $str);
                    $range = range(1, $str);
                    //$data['rent_duration_days_option'] .= '<option value="'.$data['rent_duration_days_id'].'">'.$data['rent_duration_days'].'</option>';
                    if(isset($range) && !empty($range)) {
                        foreach($range as $val) {
                            $data['rent_duration_days_option'] .= '<option value="'.$val.'">'.$val.'</option>';
                        }
                    }
                }

                echo json_encode($data);
                die;
            }
        }

    }
?>