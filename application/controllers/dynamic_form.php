<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dynamic_form extends CI_Controller {

    var $tour_booking = 'tbl_tour_booking';
    var $subscription = 'tbl_subscription';

    function __Construct() {
        parent::__Construct();
        $this->load->model('dynamic_form/public_form_model');
        $this->load->model('dynamic_form/form_model', 'form_fields');
        $this->load->model('email_send_model', 'email_send');
    }

    function form_submit_subscribe() {
        if ($this->input->is_ajax_request()) {
            $post = $this->input->post();

            $data['class'] = 'success';
            $data['msg'] = 'Subscription sent Successfully';

            $email = $post['subscribe_email'];

            $insert_data_subscription['subscription_email'] = $email;
            $insert_data_subscription['subscription_date'] = date('Y-m-d H:m:s');

            if ($this->db->insert($this->subscription, $insert_data_subscription)) {
                $footer_contact = $this->public_model->getContent('contact-address');
                $email_admin = '';
                $subject = 'New Subscription';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= 'New Subscription';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= 'Greetings,';
                $email_admin .= '<br/><br/>';
                $email_admin .= 'A new Subscription Request has been made by ' . $email . '.';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Nepal Biking<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $res = $this->email_send->sendMail($subject, '', $body);

                if ($res == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'Subscription sent Successfully';
                } else {
                    $data['class'] = 'error';
                    $data['msg'] = 'Error sending Mail';
                }
            } else {
                $data['class'] = 'error';
                $data['msg'] = 'Error saving data.';
            }

            echo json_encode($data);
        }
    }

    function form_submit() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            $form_id = $this->input->post('form_id');

            $form = $this->public_form_model->get_forms($form_id);
            $data['class'] = 'success';
            $fields = $this->input->post('field_name');

            $result = $this->form_fields->getFormFields($form_id);
            foreach ($result as $row) {
                if ($row->field_type != 'upload' && isset($fields[$row->id])) {
                    $_POST['field_' . $row->id] = $fields[$row->id];
                    $this->form_validation->set_rules('field_' . $row->id, $row->field_label, $row->validation_rule);
                }
            }
            if (isset($result) && !empty($result)) {
                foreach ($result as $field) {
                    $field_arr[$field->field_name] = $field->id;
                }
            }

            $site = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
            $submission['form_id'] = $form->id;

            if ($form->submit_action == 'email' || $form->submit_action == 'both') {
                if ($form->admin_email) {
                    $footer_contact = $this->public_model->getContent('contact-address');
                    if ($form_id == '1') {
                        $email_admin = $email_user = '';
                        $subject = $form->admin_email_subject;

                        $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                        $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                        $email_admin .= '<tbody>';
                        $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                        $email_admin .= $subject;
                        $email_admin .= '</td></tr>';
                        $email_admin .= '<tr><td colspan="2" style="font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                        $link_arr = array(
                            '{full_name}' => ucwords($fields[$field_arr['full_name']]),
                            '{email}' => $fields[$field_arr['email_address']],
                            '{mobile}' => $fields[$field_arr['mobile_number']],
                            '{subject}' => $fields[$field_arr['subject']],
                            '{message}' => $fields[$field_arr['message']]
                        );
                        $email_admin .= strtr($form->admin_email_msg, $link_arr);
                        $email_admin .= '</td></tr>';
                        $email_admin .= '<tr><td>';
                        $email_admin .= 'Regards<br/>';
                        $email_admin .= 'Nepal Biking<br/>';
                        $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                        $email_admin .= '</td>';
                        $email_admin .= '<td>';
                        $email_admin .= $footer_contact[0]->long_description;
                        $email_admin .= '</td></tr>';

                        $body = $email_admin;
                        $res = $this->email_send->sendMail($subject, '', $body);

                        if ($form->email_to_user) {
                            $subject1 = $form->user_email_subject;
                            $to_email = $fields[$field_arr['email_address']];

                            $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                            $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                            $email_user .= '<tbody>';
                            $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                            $email_user .= $subject1;
                            $email_user .= '</td></tr>';
                            $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                            $link_arr1 = array(
                                '{full_name}' => ucwords($fields[$field_arr['full_name']])
                            );
                            $email_user .= strtr($form->user_email_msg, $link_arr1);
                            $email_user .= '</td></tr>';
                            $email_user .= '<tr><td>';
                            $email_user .= 'Regards<br/>';
                            $email_user .= 'Nepal Biking<br/>';
                            $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                            $email_user .= '</td>';
                            $email_user .= '<td>';
                            $email_user .= $footer_contact[0]->long_description;
                            $email_user .= '</td></tr>';

                            $body1 = $email_user;
                            $res1 = $this->email_send->sendMail($subject1, $to_email, $body1);
                        }
                    }
                }
            }

            if (!($form->submit_action == 'email' )) {
                $this->db->insert('tbl_form_submissions', $submission);
                $submission_fields['form_submission_id'] = $this->db->insert_id();
                foreach ($fields as $index => $value) {
                    $submission_fields['form_field_id'] = $index;
                    $submission_fields['form_field_value'] = $value;
                    $this->db->insert('tbl_form_submission_fields', $submission_fields);
                }
                $this->load->library('upload_files');
                $this->upload_files->upload($submission_fields['form_submission_id']);
            }

            $action = array();
            if ($form->success_msg)
                $data['msg'] = $form->success_msg;
            else
                $data['msg'] = 'Successfully Submitted!!!';
            $data['class'] = 'success';
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
        exit;
    }

    function verify_captcha($response_code) {
        $verify_url = 'https://www.google.com/recaptcha/api/siteverify?secret=6Ldx0g8UAAAAAGh-N0dizsQfY2DM9HPrBjEy_beT&response=' . $response_code;
        $response = json_decode(file_get_contents($verify_url), true);
        return $response['success'] ? true : false;
    }

    function tour_booking_enquire() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            /* backend model */
            $this->load->model('tour_book_model', 'tour_book');
            $this->load->model('tour_book_status_model', 'tour_book_status');
            /* backend model */
            $post = $this->input->post();

            $tour_name = $post['hidden_tour_name'];
            $tour_code = $post['hidden_tour_code'];
            //$departure_date = $this->tour_book->get_single_data('tbl_tour_departure_detail', 'tour_departure_date', $post['departure_date'], 'id');
            $departure_date = $post['departure_date'];
            $departure_date_id = $this->db->select('id')->where(array('tour_departure_date' => $post['departure_date'], 'tour_id' => $post['hidden_tour_id']))->get('tbl_tour_departure_detail')->row();


            $type = 'enquiry';
            $fixed_departure = $post['hidden_fixed_departure'];
            if ($fixed_departure == '1') {
                $type = 'booking';
            }

            $insert_book['form_type'] = $type;
            $insert_book['tour_category_id'] = $post['hidden_tour_category_id'];
            $insert_book['tour_id'] = $post['hidden_tour_id'];
            $insert_book['customer_remarks'] = 'remarks';
            $insert_book['last_booking_status_id'] = 1;

            $this->db->trans_begin();

            $res = $this->tour_book->save($insert_book, '', true);
            $tour_booking_id = $res;

            $insert_status['tour_booking_id'] = $tour_booking_id;
            //$insert_status['departure_date_id'] = $post['departure_date'];
            $insert_status['departure_date_id'] = $departure_date_id->id;
            $insert_status['number_of_pax'] = $post['nos_of_pax'];
            $insert_status['price_per_person'] = $post['price_per_person'];
            $insert_status['total_price'] = $post['total_cost'];
            $insert_status['customer_full_name'] = ucwords($post['full_name']);
            $insert_status['customer_email_address'] = $post['email_address'];
            $insert_status['customer_contact_number'] = $post['contact_nos'];
            $insert_status['customer_contact_address'] = $post['contact_address'];
            $insert_status['tour_booking_status_id'] = 1;
            $insert_status['tour_note'] = '';

            $this->tour_book_status->save($insert_status);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['class'] = 'error';
                $data['msg'] = 'Error';
            } else {
                $this->db->trans_commit();
                /* sending mail code */
                $footer_contact = $this->public_model->getContent('contact-address');
                $email_admin = $email_user = '';
                $subject = 'Tour Booking';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= $subject;
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= '<p>Greetings,</p>';
                $email_admin .= '<p>A new Tour Booking has been made for ' . $tour_name . '.</p>';
                $email_admin .= '<p>Following are the details:</p>';
                $email_admin .= '<ul style="padding-left: 20px;">';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Tour Code :</strong>';
                $email_admin .= '<span>' . $tour_code . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
                $email_admin .= '<span>' . ucwords($post['full_name']) . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
                $email_admin .= '<span>' . $post['email_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Contact :</strong>';
                $email_admin .= '<span>' . $post['contact_nos'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Address :</strong>';
                $email_admin .= '<span>' . $post['contact_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '</ul>';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Nepal Biking<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Greetings ' . ucwords($post['full_name']) . '</p>';
                $email_user .= '<p>Your Tour Booking has been made.We will be contacting you soon regarding the booking.</p>';
                $email_user .= '<p>Following are the details of your booking:</p>';
                $email_user .= '<ul style="padding-left: 20px;">';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Tour Name:</strong>';
                $email_user .= '<span>' . $tour_name . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Tour Code:</strong>';
                $email_user .= '<span>' . $tour_code . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Departure Date :</strong>';
                $email_user .= '<span>' . $departure_date . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Nos of Pax :</strong>';
                $email_user .= '<span>' . $post['nos_of_pax'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Price per Pax :</strong>';
                $email_user .= '<span> $ ' . $post['price_per_person'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Total Price :</strong>';
                $email_user .= '<span> $ ' . $post['total_cost'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '</ul>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Nepal Biking<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body_user = $email_user;

                $res = $this->email_send->sendMail($subject, '', $body);
                $res1 = $this->email_send->sendMail($subject, $post['email_address'], $body_user);
                /* sending mail code */
                if ($res == 'success' && $res1 == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'Tour Booked Successfully.Mail has been sent to you.';
                } else {
                    $data['class'] = 'success';
                    $data['msg'] = 'Tour Booked Successfully';
                }
            }
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
    }

    function tour_enquire() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            /* backend model */
            $this->load->model('tour_enquire_model', 'tour_enquire');
            /* backend model */
            $post = $this->input->post();
            $tour_name = $post['hidden_tour_name'];
            $tour_code = $post['hidden_tour_code'];
            $booking_status = $this->db->select('name')->where('id', 1)->get('tbl_booking_status')->row();

            $insert_post['tour_category_id'] = $post['hidden_tour_category_id'];
            $insert_post['tour_id'] = $post['hidden_tour_id'];
            $insert_post['departure_date'] = $post['departure_date'];
            $insert_post['number_of_pax'] = $post['nos_of_pax'];
            $insert_post['customer_full_name'] = $post['customer_full_name'];
            $insert_post['customer_email_address'] = $post['customer_email_address'];
            $insert_post['customer_contact_number'] = $post['customer_contact_number'];
            $insert_post['customer_contact_address'] = $post['customer_contact_address'];
            $insert_post['customer_enquiry'] = $post['customer_enquiry'];
            $insert_post['enquire_note'] = '';

            $res = $this->tour_enquire->save($insert_post, '', true);

            if ($res) {
                /* sending mail code */
                $footer_contact = $this->public_model->getContent('contact-address');
                $email_admin = $email_user = '';
                $subject = 'Tour Enquire (' . $booking_status->name . ')';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= $subject;
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= '<p>Greetings,</p>';
                $email_admin .= '<p>A new Tour Enquire has been made for ' . $tour_name . '.</p>';
                $email_admin .= '<p>Following are the details:</p>';
                $email_admin .= '<ul style="padding-left: 20px;">';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Tour Code :</strong>';
                $email_admin .= '<span>' . $tour_code . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
                $email_admin .= '<span>' . ucwords($post['customer_full_name']) . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
                $email_admin .= '<span>' . $post['customer_email_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Contact :</strong>';
                $email_admin .= '<span>' . $post['customer_contact_number'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Address :</strong>';
                $email_admin .= '<span>' . $post['customer_contact_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '</ul>';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Nepal Biking<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Greetings ' . ucwords($post['customer_full_name']) . '</p>';
                $email_user .= '<p>Your Tour Enquire has been made.We will be contacting you soon regarding the enquiry.</p>';
                $email_user .= '<p>Following are the details of your enquiry:</p>';
                $email_user .= '<ul style="padding-left: 20px;">';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Tour Name :</strong>';
                $email_user .= '<span>' . $tour_name . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Tour Code :</strong>';
                $email_user .= '<span>' . $tour_code . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Departure Date :</strong>';
                $email_user .= '<span>' . $post['departure_date'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Nos of Pax :</strong>';
                $email_user .= '<span>' . $post['nos_of_pax'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Enquiry :</strong>';
                $email_user .= '<span>' . $post['customer_enquiry'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '</ul>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Nepal Biking<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body_user = $email_user;

                $res = $this->email_send->sendMail($subject, '', $body);
                $res1 = $this->email_send->sendMail($subject, $post['customer_email_address'], $body_user);
                /* sending mail code */
                if ($res == 'success' && $res1 == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'Tour Enquiry made Successfully.Mail has been sent to you.';
                } else {
                    $data['class'] = 'success';
                    $data['msg'] = 'Tour Enquiry made Successfully';
                }
            } else {
                $data['class'] = 'error';
                $data['msg'] = 'Error';
            }
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
    }

    function bike_rent() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            /* backend model */
            $this->load->model('bike_book_model', 'bike_book');
            $this->load->model('bike_book_status_model', 'bike_book_status');
            /* backend model */
            $post = $this->input->post();

            $bike_name = $post['hidden_bike_name'];
            $bike_code = $post['hidden_bike_code'];
            $rent_date_id = $this->bike_book->get_single_data('tbl_bike_rent_detail', 'id', $post['rent_date_id'], 'rent_date');
            $rent_date = $post['rent_date_id'];
            $booking_status = $this->db->select('name')->where('id', 1)->get('tbl_booking_status')->row();

            $insert_book['bike_category_id'] = $post['hidden_bike_category_id'];
            $insert_book['bike_id'] = $post['hidden_bike_id'];
            $insert_book['customer_remarks'] = $post['customer_remarks'];
            $insert_book['last_booking_status_id'] = 1;

            //$insert_status['rent_date_id'] = $post['rent_date_id'];
            $insert_status['rent_date_id'] = $rent_date_id;
            $insert_status['bike_available'] = $post['quantity'];
            $insert_status['number_of_pax'] = $post['number_of_pax'];
            $insert_status['number_of_days_id'] = $post['duration'];
            $insert_status['price_per_person'] = $post['price_per_person'];
            $insert_status['total_price'] = $post['total_cost'];
            $insert_status['customer_full_name'] = ucwords($post['customer_full_name']);
            $insert_status['customer_email_address'] = $post['customer_email_address'];
            $insert_status['customer_contact_number'] = $post['customer_contact_number'];
            $insert_status['customer_contact_address'] = $post['customer_contact_address'];
            $insert_status['customer_nationality'] = $post['customer_nationality'];
            $insert_status['customer_passport_number'] = $post['customer_passport_number'];
            $insert_status['customer_dob'] = $post['customer_dob'];
            $insert_status['customer_local_address'] = $post['customer_local_address'];
            $insert_status['customer_room_number'] = $post['customer_room_number'];
            $insert_status['bike_booking_status_id'] = 1;
            $insert_status['bike_note'] = '';
            $insert_status['created_by'] = '0';

            $this->db->trans_begin();

            $res = $this->bike_book->save($insert_book, '', true);
            $bike_rent_id = $res;

            $insert_status['bike_rent_id'] = $bike_rent_id;
            $this->bike_book_status->save($insert_status);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['class'] = 'error';
                $data['msg'] = 'Error';
            } else {
                $this->db->trans_commit();
                /* sending mail code */
                $footer_contact = $this->public_model->getContent('contact-address');
                $email_admin = $email_user = '';
                $subject = 'Bike Rent Booking (' . $booking_status->name . ')';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= $subject;
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= '<p>Greetings,</p>';
                $email_admin .= '<p>A new Bike Rent Booking has been made for ' . $bike_name . '</p>';
                $email_admin .= '<p>Following are the details:</p>';
                $email_admin .= '<ul style="padding-left: 20px;">';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Bike Code :</strong>';
                $email_admin .= '<span>' . $bike_code . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
                $email_admin .= '<span>' . ucwords($post['customer_full_name']) . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
                $email_admin .= '<span>' . $post['customer_email_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Contact :</strong>';
                $email_admin .= '<span>' . $post['customer_contact_number'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Address :</strong>';
                $email_admin .= '<span>' . $post['customer_contact_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '</ul>';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Nepal Biking<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Greetings ' . ucwords($post['customer_full_name']) . '</p>';
                $email_user .= '<p>Your Bike Rent Bookings has been made.We will be contacting you soon regarding the booking.</p>';
                $email_user .= '<p>Following are the details of your booking:</p>';
                $email_user .= '<ul style="padding-left: 20px;">';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Bike Name :</strong>';
                $email_user .= '<span>' . $bike_name . '</span>';
                $email_user .= '</li>';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Bike Code :</strong>';
                $email_user .= '<span>' . $bike_code . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Rent Date :</strong>';
                $email_user .= '<span>' . $rent_date . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Nos of Pax :</strong>';
                $email_user .= '<span>' . $post['number_of_pax'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Nos of Days :</strong>';
                $email_user .= '<span>' . $post['duration'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Price per Pax :</strong>';
                $email_user .= '<span> $ ' . $post['price_per_person'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Total Price :</strong>';
                $email_user .= '<span> $ ' . $post['total_cost'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '</ul>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Nepal Biking<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body_user = $email_user;

                $res = $this->email_send->sendMail($subject, '', $body);
                $res1 = $this->email_send->sendMail($subject, $post['customer_email_address'], $body_user);
                /* sending mail code */
                if ($res == 'success' && $res1 == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'Bike Rent Booked Successfully.Mail has been sent to you.';
                } else {
                    $data['class'] = 'success';
                    $data['msg'] = 'Bike Rent Booked Successfully';
                }
            }
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
        die;
    }

    function testimonial() {
        $post = $this->input->post();
        $post['slug'] = $post['customer_full_name'];

        /* if image uploaded store image */
        if (!empty($_FILES['customer_image']['name']) && $_FILES['customer_image']['error'] == 0) {
            $file_path = 'uploads/images/testimonial/';
            $config['upload_path'] = './uploads/images/testimonial/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '1024 * 8';
            $config['max_width'] = '215';
            $config['max_height'] = '200';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('customer_image')) {
                $data['result'] = 'error';
                $data['message'] = strip_tags($this->upload->display_errors());
            } else {
                $data_image = $this->upload->data();
            }
        }
        /* if image uploaded store image */
        /* if image upload success or no image is uploaded store the data */
        $this->load->model('testimonial_model', 'testimonial');
        $post['customer_image'] = (isset($data_image) && !empty($data_image)) ? $file_path . $data_image['file_name'] : '';
        $post['status'] = '0';

        $res = $this->testimonial->save($post);

        if ($res) {
            /* sending mail code */
            $footer_contact = $this->public_model->getContent('contact-address');
            $email_admin = $email_user = '';
            $subject = 'New Review';

            $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
            $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
            $email_admin .= '<tbody>';
            $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
            $email_admin .= $subject;
            $email_admin .= '</td></tr>';
            $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
            $email_admin .= '<p>Greetings,</p>';
            $email_admin .= '<p>A new Review has been made.</p>';
            $email_admin .= '<p>Following are the details:</p>';
            $email_admin .= '<ul style="padding-left: 20px;">';
            $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
            $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
            $email_admin .= '<span>' . ucwords($post['customer_full_name']) . '</span>';
            $email_admin .= '</li>';
            $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
            $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
            $email_admin .= '<span>' . $post['customer_email_address'] . '</span>';
            $email_admin .= '</li>';
            $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
            $email_admin .= '<strong style="width: 140px; display: inline-block;">Review :</strong>';
            $email_admin .= '<span>' . $post['customer_testimonial'] . '</span>';
            $email_admin .= '</li>';
            $email_admin .= '</ul>';
            $email_admin .= '</td></tr>';
            $email_admin .= '<tr><td>';
            $email_admin .= 'Regards<br/>';
            $email_admin .= 'Nepal Biking<br/>';
            $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
            $email_admin .= '</td>';
            $email_admin .= '<td>';
            $email_admin .= $footer_contact[0]->long_description;
            $email_admin .= '</td></tr>';

            $body = $email_admin;

            $res = $this->email_send->sendMail($subject, '', $body);
            /* sending mail code */
            $data['result'] = 'success';
            $data['message'] = 'Review saved Successully';
        } else {
            $data['result'] = 'error';
            $data['message'] = 'Error in saving data';
        }
        /* if image upload success or no image is uploaded store the data */
        echo json_encode($data);
        die;
    }

    function product_order() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            /* backend model */
            $this->load->model('product_order_model', 'product_order');
            $this->load->model('product_order_status_model', 'product_order_status');
            /* backend model */
            $post = $this->input->post();
            $product_name = $post['product_name'];
            $product_code = $post['product_code'];
            $booking_status = $this->db->select('name')->where('id', 1)->get('tbl_booking_status')->row();

            $insert_order['product_category_id'] = $post['product_category_id'];
            $insert_order['product_id'] = $post['product_id'];
            $insert_order['product_series'] = '';
            $insert_order['product_model'] = $post['product_model'];
            $insert_order['last_booking_status_id'] = 1;
            $insert_order['created_by'] = 0;

            $insert_order_status['product_quantity'] = $post['product_quantity'];
            $insert_order_status['product_price'] = $post['product_price'];
            $insert_order_status['total_price'] = $post['total_price'];
            $insert_order_status['customer_full_name'] = ucwords($post['customer_full_name']);
            $insert_order_status['customer_email_address'] = $post['customer_email_address'];
            $insert_order_status['customer_contact_number'] = $post['customer_contact_number'];
            $insert_order_status['customer_special_instruction'] = $post['customer_special_instruction'];
            $insert_order_status['product_order_status_id'] = 1;
            $insert_order_status['product_note'] = '';

            $this->db->trans_begin();

            $res = $this->product_order->save($insert_order, '', true);
            $product_order_id = $res;

            $insert_order_status['product_order_id'] = $product_order_id;
            $this->product_order_status->save($insert_order_status);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['class'] = 'error';
                $data['msg'] = 'Error in Placing Order';
            } else {
                $this->db->trans_commit();
                /* sending mail code */
                $footer_contact = $this->public_model->getContent('contact-address');
                $email_admin = $email_user = '';
                $subject = 'Product Order (' . $booking_status->name . ')';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= $subject;
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= '<p>Greetings,</p>';
                $email_admin .= '<p>A new Order has been placed for ' . $product_name . '</p>';
                $email_admin .= '<p>Following are the details:</p>';
                $email_admin .= '<ul style="padding-left: 20px;">';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Product Code :</strong>';
                $email_admin .= '<span>' . $product_code . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
                $email_admin .= '<span>' . ucwords($post['customer_full_name']) . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
                $email_admin .= '<span>' . $post['customer_email_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Contact :</strong>';
                $email_admin .= '<span>' . $post['customer_contact_number'] . '</span>';
                $email_admin .= '</li>';
                if (!empty($post['customer_special_instruction'])) {
                    $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                    $email_admin .= '<strong style="width: 140px; display: inline-block;">Special Instruction :</strong>';
                    $email_admin .= '<span>' . $post['customer_special_instruction'] . '</span>';
                    $email_admin .= '</li>';
                }
                $email_admin .= '</ul>';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Nepal Biking<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Greetings ' . ucwords($post['customer_full_name']) . '</p>';
                $email_user .= '<p>Your order has been placed.We will be contacting you soon regarding the order.</p>';
                $email_user .= '<p>Following are the details of your order:</p>';
                $email_user .= '<ul style="padding-left: 20px;">';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Product Name :</strong>';
                $email_user .= '<span>' . $product_name . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Product Code :</strong>';
                $email_user .= '<span>' . $product_code . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Quantity :</strong>';
                $email_user .= '<span>' . $post['product_quantity'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Unit Price :</strong>';
                $email_user .= '<span> $ ' . $post['product_price'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Total Price :</strong>';
                $email_user .= '<span> $ ' . $post['total_price'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '</ul>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Nepal Biking<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body_user = $email_user;

                $res = $this->email_send->sendMail($subject, '', $body);
                $res1 = $this->email_send->sendMail($subject, $post['customer_email_address'], $body_user);
                /* sending mail code */
                if ($res == 'success' && $res1 == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'Order placed Successfully.Mail has been sent to you.';
                } else {
                    $data['class'] = 'success';
                    $data['msg'] = 'Order placed Successfully';
                }
            }
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
        die;
    }

    function mail_pdf() {
        if ($this->input->post()) {

            $result = $this->public_model->insert_data();
            if ($result == 'success') {

                $footer_contact = $this->public_model->getContent('contact-address');
                $attached_file = $this->input->post('filename');

                $email_user = '';

                $subject = ucfirst($this->input->post('filetype')) . " pdf file";
                $to_email = $this->input->post('email');

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Dear ' . $this->input->post('name') . ',</p>';
                $email_user .= '<p>Please find attached ' . $this->input->post('filetype') . ' details you have requested for. Let us know how we can help you.</p>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Nepal Biking<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body = $email_user;
                $res = $this->email_send->sendMail($subject, $to_email, $body, $attached_file);
                if ($res == 'success') {
                    $data['response'] = 'success';
                }

                echo json_encode($data);
            }
        }
    }

}

?>