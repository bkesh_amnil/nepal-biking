<?php

Class Content extends BASE_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $url = $this->uri->segment_array();
        $count = count($url);
        $alias = $this->uri->segment($count);

        $content = 1;
        $contact = $form = 0;
        $item_per_page = 8;

        $data['active_menu'] = $this->public_model->getActiveMenu($this->uri->segment($count));

        if ($count > 2) {
            $data['active_menu'] = $this->uri->segment($count - 2);
        } else if ($count > 1) {
            $data['active_menu'] = $this->uri->segment($count - 1);
        }

        if (empty($data['active_menu'])) {
            show_404();
        }

        $data['count'] = $count;
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url);

        if ($count > 1) {

            $type = $this->uri->segment($count - 1);

            if ($count > 2) {
                show(404);
                //#booking page now loading via modal 
            } else {
                if ($type != 'about-us' && $type != 'about-dawn-till-dusk' && $type != 'faq' && $type != 'team') {
                    $moduleid = $this->public_model->getModuleId($this->uri->segment($count - 1));
                    $data_id = $this->public_model->getDataId($alias, $this->uri->segment($count - 1));
                    if ($type != 'testimonial' && !empty($moduleid) && !empty($data_id)) {
                        $data['social_datas'] = $this->template['social_datas'] = $this->public_model->getSocialData($moduleid->id, $data_id->id);
                    }
                    switch ($type) {
                        case "tour":
                            $data['module_name'] = 'tour_detail';
                            $data['tour_slug'] = $alias;
                            $data['content'] = $this->public_model->getTourDesc($alias);
                            break;
                        case "bike-rent":
                            $data['module_name'] = 'bike_detail';
                            $data['content'] = $this->public_model->getBikeDesc($alias);
                            $data['countries'] = $this->public_model->getCountries();
                            break;
                        case "gallery":
                            $data['module_name'] = 'gallery';
                            $data['content'] = $this->public_model->getGalleryImages($alias);
                            break;
                        case "bike-accessories":
                            $data['module_name'] = 'product_detail';
                            $data['content'] = $this->public_model->getProductDesc($alias);
                            break;
                        case "testimonial":
                            $data['module_name'] = 'testimonial';
                            $data['content'] = $this->public_model->getTestimonials($alias);
                            break;
                        default:
                            $data['breadcrumb'] = '';
                            $data['module_name'] = 'detail';
                            $data['content'] = $this->public_model->getNewsEventsDesc($alias, $type);
                            $data['background_image'] = $this->public_model->getNewsEventsBackgroundImage($type);
                            break;
                    }
                } else {
                    $menuid = $this->public_model->getMenuId($type);
                    $data['modules'] = $modules = $this->public_model->getModules($menuid->id);

                    $data['content'] = '';
                    $data['module_name'] = $modules[0]['module'];

                    $data['content'] = $modules[0]['data'];
                }
            }
        } else {
            $menuid = $this->public_model->getMenuId($alias);
            $data['modules'] = $modules = $this->public_model->getModules($menuid->id);

            if (!empty($modules) && !empty($modules[0]['data'])) {
                $data['social_datas'] = $this->template['social_datas'] = $this->public_model->getSocialData($modules[0]['module_id'], $modules[0]['data'][0]->id);
            }

            if (isset($modules) && !empty($modules)) {
                if ($modules[0]['module_title'] == 'contact') {
                    $contact = 1;
                    $data['content'] = $modules[0]['data'];
                    $data['formdata'] = $modules[1]['data'][0];
                    $data['module_name'] = $modules[0]['module_title'];
                } else {
                    $data['content'] = '';
                    $data['module_name'] = $modules[0]['module'];

                    $data['content'] = $modules[0]['data'];
                    $count_data = count($data['content']);
                    $total_pages = ceil($count_data / $item_per_page);
                    //$data['total_pages'] = $total_pages;
                    $data['total_pages'] = $count_data;
                    $data['item_per_page'] = $item_per_page;

                    switch ($modules[0]['module']) {
                        case 'tour':
                            $data['count_rows'] = $this->public_model->countRows('tour');
                            $data['tour_categories'] = $this->public_model->getTourCategories();
                            $data['tour_difficulty'] = $this->public_model->getTourDifficulty();
                            break;
                        case 'news':
                        case 'event':
                            $data['count_rows'] = $this->public_model->countRows($modules[0]['module']);
                            $data['published_years'] = $this->public_model->getNewsorEventsPublishedYear($modules[0]['module']);
                            break;
                        case 'product':
                            $data['count_rows'] = $this->public_model->countRows('product');
                            $data['product_categories'] = $this->public_model->getProductCategories(0);
                            $data['child_product_categories'] = $this->public_model->getProductCategories(-1);
                            break;
                        case 'bike':
                            $data['count_rows'] = $this->public_model->countRows('bike');
                            $data['bike_categories'] = $this->public_model->getBikeCategories();
                            $data['charge_per_days'] = $this->public_model->getBikeChargeperDay();
                            break;
                        case 'race':
                            $data['race_media'] = $this->public_model->getRaceMedia($modules[0]['data'][0]->id);
                            break;
                        default:
                            break;
                    }
                }
            } else {
                $data['active_menu'] = 'departure';
                $content = 0;
                $data['alias'] = $alias;
                $data['module_name'] = 'departure';
                $data['departure_years'] = $this->public_model->getTourDepartureYear();
                $data['departure_months'] = $this->public_model->getTourDepartureMonth();
                $data['departure_nos_of_days'] = $this->public_model->getTourDepartureNosofDays();
            }
        }

        //# data for booking modal
        if (isset($type) && $type == 'tour') {
            if (isset($_POST) && !empty($_POST)) {
                $data['selected_date'] = $this->input->post('date');
                $row = $this->db->select('tour_all_allocation,tour_available, tour_booked')->where('id', $this->input->post('tour_departure_detail_id'))->get('tbl_tour_departure_detail')->row();
                $data['selected_all_allocation'] = $row->tour_all_allocation;
                $data['selected_available'] = $row->tour_available;
                $data['selected_booked'] = $row->tour_booked;
            }
            $data['bookContent'] = $dd = $this->public_model->getTourDesc($this->uri->segment($count));
//            debug($dd);
        }

        if ($content) {
            if ($contact) {
                $data['facebook'] = $this->template['facebook'];
                $data['youtube'] = $this->template['youtube'];
                $data['twitter'] = $this->template['twitter'];
                $data['instagram'] = $this->template['instagram'];
                $data['pinterest'] = $this->template['pinterest'];
                $this->template['content'] = $this->load->view('contact', $data, TRUE);
            } else {
                $this->template['content'] = $this->load->view('page', $data, TRUE);
            }
        } else {
            if ($form) {
                $this->template['content'] = $this->load->view('form', $data, TRUE);
            } else {
                $this->template['content'] = $this->load->view('page', $data, TRUE);
            }
        }
    }

    /* form */
    /* function form() {
      $url = $this->uri->segment_array();
      $count = count($url);
      $form_data_id = $this->uri->segment($count);
      $form_id = $this->uri->segment($count - 1);

      $url1 = array ('careers');
      $data['active_menu'] = 'form';
      $data['breadcrumb'] = $this->public_model->getBreadCrumb($url1);

      $data['formdata'] = $this->public_model->get_form_details($form_id);
      $data['form_relation_id'] = $data['formdata']->form_related;
      $data['apply_job_desc'] = $this->db->select('form_description')->where('id', $data['form_relation_id'])->get('tbl_form')->row();
      $data['career_detail'] = $this->public_model->get_form_data_details($form_data_id);
      $data['career_id'] = $form_data_id;
      $data['career_title'] = $data['career_detail']['Job Title'];

      $this->template['content'] = $this->load->view('form', $data, TRUE);
      } */
    /* form */
    /* load more */

    function load_more() {
        $post = $this->input->post();
        $html['data'] = '';
        $html['ids'] = explode(',', $post['ids']);
        $position = ($post['page'] * 1);
        $result = $this->public_model->getMoreData($post['type'], $position, $post['ids']);

        if (isset($result) && !empty($result)) {
            foreach ($result as $val) {
                array_push($html['ids'], $val->id);
                $name = $val->name;
                if (strlen($val->name) > 27) {
                    $name = substr($val->name, 0, 27) . ' ...';
                }
                if ($post['type'] == 'event' || $post['type'] == 'news') {
                    $name = $val->name;
                }
                $class = '';
                if ($post['type'] == 'tour' || $post['type'] == 'nepalbiketours' || $post['type'] == 'event' || $post['type'] == 'news') {
                    $img = image_thumb($val->image, 270, 270, '', true);
                    $class = ' inner-trip-wrap';
                } else {
                    $img = base_url($val->image);
                }
                if ($post['type'] == 'nepalbiketours') {
                    $post['type'] = 'tour';
                }
                $html['data'] .= '<div class="col-xs-6 col-sm-3 col-lg-3">';
                $html['data'] .= '<a href="' . site_url($post['type'] . '/' . $val->alias) . '" class="trip-wrap' . $class . '">';
                if (!empty($val->image)) {
                    $html['data'] .= '<div class="trip-img-wrap">';
                    $html['data'] .= '<img class="img-responsive" src="' . $img . '" alt="' . $val->name . '" />';
                    $html['data'] .= '</div>';
                }
                $html['data'] .= '<h2 title="' . $val->name . '">' . $name . '</h2>';
                if ($post['type'] == 'tour' || $post['type'] == 'nepalbiketours') {
                    $frequency = $this->db->select('name')->where('code', $val->departure_type)->get('tbl_departure_type')->row();
                    $html['data'] .= '<span>Tour Code : <span>' . $val->code . '</span></span>';
                    $html['data'] .= '<span>Frequency : <span>' . $frequency->name . '</span></span>';
                    $html['data'] .= '<span>Duration : <span>' . $val->duration . '</span</span>';
                } else if ($post['type'] == 'bike-rent') {
                    $html['data'] .= '<span>BIKE CODE :<span>' . $val->code . '</span></span>';
                    $html['data'] .= '<span>CHARGE :<span>$' . $val->charge_per_day . '/day' . '</span></span>';
                } else if ($post['type'] == 'bike-accessories') {
                    $html['data'] .= '<span>PRODUCT CODE :<span>' . $val->code . '</span></span>';
                    $html['data'] .= '<span>PRODUCT MODEL :<span>' . $val->product_model . '</span></span>';
                }
                $html['data'] .= '</a></div>';
            }
        }

        echo json_encode($html);
        die;
    }

    /* load more */
    /* get terms & condtion text */

    function getConditionText() {
        $bike_id = $this->input->post('bike_id');
        $this->db->select('terms');
        $this->db->where('id', $bike_id);
        $row = $this->db->get('tbl_bike')->row();

        echo (isset($row) && !empty($row)) ? json_encode($row->terms) : '';
        die;
    }

}

?>