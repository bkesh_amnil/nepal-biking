<?php

Class Search extends BASE_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $q0 = false;

        $q = $_GET["query"];
        $str['query'] = $q;

        $result = $this->public_model->getSearchResult($q);

        if (!empty($result)) {
            $q0 = true;
            foreach ($result as $ind => $val) {
                foreach ($val as $values) {
                    $key = site_url($ind);
                    if (isset($values->slug)) {
                        $key = site_url($ind . '/' . $values->slug);
                    }
                    $value = ucwords($values->name) . '|' . ucfirst($ind);

                    $str['suggestions'][] = $value;
                    $str['data'][] = $key;
                }
            }
        }
        if ($q0 == false) {
            $str['suggestions'][] = "No Search Result Found";
            $str['data'][] = 0;
        }

        echo json_encode($str);
        die;
    }

    function tour() {
        $post = $this->input->post();
        if (isset($post) && !empty($post)) {
            $html = '';

            $result = $this->public_model->getToursbyFilter(NULL, $post['country_id'], $post['difficulty_id']);

            if (isset($result) && !empty($result)) {
                foreach ($result as $val) {
                    $frequency = $this->db->select('name')->where('code', $val->departure_type)->get('tbl_departure_type')->row();

                    $html .= '<div class="col-xs-6 col-sm-3 col-lg-3">';
                    $html .= '<a href="' . site_url('tour/' . $val->slug) . '" class="trip-wrap inner-trip-wrap">';
                    $html .= '<div class="trip-img-wrap">';
                    $html .= '<img class="img-responsive" src="' . image_thumb($val->cover_image, 270, 270, '', true) . '" alt="' . $val->name . '" />';
                    $html .= '</div>';
                    $html .= '<h2>' . $val->name . '</h2>';
                    $html .= '<span>Tour Code : <span>' . $val->code . '</span></span>';
                    $html .= '<span>Frequency : <span>' . $frequency->name . '</span></span>';
                    $html .= '<span>Duration : <span>' . $val->duration . '</span></span>';
                    $html .= '</a></div>';
                }
            } else {
                $html .= "No Product Found";
            }

            echo json_encode($html);
            die;
        } else {
            $q0 = false;
            $country_id = 0;
            $difficulty_id = 0;
            $q = $_GET["query"];
            $str['query'] = $q;
            if (isset($_GET["country_id"]) || isset($_GET["difficulty_id"])) {
                $country_id = $_GET["country_id"];
                $difficulty_id = $_GET["difficulty_id"];
            }

            $result = $this->public_model->getToursbyFilter($q, $country_id, $difficulty_id);
            if (!empty($result)) {
                $q0 = true;
                foreach ($result as $val) {
                    $key = site_url('tour/' . $val->slug);
                    $value = ucwords($val->name);
                    if ($country_id == 0) {
                        $value = ucwords($val->name) . '|' . $val->tour_category_name;
                    }

                    $str['suggestions'][] = $value;
                    $str['data'][] = $key;
                }
            }
            if ($q0 == false) {
                $str['suggestions'][] = "No Tour Found";
                $str['data'][] = 0;
            }

            echo json_encode($str);
            die;
        }
    }

    function news_events() {
        $post = $this->input->post();
        if (isset($post) && !empty($post)) {
            $html = '';

            $result = $this->public_model->getNewsEventsbyFilter(NULL, $post['type'], $post['year'], $post['order']);

            if (isset($result) && !empty($result)) {
                foreach ($result as $val) {
                    $html .= '<div class="col-xs-6 col-sm-3 col-lg-3">';
                    $html .= '<a href="' . site_url($post['type'] . '/' . $val->slug) . '" class="trip-wrap">';
                    $html .= '<div class="trip-img-wrap">';
                    $html .= '<img class="img-responsive" src="' . image_thumb($val->image, 270, 270, '', true) . '" alt="' . $val->name . '" />';
                    $html .= '</div>';
                    $html .= '<h2>' . $val->name . '</h2>';
                    $html .= '</a></div>';
                }
            } else {
                $html .= "No " . $post['type'] . "Found";
            }

            echo json_encode($html);
            die;
        } else {
            $q0 = false;
            $year = 0;
            $order = 'DESC';
            $q = $_GET["query"];
            $type = $_GET["type"];
            $str['query'] = $q;
            if (isset($_GET["year"]) || isset($_GET["order"])) {
                $year = $_GET["year"];
                $order = $_GET["order"];
            }

            $result = $this->public_model->getNewsEventsbyFilter($q, $type, $year, $order);
            if (!empty($result)) {
                $q0 = true;
                foreach ($result as $val) {
                    $key = site_url($type . '/' . $val->slug);
                    $value = ucwords($val->name);

                    $str['suggestions'][] = $value;
                    $str['data'][] = $key;
                }
            }
            if ($q0 == false) {
                $str['suggestions'][] = "No " . $type . " Found";
                $str['data'][] = 0;
            }

            echo json_encode($str);
            die;
        }
    }

    function tour_departure() {
        $post = $this->input->post();
        $html = '';

        $result = $this->public_model->getTourDeparturebyFilter($post['dept_year'], $post['dept_month'], $post['dept_tour_days']);

        if (isset($result) && !empty($result)) {
            $everday = 0;
            $cur_day = date('d');
            $cur_month = date('m');
            $i = 1;
            $html .= ' <table class="table table-bordered table-hover">';
            $html .= '<thead><tr>';
            $html .= '<th>S.NO.</th>';
            $html .= '<th>Tour Code</th>';
            $html .= '<th>Tour Name</th>';
            $html .= '<th></th>';
            $html .= '<th>Date</th>';
            $html .= '<th>Duration</th>';
            $html .= '</tr></thead>';
            $html .= '<tbody>';
            foreach ($result as $ind => $value) {
                foreach ($value as $val) {
                    $nameArr = explode(' ', $val->departure_name);
                    $dateArr[] = date('d', strtotime($val->tour_departure_date));
                    $dept_date_id[] = $val->id;
                }
                $html .= '<tr>';
                $html .= '<td>' . $i++ . '</td>';
                $html .= '<td>' . $val->code . '</td>';
                $html .= '<td>' . $ind . '</td>';
                $html .= '<td><img class="img-responsive" src="' . image_thumb($val->cover_image, 150, 100, '', true) . '" /></td>';
                $html .= '<td>';
                foreach ($nameArr as $value) {
                    if (count($nameArr) == '1') {
                        $everday = 1;
                    }
                    $html .= '<span> ' . $value . '</span>';
                }
                if ($everday == '0') {
                    foreach ($dateArr as $ind => $dateVal) {
                        $class = ' active-day';
                        if ($post['dept_month'] <= $cur_month && $dateVal < $cur_day) {
                            $class = ' inactive-day';
                        }
                        $html .= ' <a class="days-departure ' . $class . '" href="javascript:void(0);" rel="' . site_url('tour/' . $val->slug . '/book') . '" data-date="' . $dept_date_id[$ind] . '"> ' . $dateVal . '</a>';
                    }
                }
                $html .= '</td>';
                $html .= '<td>' . $val->duration . '</td>';
                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
        }

        echo json_encode($html);
        die;
    }

    function product() {
        $post = $this->input->post();
        if (isset($post) && !empty($post)) {
            $html = '';
            $result = $this->public_model->getProductsbyFilter(NULL, $post['parent_id'], $post['child_id']);
            if (isset($result) && !empty($result)) {
                foreach ($result as $val) {
                    $html .= '<div class="col-xs-6 col-sm-3 col-lg-3">';
                    $html .= '<a href="' . site_url('bike-accessories/' . $val->slug) . '" class="trip-wrap">';
                    $html .= '<div class="trip-img-wrap">';
                    $html .= '<img class="img-responsive" src="' . base_url($val->cover_image) . '" alt="' . $val->name . '" />';
                    $html .= '</div>';
                    $html .= '<h2>' . $val->name . '</h2>';
                    $html .= '</a></div>';
                }
            } else {
                $html .= "No Product Found";
            }

            echo json_encode($html);
            die;
        } else {
            $q0 = false;
            $parent_id = 0;
            $child_id = 0;
            $q = $_GET["query"];
            $str['query'] = $q;
            if (isset($_GET["parent_id"]) || isset($_GET["child_id"])) {
                $parent_id = $_GET["parent_id"];
                $child_id = $_GET["child_id"];
            }

            $result = $this->public_model->getProductsbyFilter($q, $parent_id, $child_id);
            if (!empty($result)) {
                $q0 = true;
                foreach ($result as $val) {
                    $key = site_url('bike-accessories/' . $val->slug);
                    $value = ucwords($val->name);
                    if ($parent_id == 0) {
                        $value = ucwords($val->name) . '|' . $val->product_category_name;
                    }

                    $str['suggestions'][] = $value;
                    $str['data'][] = $key;
                }
            }
            if ($q0 == false) {
                $str['suggestions'][] = "No Product Found";
                $str['data'][] = 0;
            }

            echo json_encode($str);
            die;
        }
    }

    function bike_rent() {
        $post = $this->input->post();
        if (isset($post) && !empty($post)) {
            $html = '';
            $result = $this->public_model->getBikesbyFilter(NULL, $post['category_id'], $post['charge']);
            if (isset($result) && !empty($result)) {
                foreach ($result as $val) {
                    $html .= '<div class="col-xs-6 col-sm-3 col-lg-3">';
                    $html .= '<a href="' . site_url('bike-rent/' . $val->slug) . '" class="bikes-wrap">';
                    $html .= '<div class="trip-img-wrap">';
                    $html .= '<img class="img-responsive" src="' . base_url($val->cover_image) . '" alt="' . $val->name . '" />';
                    $html .= '</div>';
                    $html .= '<h2>' . $val->name . '</h2>';
                    $html .= '</a></div>';
                }
            } else {
                $html .= "No Bike Found";
            }

            echo json_encode($html);
            die;
        } else {
            $q0 = false;
            $category_id = 0;
            $charge = 0;
            $q = $_GET["query"];
            $str['query'] = $q;
            if (isset($_GET["category_id"]) || isset($_GET["charge"])) {
                $category_id = $_GET["category_id"];
                $charge = $_GET["charge"];
            }

            $result = $this->public_model->getBikesbyFilter($q, $category_id, $charge);
            if (!empty($result)) {
                $q0 = true;
                foreach ($result as $val) {
                    $key = site_url('bike-rent/' . $val->slug);
                    $value = ucwords($val->name);
                    if ($category_id == 0) {
                        $value = ucwords($val->name) . '|' . $val->bike_category_name;
                    }

                    $str['suggestions'][] = $value;
                    $str['data'][] = $key;
                }
            }
            if ($q0 == false) {
                $str['suggestions'][] = "No Bike Found";
                $str['data'][] = 0;
            }

            echo json_encode($str);
            die;
        }
    }

    function bike_search() {
        $q0 = false;
        $category_id = 0;
        $charge = 0;
        $q = $_GET["query"];
        $str['query'] = $q;

        $result = $this->public_model->getBikesbyFilter($q, $category_id, $charge);
        if (!empty($result)) {
            $q0 = true;
            foreach ($result as $val) {
                $key = site_url('bike-rent/' . $val->slug);
                $value = ucwords($val->name);
                if ($category_id == 0) {
                    $value = ucwords($val->name) . '|' . $val->bike_category_name;
                }

                $str['suggestions'][] = $value;
                $str['data'][] = $key;
            }
        }
        if ($q0 == false) {
            $str['suggestions'][] = "No Bike Found";
            $str['data'][] = 0;
        }

        echo json_encode($str);
        die;
    }

}

?>