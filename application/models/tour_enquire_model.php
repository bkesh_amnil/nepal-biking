<?php

class Tour_enquire_Model extends My_Model
{

    protected $table = 'tbl_tour_enquire';
    var $tour = 'tbl_tour';
    var $tour_category = 'tbl_tour_category';

    public $id = '',
        $tour_category_id = '',
        $tour_id = '',
        $departure_date = '',
        $number_of_pax = '',
        $customer_full_name = '',
        $customer_email_address = '',
        $customer_contact_number = '',
        $customer_contact_address = '',
        $customer_enquiry = '',
        $enquire_note = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            /*array(
                'field' => 'enquire_note',
                'label' => 'Enquire Note',
                'rules' => 'trim|required',
            )*/
        );

        return $array;
    }

    public function getTourEnquiries() {
        $this->db->select($this->table . '.*, ' . $this->tour . '.name as tour_name, ' . $this->tour_category . '.name as tour_category_name');
        $this->db->join($this->tour, $this->tour . '.id = ' . $this->table . '.tour_id');
        $this->db->join($this->tour_category, $this->tour_category . '.id = ' . $this->table . '.tour_category_id');
        $this->db->order_by($this->table . '.id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getTourEnquireCustomers() {
        $this->db->select('DISTINCT(customer_full_name)');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExportData($field_type, $field_value) {
        $query = "SELECT
                    DATE_FORMAT(FROM_UNIXTIME(`tbl_tour_enquire`.`created_date`), '%Y-%m-%d') AS enquire_date,
                    `tbl_tour_category`.`name` as tour_category_name,
                    `tbl_tour`.`name` as tour_name,
                    `code`,
                    `departure_date`,
                    `number_of_pax`,
                    `customer_full_name`,
                    `customer_email_address`,
                    `customer_contact_number`,
                    `customer_contact_address`,
                    `customer_enquiry`,
                    `enquire_note`,
                    `tbl_tour_enquire`.`created_date`
                    FROM `tbl_tour_enquire`
                    JOIN `tbl_tour_category` ON `tbl_tour_category`.`id` = `tbl_tour_enquire`.`tour_category_id`
                    JOIN `tbl_tour` ON `tbl_tour`.`id` = `tbl_tour_enquire`.`tour_id`";

        if($field_type == 'tour_category') {
            $query .= " WHERE tour_category_id = " . $field_value;
        } else if($field_type == 'tour') {
            $query .= " WHERE tour_id = " . $field_value;
        } else if($field_type == 'customer_name') {
            $query .= " WHERE customer_full_name = '".$field_value."'";
        } else if($field_type == 'enquire_date') {
            $query .= " WHERE DATE_FORMAT(FROM_UNIXTIME(`tbl_tour_enquire`.`created_date`), '%Y-%m-%d') = '".$field_value."'";
        }

        $query .= " ORDER BY `tbl_tour_enquire`.`id` DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();

    }

}