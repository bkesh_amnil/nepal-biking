<?php

class Dynamic_form_model extends My_Model
{

	protected $table = 'tbl_form';

    public $id = '',
            $form_name = '',
            $form_related = '',
            $form_relation_link = '',
            $form_description = '',
            $form_unique_name = '',
            $form_title = '',
            $form_attribute = '',
            $form_class = '',
            $status = '',
            $category_id = '',
            $submit_action = '',
            $success_msg = '',
            $admin_email = '',
            $admin_email_subject = '',
            $admin_email_msg = '',
            $email_to_user = '',
            $user_email_subject = '',
            $user_email_msg = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'form_title',
                'label' => 'Form Title',
                'rules' => 'trim|required|unique['.$this->table.'.form_title.'.$id.']',
            ),
            array(
                'field' => 'form_name',
                'label' => 'Form Name',
                'rules' => 'trim|required|unique['.$this->table.'.form_name.'.$id.']',
            )
        );

        return $array;
    }

	/*function SaveForm($form_data, $id)
	{
		if($id)
		{
			$form_data['updated_by']	= current_admin_id(); 
			$form_data['updated_date']	= get_now();
			$this->db->where('id', $id);
			$this->db->update($this->table, $form_data);
		}
		else
		{
			$form_data['created_by']	= current_admin_id(); 
			$form_data['created_date']	= get_now();
			$this->db->insert($this->table, $form_data);
		}
		
		if ($this->db->affected_rows() == '1')
		{
			$id = $this->db->insert_id();
			$data['form_unique_name'] = $form_data['form_name'] . '-' . $id;
			$this->db->where('id', $id);
			$this->db->update($this->table, $data);
			return TRUE;
		}
		
		return FALSE;
	}*/
	
	function get_forms($id = 0, $paginate = FALSE, $uri = 0, $order_by = 'id', $order = 'asc')
	{
		$this->db->order_by($order_by, $order);
		
		if($id == 0 ) {
			$result = $this->db->get($this->table)->result();	
		} 	else  {
			$this->db->where($this->table . ".id", $id);
			$result = $this->db->get($this->table)->row();
		}
		return $result;	
	}

	function get_available_forms($id = false)
	{
		$this->db->select("id, form_name");
		if($id){
			$this->db->where("id !=", $id);
		}
		$data = $this->db->get($this->table)->result();
		if(!empty($data)){
			return $data;	
		}else{
			return false;	
		}
	}

	function getFormAction($form_id) {
		$this->db->select('submit_action');
		$this->db->where('id', $form_id);
		$row = $this->db->get($this->table)->row();

		return (isset($row) && !empty($row)) ? $row : '';
	}
}
?>