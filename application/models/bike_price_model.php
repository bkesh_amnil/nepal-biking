<?php

class Bike_Price_Model extends My_Model
{

    protected $table = 'tbl_bike_price';
    var $bike_rent_days = 'tbl_bike_rent_days';

    public $id = '',
        $bike_id = '',
        $description = '',
        $valid_from = '',
        $valid_to = '',
        $number_of_days_id = '',
        $unit_price = '',
        $is_outside_valley = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if($id == 0) {
            $array = array(
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'valid_from',
                    'label' => 'Valid From',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'valid_to',
                    'label' => 'Valid To',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'unit_price',
                    'label' => 'Unit Price',
                    'rules' => 'trim|required',
                ),
            );
        }
        return $array;
    }

    public function getBikePrice($bike_id) {
        $this->db->select($this->table . '.id, bike_id, description, valid_from, valid_to, number_of_days_id, name, unit_price, is_outside_valley, ' . $this->table . '.status');
        $this->db->join($this->bike_rent_days, $this->bike_rent_days . '.id = ' . $this->table . '.number_of_days_id');
        $this->db->where('bike_id', $bike_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}