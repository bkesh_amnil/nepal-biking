<?php

class Tour_book_Model extends My_Model
{

    protected $table = 'tbl_tour_booking';
    var $tour = 'tbl_tour';
    var $tour_departure_detail = 'tbl_tour_departure_detail';
    var $booking_status = 'tbl_booking_status';
    var $tour_booking_status = 'tbl_tour_booking_status';

    public $id = '',
        $form_type = '',
        $tour_category_id = '',
        $tour_id = '',
        $customer_remarks = '',
        $last_booking_status_id = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'tour_category_id',
                'label' => 'Tour Category',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'tour_id',
                'label' => 'Tour',
                'rules' => 'trim|required|integer',
            )
        );

        return $array;
    }

    public function getTourBookings() {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_tour_booking`.`id`,
                      `tbl_tour_booking`.`created_date`,
                      `tbl_tour_booking`.`created_by`,
                      `tbl_tour`.`name` AS tour_name,
                      `customer_full_name`,
                      `tour_departure_date`,
                      `number_of_pax`,
                      `price_per_person`,
                      `total_price`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_tour_booking_status.id AS booking_id,
                      tour_booking_id
                    FROM (`tbl_tour_booking`)
                      JOIN `tbl_tour_booking_status`
                        ON `tbl_tour_booking_status`.`tour_booking_id` = `tbl_tour_booking`.`id`
                      JOIN `tbl_tour`
                        ON `tbl_tour`.`id` = `tbl_tour_booking`.`tour_id`
                      JOIN `tbl_tour_departure_detail`
                        ON `tbl_tour_departure_detail`.`id` = `tbl_tour_booking_status`.`departure_date_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_tour_booking_status`.`tour_booking_status_id`
                        ORDER BY tbl_tour_booking_status.id DESC)AS t
                    GROUP BY t.tour_booking_id
                    ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExportData($field_type, $field_value) {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_tour_booking`.`id`,
                      `tbl_tour_booking`.`created_date`,
                      `tbl_tour_booking`.`created_by`,
                      `tbl_tour_booking`.`customer_remarks`,
                      `tbl_tour_booking`.`tour_id`,
                      `tbl_tour_booking`.`tour_category_id`,
                      `tbl_tour_booking`.`last_booking_status_id`,
                      `tbl_tour_category`.`name` AS tour_category_name,
                      `tbl_tour`.`name` AS tour_name,
                      `tbl_tour`.`code`,
                      `customer_full_name`,
                      `customer_email_address`,
                      `customer_contact_number`,
                      `customer_contact_address`,
                      `tour_departure_date`,
                      `number_of_pax`,
                      `price_per_person`,
                      `total_price`,
                      `tbl_booking_status`.`id` AS booking_status_id,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_tour_booking_status.id AS booking_id,
                      tour_booking_id,
                      DATE_FORMAT(FROM_UNIXTIME(`tbl_tour_booking`.`created_date`), '%Y-%m-%d') AS booking_date
                    FROM (`tbl_tour_booking`)
                      JOIN `tbl_tour_booking_status`
                        ON `tbl_tour_booking_status`.`tour_booking_id` = `tbl_tour_booking`.`id`
                      JOIN `tbl_tour_category`
                        ON `tbl_tour_category`.`id` = `tbl_tour_booking`.`tour_category_id`
                      JOIN `tbl_tour`
                        ON `tbl_tour`.`id` = `tbl_tour_booking`.`tour_id`
                      JOIN `tbl_tour_departure_detail`
                        ON `tbl_tour_departure_detail`.`id` = `tbl_tour_booking_status`.`departure_date_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_tour_booking_status`.`tour_booking_status_id`
                        ORDER BY tbl_tour_booking_status.id DESC)AS t";

        if($field_type == 'tour_category') {
            $query .= " WHERE t.`tour_category_id` = " . $field_value;
        } else if($field_type == 'tour') {
            $query .= " WHERE t.`tour_id` = " . $field_value;
        } else if($field_type == 'customer_name') {
            $query .= " WHERE t.`customer_full_name` = '".$field_value."'";
        } else if($field_type == 'booking_status') {
            $query .= " WHERE t.`last_booking_status_id` = ". $field_value;
        } else if($field_type == 'booking_date' && !empty($field_value)) {
            $query .= " WHERE t.booking_date = '".$field_value."'";
        } else if($field_type == 'departure_date' && !empty($field_value)) {
            $query .= " WHERE t.`tour_departure_date` = '".$field_value."'";
        }

        $query .= " GROUP BY t.tour_booking_id
                    ORDER BY t.id DESC";

        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}