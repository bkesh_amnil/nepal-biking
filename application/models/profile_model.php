<?php

class Profile_Model extends My_Model
{

    public $table = 'tbl_user';

    public $id = '',
        $name = '',
        $username = '',
        $password = '',
        $email = '',
        $status = '',
        $role_id = '';

    public function __construct()
    {
        parent::__construct();
    }

    public $rules =
        array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|callback_unique_username',
            ),
            /*array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|matches[password]'
            ),*/
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|valid_email|callback_unique_email'
            ),
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim'
            )
        );

}