<?php

class Tour_download_Model extends My_Model
{

    protected $table = 'tbl_download_detail';
    var $tour = 'tbl_tour';

    public $id = '',
        $type = '',
        $type_id = '',
        $full_name = '',
        $email = '',
        $download_date = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = false;
        $this->updated_timestamp = false;
        $this->created_by = false;
        $this->updated_by = false;
    }

    public function getTourDownloads() {
        $this->db->select($this->table . '.*, ' . $this->tour . '.name as tour_name');
        $this->db->join($this->tour, $this->tour . '.id = ' . $this->table . '.type_id AND type = "tour"');
        $this->db->order_by($this->table . '.id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}