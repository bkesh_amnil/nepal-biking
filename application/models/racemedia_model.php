<?php

class Racemedia_Model extends My_Model
{

    protected $table = 'tbl_race_media';

    public $id = '',
        $race_id = '',
        $type = '',
        $image = '',
        $title = '',
        $caption = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getSavedMedia($id) {
        $this->db->where('race_id', $id);
        $result = $this->db->get($this->table)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
               $array[$val->type][] = $val;
            }
        }

        return (isset($array) && !empty($array)) ? $array : array();
    }

}