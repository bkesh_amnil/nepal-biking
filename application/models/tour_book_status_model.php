<?php

class Tour_book_status_Model extends My_Model
{

    protected $table = 'tbl_tour_booking_status';
    var $tour = 'tbl_tour';
    var $tour_departure_detail = 'tbl_tour_departure_detail';
    var $booking_status = 'tbl_booking_status';
    var $tour_booking = 'tbl_tour_booking';

    public $id = '',
        $tour_booking_id = '',
        $departure_date_id = '',
        $number_of_pax = '',
        $price_per_person = '',
        $total_price = '',
        $customer_full_name = '',
        $customer_email_address = '',
        $customer_contact_number = '',
        $customer_contact_address = '',
        $tour_booking_status_id = '',
        $tour_note = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'departure_date_id',
                'label' => 'Departure Date',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'number_of_pax',
                'label' => 'Number of pax',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'price_per_person',
                'label' => 'Price per Person',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'total_price',
                'label' => 'Total Cost',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_full_name',
                'label' => 'Full Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'customer_contact_number',
                'label' => 'Contact Address',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_contact_address',
                'label' => 'Contact Nos',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getSavedData($tour_booking_id, $type) {
        $this->db->select($this->table . '.*, ' . $this->tour . '.name as tour_name, ' . $this->tour_departure_detail . '.tour_departure_date, tour_all_allocation, tour_available, tour_booked, ' . $this->booking_status . '.name as booking_status, ' . $this->tour_booking . '.tour_id, ' . $this->tour_booking . '.tour_category_id');
        $this->db->join($this->tour_booking, $this->tour_booking. '.id = ' . $this->table . '.tour_booking_id');
        $this->db->join($this->tour, $this->tour . '.id = '. $this->tour_booking . '.tour_id');
        $this->db->join($this->tour_departure_detail, $this->tour_departure_detail . '.id = ' . $this->table . '.departure_date_id');
        $this->db->join($this->booking_status, $this->booking_status . '.id = ' . $this->table . '.tour_booking_status_id');
        $this->db->where('tour_booking_id', $tour_booking_id);
        $this->db->order_by('id', 'DESC');
        if($type == 'logs') {
            $result = $this->db->get($this->table)->result();

            return (isset($result) && !empty($result)) ? $result : array();
        } else {
            $this->db->limit(1);
            $row = $this->db->get($this->table)->row();

            return (isset($row) && !empty($row)) ? $row : '';
        }
    }

    public function getTourBookingCustomers() {
        $this->db->select('DISTINCT(customer_full_name)');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}