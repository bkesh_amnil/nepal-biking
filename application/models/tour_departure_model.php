<?php

class Tour_Departure_Model extends My_Model
{

    protected $table = 'tbl_tour_departure_detail';

    public $id = '',
        $tour_id = '',
        $tour_departure_date = '',
        $tour_all_allocation = '',
        $tour_available = '',
        $tour_booked = '',
        $is_fixed_departure = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'tour_departure_date',
                'label' => 'Departure Date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'tour_all_allocation',
                'label' => 'All Allocation',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'tour_available',
                'label' => 'Available',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'tour_booked',
                'label' => 'Booked',
                'rules' => 'trim|required|integer'
            )
        );

        return $array;
    }

    public function getTourDeparture($tour_id) {
        $this->db->select('id, tour_id, tour_departure_date, tour_all_allocation, tour_available, tour_booked, is_fixed_departure, status');
        $this->db->where('tour_id', $tour_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}