<?php

class Tour_Inclusion_Exclusion_Model extends My_Model
{

    protected $table = 'tbl_tour_inclusion_exclusion';

    public $id = '',
        $tour_id = '',
        $tour_data_type = '',
        $tour_data = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if($id == 0) {
            $array = array(
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'tour_data',
                    'label' => 'Tour Data',
                    'rules' => 'trim|required',
                ),
            );
        }
        return $array;
    }

    public function getTourInclusionExclustion($tour_id) {
        $this->db->select('id, tour_id, tour_data_type, tour_data, status');
        $this->db->where('tour_id', $tour_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}