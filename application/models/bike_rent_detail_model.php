<?php

class Bike_Rent_Detail_Model extends My_Model
{

    protected $table = 'tbl_bike_rent_detail';

    public $id = '',
        $bike_id = '',
        $rent_date = '',
        $all_allocation = '',
        $available = '',
        $booked = '',
        $is_fixed_departure = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();          
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'rent_date',
                'label' => 'Departure Date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'all_allocation',
                'label' => 'All Allocation',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'available',
                'label' => 'Available',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'booked',
                'label' => 'Booked',
                'rules' => 'trim|required|integer'
            )
        );

        return $array;
    }

    public function getBikeRent($bike_id) {
        $this->db->select('id, bike_id, rent_date, all_allocation, available, booked, is_fixed_departure, status');
        $this->db->where('bike_id', $bike_id);
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}