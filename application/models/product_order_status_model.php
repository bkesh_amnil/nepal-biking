<?php

class Product_order_status_Model extends My_Model
{

    protected $table = 'tbl_product_order_status';
    var $product_order = 'tbl_product_order';
    var $product = 'tbl_product';
    var $booking_status = 'tbl_booking_status';

    public $id = '',
        $product_quantity = '',
        $product_price = '',
        $total_price = '',
        $customer_full_name = '',
        $customer_email_address = '',
        $customer_contact_number = '',
        $customer_special_instruction = '',
        $product_order_status_id = '',
        $product_note = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'customer_full_name',
                'label' => 'Full Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'customer_contact_number',
                'label' => 'Contact Nos.',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getSavedData($product_order_id, $type) {
        $this->db->select($this->table . '.*, ' . $this->product . '.name as product_name, ' . $this->booking_status . '.name as booking_status');
        $this->db->join($this->product_order, $this->product_order . '.id = ' . $this->table . '.product_order_id');
        $this->db->join($this->product, $this->product . '.id = '. $this->product_order . '.product_id');
        $this->db->join($this->booking_status, $this->booking_status . '.id = ' . $this->table . '.product_order_status_id');
        $this->db->where('product_order_id', $product_order_id);
        $this->db->order_by('id', 'DESC');
        if($type == 'logs') {
            $result = $this->db->get($this->table)->result();

            return (isset($result) && !empty($result)) ? $result : array();
        } else {
            $this->db->limit(1);
            $row = $this->db->get($this->table)->row();

            return (isset($row) && !empty($row)) ? $row : '';
        }
    }

    public function getProductOrderCustomers() {
        $this->db->select('DISTINCT(customer_full_name)');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}