<?php

class Tour_Map_Model extends My_Model
{

    protected $table = 'tbl_tour_map';

    public $id = '',
        $tour_id = '',
        $tour_latitude = '',
        $tour_longitude = '',
        $tour_marker_title = '',
        $tour_marker_address = '',
        $tour_marker_description = '',
        $tour_marker_label = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function getTourMaps($tour_id) {
        $this->db->select('id, tour_id, latitude, longitude, marker_title, marker_address, marker_description, marker_label, marker_sort_order');
        $this->db->where('tour_id', $tour_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}