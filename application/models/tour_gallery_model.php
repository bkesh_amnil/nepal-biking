<?php

class Tour_Gallery_Model extends My_Model
{

    protected $table = 'tbl_tour_gallery';

    public $id = '',
        $tour_id = '',
        $image = '',
        $title = '',
        $caption = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if($id == 0) {
            $array = array(
                array(
                    'field' => 'image',
                    'label' => 'Image',
                    'rules' => 'trim|required'
                )
            );
        } else {
            $array = array(
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'required'
                )
            );
        }

        return $array;
    }

    public function getTourGallery($tour_id) {
        $this->db->select('id, tour_id, image, title, caption, status');
        $this->db->where('tour_id', $tour_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}