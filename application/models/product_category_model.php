<?php

class Product_category_Model extends My_Model
{

    protected $table = 'tbl_product_category';
    var $product_order = 'tbl_product_order';

    public $id = '',
        $name = '',
        $parent_id = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
        );

        return $array;
    }

    public function get_product_categories_list($parent_cat = 0, $status = "all") {
        $this->db->where('parent_id', $parent_cat);
        if($status != "all"){
            $this->db->where('status', $status);
        }
        $this->db->order_by('position', 'ASC');
        $result = $this->db->get($this->table)->result();

        $catList = false;
        if(!empty($result)){
            foreach($result as $ind=>$val){
                $dataArr = array();
                foreach($val as $index=>$value){
                    $dataArr[$index] = $value;
                }
                if(!empty($dataArr)){
                    $dataArr["childs"] = $this->get_product_categories_list($val->id, $status);
                }
                $dataArr = (object) $dataArr;
                $catList[] = $dataArr;
            }
        }
        return $catList;
    }

    public function getProductOrderCategory() {
        $this->db->select('DISTINCT(' . $this->table . '.id), name');
        $this->db->join($this->product_order, $this->product_order . '.product_category_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}