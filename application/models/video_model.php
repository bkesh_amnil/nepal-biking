<?php

class Video_Model extends My_Model
{

    protected $table = 'tbl_video';

    public $id = '',
            $name = '',
            $slug ='',
            $video_id= '',
            $youtube_link='',
            $description,
            $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Video Name',
                'rules' => 'trim|required|xss_clean|unique[tbl_video.name.' . $id . ']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Video Alias',
                'rules' => 'trim|required|xss_clean|unique[tbl_video.slug.' . $id . ']',
            ),
            array(
                'field' => 'youtube_link',
                'label' => 'Youtube Link',
                'rules' => 'trim|required|uniue[tbl_video.youtube_link.' . $id . '] ',
            )
        );

        return $array;
    }

    function get_videos_for_sort()
    {
        $this->db->select('id, name');

        $this->db->order_by('poisiton');
        $result = $this->db->get($this->table)->result();

        return $result;
    }

}