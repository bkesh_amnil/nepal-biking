<?php

class Public_model extends CI_Model {
    
    var $site_menus = 'tbl_site_menus';
    var $profile = 'tbl_profile';
    var $profile_detail = 'tbl_profile_detail';
    var $module = 'tbl_module';
    var $site = 'tbl_site';
    var $categories = 'tbl_category';
    var $menu = 'tbl_menu';
    var $banner = 'tbl_banner';
    var $content = 'tbl_content';
    var $menu_page_settings = 'tbl_menu_page_settings';
    var $popup_image = 'tbl_popup_image';
    var $social_media = 'tbl_social_media';
    var $form = 'tbl_form';
    var $form_field = 'tbl_form_field';
    var $form_submission = 'tbl_form_submission';
    var $form_submission_fields = 'tbl_form_submission_fields';
    var $tour = 'tbl_tour';
    var $tour_category = 'tbl_tour_category';
    var $tour_gallery = 'tbl_tour_gallery';
    var $tour_inclusion_exclusion = 'tbl_tour_inclusion_exclusion';
    var $tour_map = 'tbl_tour_map';
    var $tour_price = 'tbl_tour_price';
    var $departure_type = 'tbl_departure_type';
    var $tour_departure_detail = 'tbl_tour_departure_detail';
    var $difficulty_manager = 'tbl_difficulty';
    var $bike = 'tbl_bike';
    var $bike_category = 'tbl_bike_category';
    var $bike_gallery = 'tbl_bike_gallery';
    var $bike_price = 'tbl_bike_price';
    var $bike_rent_detail = 'tbl_bike_rent_detail';
    var $bike_rent_days = 'tbl_bike_rent_days';
    var $news = 'tbl_news';
    var $event = 'tbl_event';
    var $testimonial = 'tbl_testimonial';
    var $countries = 'tbl_countries';
    var $product = 'tbl_product';
    var $product_category = 'tbl_product_category';
    var $product_gallery = 'tbl_product_gallery';
    var $video = 'tbl_video';
    var $faq = 'tbl_faq';
    var $race_media = 'tbl_race_media';
    var $gallery = 'tbl_gallery';
    var $gallery_media = 'tbl_gallery_media';
    var $social_data = 'tbl_social_data';
    var $background_image = 'tbl_background_image';
    var $logos = 'tbl_logos';

    function getMenu($id, $parent = 0) {
        $this->db->where($this->site_menus . '.id', $id);
        $this->db->where($this->menu . '.menu_parent', $parent);
        $this->db->where($this->menu . '.status', 1);
        $this->db->order_by($this->menu . '.position', 'ASC');
        $this->db->select($this->menu .' .id, ' . $this->menu . '.name, slug,menu_opens');
        $this->db->from($this->menu);
        $this->db->join($this->site_menus, $this->site_menus . '.id = ' .$this->menu. '.menu_type_id');
        $result = $this->db->get()->result();
        $list = "";

        if (!empty($result) && is_array($result)) {
          foreach ($result as $ind => $menu) {
            $menuListArr = "";
            foreach ($menu as $mind => $mval) {
              $menuListArr[$mind] = $mval;
            }
            $menuListArr['childs'] = $this->getMenu($id, $menu->id);
            $list[] = $menuListArr;
          }
        }
        if (empty($list)) {
          return array();
        }
        return $list;
    }

    function getActiveMenu($slug) {
        $this->db->select('menu_parent, slug');
        $this->db->where('slug', $slug);
        $row = $this->db->get($this->menu)->row();

        if(isset($row) && !empty($row)) {
            if(isset($row) && $row->menu_parent != '0') {
                $this->db->select('slug');
                $this->db->where('id', $row->menu_parent);
                $row1 = $this->db->get($this->menu)->row();

                return $row1->slug;
            } else {
                return $row->slug;
            }
        }

        return '';
    }

    function getBanner() {
        $this->db->select('name, description, image, link_type, link_id, link_url, opens');
        $this->db->where('status', '1');
        $this->db->order_by('position');
        $result = $this->db->get($this->banner)->result();

        $list = "";
        if(isset($result) && !empty($result) && is_array($result)) {
            foreach($result as $row) {
                $data['info'] = $row;
                if(!empty($row->link_id)) {
                switch($row->link_type) {
                    case "menu":
                        $data['url'] = $this->Alias($row->link_id, 'menu');
                        break;
                    case "tour":
                        $data['url'] = $this->Alias($row->link_id, 'tour');
                        break;
                    case "bike":
                        $data['url'] = $this->Alias($row->link_id, 'bike');
                        break;
                    case "accessories":
                        $data['url'] = $this->Alias($row->link_id, 'product');
                        break;
                    case "url":
                        $data['url'] = $row->external_url;
                        break;
                    default:
                        $data['url'] = '';
                        break;
                }
                }
                $list[] = $data;
            }
            return $list;
        }

        return array();
    }

    function Alias($id, $table){

        $this->db->select('slug');
        $this->db->where('id', $id);
        $row = $this->db->get($this->$table)->row();
        
        if(!empty($row)) {
        switch($table) {
            case "menu":
                $url = site_url($row->slug);
                break;
            case "tour":
                $url = site_url('tour/' . $row->slug);
                break;
            case "bike":
                $url = site_url('bike-rent/' . $row->slug);
                break;
            default:
                $url = site_url('bike-accessories/' . $row->slug);
                break;
        }
        }

        return (isset($row) && !empty($row)) ? $url : '';
    }

    function getBreadCrumb($url) {
        $count = count($url);
        $current_url = current_url();
        $alias = $this->uri->segment($count);
        $html = '';
        $crumb = '';

        if($count > 1) {
            $type = $this->uri->segment(1);
            if($count > 2) {
                $data = $this->getNameforBreadCrumbByAlias($this->uri->segment($count - 1), $type);
                $html .= '<li><a href="' . site_url($type) . '">' . ucfirst($type) . '</a></li>';
                $html .= '<li><a href="' . site_url($type . '/' . $this->uri->segment($count - 1)) . '">' . $data['name'] . '</a></li>';
                $html .= '<li><a class="active">'.ucfirst($this->uri->segment($count)).'</a> </li>';
            } else {
                if ($type != 'about-us' && $type != 'about-dawn-till-dusk' && $type != 'faq' && $type != 'team') {
                    $data = $this->getNameforBreadCrumbByAlias($alias, $type);
                    $type_url = $type;
                    if($type == 'tour') {
                        $type_url = 'nepalbiketours';
                    } else if($type == 'gallery') {
                        $type_url = 'bikinginnepal-gallery';
                    }
                    $html .= '<li><a href="' . site_url($type_url) . '">' . ucfirst(str_replace('-' , ' ', $type)) . '</a></li>';
                    $html .= '<li><a class="active">' . $data['name'] . '</a></li>';
                } else {
                    $menu_name = $this->getNameforBreadCrumb($this->uri->segment(1), 'content');
                    if (isset($menu_name) && !empty($menu_name)) {
                        $html .= $menu_name;
                    } else {
                        $html .= '<li><a class="active" href="javascript:void(0);">' . str_replace('-', ' ', ucwords($this->uri->segment(1))) . '</a></li>';
                    }
                }
            }
        } else {
            $url = site_url($this->uri->segment(1));
            $menu_name = $this->getNameforBreadCrumb($this->uri->segment(1), 'content');
            if(isset($menu_name) && !empty($menu_name)) {
                $html .= $menu_name;
            } else {
                $html .= '<li><a class="active" href="javascript:void(0);">'.str_replace('-', ' ', ucwords($this->uri->segment(1))).'</a></li>';
            }
        }

        return $html;
    }

    function getNameforBreadCrumb($alias, $type) {
        if($type == 'content') {
            $this->db->select('menu_parent, name');
            $this->db->where('slug', $alias);
            $row = $this->db->get($this->menu)->row();

            if(isset($row) && !empty($row)) {
                if($row->menu_parent != '0') {
                    $this->db->select('name');
                    $this->db->where('id', $row->menu_parent);
                    $row1 = $this->db->get($this->menu)->row();

                    return '<li>'.ucwords($row1->name).'</li><li class="active"><a href="'.base_url($alias).'">'.ucwords($row->name).'</a></li>';
                } else {
                    return '<li><a  class="active" href="'.base_url($alias).'">'.ucwords($row->name).'</a></li>';
                }
            }/* else {
                $this->db->select('product_category_name');
                $this->db->where('product_category_alias', $alias);
                $row2 = $this->db->get($this->product_category)->row();

                if(isset($row2) && !empty($row2)) {
                    return '<li class="active">'.ucwords($row2->product_category_name).'</li>';
                }
            }*/
        }

        return NULL;
    }

    function getNameforBreadCrumbByAlias($alias, $type) {
        if($type == 'bike-rent') {
            $type = 'bike';
        } elseif($type == 'bike-accessories') {
            $type= 'product';
        }
        if($type != 'testimonial') {
            $this->db->select('name');
        } else {
            $this->db->select('customer_full_name as name');
        }
        $this->db->where('slug', $alias);
        $row = $this->db->get($this->$type)->row();

        if(isset($row) && !empty($row)) {
            $data['name'] = $row->name;
            return $data;
        }
    }
    /* cms */
    function getModuleId($slug) {
        if($slug == 'bike-rent') {
            $slug = 'bike';
        } else if($slug == 'bike-accessories') {
            $slug = 'product';
        }
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $row = $this->db->get($this->module)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getDataId($slug, $table) {
        if($table == 'bike-rent') {
            $table = 'bike';
        } elseif($table == 'bike-accessories') {
            $table = 'product';
        }
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $row = $this->db->get('tbl_' . $table)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getMenuId($slug) {
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $this->db->where('status', 1);
        $row = $this->db->get($this->menu)->row();

        if(isset($row) && !empty($row)) {
            return $row;
        } else {
            $this->db->select($this->menu . '.id');
            $this->db->join($this->product_category, $this->product_category . '.id = ' . $this->menu . '.product_category_id');
            $this->db->where($this->product_category . '.product_category_alias', $slug);
            $row1 = $this->db->get($this->menu)->row();

            if(isset($row1) && !empty($row1)) {
                return $row1;
            }
        }

        return '';
    }

    function getModules($menuId, $contentAlias = false, $paginate = false, $start = 0, $per_page = 10) {
        $this->db->select($this->menu_page_settings . '.module_id, ' . $this->menu_page_settings . '.content_ids, ' . $this->menu_page_settings . '.module_title, category_id, ' . $this->module . '.slug as module_name');
        $this->db->where($this->menu_page_settings . '.menu_id', $menuId);
        $this->db->order_by("position", "asc");
        $this->db->from($this->menu_page_settings);
        $this->db->join($this->module, $this->module . '.id = ' . $this->menu_page_settings . '.module_id');
        $result = $this->db->get()->result();

        $modules = array();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $ind => $data) {
                $tblName = $this->getTableName($data->module_name);
                if (!empty($tblName)) {
                    $modules[$ind] = array("module_id" => $data->module_id, "module" => $data->module_name, "module_title" => $data->module_title, "data" => $this->getData($tblName, $data->content_ids, $data->category_id, $paginate, $start, $per_page));
                }
            }
        }

        return $modules;
    }

    function getTableName($moduleName = "") {
        switch ($moduleName) {
            case "menu":
                $tblName = "tbl_menu";
                $fld = "name";
                break;
            case "content":
                $tblName = "tbl_content";
                break;
            case "advertisement":
                $tblName = "tbl_advertisement";
                break;
            case "dynamic_form":
                $tblName = "tbl_form";
                break;
            case "form_fields":
                $tblName = "tbl_form";
                break;
            case "faq":
                $tblName = 'tbl_faq';
                break;
            case 'tour':
                $tblName = 'tbl_tour';
                break;
            case 'news':
                $tblName = 'tbl_news';
                break;
            case 'event':
                $tblName = 'tbl_event';
                break;
            case 'gallery';
                $tblName = 'tbl_gallery';
                break;
            case 'product':
                $tblName = 'tbl_product';
                break;
            case 'bike':
                $tblName = 'tbl_bike';
                break;
            case 'race':
                $tblName = 'tbl_race';
                break;
            case 'video':
                $tblName = 'tbl_video';
                break;
            case 'team':
                $tblName = 'tbl_team';
                break;
            case 'testimonial':
                $tblName = 'tbl_testimonial';
                break;
            default:
                $tblName = "";
        }
        return $tblName;
    }

    function getData($tblName, $ids, $category_id, $paginate = false, $start = 0, $per_page = 10) {
        if($category_id == '1' || ($category_id == '0' && $tblName == 'tbl_content') || $tblName == 'tbl_race') {
            $ids = explode(',', $ids);
            $this->db->where_in($tblName . ".id", $ids);
        }
        if ($paginate)
            $this->db->limit($per_page, $start);

        if($tblName == 'tbl_content') {
            $this->db->select($tblName . '.*');
            $this->db->join($this->categories, $this->categories . '.id = ' . $tblName . ' .category_id');
            if($category_id != '1' && $category_id != '0') {
                $this->db->where($tblName . '.category_id', $category_id);
            }
            $this->db->order_by($tblName . '.position');
        }

        if($tblName != 'tbl_form') {
            $this->db->where($tblName . '.status', 1);
        }

        if($tblName == 'tbl_news' || $tblName == 'tbl_event' || $tblName == 'tbl_tour') {
            $this->db->limit(8);
            $this->db->order_by($tblName . '.id', 'DESC');
        } else {
            if($tblName != 'tbl_race' && $tblName != 'tbl_form') {
                $this->db->order_by($tblName . '.position');
            }
        }

        $data = $this->db->get($tblName)->result();

        return $data;
    }

    function getSocialData($module_id, $data_id) {
        $this->db->select('title, description, image, link, social_site');
        $this->db->where('module_id', $module_id);
        $this->db->where('data_id', $data_id);
        $result = $this->db->get($this->social_data)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $array[$val->social_site] = $val;
            }
        }

        return (isset($array) && !empty($array)) ? $array : array();
    }

    function getGalleryImages($alias) {
        $this->db->select('media, title, caption');
        $this->db->join($this->gallery_media, $this->gallery_media . '.gallery_id = ' . $this->gallery . '.id');
        $this->db->where('type', 'Image');
        $this->db->where($this->gallery . '.slug', $alias);
        $result = $this->db->get($this->gallery)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* cms */
    function getPopupImage() {
        $cur_date = strtotime(date('Y-m-d'));
        $query = "SELECT popup_image FROM tbl_popup_image WHERE " . $cur_date . " BETWEEN publish_date AND unpublish_date";
        $row = $this->db->query($query)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }
    /* tours */
    function countRows($type) {
        $query = "SELECT COUNT(id) AS count FROM " . $this->$type;
        $row = $this->db->query($query)->row();

        return (isset($row) && !empty($row)) ? $row->count : '';
    }

    function getTours($type, $limit) {
        $this->db->select('name, slug, cover_image, short_description');
        $this->db->where('status', 1);
        if($type == 'highlight') {
            $this->db->where('is_highlighted', 1);
            $this->db->order_by('position');
        } else {
            $this->db->order_by('id', 'DESC');
        }
        $this->db->limit($limit);
        $result = $this->db->get($this->tour)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourCategories() {
        $this->db->select('DISTINCT('.$this->tour_category . '.id), ' . $this->tour_category . '.name as tour_category_name');
        $this->db->join($this->tour, $this->tour . '.tour_category_id = ' . $this->tour_category . '.id');
        $this->db->where($this->tour_category . '.status', 1);
        $this->db->where($this->tour . '.status', 1);
        $result = $this->db->get($this->tour_category)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourDifficulty() {
        $this->db->select('DISTINCT('. $this->difficulty_manager . '.id), ' .$this->difficulty_manager . '.name as difficulty_name');
        $this->db->join($this->tour, $this->tour . '.difficulty_id = ' . $this->difficulty_manager . '.id');
        $this->db->where($this->difficulty_manager . '.status', 1);
        $this->db->where($this->tour . '.status', 1);
        $result = $this->db->get($this->difficulty_manager)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourDesc($slug) {
        $this->db->select($this->tour . '.id, tour_category_id, ' . $this->tour . '.code, ' . $this->tour . '.name, cover_image, file, map_file, duration, minimum_group_size, best_season, altitude, difficulty_id, is_highlighted, support, short_description, description, itinerary, ' . $this->departure_type . '.name as departure_name, '. $this->difficulty_manager . '.name as difficulty_name, ' . $this->difficulty_manager . '.code as difficulty_code');
        $this->db->join($this->departure_type, $this->departure_type . '.code = ' . $this->tour . '.departure_type');
        $this->db->join($this->difficulty_manager, $this->difficulty_manager . '.id = ' . $this->tour . '.difficulty_id');
        $this->db->where('slug', $slug);
        $row = $this->db->get($this->tour)->row();
        if(isset($row) && !empty($row)) {
            $data['info'] = $row;
            $data['gallery'] = $this->getTourGallery($row->id);
            $data['inexclusions'] = $this->getTourInclusionExclusion($row->id);
            $data['tour_map'] = $this->getTourMaps($row->id);
            $data['tour_price'] = $this->getTourPrice($row->id);
            $data['tour_departure_dates'] = $this->getTourDepartureDates($row->id);
        }

        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getTourGallery($tour_id) {
        $this->db->select('image, title, caption');
        $this->db->where('tour_id', $tour_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->tour_gallery)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourInclusionExclusion($tour_id) {
        $this->db->select('tour_data_type, tour_data');
        $this->db->where('tour_id', $tour_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->tour_inclusion_exclusion)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $data[$val->tour_data_type][] = $val->tour_data;
            }
        }
        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getTourMaps($tour_id) {
        $this->db->select('latitude, longitude, marker_title, marker_address, marker_description');
        $this->db->where('tour_id', $tour_id);
        $this->db->order_by('marker_sort_order');
        $result = $this->db->get($this->tour_map)->result();

        if(isset($result) && !empty($result)) {
            $locations = array();
            $i = 1;
            foreach($result as $val) {
                $Tlat = (string)$val->latitude;
                $Tlng = (string)$val->longitude;
                $Tid = (string)$i;
                $Tname = (string)$val->marker_title;
                $Taddr = (string)$val->marker_address;
                $Tdesc = (string)$val->marker_description;

                $locations[] = array('label'=>$Tid,'lat'=>$Tlat,'long'=>$Tlng,'titl'=>$Tname,'addr'=>$Taddr,'desc'=>$Tdesc);
                $i++;
            }
            $locations = json_encode($locations);
        }

        return (isset($locations) && !empty($locations)) ? $locations : array();
    }

    function getTourPrice($tour_id) {
        $this->db->select('valid_from, valid_to, number_of_pax, unit_price');
        $this->db->where('tour_id', $tour_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->tour_price)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourDepartureDates($tour_id)
    {
        $current_date = date('Y-m-d');
        $query = "SELECT DISTINCT(tbl_tour_departure_detail.`id`),tour_departure_date, tour_all_allocation,tour_available,tour_booked,is_fixed_departure/*, number_of_pax, unit_price*/
                    FROM `tbl_tour_departure_detail`
                    JOIN `tbl_tour_price` ON tbl_tour_departure_detail.`tour_departure_date` BETWEEN tbl_tour_price.`valid_from` AND tbl_tour_price.`valid_to`
                    WHERE tbl_tour_departure_detail.`status` = 1
                    AND tbl_tour_price.`status` = 1
                    AND tbl_tour_departure_detail.`tour_departure_date` >= '" . $current_date . "'
                    AND tbl_tour_departure_detail.`tour_id` = " . $tour_id . "
                    AND tbl_tour_price.`tour_id` = " . $tour_id;
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getToursbyFilter($search_value, $tour_category_id, $difficulty_id) {
        $this->db->select($this->tour . '.name, ' . $this->tour . '.code, ' . $this->tour . '.slug, cover_image, departure_type, duration, ' . $this->tour_category . '.name as tour_category_name');
        $this->db->join($this->tour_category, $this->tour_category . '.id = ' . $this->tour . '.tour_category_id');
        $this->db->where($this->tour . '.status', 1);
        if($tour_category_id != '0') {
            $this->db->where('tour_category_id', $tour_category_id);
        }
        if($search_value != '') {
            $this->db->like($this->tour . '.name', $search_value, 'both');
        }
        if($difficulty_id != '0') {
            $this->db->where('difficulty_id', $difficulty_id);
        }
        $this->db->order_by($this->tour . '.id', 'DESC');
        $result = $this->db->get($this->tour)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getDateDetail(/*$date_id*/ $date, $id, $type) {
        if($type == 'tour') {
            $this->db->select('tour_all_allocation, tour_available, tour_booked, is_fixed_departure');
            /*$this->db->where('id', $date_id);*/
            $this->db->where('tour_id', $id);
            $this->db->where('tour_departure_date', $date);
            $row = $this->db->get($this->tour_departure_detail)->row();
        } else {
            $this->db->select('all_allocation, available, booked, is_fixed_departure');
            //$this->db->where('id', $date_id);
            $this->db->where('bike_id', $id);
            $this->db->where('rent_date', $date);
            $row = $this->db->get($this->bike_rent_detail)->row();
        }

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getPriceDetail($nos_of_pax, $sel_date, $id, $type) {
        if($type == 'tour') {
            $query = "SELECT unit_price
                        FROM " . $this->tour_price . "
                        WHERE tour_id = " . $id . "
                        AND ('" . $sel_date . "' BETWEEN valid_from AND valid_to)
                        AND ('" . $nos_of_pax . "' BETWEEN min_pax AND max_pax
                        OR min_pax = " . $nos_of_pax . "
                        OR max_pax = " . $nos_of_pax . ")";
        } else {
            $rent_days = $nos_of_pax;
            /* $nos_of_pax = $rent_days */
            $number_of_days_id = '1';
            if($rent_days > 5 && $rent_days <= 10) {
                $number_of_days_id = '2';
            } elseif($rent_days > 10) {
                $number_of_days_id = '3';
            }
            $query = "SELECT unit_price, number_of_days_id, name
                        FROM " . $this->bike_price . "
                        JOIN " . $this->bike_rent_days . " ON " . $this->bike_price . ".number_of_days_id = " . $this->bike_rent_days . ".id
                        WHERE bike_id = " . $id . "
                        AND '" . $sel_date . "' BETWEEN valid_from AND valid_to
                        AND number_of_days_id = " . $number_of_days_id;
        }

        $row = $this->db->query($query)->row();

        return(isset($row) && !empty($row)) ? $row : '';
    }

    function getTourDepartureYear() {
        $query = "SELECT DISTINCT(YEAR(STR_TO_DATE(tour_departure_date, '%Y-%m-%d')))  AS years
                        FROM " . $this->tour_departure_detail;
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourDepartureMonth() {
        $query = "SELECT DISTINCT(MONTHNAME(STR_TO_DATE(tour_departure_date, '%Y-%m-%d')))  AS months, MONTH(tour_departure_date) as month_names
                        FROM " . $this->tour_departure_detail . "
                        ORDER BY month_names";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourDepartureNosofDays() {
        $this->db->select('DISTINCT(duration)');
        $this->db->where('status', 1);
        $result = $this->db->get($this->tour)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getTourDeparturebyFilter($year, $month, $days) {
        $query = "SELECT DISTINCT( " . $this->tour . ".name), " . $this->tour . ".code, cover_image, duration, " . $this->tour . ".slug, " . $this->departure_type . " .name as departure_name, tour_departure_date, " . $this->tour_departure_detail . ".id/*, number_of_pax*/
                    FROM " . $this->tour . "
                    JOIN " . $this->tour_departure_detail . "  ON " . $this->tour_departure_detail . ".`tour_id` = " . $this->tour . " .id
                    JOIN " . $this->departure_type . " ON " . $this->departure_type . ".`code` = " . $this->tour . ".`departure_type`
                    /*JOIN " . $this->tour_price . " ON " . $this->tour_departure_detail . " .`tour_departure_date` BETWEEN " . $this->tour_price . ".`valid_from` AND " . $this->tour_price . ".`valid_to`*/
                    WHERE " . $this->tour . ".`status` =  1
                    AND " . $this->tour_departure_detail . ".`status` =  1
                    /*AND " . $this->tour_price . ".`status` =  1*/
                    AND YEAR(tour_departure_date) =  " . $year . "
                    AND MONTH(tour_departure_date) =  " . $month;
        if($days != 0) {
            $query .= " AND duration = '".$days."'";
        }
        $result = $this->db->query($query)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $data[$val->name][] = $val;
            }
        }
        
        return (isset($data) && !empty($data)) ? $data : array();
    }
    /* tours */
    /* bike rent */
    function getBikeHire() {
        $this->db->select('code, name, slug, cover_image, file, short_description');
        $this->db->where('status', 1);
        $this->db->order_by('position');
        $result = $this->db->get($this->bike)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikeCategories() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->bike_category)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikeChargeperDay() {
        $this->db->select('DISTINCT(charge_per_day)');
        $result = $this->db->get($this->bike)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikesbyFilter($search_value, $category_id, $charge) {
        $this->db->select($this->bike . '.name, ' . $this->bike . '.slug, cover_image, ' . $this->bike_category . '.name as bike_category_name');
        $this->db->join($this->bike_category, $this->bike_category . '.id = ' . $this->bike . '.bike_category_id');
        $this->db->where($this->bike . '.status', 1);
        if ($category_id != '0') {
            $this->db->where('bike_category_id', $category_id);
        }
        if ($search_value != '') {
            $this->db->like($this->bike . '.name', $search_value, 'both');
        }
        if ($charge != '0') {
            $this->db->where($this->bike . '.charge_per_day', $charge);
        }
        $this->db->order_by($this->bike . '.id', 'DESC');
        $result = $this->db->get($this->bike)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikeDesc($alias) {
        $this->db->select('id, bike_category_id, code, name, bike_model, size, cost, charge_per_day, cover_image, file, available_quantity, short_description, description, terms');
        $this->db->where('slug', $alias);
        $row = $this->db->get($this->bike)->row();

        if(isset($row) && !empty($row)) {
            $data['info'] = $row;
            $data['gallery'] = $this->getBikeGallery($row->id);
            $data['bike_price'] = $this->getBikePrice($row->id);
            $data['bike_rent_dates'] = $this->getBikeRentDates($row->id);
            $data['bike_rent_days'] = $this->getBikeRentDays();
        }

        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getBikeGallery($bike_id) {
        $this->db->select('image, title, caption');
        $this->db->where('status', 1);
        $this->db->where('bike_id', $bike_id);
        $result = $this->db->get($this->bike_gallery)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikePrice($bike_id) {
        $this->db->select('valid_from, valid_to, unit_price, is_outside_valley, name, ' . $this->bike_rent_days . '.id as days_id');
        $this->db->join($this->bike_rent_days, $this->bike_rent_days . '.id = ' . $this->bike_price . '.number_of_days_id');
        $this->db->where($this->bike_rent_days . ' .status =', 1);
        $this->db->where($this->bike_price . ' .status =', 1);
        $this->db->where('bike_id', $bike_id);
        $result = $this->db->get($this->bike_price)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikeRentDates($bike_id) {
        $current_date = date('Y-m-d');
        $query = "SELECT DISTINCT(tbl_bike_rent_detail.`id`), rent_date, all_allocation, available, booked, is_fixed_departure, name, number_of_days_id, unit_price
                    FROM `tbl_bike_rent_detail`
                    JOIN `tbl_bike_price` ON tbl_bike_rent_detail.`rent_date` BETWEEN tbl_bike_price.`valid_from` AND tbl_bike_price.`valid_to`
                    JOIN `tbl_bike_rent_days` ON tbl_bike_rent_days.`id` = tbl_bike_price.`number_of_days_id`
                    WHERE tbl_bike_rent_detail.`status` = 1
                    AND tbl_bike_price.`status` = 1
                    AND tbl_bike_rent_detail.`rent_date` >= '".$current_date."'
                    AND tbl_bike_rent_detail.`bike_id` = " . $bike_id . "
                    AND tbl_bike_price.`bike_id` = " . $bike_id;
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getBikeRentDays() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->bike_rent_days)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* bike rent */
    /* news */
    function getNews($type, $limit) {
        $this->db->select('name, slug, image');
        $this->db->where('status', 1);
        if($type == 'highlight') {
            $this->db->where('is_highlighted', 1);
            $this->db->order_by('position');
        } else {
            $this->db->order_by('id', 'DESC');
        }
        $this->db->limit($limit);
        $result = $this->db->get($this->news)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getNewsorEventsPublishedYear($type) {
        $query = "SELECT DISTINCT(YEAR(STR_TO_DATE(publish_date, '%Y-%m-%d')))  AS years
                        FROM " . $this->$type . " WHERE status = '1' ORDER BY years DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getNewsEventsbyFilter($search_value, $type, $year, $order) {
        $this->db->order_by('id', $order);
        $this->db->select('name, slug, image');
        if($year != 0) {
            $this->db->where('YEAR(publish_date)', $year);
        }
        if($search_value != '') {
            $this->db->like('name', $search_value, 'both');
        }
        
        $this->db->where('status', 1);

        $result = $this->db->get($this->$type)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getNewsEventsDesc($alias, $type) {
        if($type == 'event') {
            $this->db->select('name, image, short_description, description, publish_date, unpublish_date, google_form_link');
        } else {
            $this->db->select('name, image, short_description, description, publish_date');
        }
        $this->db->where('slug', $alias);
        $result = $this->db->get($this->$type)->row();

        return (isset($result) && !empty($result)) ? $result : '';
    }

    function getNewsEventsBackgroundImage($type) {
        $this->db->select('image');
        if($type == 'event') {
            $this->db->where('menu_id', '9');
        } else {
            $this->db->where('menu_id', '10');
        }
        $this->db->where('status', '1');
        $row = $this->db->get($this->background_image)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }
    /* news */
    /* events */
    function getEvents($type, $limit) {
        $this->db->select('name, slug, image, short_description, event_date');
        $this->db->where('status', 1);
        if($type == 'highlight') {
            $this->db->where('is_highlighted', 1);
            $this->db->order_by('position');
        } else {
            $this->db->order_by('id', 'DESC');
        }
        $this->db->order_by('position');
        $this->db->limit($limit);
        $result = $this->db->get($this->event)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* events */
    /* product / bike accessories */
    function getProductCategories($parent_id = NULL) {
        $this->db->select('id, name, parent_id');
        if($parent_id == 0) {
            $this->db->where('parent_id', $parent_id);
        } else {
            $this->db->where('parent_id !=', 0);
        }
        $result = $this->db->get($this->product_category)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getProductsbyFilter($search_value, $parent_category_id, $child_category_id) {
        $this->db->select($this->product . '.name, ' . $this->product . '.slug, cover_image, ' . $this->product_category . '.name as product_category_name');
        $this->db->join($this->product_category, $this->product_category . '.id = ' . $this->product . '.product_category_id');
        $this->db->where($this->product . '.status', 1);
        if ($parent_category_id != '0') {
            $this->db->where('parent_id', $parent_category_id);
        }
        if ($search_value != '') {
            $this->db->like($this->product . '.name', $search_value, 'both');
        }
        if ($child_category_id != '0') {
            $this->db->where($this->product . '.product_category_id', $child_category_id);
        }
        $this->db->order_by($this->product . '.id', 'DESC');
        $result = $this->db->get($this->product)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getProductDesc($alias) {
        $this->db->select('product_category_id, id, code, name, product_model, normal_price, promotional_price, cover_image, in_stock, is_new, is_special_offer, short_description, description');
        $this->db->where('slug', $alias);
        $row = $this->db->get($this->product)->row();

        if(isset($row) && !empty($row)) {
            $data['info'] = $row;
            $data['gallery'] = $this->getProductGallery($row->id);
        }

        return (isset($data) & !empty($data)) ? $data : array();
    }

    function getProductGallery($product_id) {
        $this->db->select('image, title, caption');
        $this->db->where('product_id', $product_id);
        $result = $this->db->get($this->product_gallery)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* product / bike accessories */
    /* race media */
    function getRaceMedia($race_id) {
        $this->db->select('type, image, title, caption');
        $this->db->where('race_id', $race_id);
        $result = $this->db->get($this->race_media)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $array[$val->type][] = $val;
            }
        }

        return (isset($array) && !empty($array)) ? $array : array();
    }
    /* race media */
    /* form */
    function getFormFields($form_id) {
        $this->db->order_by('position', 'ASC');
        $this->db->where('form_id', $form_id);
        $result = $this->db->get($this->form_field)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function get_form_details($form_id){
        $this->db->where('form_status', 'Published');
        $this->db->where('id', $form_id);
        $row = $this->db->get($this->form)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function get_formElemOrData($formId){
        $array = '';
        $this->db->select('form_field_value, field_label, form_submission_id');
        $this->db->where($this->form_submission . '.form_id', $formId);
        $this->db->where($this->form_field . '.form_id', $formId);
        $this->db->order_by($this->form_field . '.field_order', 'ASC');
        $this->db->join($this->form_field, $this->form_field . '.id = ' .$this->form_submission_fields . '.form_field_id');
        $this->db->join($this->form_submission, $this->form_submission . '.id = ' . $this->form_submission_fields . '.form_submission_id');
        $result = $this->db->get($this->form_submission_fields)->result();
        if(isset($result) && !empty($result)){
            foreach($result as $ind=>$res){
                $array[$res->form_submission_id][$res->field_label] = $res->form_field_value;
            }
        }

        return $array;
    }

    function get_form_data_details($formsubmission_id){
        $this->db->select('form_field_value, field_label');
        $this->db->where($this->form_submission_fields . '.form_submission_id', $formsubmission_id);
        $this->db->join($this->form_field, $this->form_field . '.id = ' . $this->form_submission_fields . '.form_field_id');
        $this->db->join($this->form_submission, $this->form_submission . '.id = ' . $this->form_submission_fields . '.form_submission_id');
        $this->db->order_by($this->form_field . '.field_order', 'ASC');
        $result = $this->db->get($this->form_submission_fields)->result();
        if(isset($result) && !empty($result)){
            foreach($result as $ind=>$res){
                $array[$res->field_label] = $res->form_field_value;
            }
        }
        return $array;
    }
    /* form */
    /* testimonials */
    function getTestimonials($slug = NULL) {
        $this->db->select('customer_full_name, slug, customer_testimonial, customer_image, created_date, iso, printable_name');
        $this->db->join($this->countries, $this->testimonial . '.customer_country_id = ' . $this->countries . '.id');
        $this->db->where('status', 1);
        if(!empty($slug)) {
            $this->db->where('slug', $slug);
        } else {
            $this->db->order_by('position');
        }
        $result = $this->db->get($this->testimonial)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* testimonials */
    /* search */
    function getSearchResult($search_value) {
        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->like('name', $search_value, 'both');
        $bike_result = $this->db->get($this->bike)->result();

        if(isset($bike_result) && !empty($bike_result)) {
            $result['bike-rent'] = $bike_result;
        }

        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->like('name', $search_value, 'both');
        $event_result = $this->db->get($this->event)->result();

        if(isset($event_result) && !empty($event_result)) {
            $result['event'] = $event_result;
        }

        $this->db->select('question as name, slug');
        $this->db->where('status', 1);
        $this->db->like('question', $search_value, 'both');
        $faq_result = $this->db->get($this->faq)->result();

        if(isset($faq_result) && !empty($faq_result)) {
            $result['faq'] = $faq_result;
        }

        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->like('name', $search_value, 'both');
        $news_result = $this->db->get($this->news)->result();

        if(isset($news_result) && !empty($news_result)) {
            $result['news'] = $news_result;
        }

        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->like('name', $search_value, 'both');
        $product_result = $this->db->get($this->product)->result();

        if(isset($product_result) && !empty($product_result)) {
            $result['bike-accessories'] = $product_result;
        }

        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->like('name', $search_value, 'both');
        $tour_result = $this->db->get($this->tour)->result();

        if(isset($tour_result) && !empty($tour_result)) {
            $result['tour'] = $tour_result;
        }

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* search */
    /* load more */
    function getMoreData($type, $position, $ids) {
        if($type == 'nepalbiketours') {
            $type = 'tour';
        } else if($type == 'bike-rent') {
            $type = 'bike';
        } else if($type == 'bike-accessories') {
            $type = 'product';
        }
        if($type == 'news') {
            $this->db->select('id, name,slug as alias, image as image');
        } else if($type == 'event') {
            $this->db->select('id, name, slug as alias, image as image');
        } else if($type == 'tour') {
            $this->db->select('id, name, slug as alias, cover_image as image, code, duration, departure_type');
        } else if($type == 'bike') {
            $this->db->select('id, name, slug as alias, cover_image as image, code, charge_per_day');
        } else if($type == 'product') {
            $this->db->select('id, name, slug as alias, cover_image as image, code, product_model');
        }
        $this->db->where('status', 1);
        $this->db->limit(8);

        if(!empty($ids)) {
            $this->db->where_not_in($this->$type . '.id', explode(',', $ids));
        }

        $result = $this->db->get($this->$type)->result();
        
        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* load more */
    /* nationality */
    function getCountries() {
        $this->db->select('id, name, printable_name');
        $result = $this->db->get($this->countries)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* nationality */
    function getContent($content_slug) {
        $this->db->select('long_description');
        $this->db->where('slug', $content_slug);
        $result = $this->db->get($this->content)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getLogos() {
        $this->db->select('name, image, url');
        $this->db->where('status', 1);
        $this->db->order_by('position');
        $result = $this->db->get($this->logos)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function insert_data() {
        $insert_data['type']            = $this->input->post('filetype');
        $insert_data['type_id']         = $this->input->post('fileid');
        $insert_data['full_name']       = $this->input->post('name');
        $insert_data['email']   	= $this->input->post('email');
        $insert_data['download_date']   = time();
        $this->db->insert("tbl_download_detail", $insert_data);

        if($this->db->affected_rows() == 1) {
            return 'success';
        }
        return 'error';
    }
}
?>