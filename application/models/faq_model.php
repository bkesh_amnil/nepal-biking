<?php

class Faq_Model extends My_Model
{

	protected $table = 'tbl_faq';

	public $id = '',
			$question = '',
			$slug = '',
			$answer = '',
			$status = '';

	public function __construct()
	{
		parent::__construct();
		$this->created_timestamp = true;
		$this->updated_timestamp = true;
		$this->created_by = true;
		$this->updated_by = true;
	}

	public function rules($id)
	{
		$array = array(
				array(
						'field' => 'question',
						'label' => 'Question',
						'rules' => 'trim|required|unique[tbl_faq.question.'.$id.']',
				),
				array(
						'field' => 'slug',
						'label' => 'Slug',
						'rules' => 'trim|required|unique[tbl_faq.slug.'.$id.']',
				),
				array(
						'field' => 'answer',
						'label' => 'Answer',
						'rules' => 'trim|required|xss_clean',
				)
		);

		return $array;
	}

}
?>