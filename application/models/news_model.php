<?php
	class News_model extends My_Model {

		protected $table = 'tbl_news';

		public $id = '',
				$name = '',
				$slug = '',
				$image = '',
				//$background_image = '',
				$short_description = '',
				$description='',
				$publish_date = '',
				$unpublish_date='',
				$is_highlighted = '',
				$meta_keywords = '',
				$meta_description = '',
				$status = '';

		public function __construct()
		{
			parent::__construct();
			$this->created_timestamp = true;
			$this->updated_timestamp = true;
			$this->created_by = true;
			$this->updated_by = true;
		}

		public function rules($id)
		{
			$array = array(
					array(
							'field' => 'name',
							'label' => 'Ttile',
							'rules' => 'trim|required|xss_clean|unique[tbl_news.name.'.$id.']',
					),
					array(
							'field' => 'slug',
							'label' => 'Alias',
							'rules' => 'trim|required|xss_clean|unique[tbl_news.slug.'.$id.']',
					),
					array(
						'field' => 'image',
						'label' => 'Image',
						'rules' => 'required|valid_size['.$this->table.'.image]',
					),
					/*array(
		                'field' => 'background_image',
		                'label' => 'Background Image',
		                'rules' => 'required|valid_size['.$this->table.'.background_image]'
		            ),*/
					array(
							'field' => 'publish_date',
							'label' => 'News Publish Date',
							'rules' => 'required',
					),
					array(
							'field' => 'unpublish_date',
							'label' => 'News Unpublish Date',
							'rules' => 'required',
					),
					array(
							'field' => 'description',
							'label' => 'News Description',
							'rules' => 'required',
					),
					array(
							'field' => 'is_highlighted',
							'label' => 'News Is Highlighted',
							'rules' => 'trim|required',
					),
					array(
							'field' => 'status',
							'label' => 'Status',
							'rules' => 'trim|required',
					)
			);

			return $array;
		}
	}
?>