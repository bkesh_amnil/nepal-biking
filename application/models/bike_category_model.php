<?php

class Bike_Category_Model extends My_Model
{

    protected $table = 'tbl_bike_category';
    var $bike_rent = 'tbl_bike_rent';

    public $id = '',
        $name = '',
        $slug = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique['.$this->table.'.slug.'.$id.']',
            )
        );

        return $array;
    }

    public function getBikeCategories() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getBikeRentCategory() {
        $this->db->select('DISTINCT(' . $this->table . '.id), name');
        $this->db->join($this->bike_rent, $this->bike_rent . '.bike_category_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }



}