<?php

class Import_form_data_model extends My_Model
{
    var $event = 'tbl_event';
    var $race = 'tbl_race';

    public function getEventsRacewitForm($type) {
        $this->db->select('id, name');
        $this->db->where('google_form_link !=', '');
        $this->db->where('status', 1);
        $result = $this->db->get($this->$type)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}
?>