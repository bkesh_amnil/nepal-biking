<?php

class Bike_book_Model extends My_Model
{

    protected $table = 'tbl_bike_rent';
    var $bike = 'tbl_bike';
    var $bike_rent_detail = 'tbl_bike_rent_detail';
    var $booking_status = 'tbl_booking_status';

    public $id = '',
        $bike_category_id = '',
        $bike_id = '',
        $customer_remarks = '',
        $last_booking_status_id = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'bike_category_id',
                'label' => 'Bike Category',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'bike_id',
                'label' => 'Bike',
                'rules' => 'trim|required|integer',
            )
        );

        return $array;
    }

    public function getBikeBookings() {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_bike_rent`.`id`,
                      `tbl_bike_rent`.`created_date`,
                      `tbl_bike_rent`.`created_by`,
                      `tbl_bike`.`name` AS bike_name,
                      `customer_full_name`,
                      `rent_date`,
                      `number_of_pax`,
                      `price_per_person`,
                      `total_price`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_bike_rent_status.id AS booking_id,
                      bike_rent_id
                    FROM (`tbl_bike_rent`)
                      JOIN `tbl_bike_rent_status`
                        ON `tbl_bike_rent_status`.`bike_rent_id` = `tbl_bike_rent`.`id`
                      JOIN `tbl_bike`
                        ON `tbl_bike`.`id` = `tbl_bike_rent`.`bike_id`
                      JOIN `tbl_bike_rent_detail`
                        ON `tbl_bike_rent_detail`.`id` = `tbl_bike_rent_status`.`rent_date_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_bike_rent_status`.`bike_booking_status_id`
                        ORDER BY tbl_bike_rent_status.id DESC)AS t
                   GROUP BY t.bike_rent_id
                   ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExportData($field_type, $field_value) {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_bike_rent`.`id`,
                      `tbl_bike_rent`.`bike_category_id`,
                      `tbl_bike_rent`.`bike_id`,
                      `tbl_bike_rent`.`last_booking_status_id`,
                      `tbl_bike_rent`.`created_date`,
                      `tbl_bike_rent`.`created_by`,
                      `tbl_bike_category`.`name` AS bike_category_name,
                      `tbl_bike`.`name` AS bike_name,
                      `tbl_bike`.`code`,
                      `customer_full_name`,
                      `customer_email_address`,
                      `customer_contact_number`,
                      `customer_contact_address`,
                      `customer_nationality`,
                      `customer_passport_number`,
                      `customer_dob`,
                      `customer_local_address`,
                      `customer_room_number`,
                      `rent_date`,
                      `number_of_pax`,
                      `number_of_days_id`,
                      `price_per_person`,
                      `total_price`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_bike_rent_status.id AS booking_id,
                      bike_rent_id
                    FROM (`tbl_bike_rent`)
                      JOIN `tbl_bike_rent_status`
                        ON `tbl_bike_rent_status`.`bike_rent_id` = `tbl_bike_rent`.`id`
                      JOIN `tbl_bike`
                        ON `tbl_bike`.`id` = `tbl_bike_rent`.`bike_id`
                      JOIN `tbl_bike_category`
                        ON `tbl_bike_category`.`id` = `tbl_bike_rent`.`bike_category_id`
                      JOIN `tbl_bike_rent_detail`
                        ON `tbl_bike_rent_detail`.`id` = `tbl_bike_rent_status`.`rent_date_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_bike_rent_status`.`bike_booking_status_id`
                        ORDER BY tbl_bike_rent_status.id DESC)AS t";

        if($field_type == 'bike_category') {
            $query .= " WHERE t.`bike_category_id` = " . $field_value;
        } else if($field_type == 'bike') {
            $query .= " WHERE t.`bike_id` = " . $field_value;
        } else if($field_type == 'customer_name') {
            $query .= " WHERE t.`customer_full_name` = '".$field_value."'";
        } else if($field_type == 'rent_status') {
            $query .= " WHERE t.`last_booking_status_id` = ". $field_value;
        } /*else if($field_type == 'rent_days') {
            $query .= " WHERE t.booking_date = '".$field_value."'";
        }*/ else if($field_type == 'rent_date' && !empty($field_value)) {
            $query .= " WHERE t.`rent_date` = '".$field_value."'";
        }

        $query .= " GROUP BY t.bike_rent_id
                   ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}