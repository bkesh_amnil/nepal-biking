<?php

class Product_Model extends My_Model
{

    protected $table = 'tbl_product';
    var $product_category = 'tbl_product_category';
    var $product_order = 'tbl_product_order';

    public $id = '',
        $product_category_id = '',
        $code = '',
        $name = '',
        $slug = '',
        $cover_image = '',
        $product_model = '',
        $normal_price = '',
        $promotional_price = '',
        $in_stock = '',
        $is_new = '',
        $is_special_offer = '',
        $short_description = '',
        $description = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if(empty($id)) {
            $array = array(
                array(
                    'field' => 'product_category_id',
                    'label' => 'Product Category',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|unique[' . $this->table . ' .code.' . $id . ']',
                ),
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[' . $this->table . ' .name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[' . $this->table . ' .slug.' . $id . ']',
                ),
                array(
                    'field' => 'product_model',
                    'label' => 'Product Model',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'normal_price',
                    'label' => 'Normal Price',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'promotional_price',
                    'label' => 'Promotional Price',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'in_stock',
                    'label' => 'In Stock',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'is_new',
                    'label' => 'Is New',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'is_special_offer',
                    'label' => 'Is Special Offer',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cover_image',
                    'label' => 'Cover Image',
                    'rules' => 'trim|required|valid_size['.$this->table.'.cover_image]',
                ),
                array(
                    'field' => 'short_description',
                    'label' => 'Short Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'product_category_id',
                    'label' => 'Product Category',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|unique[' . $this->table . ' .code.' . $id . ']',
                ),
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[' . $this->table . ' .name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[' . $this->table . ' .slug.' . $id . ']',
                ),
                array(
                    'field' => 'product_model',
                    'label' => 'Product Model',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'normal_price',
                    'label' => 'Normal Price',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'promotional_price',
                    'label' => 'Promotional Price',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'in_stock',
                    'label' => 'In Stock',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'is_new',
                    'label' => 'Is New',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'is_special_offer',
                    'label' => 'Is Special Offer',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'short_description',
                    'label' => 'Short Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cover_image',
                    'label' => 'Cover Image',
                    'rules' => 'valid_size['.$this->table.'.cover_image]',
                )
            );
        }

        return $array;
    }

    public function getProducts() {
        $this->db->select($this->table . '.*, ' . $this->product_category . '.name as category_name');
        $this->db->join($this->product_category, $this->product_category . '.id = ' . $this->table . '.product_category_id');
        $this->db->order_by('position');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getProductModels() {
        $this->db->select('product_model');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getProductDetailfromCategory($search_id, $type) {
        if($type == 'product_category_id') {
            $this->db->select('id, name, product_model, promotional_price');
            $this->db->where('status', 1);
            $this->db->where('product_category_id', $search_id);
            $result = $this->db->get($this->table)->result();

            return (isset($result) && !empty($result)) ? $result : array();
        } else {
            $this->db->select('product_model, promotional_price');
            $this->db->where('id', $search_id);
            $row = $this->db->get($this->table)->row();

            return (isset($row) && !empty($row)) ? $row : '';
        }

    }

    public function getProductOrderNames(){
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->product_order, $this->product_order . '.product_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}