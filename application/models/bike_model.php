<?php

class Bike_Model extends My_Model
{

    protected $table = 'tbl_bike';
    var $bike_rent = 'tbl_bike_rent';

    public $id = '',
        $bike_category_id = '',
        $code = '',
        $name = '',
        $slug = '',
        $bike_model = '',
        $cover_image = '',
        $file = '',
        $available_quantity = '',
        $size = '',
        $cost = '',
        $charge_per_day = '',
        $short_description = '',
        $description = '',
        $terms = '',
        $rent_type = '',
        $rent_year = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if(empty($id)) {
            $array = array(
                array(
                    'field' => 'bike_category_id',
                    'label' => 'Bike Category',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|unique[' . $this->table . ' .code.' . $id . ']',
                ),
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[' . $this->table . ' .name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[' . $this->table . ' .slug.' . $id . ']',
                ),
                array(
                    'field' => 'bike_model',
                    'label' => 'Bike Model',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'available_quantity',
                    'label' => 'Available Quantity',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'size',
                    'label' => 'Size',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cost',
                    'label' => 'Bike Cost',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'charge_per_day',
                    'label' => 'Charge per Day',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cover_image',
                    'label' => 'Cover Image',
                    'rules' => 'trim|required|valid_size['.$this->table.'.cover_image]',
                ),
                array(
                    'field' => 'file',
                    'label' => 'File',
                    'rules' => 'trim|required|valid_file['.$this->table.'.file]',
                ),
                array(
                    'field' => 'rent_type',
                    'label' => 'Rent Type',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'rent_year[]',
                    'label' => 'Rent Year',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'short_description',
                    'label' => 'Short Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'terms',
                    'label' => 'Terms',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'bike_category_id',
                    'label' => 'Bike Category',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|unique[' . $this->table . ' .code.' . $id . ']',
                ),
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[' . $this->table . ' .name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[' . $this->table . ' .slug.' . $id . ']',
                ),
                array(
                    'field' => 'bike_model',
                    'label' => 'Bike Model',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'available_quantity',
                    'label' => 'Available Quantity',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'size',
                    'label' => 'Size',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cost',
                    'label' => 'Bike Cost',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'charge_per_day',
                    'label' => 'Charge per Day',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'short_description',
                    'label' => 'Short Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'terms',
                    'label' => 'Terms',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cover_image',
                    'label' => 'Cover Image',
                    'rules' => 'valid_size['.$this->table.'.cover_image]',
                ),
                array(
                    'field' => 'file',
                    'label' => 'File',
                    'rules' => 'trim|required|valid_file['.$this->table.'.file]',
                )
            );
        }

        return $array;
    }

    public function getBikes($bike_category_id = NULL) {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        if(!empty($bike_category_id)) {
            $this->db->where('bike_category_id', $bike_category_id);
        }
        $this->db->order_by('id', 'ASC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getBikeRentNames() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->bike_rent, $this->bike_rent . '.bike_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getBikeRentDates($bike_id) {
        $current_date = date('Y-m-d');
        $query = "SELECT DISTINCT(tbl_bike_rent_detail.`id`), rent_date, all_allocation, available, booked, is_fixed_departure
                    FROM `tbl_bike_rent_detail`
                    JOIN `tbl_bike_price` ON tbl_bike_rent_detail.`rent_date` BETWEEN tbl_bike_price.`valid_from` AND tbl_bike_price.`valid_to`
                    JOIN `tbl_bike_rent_days` ON tbl_bike_rent_days.`id` = tbl_bike_price.`number_of_days_id`
                    WHERE tbl_bike_rent_detail.`status` = 1
                    AND tbl_bike_price.`status` = 1
                    AND tbl_bike_rent_detail.`rent_date` >= '".$current_date."'
                    AND tbl_bike_rent_detail.`bike_id` = " . $bike_id . "
                    AND tbl_bike_price.`bike_id` = " . $bike_id;
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}