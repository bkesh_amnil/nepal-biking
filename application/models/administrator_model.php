<?php

class Administrator_model extends My_Model
{

    public function get_module_contents($siteId, $moduleId, $position, $selected = '', $category = '', $selectAll = TRUE)
    {
        if(empty($siteId) || empty($moduleId))
        {
            return false;
        }

        $selected = explode(',', $selected);

        $moduleName = $this->global_model->get_single_data('tbl_module', 'slug', $moduleId, 'id');

        $fld = "name";

        $category_id = $category;

        /*if(!empty($category) && $category != "all"){
            $this->db->where('slug', $category);
            $result = $this->db->get('tbl_category')->row();
            if(isset($result->id) && !empty($result->id )){
                $category_id = $result->id;
            }
        }*/

        switch($moduleName){
            case "menu":
                $tblName = "tbl_menu";
                break;
            case "content":
                $tblName = "tbl_content";
                break;
            case "gallery":
                $tblName = "tbl_gallery";
                break;
            case "banner":
                $tblName = "tbl_banner";
                break;
            case "news":
                $tblName = "tbl_news";
                break;
            case "advertisement":
                $tblName = "tbl_advertisement";
                break;
            case "dynamic_form":
                $tblName = "tbl_form";
                $fld = "form_title";
                break;
            case "form_fields":
                $tblName = "tbl_form";
                $fld = "form_title";
                break;
            case "video":
                $tblName = "tbl_video";
                break;
            case "race":
                $tblName = "tbl_race";
                break;
            default:
                $tblName = "";
        }

        if($tblName == ""){
            return false;
        }

        //$this->db->where('site_id', $siteId);
        if($tblName == "form")
        {
            $this->db->where('form_status', 'Published');
        }
        else if($tblName == "menu")
        {
            $this->db->select('menu.*');
            $this->db->where('menu_parent', '0');
            $this->db->join('site_menus', "site_menus.id = menu.menu_type_id");
            $this->db->where('menu.status', '1');
        }
        else
        {
            $this->db->where('status', '1');
        }

        if(!empty($category_id)){
            if($category_id != "0"){
                /*$this->db->where("(category_id = '".$category_id."' OR category_id = '')");
            }else{*/
                $this->db->where('category_id', $category_id);
            }
        }
        $data = $this->db->get($tblName)->result();

        $result = '';

        if(is_array($data) && !empty($data)){
            $result = '<ul class="contentListModules">';
            if(!empty($category) && $category != "0"){
                if($selectAll == TRUE){
                    $allchecked = 'checked="checked"';
                    $allselClass = 'class="selectedRow"';
                }else{
                    $allchecked = 'checked="checked"';
                    $allselClass = 'class="selectedRow"';
                }
                $result .= '<li '.$allselClass.' style="width:865px"><input type="checkbox" name="allSelectedModuleContent['.$position.']" value="true" class="selectAllWithinCategory selectAllModuleContent_'.$position.'"' . $allchecked .'  />All Within Category</li>';
            }else{
                $selectAll = FALSE;
            }
            foreach($data as $ind=>$val){
                if(in_array($val->id, $selected))
                {
                    $checked = 'checked="checked"';
                    $selClass = 'class="selectedRow"';
                }else{
                    if(!empty($category_id)){
                        $checked = 'checked="checked"';
                        $selClass = 'class="selectedRow"';
                    }else{
                        $checked = '';
                        $selClass = '';
                    }
                }
                if($selectAll == TRUE){
                    $hidden = ' style="display:none"';
                }else{
                    $hidden = '';
                }
                $result .= '<li '.$selClass.' '.$hidden.' title="'. $val->id . '"><input type="checkbox" name="selectedModuleContent['.$position.'][]" value="'.$val->id.'" class="selectedModuleContent_'.$position.'"' . $checked .'  /> '.$val->$fld."</li>";
            }
            $result .= "</ul>";
        }
        if(empty($result)){
            $result = "No Content Available Within This Module";
        }
        echo $result;
    }

    public function getAvailableMenus($menuType, $menu_parent='', $level = 0){
        if(empty($menuType)){
            $options = "";
        }else{
            $this->db->where("id", $menuType);
            $settingData = $this->db->get('tbl_site_menus')->row();
            if($settingData->submenus == "yes"){
                if($settingData->depth == 0){
                    $depth = 999;
                }else{
                    $depth = $settingData->depth;
                }
                $options = $this->getMenus($settingData->id, $depth);
            }else{
                $options = "";
            }
        }
        $html = '<option value="">Parent</option>';
        if(!empty($options) && is_array($options)){
            $html .= $this->generateHtml($options, $menu_parent);

        }
        return $html;
    }

    public function getMenus($typeId, $depth = 0, $parent = 0){
        if($depth == 0){
            return false;
        }
        $this->db->where('menu_type_id', $typeId);
        $this->db->where('status', "1");
        $this->db->where('menu_parent', $parent);
        $menuData = $this->db->get('tbl_menu')->result();
        if(!empty($menuData) && is_array($menuData)){
            foreach($menuData as $ind=>$val){
                $data[] = array("id"=>$val->id, "name"=>$val->name, "depth"=>$depth, "childs"=>$this->getMenus($typeId, ($depth - 1), $val->id));
            }
        }
        if(!empty($data) && is_array($data)){
            return $data;
        }
        return false;
    }

    function generateHtml($options, $menu_parent, $maxDepth = 0){
        $html = "";
        foreach($options as $ind=>$val){
            $depth = $val['depth'];
            if($maxDepth == 0){
                $maxDepth = $depth;
            }
            $actDepth = $maxDepth - $depth;
            $spaces = "";
            for($i = 0; $i < $actDepth; $i++){
                $spaces .= '&nbsp;';
            }
            if($val['id'] == $menu_parent){
                $selected = 'selected="selected"';
            }else{
                $selected = '';
            }
            $html .= '<option value="'.$val['id'].'" '.$selected.'>'.$spaces.$val['name'].'</option>';
            if(!empty($val['childs']) && is_array($val['childs'])){
                $html .= $this->generateHtml($val['childs'], $menu_parent, $maxDepth);
            }
        }
        return $html;
    }
}
?>