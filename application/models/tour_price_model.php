<?php

class Tour_Price_Model extends My_Model
{

    protected $table = 'tbl_tour_price';

    public $id = '',
        $tour_id = '',
        $description = '',
        $valid_from = '',
        $valid_to = '',
        $number_of_pax = '',
        $min_pax = '',
        $max_pax = '',
        $unit_price = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if($id == 0) {
            $array = array(
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'valid_from',
                    'label' => 'Valid From',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'valid_to',
                    'label' => 'Valid To',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'number_of_pax',
                    'label' => 'Number of Pax',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'unit_price',
                    'label' => 'Unit Price',
                    'rules' => 'trim|required',
                ),
            );
        }
        return $array;
    }

    public function getTourPrice($tour_id) {
        $this->db->select('id, tour_id, description, valid_from, valid_to, number_of_pax, unit_price, status');
        $this->db->where('tour_id', $tour_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}