<?php

class Race_Model extends My_Model
{

    protected $table = 'tbl_race';

    public $id = '',
        $name = '',
        $slug = '',
        $venue = '',
        $start_point = '',
        $start_point_latitude = '',
        $start_point_longitude = '',
        $finish_point = '',
        $finish_point_latitude = '',
        $finish_point_longitude = '',
        $start_date = '',
        $end_date = '',
        $cover_image = '',
        $backgroud_image = '',
        $entry_fee = '',
        $categories = '',
        $events = '',
        $description = '',
        $itinerary = '',
        $contact = '',
        $youtube_link = '',
        $google_form_link = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[' . $this->table . '.name.' . $id . ']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique[' . $this->table . '.slug.' . $id . ']',
            ),
            array(
                'field' => 'venue',
                'label' => 'Venue',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'start_point',
                'label' => 'Start Point',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'start_point_latitude',
                'label' => 'Start Point Latitude',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'start_point_longitude',
                'label' => 'Start Point Longitude',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'finish_point',
                'label' => 'Finish Point',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'finish_point_latitude',
                'label' => 'Finish Point Latitude',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'finish_point_longitude',
                'label' => 'Finish Point Longitude',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'start_date',
                'label' => 'Start Date',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'cover_image',
                'label' => 'Cover Image',
                'rules' => 'valid_size['.$this->table.'.cover_image]',
            ),
            array(
                'field' => 'backgroud_image',
                'label' => 'Background Image',
                'rules' => 'valid_size['.$this->table.'.backgroud_image]'
            ),
            array(
                'field' => 'entry_fee',
                'label' => 'Entry Fees',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'categories',
                'label' => 'Categories',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'events',
                'label' => 'Events',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'itinerary',
                'label' => 'Itinerary',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'contact',
                'label' => 'Contact',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'youtube_link',
                'label' => 'Youtube Link',
                'rules' => 'trim|required|prep_url',
            )
        );

        return $array;
    }

}