<?php

class Bike_book_status_Model extends My_Model
{

    protected $table = 'tbl_bike_rent_status';
    var $bike = 'tbl_bike';
    var $bike_rent_detail = 'tbl_bike_rent_detail';
    var $booking_status = 'tbl_booking_status';
    var $bike_rent = 'tbl_bike_rent';

    public $id = '',
        $bike_rent_id = '',
        $rent_date_id = '',
        $bike_available = '',
        $number_of_pax = '',
        $number_of_days_id = '',
        $price_per_person = '',
        $total_price = '',
        $customer_full_name = '',
        $customer_email_address = '',
        $customer_contact_number = '',
        $customer_contact_address = '',
        $customer_nationality = '',
        $customer_passport_number ='',
        $customer_dob = '',
        $customer_local_address = '',
        $customer_room_number ='',
        $bike_booking_status_id = '',
        $bike_note = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'rent_date_id',
                'label' => 'Rent Date',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'number_of_pax',
                'label' => 'Number of pax',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'price_per_person',
                'label' => 'Price per Person',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'total_price',
                'label' => 'Total Cost',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_full_name',
                'label' => 'Full Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'customer_contact_number',
                'label' => 'Contact Address',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_contact_address',
                'label' => 'Contact Nos',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getSavedData($bike_rent_id, $type) {
        $this->db->select($this->table . '.*, ' . $this->bike . '.name as bike_name, ' . $this->bike_rent_detail . '.rent_date, all_allocation,available, booked, ' . $this->booking_status . '.name as booking_status');
        $this->db->join($this->bike_rent, $this->bike_rent. '.id = ' . $this->table . '.bike_rent_id');
        $this->db->join($this->bike, $this->bike . '.id = '. $this->bike_rent . '.bike_id');
        $this->db->join($this->bike_rent_detail, $this->bike_rent_detail . '.id = ' . $this->table . '.rent_date_id');
        $this->db->join($this->booking_status, $this->booking_status . '.id = ' . $this->table . '.bike_booking_status_id');
        $this->db->where('bike_rent_id', $bike_rent_id);
        $this->db->order_by('id', 'DESC');
        if($type == 'logs') {
            $result = $this->db->get($this->table)->result();

            return (isset($result) && !empty($result)) ? $result : array();
        } else {
            $this->db->limit(1);
            $row = $this->db->get($this->table)->row();

            return (isset($row) && !empty($row)) ? $row : '';
        }
    }

    public function getBikeRentCustomers() {
        $this->db->select('DISTINCT(customer_full_name)');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}