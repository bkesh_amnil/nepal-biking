<?php

class Category_Model extends My_Model
{

    protected $table = 'tbl_category';

    public $id = '',
            $name = '',
            $slug = '',
            $parent_category = '0',
            $status = '',
            $is_default = '0';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public $rules =
        array(
            array(
                'field' => 'name',
                'label' => 'Category Name',
                'rules' => 'trim|required|callback_unique_category',
            )
        );

    function get_categories_list($parent_cat = 0, $status = "all"){
        $this->db->where('parent_category', $parent_cat);
        if($status != "all"){
            $this->db->where('status', $status);
        }
        $this->db->order_by('is_default', 'ASC');
        $this->db->order_by('name', 'ASC');
        $result = $this->db->get($this->table)->result();

        $catList = false;
        if(!empty($result)){
            foreach($result as $ind=>$val){
                $dataArr = array();
                foreach($val as $index=>$value){
                    $dataArr[$index] = $value;
                }
                if(!empty($dataArr)){
                    $dataArr["childs"] = $this->get_categories_list($val->id, $status);
                }
                $dataArr = (object) $dataArr;
                $catList[] = $dataArr;
            }
        }
        return $catList;
    }

    function get_all_categories(){
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $this->db->order_by('is_default', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}