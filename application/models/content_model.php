<?php

class Content_Model extends My_Model
{

    protected $table = 'tbl_content';

    public $id = '',
            $name = '',
            $slug = '',
            $category_id = '',
            $short_description = '',
            $long_description = '',
            $image = '',
            $background_image = '',
            $meta_keywords = '',
            $meta_description = '',
            $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[tbl_content.name.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique[tbl_content.slug.'.$id.']',
            ),
            array(
                'field' => 'category_id',
                'label' => 'Category',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'long_description',
                'label' => 'Long Description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'valid_size['.$this->table.'.image]',
            ),
            array(
                'field' => 'background_image',
                'label' => 'Background Image',
                'rules' => 'valid_size['.$this->table.'.background_image]'
            ),

        );

        return $array;
    }

}