<?php

class Tour_category_Model extends My_Model
{

    protected $table = 'tbl_tour_category';
    var $tour_booking = 'tbl_tour_booking';
    var $tour_enquire = 'tbl_tour_enquire';

    public $id = '',
        $name = '',
        $slug = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique['.$this->table.'.slug.'.$id.']',
            )
        );

        return $array;
    }

    public function getTourCategories() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getTourBookingCategory() {
        $this->db->select('DISTINCT(' . $this->table . '.id), name');
        $this->db->join($this->tour_booking, $this->tour_booking . '.tour_category_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getTourEnquireCategory() {
        $this->db->select('DISTINCT(' . $this->table . '.id), name');
        $this->db->join($this->tour_enquire, $this->tour_enquire . '.tour_category_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}