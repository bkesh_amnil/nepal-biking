<?php

class Image_manager_Model extends My_Model
{

    protected $table = 'tbl_background_image';
    var $menu = 'tbl_menu';

    public $id = '',
        $menu_id = '',
        $image = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'menu_id',
                'label' => 'Menu',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                /*'rules' => 'trim|required|valid_size['.$this->table.'.image]',*/
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getData() {
        $this->db->select($this->table . '.*, ' . $this->menu . '.name');
        $this->db->join($this->menu, $this->menu . '.id = ' . $this->table . '.menu_id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}