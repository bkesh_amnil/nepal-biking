<?php

class Gallery_Model extends My_Model
{

    protected $table = 'tbl_gallery';

    public $id = '',
            $name = '',
            $slug = '',
            $category_id = '',
            $cover = '',
            $meta_keywords = '',
            $meta_description = '',
            $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if(empty($id)) {
            $array = array(
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[tbl_gallery.name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[tbl_gallery.slug.' . $id . ']',
                ),
                array(
                    'field' => 'cover',
                    'label' => 'Cover Image',
                    'rules' => 'required|valid_size['.$this->table.'.cover]',
                ),
                array(
                'field' => 'image',
                'label' => 'Images',
                'rules' => 'required',
                )
            );
        } else {
            $array = array(
                array(
                    'field' => 'name',
                    'label' => 'Gallery Name',
                    'rules' => 'trim|required|unique[tbl_gallery.name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[tbl_gallery.slug.' . $id . ']',
                ),
                array(
                    'field' => 'cover',
                    'label' => 'Cover Image',
                    'rules' => 'valid_size['.$this->table.'.cover]',
                )
            );
        }

        return $array;
    }

    public function getSavedMedia($id)
    {
        $query = "select *
                    from `tbl_gallery_media` gm
                    where gm.`gallery_id` = $id";

        return $this->query($query);
    }

}