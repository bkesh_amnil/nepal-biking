<?php

class Bike_Rent_Model extends My_Model
{

    protected $table = 'tbl_bike_rent_days';
    var $bike_rent_status = 'tbl_bike_rent_status';

    public $id = '',
        $name = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[' . $this->table . '.name.' . $id . ']',
            )
        );

        return $array;
    }

    public function getBikeRentDays() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getRentDays() {
        $this->db->select('DISTINCT(' . $this->table . '.id), name');
        $this->db->join($this->bike_rent_status, $this->bike_rent_status . '.bike_booking_status_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}