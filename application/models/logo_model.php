<?php

class Logo_Model extends My_Model
{

    protected $table = 'tbl_logos';

    public $id = '',
        $name = '',
        $url = '',
        $image = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

}