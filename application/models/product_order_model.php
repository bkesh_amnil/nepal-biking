<?php

class Product_order_Model extends My_Model
{

    protected $table = 'tbl_product_order';
    var $product = 'tbl_product';

    public $id = '',
        $product_category_id = '',
        $product_id = '',
        $product_series = '',
        $product_model = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'product_category_id',
                'label' => 'Product Category',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'product_id',
                'label' => 'Product',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'customer_full_name',
                'label' => 'Full Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'customer_email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'customer_contact_number',
                'label' => 'Contact Nos.',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    function getProductOrder() {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_product_order`.`id`,
                      `tbl_product_order`.`created_date`,
                      `tbl_product_order`.`created_by`,
                      `tbl_product`.`name` AS product_name,
                      `customer_full_name`,
                      `product_quantity`,
                      `product_price`,
                      `total_price`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_product_order_status.id AS order_id,
                      product_order_id
                    FROM (`tbl_product_order`)
                      JOIN `tbl_product_order_status`
                        ON `tbl_product_order_status`.`product_order_id` = `tbl_product_order`.`id`
                      JOIN `tbl_product`
                        ON `tbl_product`.`id` = `tbl_product_order`.`product_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_product_order_status`.`product_order_status_id`
                        ORDER BY tbl_product_order_status.id DESC)AS t
                    GROUP BY t.product_order_id
                    ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExportData($field_type, $field_value) {
        $query = "SELECT * FROM
                    (SELECT
                      DATE_FORMAT(FROM_UNIXTIME(`tbl_product_order`.`created_date`), '%Y-%m-%d') AS order_date,
                      `tbl_product_order`.`id`,
                      `tbl_product_order`.`product_category_id`,
                      `tbl_product_order`.`product_id`,
                      `tbl_product_order`.`created_date`,
                      `tbl_product_order`.`created_by`,
                      `tbl_product_order`.`last_booking_status_id`,
                      `tbl_product_order`.`product_model`,
                      `tbl_product_category`.`name` AS product_category_name,
                      `tbl_product`.`name` AS product_name,
                      `tbl_product`.`code`,
                      `customer_full_name`,
                      `customer_email_address`,
                      `customer_contact_number`,
                      `customer_special_instruction`,
                      `product_quantity`,
                      `product_price`,
                      `total_price`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_product_order_status.id AS order_id,
                      product_order_id
                    FROM (`tbl_product_order`)
                      JOIN `tbl_product_order_status`
                        ON `tbl_product_order_status`.`product_order_id` = `tbl_product_order`.`id`
                      JOIN `tbl_product_category`
                        ON `tbl_product_category`.`id` = `tbl_product_order`.`product_category_id`
                      JOIN `tbl_product`
                        ON `tbl_product`.`id` = `tbl_product_order`.`product_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_product_order_status`.`product_order_status_id`
                        ORDER BY tbl_product_order_status.id DESC)AS t";

        if($field_type == 'product_category') {
            $query .= " WHERE t.`product_category_id` = " . $field_value;
        } else if($field_type == 'product') {
            $query .= " WHERE t.`product_id` = " . $field_value;
        } else if($field_type == 'customer_name') {
            $query .= " WHERE t.`customer_full_name` = '".$field_value."'";
        } else if($field_type == 'booking_status') {
            $query .= " WHERE t.`last_booking_status_id` = ". $field_value;
        } else if($field_type == 'order_date' && !empty($field_value)) {
            $query .= " WHERE t.order_date = '".$field_value."'";
        }

        $query .= " GROUP BY t.product_order_id
                    ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}