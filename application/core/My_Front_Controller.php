<?php

class My_Front_Controller extends CI_Controller
{

    public $template = 'front/layout/default';
    public $data = array();
    public $global_config;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('configuration_model', 'configuration');
        $this->global_config = $this->configuration->get(1);

        define('SITENAME', $this->global_config->site_title);
        define('SITEMAIL', $this->global_config->site_from_email);

        $this->data['meta_title'] = SITENAME;
        $this->data['show_add_link'] = false;
    }

    public function render()
    {
        $this->load->view($this->template, $this->data);
    }

}