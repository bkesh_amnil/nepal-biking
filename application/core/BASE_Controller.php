<?php

class BASE_Controller extends CI_Controller {

    protected $template = array();
    protected $base_template = "layout/layout";
    protected $current_user = array();

    function __construct() {
        parent::__construct();
        //track the IP if unique store in the database.
        $newip = $this->input->ip_address();
        //check if the IP is unique
        $this->db->select('id')
                ->from('tbl_ip_track')
                ->where('ip_address',$newip)
                ->limit(1);
        $query = $this->db->get();
        if($query->num_rows() < 1)
        {
            $this->db->insert('tbl_ip_track',array('ip_address'=>$newip));
        }

        $url = $this->uri->segment_array();
        $count = count($url);

        if($count > 1) {
            $alias = $this->uri->segment($count);
            $table = $this->uri->segment(1);
            switch($table) {
                case "about-dawn-till-dusk":
                    $table = "content";
                    break;
                case "event":
                    $table = "event";
                    break;
                case "news":
                    $table = "news";
                    break;
                case "gallery";
                    $table = "gallery";
                    break;
                case "tour":
                    $table = "tour";
                    switch($count) {
                        case "3":
                            $alias = $this->uri->segment($count - 1);
                            break;
                        default:
                            break;
                    }
                    break;
                case "bike-rent":
                    $table = "bike";
                    break;
                case "bike-accessories":
                    $table = "product";
                    break;
                default:
                    $table = "menu";
                    $alias = $this->uri->segment(1);
                    break;
            }
            /*$alias = $this->uri->segment($count - 1);
            $table = $this->uri->segment($count - 2);*/
        } else {
            $alias = $this->uri->segment($count);
            $table = $this->uri->segment($count - 1);
        }

        $row = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
        $meta_contents = $this->getMetaKeywordsDescription($alias, 'menu');
        if($count > 1) {
            $meta_contents = $this->getMetaKeywordsDescription($alias, $table);
            $page_title = $this->getMetaKeywordsDescription($table, 'menu');
        }

        $this->template['site_name'] = $row->site_name;
        $this->template['site_title'] = $row->site_title;
        $this->template['site_logo'] = $row->site_logo;
        $this->template['site_keywords'] = $row->site_keyword;
        $this->template['site_description'] = $row->site_description;
        $this->template['site_author'] = $row->site_author;

        $this->template['main_menus'] = $this->public_model->getMenu(2);
        $this->template['top_menus'] = $this->public_model->getMenu(1);
        $this->template['bottom_menus'] = $this->public_model->getMenu(3);
        $this->template['footer_logos'] = $this->public_model->getLogos();

        $this->template['facebook'] = $row->facebook;
        $this->template['twitter'] = $row->twitter;
        $this->template['gplus'] = $row->gplus;
        $this->template['skype'] = $row->skype;
        $this->template['youtube'] = $row->youtube;
        $this->template['instagram'] = $row->instagram;
        $this->template['pinterest'] = $row->pininterest;

        if(isset($meta_contents) && !empty($meta_contents)) {
            $this->template['meta_keywords'] = $meta_contents->meta_keywords;
            $this->template['meta_description'] = $meta_contents->meta_description;
            if(isset($meta_contents->page_title)) {
                $this->template['page_title'] = $meta_contents->page_title;
                $this->template['menu_heading'] = $meta_contents->menu_heading;
            }
        }

        if(isset($page_title) && !empty($page_title)) {
            $this->template['page_title'] = $page_title->page_title . ' | ' . ucwords(str_replace('-', ' ', $alias));
            $this->template['menu_heading'] = $page_title->menu_heading;
        }
        // Assign current_user to the instance so controllers can use it
        /*if ($this->check_login()) {
            $this->template['current_user'] = $this->session->all_userdata();
            $this->current_user = $this->session->all_userdata();
        } else {
            $this->template['current_user'] = null;
            $this->current_user = null;
        }*/

        $this->template['countries'] = $this->public_model->getCountries();

    }

    /*public function check_login() {
        return ($this->session->userdata('logged_in'));
    }*/

    public function _output($output) {
        echo $this->load->view($this->base_template, $this->template, TRUE);
    }

    public function getMetaKeywordsDescription($alias, $table) {
        if($alias == 'tour') {
            $alias = 'nepalbiketours';
        } else if($alias == 'news') {
            $alias = 'bike-news-updates';
        } else if($alias == 'event') {
            $alias = 'mountainbike-events-in-Nepal';
        } else if($alias == 'gallery') {
            $alias = 'bikinginnepal-gallery';
        }
        if($table!= 'testimonial' && $table != 'search' && $table != 'booking' && $table != 'about-us' && $table != 'about-dawn-till-dusk' && $table != 'faq' && $table != 'team') {
            if ($table == 'bike-rent') {
                $table = 'bike';
            } else if ($table == 'bike-accessories') {
                $table = 'product';
            } else if($table == 'tour') {
                $table = 'tour';
            }
            if($table == 'menu') {
                $this->db->select('page_title, menu_heading, meta_keywords, meta_description');
            } else {
                $this->db->select('meta_keywords, meta_description');
            }
            if (!empty($alias)) {
                $this->db->where('slug', $alias);
            } else {
                $this->db->where('slug', 'home');
            }
            $row = $this->db->get('tbl_' . $table)->row();

            return (isset($row) && !empty($row)) ? $row : '';
        }
    }

}