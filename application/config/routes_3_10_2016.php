<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/*$route['default_controller'] = 'welcome';
$route['404_override'] = '';*/
/* Front End Routes */
$route['default_controller'] = "home";
$route['download/(:any)'] = "download/index/$1";
$route['booking/tour'] = "booking/tour/$1";

$route['search/tour'] = "search/tour/$1";
$route['search/news_events'] = "search/news_events/$1";
$route['search/tour_departure'] = "search/tour_departure/$1";
$route['search/tour/(:any)'] = "search/tour/$1";
$route['search/news_events/(:any)'] = "search/news_events/$1";
$route['search/bike_search'] = "search/bike_search/$1";
$route['search'] = "search/index/$1";

$route['dynamic_form/tour_booking_enquire'] = "dynamic_form/tour_booking_enquire/$1";
$route['dynamic_form/form_submit_subscribe'] = "dynamic_form/form_submit_subscribe/$1";
$route['dynamic_form/bike_rent'] = "dynamic_form/bike_rent/$1";
$route['dynamic_form/testimonial'] = "dynamic_form/testimonial/$1";
$route['dynamic_form/product_order'] = "dynamic_form/product_order/$1";
$route['dynamic_form/tour_enquire'] = "dynamic_form/tour_enquire/$1";
$route['dynamic_form/(:any)'] = "dynamic_form/form_submit/$1";

$route['content/load_more'] = "content/load_more/$1";
//$route['(:any)'] = "content/index/$1";
$route['404_override'] = "content/index/$1";

/* Front End Routes */

/* Admin End Routes */
$route[BACKENDFOLDER] = BACKENDFOLDER.'/user/login';
$route[BACKENDFOLDER.'/login'] = BACKENDFOLDER.'/user/login';
$route[BACKENDFOLDER.'/logout'] = BACKENDFOLDER.'/user/logout';
$route[BACKENDFOLDER.'/retrieve-password'] = BACKENDFOLDER.'/user/getPassword';
/* Admin End Routes */

/* End of file routes.php */
/* Location: ./application/config/routes.php */