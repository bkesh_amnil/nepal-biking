<h3 class="post-wrap-title">UPCOMING EVENTS</h3>
<div class="row">
    <?php
    if(!empty($latest_events)) {
        foreach($latest_events as $event) {
        ?>
            <div class="col-xs-12 col-sm-6 col-lg-6">
                <a href="<?php echo site_url('event/' . $event->slug) ?>" class="post-wrap">
                    <?php if(!empty($event->image)) { ?>
                        <div class="post-img-wrap">
                            <img class="" src="<?php echo base_url($event->image) ?>" alt="<?php echo $event->name ?>" />
                        </div>
                    <?php } ?>
                    <h3><?php echo $event->name ?></h3>
                    <span class="events-date"> <?php echo date('d F Y', strtotime($event->event_date)) ?> </span>
                </a>
            </div>
        <?php
        }
    } else { ?>
        <p><?php echo 'No Events added as of Yet.' ?></p>
    <?php } ?>
</div>