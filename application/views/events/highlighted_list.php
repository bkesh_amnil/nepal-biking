<div class="highlighted-post">
    <h3 class="post-wrap-title">What's Hot</h3>
    <div class="whats-hot-wrap">
        <?php
        if(isset($hot_events) && !empty($hot_events)) {
            foreach ($hot_events as $event) {
                ?>
                <div class="row">
                    <?php if(!empty($event->image)) { ?>
                        <div class="col-sm-4 col-lg-6">
                            <img class="img-responsive" src="<?php echo image_thumb($event->image, 270, 416, '', true) ?>" alt="<?php echo $event->name ?>" />
                        </div>
                    <?php } ?>

                    <div class="col-sm-8 col-lg-6">
                        <h3><?php echo $event->name ?></h3>
                        <span class="post-date"><?php echo date('d F Y', strtotime($event->event_date)) ?></span>

                        <p><?php echo $event->short_description ?></p>
                        <a class="btn-view-detail" href="<?php echo site_url('event/' . $event->slug) ?>">View Details <i class=" glyphicon glyphicon-menu-right"></i>
                            <i class=" glyphicon glyphicon-menu-right"></i></a>
                    </div>
                </div>
                <?php
            }
        } else { ?>
            <p><?php echo 'No Event Highlighted as of yet.' ?></p>
        <?php }?>
    </div>
</div>