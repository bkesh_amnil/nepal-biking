<h3 class="sections-title">Events</h3>
                
<form class="filter-tour-wrap">
	<div class="row">
	    <div class="col-sm-12 col-lg-3">
	        <h3>FILTER EVENTS</h3>
	    </div>

	    <?php $this->load->view('form/filter_news_events.php'); ?>
	</div>
</form>

<div class="row" id="news-events-listing">
	<?php
        foreach($content as $event) {
            $ids[] = $event->id;
        ?>
	<div class="col-xs-6 col-sm-3 col-lg-3">
	    <a href="<?php echo site_url('event/' . $event->slug) ?>" class="trip-wrap">
	    	<?php if(!empty($event->image)) { ?>
		        <div class="trip-img-wrap">
		            <img class="img-responsive" src="<?php echo image_thumb($event->image, 270, 270, '', true) ?>" alt="<?php echo $event->name ?>" />
		        </div>
	        <?php } ?>
	        <h2><?php echo $event->name ?></h2>
	    </a>
	</div>
	<?php } ?>
</div>
<?php if($count_rows > $item_per_page) { ?>
	<div class="processing load-more-processing" style="display:none;"></div>
        <button class="btn-more" id="load_more_button">Load More...</button>
<?php } ?>
<input type="hidden" id="hidden_type" value="<?php echo $module_name ?>" />
<input type="hidden" id="hidden_ids" value="<?php echo implode(',', $ids) ?>">