<?php if(isset($bike_categories) && !empty($bike_categories)) { ?>
    <div class="col-sm-4 col-lg-3">
        <select class="form-control" id="bike_category">
            <option value="0">Search by Category (all)</option>
            <?php foreach($bike_categories as $bike_category) { ?>
                <option value="<?php echo $bike_category->id ?>"><?php echo ucfirst($bike_category->name) ?></option>
            <?php } ?>
        </select>
    </div>
<?php } ?>

<div class="col-sm-4 col-lg-3">
    <div class="filter-name-wrap">
        <input class="form-control" type="text" placeholder="Search by Bike Name (all)" id="bike_name_search" />
        <i class="glyphicon glyphicon-search"></i>
    </div>
</div>

<?php if(isset($charge_per_days) && !empty($charge_per_days)) { ?>
    <div class="col-sm-4 col-lg-3">
        <select class="form-control" id="charge_per_day">
            <option value="0">Search by Charge per Day (all)</option>
            <?php foreach($charge_per_days as $charge_per_day) { ?>
                <option value="<?php echo $charge_per_day->charge_per_day ?>"><?php echo '$ ' .$charge_per_day->charge_per_day ?></option>
            <?php } ?>
        </select>
    </div>
<?php } ?>