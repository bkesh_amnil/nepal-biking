<?php if(isset($tour_categories) && !empty($tour_categories)) { ?>
    <div class="col-sm-4 col-lg-3">
        <select class="form-control" id="tour_country">
            <option value="0">Search by Country (all)</option>
            <?php foreach($tour_categories as $tour_category) { ?>
            <option value="<?php echo $tour_category->id ?>"><?php echo ucfirst($tour_category->tour_category_name) ?></option>
            <?php } ?>
        </select>
    </div>
<?php } ?>

<div class="col-sm-4 col-lg-3">
    <div class="filter-name-wrap">
        <input class="form-control" type="text" placeholder="Search by Tour Name (all)" id="tour_name_search" />
        <i class="glyphicon glyphicon-search"></i>
    </div>
</div>

<?php if(isset($tour_difficulty) && !empty($tour_difficulty)) { ?>
<div class="col-sm-4 col-lg-3">
    <select class="form-control" id="tour_difficulty">
        <option value="0">Search by Difficuly (all)</option>
        <?php foreach($tour_difficulty as $val) { ?>
        <option value="<?php echo $val->id ?>"><?php echo $val->difficulty_name ?></option>
        <?php } ?>
    </select>
</div>
<?php } ?>