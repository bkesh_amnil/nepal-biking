<h3>Inquiry Form</h3>
<?php $fields = $this->public_model->getFormFields($formdata->id); ?>
<div style="display: none;" class="processing extra-processing"></div>
<form name="<?php echo $formdata->form_name ?>" class="feedback-form" method="POST" action="<?php echo base_url('dynamic_form/submit') ?>" enctype="multipart/form-data">
    <div class="row">
        <?php
        foreach ($fields as $field) {
            $divClass = 'col-lg-6';
            if ($field->field_type == 'textarea') {
                $divClass = 'col-lg-12';
            }
            $validation_class = '';
            $field_name = 'field_name[' . $field->id . ']';
            $validataion_rules = explode('|', $field->validation_rule);
            foreach ($validataion_rules as $validation_rule) {
                $validation_class .= $validation_rule . ' ';
            }
            if ($field->field_name == 'mobile_number') {
                $validation_class .= ' number ';
            }
            $value = '';
            ?>
            <div class="<?php echo $divClass; ?>">
                <?php
                switch ($field->field_type) {
                    case 'textarea':
                        $attribute = 'class="form-control ' . $validation_class . '" name="' . $field->field_name . '" placeholder="' . $field->field_placeholder . '"';
                        echo form_textarea($field_name, $value, $attribute);
                        break;
                    default:
                        $attribute = 'class="form-control ' . $validation_class . '" type="text" name="' . $field->field_label . '" placeholder="' . $field->field_placeholder . '"';
                        echo form_input($field_name, $value, $attribute);
                        break;
                }
                ?>
            </div>
        <?php } ?>
        <div class="col-sm-12 col-lg-12">
            <div class="form-group">
                <div id="recaptcha1"></div>
            </div>
        </div>
        <div class="col-lg-4">
            <input class="btn btn-send pull-right" type="submit" value="Send" style="display:none;" />
        </div>
    </div>
    <input type="hidden" name="form_id" value="<?php echo $formdata->id ?>" />
    <input type="hidden" name="current_url" value="<?php echo current_url() ?>" />
</form>