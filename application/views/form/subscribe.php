<div style="display: none;" class="processing subscribe-processing"></div>
<form name="subscribe" class="subscribe" method="POST" action="<?php echo base_url('dynamic_form/form_submit_subscribe') ?>" enctype="multipart/form-data">
    <label>SUBSCRIBE US</label>
    <div class="btn-group">
	    <input type="text" name="subscribe_email" class="required email" placeholder="EMAIL@EXAMPLE.COM" />
	    <button class="btn-subscribe"><i class=" glyphicon glyphicon-menu-right"></i></button>
    </div>
</form>