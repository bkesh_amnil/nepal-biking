<h3>Order Form</h3>
<div style="display: none;" class="processing similar-processing"></div>
<?php if ($content['info']->in_stock) { ?>
    <form name="product_order" method="POST" action="<?php echo base_url('dynamic_form/product_order') ?>" enctype="multipart/form-data">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Quantity</label>
                <select name="product_quantity" id="product_quantity" class="form-control">
                    <?php
                    $range = range(1, 50);
                    foreach ($range as $val) {
                        ?>
                        <option value="<?php echo $val ?>"><?php echo $val ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Unit Price(in Rs.)</label>
                <input class="form-control product_price" type="hidden" name="product_price" value="<?php echo $content['info']->promotional_price ?>" />
                <input class="form-control product_price" type="text" name="product_price" placeholder="Price Per Person" value="<?php echo $content['info']->promotional_price ?>" disabled />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Total Cost(in Rs.)</label>
                <input class="form-control total_price" type="hidden" name="total_price" value="<?php echo $content['info']->promotional_price ?>" />
                <input class="form-control total_price" type="text" name="total_price" id="" placeholder="Total Cost" value="<?php echo $content['info']->promotional_price ?>" disabled />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Full Name</label>
                <input class="form-control required" type="text" name="customer_full_name" placeholder="Full Name" />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Email Address</label>
                <input class="form-control required email" type="text" name="customer_email_address" placeholder="Email Address" />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Contact Number</label>
                <input class="form-control number required" type="text" name="customer_contact_number" placeholder="Contact Number" />
            </div>

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <label>Special Instruction</label>
                <textarea class="form-control" name="customer_special_instruction" placeholder="Special Instruction"></textarea>
            </div>

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <div class="form-group">
                    <div id="recaptcha1"></div>
                </div>
            </div>
        </div>
        <div class="inquiry-form-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-6">
                    <input class="btn btn-send" type="submit" value="ORDER NOW" style="display: none;" />
                </div>
            </div>
        </div>
        <input type="hidden" name="product_category_id" value="<?php echo $content['info']->product_category_id ?>">
        <input type="hidden" name="product_id" value="<?php echo $content['info']->id ?>">
        <input type="hidden" name="product_model" value="<?php echo $content['info']->product_model ?>">
        <input type="hidden" name="product_name" value="<?php echo $content['info']->name ?>">
        <input type="hidden" name="product_code" value="<?php echo $content['info']->code ?>">
    </form>
<?php } else { ?>
    <p>No Product in Stock as of Now.</p>
<?php } ?>
