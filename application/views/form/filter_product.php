<?php if(isset($product_categories) && !empty($product_categories)) { ?>
    <div class="col-sm-4 col-lg-3">
        <select class="form-control" id="parent_product_category">
            <option value="0">Search by Category (all)</option>
            <?php foreach($product_categories as $product_category) { ?>
                <option value="<?php echo $product_category->id ?>"><?php echo ucfirst($product_category->name) ?></option>
            <?php } ?>
        </select>
    </div>
<?php } ?>
<?php if(isset($child_product_categories) && !empty($child_product_categories)) { ?>
    <div class="col-sm-4 col-lg-3">
        <select class="form-control" id="child_product_category">
            <option value="0">Search by Child Category (all)</option>
            <?php foreach($child_product_categories as $child_category) { ?>
                <option value="<?php echo $child_category->id ?>" class="<?php echo $child_category->parent_id ?>"><?php echo ucfirst($child_category->name) ?></option>
            <?php } ?>
        </select>
    </div>
<?php } ?>
    <div class="col-sm-4 col-lg-3">
        <div class="filter-name-wrap">
            <input class="form-control" type="text" placeholder="Search by Bike Accessories Name" id="product_name_search" />
            <i class="glyphicon glyphicon-search"></i>
        </div>
    </div>