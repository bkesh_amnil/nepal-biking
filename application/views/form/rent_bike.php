<?php if (isset($content['bike_rent_dates']) && !empty($content['bike_rent_dates'])) { ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="booking-status-wrap">
                <h3>AVAILABILITY STATUS</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Rent Date</th>
                                <th>Total Booking Available</th>
                                <th>Total Booked</th>
                                <th>Total Available</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span id="selected_rent_date"><?php echo $content['bike_rent_dates'][0]->rent_date ?></span></td>
                                <td><span id="total_booking_available"><?php echo $content['bike_rent_dates'][0]->all_allocation ?></span></td>
                                <td><span id="total_booked"><?php echo $content['bike_rent_dates'][0]->booked ?></span></td>
                                <td><span id="total_available"><?php echo $content['bike_rent_dates'][0]->available ?></span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<h3>Rent Request Form</h3>
<div style="display: none;" class="processing similar-processing"></div>
<?php if (isset($content['bike_rent_dates']) && !empty($content['bike_rent_dates'])) { ?>
    <form name="bike_rent" method="POST" action="<?php echo base_url('dynamic_form/bike_rent') ?>" enctype="multipart/form-data">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Rent Start Date</label>
                <?php
                foreach ($content['bike_rent_dates'] as $dval) {
                    $array[] = $dval->rent_date;
                }

                $commaList = implode(',', $array);
                ?>
                <input type="hidden" id="hidden_selected_dates" value="<?php echo $commaList ?>">
                <input type="text" name="rent_date_id" id="rent_date_id" class="form-control required" placeholder="Rent Start Date">
                <!--<select class="form-control" id="rent_date" name="rent_date_id">
                <?php /* foreach($content['bike_rent_dates'] as $dval) { */ ?>
                        <option value="<?php /* echo $dval->id */ ?>"><?php /* echo $dval->rent_date */ ?></option>
                <?php /* } */ ?>
                </select>-->
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Rent Duration Days</label>
                <?php
                $range = range(1, 30);
                /* $str = $content['bike_rent_dates'][0]->name;
                  $str = preg_replace('/\D/', '', $str);
                  $range = range(1, $str); */
                ?>
                <select class="form-control" id="duration" name="duration">
                    <!--<option value="<?php /* echo $content['bike_rent_dates'][0]->number_of_days_id */ ?>"><?php /* echo $content['bike_rent_dates'][0]->name */ ?></option>-->
                    <?php
                    if (isset($range) && !empty($range)) {
                        foreach ($range as $val) {
                            ?>
                            <option value="<?php echo $val ?>"><?php echo $val ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <?php
                $range = range(1, 20);
                ?>
                <label>Number of Pax</label>
                <select class="form-control" id="number_of_pax" name="number_of_pax">
                    <?php foreach ($range as $val) { ?>
                        <option value="<?php echo $val ?>"><?php echo $val ?></option>
                    <?php } ?>
                </select>
            </div>
            <input type="hidden" name="quantity" value="">

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Price Per Bike(in $)</label>
                <input class="form-control price_per_person" type="hidden" name="price_per_person" value="<?php //echo $content['bike_rent_dates'][0]->unit_price  ?>" />
                <input class="form-control price_per_person" type="text" name="price_per_person" placeholder="Price Per Person" value="<?php //echo $content['bike_rent_dates'][0]->unit_price  ?>" disabled />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Total Cost(in $)</label>
                <input class="form-control total_cost" type="hidden" name="total_cost" value="<?php //echo $content['bike_rent_dates'][0]->unit_price  ?>" />
                <input class="form-control total_cost" type="text" name="total_cost" id="" placeholder="Total Cost" value="<?php //echo $content['bike_rent_dates'][0]->unit_price  ?>" disabled />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Full Name</label>
                <input class="form-control required" type="text" name="customer_full_name" placeholder="Full Name" />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Email Address</label>
                <input class="form-control required email" type="text" name="customer_email_address" placeholder="Email Address" />
            </div>
            <div class="col-xs-12 col-sm-6 col-lg-6 date">
                <label>Date of Birth</label>
                <input class="form-control" type="text" name="customer_dob" placeholder="Date of Birth" id="datepicker1" />

            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Nationality</label>
                <select class="form-control" name="customer_nationality">
                    <?php foreach ($countries as $country) { ?>
                        <option value="<?php echo $country->name ?>"><?php echo $country->printable_name ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Passport Number</label>
                <input class="form-control required" type="text" name="customer_passport_number" placeholder="Passport Number" />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Contact Number</label>
                <input class="form-control number" type="text" name="customer_contact_number" placeholder="Contact Number" />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Contact Address / Hotel</label>
                <input class="form-control" type="text" name="customer_contact_address" placeholder="Contact Address" />
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-6">
                <label>Room Number</label>
                <input class="form-control" type="text" name="customer_room_number" placeholder="Room Number" />
            </div>

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <label>Local Address</label>
                <textarea class="form-control" name="customer_local_address" placeholder="Local Address"></textarea>
            </div>

            <div class="col-xs-12 col-sm-12 col-lg-12">
                <label>Remarks</label>
                <textarea class="form-control" name="customer_remarks" placeholder="Remarks"></textarea>
            </div>

            <div class="col-sm-12 col-lg-12">
                <div class="form-group">
                    <div id="recaptcha1"></div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-lg-6">
                <input type="checkbox" name="terms" class="required"/> <span>I agree to the <a href="javascript:void(0);" class="terms-condition-bike">terms & conditions</a></span>
            </div>

        </div>

        <input type="hidden" name="hidden_bike_category_id" value="<?php echo $content['info']->bike_category_id ?>" />
        <input type="hidden" name="hidden_bike_id" value="<?php echo $content['info']->id ?>" />
        <input type="hidden" name="hidden_bike_name" value="<?php echo $content['info']->name ?>">
        <input type="hidden" name="hidden_bike_code" value="<?php echo $content['info']->code ?>">
    </form>
    <div class="inquiry-form-footer clearfix">
        <div class="clearfix">
            <input class="btn btn-send pull-right" type="submit" value="BOOK NOW" style="display: none;"/>
        </div>
    </div>
<?php } else { ?>
    <p>No Booking Available as of Now.</p>
<?php } ?>
