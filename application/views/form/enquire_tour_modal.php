<!-- Modal -->
<div class="modal fade modal-default" id="enquireModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content enquireform-wrap">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ENQUIRE FORM</h4>
                <div style="display: none;" class="processing similar-right-processing"></div>
            </div>
            <div style="display: none;" class="processing similar-right-processing"></div>
            <form name="tour_enquire" method="POST" action="<?php echo base_url('dynamic_form/tour_enquire') ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <label>Departure Date *</label>
                            <input type="hidden" id="hidden_selected_dates" value="0">
                            <input type="text" name="departure_date" id="departure_date_enquire" class="form-control required" placeholder="Departure Date">
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <?php
                            $range = range(1, 20);
                            ?>
                            <label>Number of Pax</label>
                            <select class="form-control" id="nos_of_pax" name="nos_of_pax">
                                <?php foreach ($range as $val) { ?>
                                    <option value="<?php echo $val ?>"><?php echo $val ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <label>Full Name *</label>
                            <input class="form-control required" type="text" name="customer_full_name" placeholder="Full Name" />
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <label>Email Address *</label>
                            <input class="form-control required email" type="text" name="customer_email_address" placeholder="Email Address" />
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <label>Contact Address *</label>
                            <input class="form-control required" type="text" name="customer_contact_address" placeholder="Contact Address" />
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <label>Contact Number *</label>
                            <input class="form-control number required" type="text" name="customer_contact_number" placeholder="Contact Number" />
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <label>Enquiry *</label>
                            <textarea class="form-control required" placeholder="Enquiry" name="customer_enquiry"></textarea>
                        </div>

                        <div class="col-sm-12 col-lg-12">
                            <div class="form-group">
                                <div id="recaptcha1"></div>
                            </div>
                        </div>
                        <input type="hidden" name="hidden_tour_name" value="<?php echo $bookContent['info']->name ?>" />
                        <input type="hidden" name="hidden_tour_category_id" value="<?php echo $bookContent['info']->tour_category_id ?>" />
                        <input type="hidden" name="hidden_tour_id" value="<?php echo $bookContent['info']->id ?>" />
                        <input type="hidden" name="hidden_tour_code" value="<?php echo $bookContent['info']->code ?>" />

                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn-enquire-submit btn-all pull-right" type="submit" name="submit_button" value="Enquire" style="display:none;"/>
                </div>
            </form>
        </div>
    </div>
</div>