<div class="col-sm-4 col-lg-3">
    <select class="form-control" id="departure_years">
        <?php
        if(isset($departure_years) && !empty($departure_years)) {
            foreach($departure_years as $departure_year) {
                $selected = '';
                if($departure_year->years == date('Y')) {
                    $selected = ' selected="selected"';
                }
                ?>
                <option value="<?php echo $departure_year->years ?>"<?php echo $selected ?>><?php echo $departure_year->years ?></option>
                <?php
            }
        }
        ?>
    </select>
</div>

<div class="col-sm-4 col-lg-3">
    <select class="form-control" id="departure_months">
        <?php
        if(isset($departure_months) && !empty($departure_months)) {
            foreach($departure_months as $departure_month) {
                $selected = '';
                switch ($departure_month->months) {
                    case "January":
                        $m = "01";
                        break;
                    case "February":
                        $m = "02";
                        break;
                    case "March":
                        $m = "03";
                        break;
                    case "April":
                        $m = "04";
                        break;
                    case "May":
                        $m = "05";
                        break;
                    case "June":
                        $m = "06";
                        break;
                    case "July":
                        $m = "07";
                        break;
                    case "August":
                        $m = "08";
                        break;
                    case "September":
                        $m = "09";
                        break;
                    case "October":
                        $m = "10";
                        break;
                    case "November":
                        $m = "11";
                        break;
                    case "December":
                        $m = "12";
                        break;
                    default:
                        break;
                }
                if($m == date('m')) {
                    $selected = ' selected="selected"';
                }
                ?>
                <option value="<?php echo $m ?>"<?php echo $selected ?>><?php echo $departure_month->months ?></option>
                <?php
            }
        }
        ?>
    </select>
</div>

<div class="col-sm-4 col-lg-3">
    <select class="form-control" id="departure_nos_of_days">
        <option value="0">Number of Tour Days</option>
        <?php
        if(isset($departure_nos_of_days) && !empty($departure_nos_of_days)) {
            foreach($departure_nos_of_days as $departure_nos_of_days) {
                ?>
                <option value="<?php echo $departure_nos_of_days->duration ?>"><?php echo $departure_nos_of_days->duration ?></option>
                <?php
            }
        }
        ?>
    </select>
</div>