<div class="row">
    <div class="col-sm-12 col-lg-12">
        <h3>ENQUIRE FORM</h3>
        <div style="display: none;" class="processing similar-right-processing"></div>
        <form name="tour_booking_enquire" method="POST" action="<?php echo base_url('dynamic_form/tour_enquire') ?>" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-6 col-lg-6">
                    <label>Departure Date *</label>
                    <input type="hidden" id="hidden_selected_dates" value="0">
                    <input type="text" name="departure_date" id="departure_date" class="form-control required" placeholder="Departure Date">
                </div>

                <div class="col-sm-6 col-lg-6">
                    <?php
                    $range = range(1, 20);
                    ?>
                    <label>Number of Pax</label>
                    <select class="form-control" id="nos_of_pax" name="nos_of_pax">
                        <?php foreach($range as $val) { ?>
                            <option value="<?php echo $val ?>"><?php echo $val ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-sm-6 col-lg-6">
                    <label>Full Name *</label>
                    <input class="form-control required" type="text" name="customer_full_name" placeholder="Full Name" />
                </div>

                <div class="col-sm-6 col-lg-6">
                    <label>Email Address *</label>
                    <input class="form-control required email" type="text" name="customer_email_address" placeholder="Email Address" />
                </div>

                <div class="col-sm-6 col-lg-6">
                    <label>Contact Address *</label>
                    <input class="form-control required" type="text" name="customer_contact_address" placeholder="Contact Address" />
                </div>

                <div class="col-sm-6 col-lg-6">
                    <label>Contact Number *</label>
                    <input class="form-control number required" type="text" name="customer_contact_number" placeholder="Contact Number" />
                </div>

                <div class="col-sm-6 col-lg-6">
                    <label>Enquiry *</label>
                    <textarea class="form-control required" placeholder="Enquiry" name="customer_enquiry"></textarea>
                </div>

                <div class="col-sm-6 col-lg-6">
                    <?php
                    // init variables
                    $min_number = 1;
                    $max_number = 15;
                    // generating random numbers
                    $random_number1 = mt_rand($min_number, $max_number);
                    $random_number2 = mt_rand($min_number, $max_number);
                    ?>
                    <input type="text" value="<?php echo $random_number1 . ' + ' . $random_number2 . ' = ';?>" disabled="disabled" />
                    <input name="captchaResult1" class="required number math" type="text" size="2" placeholder="sum" />
                    <input name="firstNumber1" type="hidden" value="<?php echo $random_number1; ?>" />
                    <input name="secondNumber1" type="hidden" value="<?php echo $random_number2; ?>" />
                </div>

                <div class="col-sm-6 col-lg-6">
                    <input class="btn-enquire-submit" type="submit" name="submit_button" value="Enquire" style="display:none;"/>
                </div>
            </div>
            <input type="hidden" name="hidden_tour_name" value="<?php echo $content['info']->name ?>" />
            <input type="hidden" name="hidden_tour_category_id" value="<?php echo $content['info']->tour_category_id ?>" />
            <input type="hidden" name="hidden_tour_id" value="<?php echo $content['info']->id ?>" />
            <input type="hidden" name="hidden_tour_code" value="<?php echo $content['info']->code ?>" />
        </form>
    </div>
</div>
<!--<a href="#" class="close-btn"></a>-->