<div class="col-sm-4 col-lg-3">
    <select class="form-control" id="news_events_year">
        <option value="0">Select Year (all)</option>
        <?php
        if(isset($published_years) && !empty($published_years)) {
        	foreach($published_years as $published_year) { 
        		?>
        		<option value="<?php echo $published_year->years ?>"><?php echo $published_year->years ?></option>
        		<?php
        	}
        }
        ?>
    </select>
</div>

<div class="col-sm-4 col-lg-3">
    <div class="filter-name-wrap">
        <input class="form-control" type="text" placeholder="Search by Name (all)" id="news_events_name_search" />
        <i class="glyphicon glyphicon-search"></i>
    </div>
</div>
	    
<div class="col-sm-4 col-lg-3">
    <select class="form-control" id="news_events_order">
        <option value="DESC">Newer First</option>
        <option value="ASC">Older First</option>
    </select>
</div>
<input type="hidden" id="hidden_type_page" value="<?php echo ($module_name == 'event') ? 'event' : 'news' ?>" />