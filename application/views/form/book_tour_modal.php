<!-- Modal -->
<div class="modal fade modal-default" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content bookingform-wrap">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">BOOKING FORM</h4>
            </div>
            <div style="display: none;" class="processing similar-processing"></div>
            <form name="tour_booking_enquire" method="POST" action="<?php echo base_url('dynamic_form/tour_booking_enquire') ?>" enctype="multipart/form-data">                
                <div class="modal-body">
                    <?php
//                    debug($bookContent);
                    if (isset($bookContent['tour_departure_dates']) && !empty($bookContent['tour_departure_dates'])) {
                        ?>
                        <div class="row"  id="current-booking-staus">
                            <div class="col-sm-8 col-lg-8">
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <label>Departure Date</label>
                                        <?php
                                        foreach ($bookContent['tour_departure_dates'] as $dval) {
                                            $array[] = $dval->tour_departure_date;
                                        }

                                        $commaList = implode(',', $array);
                                        ?>
                                        <input type="hidden" id="hidden_selected_dates" value="<?php echo $commaList ?>">
                                        <input type="text" name="departure_date" id="departure_date" class="form-control" placeholder="Departure Date">
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <?php
                                        $range = range(1, 20);
                                        ?>
                                        <label>Number of Pax</label>
                                        <select class="form-control" id="nos_of_pax" name="nos_of_pax">
                                            <option>Select</option>
                                            <?php foreach ($range as $val) { ?>
                                                <option value="<?php echo $val ?>"><?php echo $val ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Price Per Person(in $)</label>
                                        <input class="form-control price_per_person" type="hidden" name="price_per_person" value="" />
                                        <input class="form-control price_per_person" type="text" name="price_per_person" placeholder="Price Per Person" value="" disabled />
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Total Cost(in $)</label>
                                        <input class="form-control total_cost" type="hidden" name="total_cost" value="" />
                                        <input class="form-control total_cost" type="text" name="total_cost" id="" placeholder="Total Cost" value="" disabled />
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Full Name *</label>
                                        <input class="form-control required" type="text" name="full_name" placeholder="Full Name" />
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Email Address *</label>
                                        <input class="form-control required email" type="text" name="email_address" placeholder="Email Address" />
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Contact Address *</label>
                                        <input class="form-control required" type="text" name="contact_address" placeholder="Contact Address" />
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Contact Number *</label>
                                        <input class="form-control number required" type="text" name="contact_nos" placeholder="Contact Number" />
                                    </div>

                                    <div class="col-sm-6 col-lg-6">
                                        <label>Remarks</label>
                                        <textarea class="form-control" placeholder="Remarks" name="remarks"></textarea>
                                    </div>
                                    <div class="col-sm-12 col-lg-12">
                                        <div class="form-group">
                                            <div id="recaptcha2"></div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="hidden_tour_name" value="<?php echo $bookContent['info']->name ?>" />
                                <input type="hidden" name="hidden_fixed_departure" id="hidden_fixed_departure" value="<?php echo $bookContent['tour_departure_dates'][0]->is_fixed_departure ?>" />
                                <input type="hidden" name="hidden_tour_category_id" value="<?php echo $bookContent['info']->tour_category_id ?>" />
                                <input type="hidden" name="hidden_tour_id" value="<?php echo $bookContent['info']->id ?>" />
                                <input type="hidden" name="hidden_tour_code" value="<?php echo $bookContent['info']->code ?>">

                                            <!--<input type="hidden" id="hidden_selected_dates" value="0">-->
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="booking-status-wrap">
                                    <h3>BOOKING STATUS</h3> 
                                    <ul>
                                        <li><div>Departure date</div><span id="selected_departure_date"><?php echo (isset($selected_date) && !empty($selected_date)) ? $selected_date : $bookContent['tour_departure_dates'][0]->tour_departure_date ?></span></li>
                                        <li><div>Total Booking Available</div><span id="total_booking_available"><?php echo (isset($selected_all_allocation) && !empty($selected_all_allocation)) ? $selected_all_allocation : $bookContent['tour_departure_dates'][0]->tour_all_allocation ?></span></li>
                                        <li><div>Total Booked</div><span id="total_booked"><?php echo (isset($selected_booked) && !empty($selected_booked)) ? $selected_booked : $bookContent['tour_departure_dates'][0]->tour_booked ?></span></li>
                                        <li><div>Total Available</div><span id="total_available"><?php echo (isset($selected_available) && !empty($selected_available)) ? $selected_available : $bookContent['tour_departure_dates'][0]->tour_available ?></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div id="current-booking-staus-no">
                            <p>No Booking Available as of Now.</p>
                            <p class="enquire-link text-center">But you can send in your enquiry <a href="javascript:void(0);" rel="" class="btn-enquire">here</a>.</p>
                        </div>

                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <input class="btn-booking-submit btn-all pull-right" id="btn-send" type="submit" name="submit_button" value="Book" />
                </div>
            </form>
        </div>
    </div>
</div>