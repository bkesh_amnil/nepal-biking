<div class="modal fade" id="exampleModal-testimonial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Review Form</h4>
            </div>
            <div class="modal-body">
                <div style="display: none;" class="processing testimonial-processing"></div>
                <form id="testimonial_form" name="testimonial_form" method="post" action="<?php echo base_url('dynamic_form/testimonial') ?>" enctype="multipart/form-data">
                   <div class="row">
                       <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control required" id="customer_full_name" name="customer_full_name" placeholder="Full Name *">
                            </div>
                       </div>
                       <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control required email" id="customer_email_address" name="customer_email_address" placeholder="Email Address *">
                            </div>
                       </div>
                       <div class="col-md-6">
                            <div class="form-group">
                                <select name="customer_country_id" id="customer_country_id" class="form-control">
                                <?php
                                if(isset($countries) && !empty($countries)) {
                                    foreach($countries as $val) {
                                        ?>
                                        <option value="<?php echo $val->id ?>"><?php echo $val->printable_name ?></option>
                                        <?php
                                    }
                                }
                                ?>
                                </select>
                            </div>
                       </div>
                       <div class="col-md-6">
                            <div class="form-group">
                                <input type="file" name="customer_image" id="customer_image" class="form-control" placeholder="Image">
                            </div>
                       </div>
                       <div class="col-md-12">
                            <div class="form-group">
                                <textarea rows="7" name="customer_testimonial" class="form-control required" id="customer_testimonial"
                                    placeholder="Review *"></textarea>
                            </div>
                       </div>
                   </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary modal-send" id="send_testimonial">Send</button>
            </div>
        </div>
    </div>
</div>