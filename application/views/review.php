<h3>OUR CUSTOMER SAY</h3>
<div class="container">
    <div id="customer" class="owl-carousel">
        <?php foreach($reviews as $review) { ?>
            <div class="customer">
                <?php if(isset($review->customer_image) && !empty($review->customer_image)) { ?>
                    <img src="<?php echo base_url($review->customer_image) ?>" alt="<?php echo $review->customer_name ?>"/>
                <?php } ?>
                <div class="customer-say">
                    <p><?php echo $review->customer_review ?></p>
                    <span class="customer-name"><?php echo $review->customer_name ?></span>
                    <?php if(isset($review->customer_designation) && !empty($review->customer_designation)) { ?>
                        <span class="customer-type"><?php echo $review->customer_designation ?></span>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>