<?php if(isset($breadcrumb) && !empty($breadcrumb)) { ?>
	<section class="breadcrumb-wrap">
		<?php $this->load->view('common/breadcrumb', $breadcrumb) ?>
	</section>
<?php } ?>
<section class="inner-banner" style="background-image: url(<?php echo base_url('img/inner-cover-about-landing.jpg') ?>"> </section>
<section class="inner-contents">
	<div class="container">
		<div class="search-collection">
			<h2 class="search-title">Search result for <?php echo $search_text ?></h2>
			<?php
			if(isset($search_result) && !empty($search_result)) {
				foreach($search_result as $ind => $search_val) {
					if($ind == 'Product') {
						?>
						<!-- <h2 class="search-title"><?php echo $ind ?></h2> -->
						<?php 
						foreach($search_val as $indx => $value){
							if($indx != "Kid's Collection") {
							?>
							<div class="product-collection-wrap clearfix">
								<div class="row clearfix">
									<div class="col-lg-12">
										<div class="product-collection-title">
											<span class="product-accessories-title"><?php echo $indx ?></span>
										</div>
									</div>
									<?php foreach($value as $index => $vals) { ?>
										<div class="col-xs-6 col-sm-3 col-lg-3">
			                                <div class="product-wrap">
			                                    <div class="product-img-wrap">
			                                        <img class="img-responsive" src="<?php echo base_url('image/product/' . $vals[0]['image']) ?>" />
			                                    </div>   
			                                    <div class="color-wrap">
			                                    	<?php foreach($vals as $val) { ?>
			                                        	<button class="colors" style="background:<?php echo $val['color_code'] ?>" rel="<?php echo base_url('image/product/' . $val['image']) ?>"/button>
		                                    		<?php } ?>
			                                    </div>
			                                    <span class="product-title"><?php echo $index ?></span>
			                                    <a class="product-enquire hvr-sweep-to-right" href="javascript:void(0);" id="<?php echo $val['product_id'] ?>" data-categoryid="<?php echo $val['product_category_id'] ?>">ENQUIRE</a>
			                                </div>
			                            </div>
									<?php } ?>
								</div>
							</div>
							<?php 
							}
						}
					} else {
						?>
						<!-- <h2 class="search-title"><?php echo $ind ?></h2> -->
						<?php foreach($search_val as $val){ ?>
			                <div class="product-collection-wrap clearfix">
								<div class="row clearfix">
									<div class="col-lg-12">
			                	<a href="<?php echo $val['url'] ?>">
				                	<h2><?php echo $val['title'] ?></h2>
				                	<p><?php echo $val['data'] ?></p>
			                	</a>
			                </div>
			            </div>
			        </div>
		                <?php 
	            		}
	            	}
        		}
    		}else { ?>
            <p><?php echo 'No search result found for ' . $search_text ?></p>
            <?php } ?>
        </div>
	</div>
</section>