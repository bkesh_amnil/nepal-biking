<div class="container">
    <h3 class="sections-title">Rent Bike</h3>

    <form class="filter-tour-wrap">
        <div class="row">
            <div class="col-sm-12 col-lg-3">
                <h3>FILTER YOUR BIKES</h3>
            </div>

            <?php $this->load->view('form/filter_bike.php'); ?>
        </div>
    </form>

    <div class="row" id="product-listing">
        <?php
        if(isset($content) && !empty($content)) {
            foreach($content as $ind => $val) {
                $ids[] = $val->id;
                $name = $val->name;
                if(strlen($val->name) > 34) {
                      $name = substr($val->name, 0, 34) . ' ...';
                }
                ?>
                <div class="col-xs-6 col-sm-3 col-lg-3">
                    <a href="<?php echo site_url('bike-rent/' . $val->slug) ?>" class="trip-wrap inner-trip-wrap">
                        <div class="trip-img-wrap">
                            <img class="img-responsive" src="<?php echo base_url($val->cover_image) ?>" alt="<?php echo $val->name ?>" />
                        </div>
                        <h2 title="<?php echo $val->name ?>"><?php echo $name ?></h2>
                        <span>BIKE CODE :<span><?php echo $val->code ?></span></span>
                        <span>CHARGE :<span><?php echo '$'.$val->charge_per_day.'/day' ?></span></span>
                    </a>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <?php if($count_rows > $item_per_page) { ?>
        <div class="processing load-more-processing" style="display:none;"></div>
        <button class="btn-more" id="load_more_button">Load More...</button>
    <?php } ?>
</div>
<input type="hidden" id="hidden_type" value="<?php echo $active_menu ?>" />
<input type="hidden" id="hidden_ids" value="<?php echo implode(',', $ids) ?>">