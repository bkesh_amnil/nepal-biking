<h3 class="sections-title">Bike Detail</h3>
<div class="hire-bike-section bike-detail-banner">
    <?php if(isset($social_datas) && !empty($social_datas)) { ?>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_sharing_toolbox share-page"></div>
    <?php } ?>
    <div class="bike-costs-name">
        <h2 class="bike-name"><?php echo $content['info']->name ?></h2>
            <span class="bike-costs">
                Rent Cost
                <span>$<?php echo $content['info']->charge_per_day .'/per day' ?></span>
            </span>
    </div>
    <div class="bike-img-full">
        <div id="bike-detail-slider" class="owl-carousel">
            <img src="<?php echo base_url($content['info']->cover_image) ?>" alt="<?php echo $content['info']->name ?>">
            <?php
            if(isset($content['gallery']) && !empty($content['gallery'])) {
                foreach($content['gallery'] as $img) {
                    ?>
                    <img src="<?php echo base_url($img->image) ?>" alt="<?php echo $img->title ?>">
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<div class="description-wrap">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-6">
            <div class="bike-description-wrap">
                <h3>Description</h3>
                <?php echo $content['info']->description ?>
                <h3>Bike Code</h3>
                <p><?php echo $content['info']->code ?></p>
                <h3>Bike Model</h3>
                <p><?php echo $content['info']->bike_model ?></p>
                <h3>Bike Available Size</h3>
                <p><?php echo $content['info']->size ?></p>
                <h3>Bike Cost</h3>
                <p><?php echo '$' .$content['info']->cost ?></p>
                <a href="javascript:void(0);" rel="<?php echo $content['info']->file ?>" class="download-bike-detail">Download .PDF</a>
            </div>
        </div>
        <form id="download_pdf_file" method="post" action="<?php echo base_url('download/index') ?>">
            <input type="hidden" id="file" name="file" value="">
        </form>
        <div class="col-xs-12 col-sm-6 col-lg-6 inquiry-form">
            <?php $this->load->view('form/rent_bike.php') ?>
        </div>
    </div>
</div>