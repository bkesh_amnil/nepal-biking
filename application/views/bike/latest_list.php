<div class="container">
    <h3 class="sections-title">Hire A BIKE</h3>
    <div id="hire-bike-slider" class="owl-carousel " style="background-image: url(img/bike-slider-bg.png)">
        <?php foreach($latest_bike_tours as $ind=>$latest_bike_tour) { ?>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-8">
                    <div class="bike-img">
                        <img src="<?php echo base_url($latest_bike_tour->cover_image) ?>" alt="<?php echo $latest_bike_tour->name ?>" />
                    </div>
                </div>
                <!--<div class="bookingform-wrap">
                    <?php /*$this->load->view('form/book_tour.php'); */?>
                </div>-->
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <h3 class="trek-code"><?php echo $latest_bike_tour->name ?></h3>
                    <div class="bike-info">
                        <div class="bike-code">Code : <span><?php echo $latest_bike_tour->code ?></span></div>
                        <p><?php echo $latest_bike_tour->short_description ?></p>
                        <a class="btn-download-pdf" href="javascript:void(0);" rel="<?php echo $latest_bike_tour->file ?>">Download Pdf</a>
                        <a class="btn-book-bike" href="<?php echo site_url('bike-rent/' . $latest_bike_tour->slug) ?>">Book The Bike</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <form id="download_pdf_file" method="post" action="<?php echo base_url('download/index') ?>">
        <input type="hidden" id="file" name="file" value="">
    </form>

    <form class="bike-search-wrap" method="post" action="<?php echo base_url('search/bike_search') ?>">
        <input class="form-control" type="text" placeholder="SEARCH BIKE" name="bike_search">
        <i class="glyphicon glyphicon-search"></i>
    </form>
</div>