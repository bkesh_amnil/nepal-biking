<h3 class="sections-title">Bike Accessories Detail</h3>
<div class="hire-bike-section bike-detail-banner">
    <?php if($content['info']->is_new == '1') { ?>
        <div class="ribbon-wrapper"><div class="ribbon">NEW</div></div>
    <?php } ?>
    <?php if($content['info']->is_special_offer == '1') { ?>
        <div class="ribbon-wrapper special"><div class="ribbon">SPECIAL OFFER</div></div>
    <?php } ?>
    <?php if(isset($social_datas) && !empty($social_datas)) { ?>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <div class="addthis_sharing_toolbox share-page"></div>
    <?php } ?>
    <div class="bike-costs-name">
        <h2 class="bike-name"><?php echo $content['info']->name ?></h2>
            <span class="bike-costs">
                Price
                <span>Rs.<?php echo $content['info']->normal_price ?>(Normal)/Rs.<?php echo $content['info']->promotional_price ?>(Promotional)</span>
            </span>
    </div>
    <div class="bike-img-full">
        <div id="bike-detail-slider" class="owl-carousel">
            <img src="<?php echo base_url($content['info']->cover_image) ?>" alt="<?php echo $content['info']->name ?>">
            <?php
            if(isset($content['gallery']) && !empty($content['gallery'])) {
                foreach($content['gallery'] as $img) {
                    ?>
                    <img src="<?php echo base_url($img->image) ?>" alt="<?php echo $img->title ?>">
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<div class="description-wrap">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-lg-6">
            <div class="product-description-wrap">
                <h3>Description</h3>
                <?php echo $content['info']->description ?>
                <h3>Product Code</h3>
                <p><?php echo $content['info']->code ?></p>
                <h3>Product Model</h3>
                <p><?php echo $content['info']->product_model ?></p>
                <h3>In  Stock ?</h3>
                <p><?php echo ($content['info']->in_stock) ? 'Yes' : 'No' ?></p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-lg-6 inquiry-form">
            <?php $this->load->view('form/order_product.php') ?>
        </div>
    </div>
</div>