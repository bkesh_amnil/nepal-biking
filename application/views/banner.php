<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
    <div id="package-banner" class="owl-carousel">
		<?php
        foreach($banners as $banner) {
            $url = "javascript:void(0);";
            if(isset($banner['url']) && !empty($banner['url'])) {
                $url = $banner['url'];
            }
            ?>
        <div class="package-wrap">
			<a href="<?php echo $url ?>"><img class="img-responsive" src="<?php echo base_url($banner['info']->image) ?>" alt="<?php echo $banner['info']->name ?>"/></a>
			
            <div class="about-package">
                <div class="banner-description">
                    <?php echo $banner['info']->description ?>
                </div>
                <div class="banner-title">
                    <a href="<?php echo $url ?>"><h2><?php echo $banner['info']->name ?></h2></a>
                    <?php 

                    ?>
                    <a class="btn-detail" href="<?php echo $url ?>">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
            </div>
		</div>
		<?php } ?>
	</div>
</div>