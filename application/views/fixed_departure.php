<h3 class="sections-title">FIXED DEPARTURES</h3>
                
<form class="filter-tour-wrap">
    <div class="row">
        <div class="col-sm-12 col-lg-3">
            <h3>FILTER TOUR DEPARTURES</h3>
        </div>
        
        <?php $this->load->view('form/filter_fixed_departure.php'); ?>
        
    </div>
</form>

<div class="filter-contents-wrap table-responsive">
    <p class="info">Plese Filter Departure</p>
</div>

<form id="book_now_date" method="post" action="">
    <input type="hidden" id="date" name="date" value="">
    <input type="hidden" id="tour_departure_detail_id" name="tour_departure_detail_id" value="">
    <input type="hidden" id="url" name="url" value="">
</form>