<h3 class="sections-title">Site Map</h3>
<div class="site-map">
<div class="row">
	<?php
	if(isset($content) && !empty($content)) {
		foreach($content as $menu) {
			$data[$menu->menu_type_id][] = $menu;
			$alias = site_url();
			if($menu->slug != 'home') {
				$alias = site_url($menu->slug);
			}
		?>
		<div class="col-xs-6 col-sm-3 col-lg-3" class="trip-wrap">
			<a href="<?php echo $alias ?>">
				<span><?php echo $menu->name ?></span>
			</a>
		</div>
		<?php
		}
	}
	?>
</div>
</div>