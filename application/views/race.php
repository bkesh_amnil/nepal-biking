<?php
if(isset($content) && !empty($content)) {
    $social_data_individual = $this->public_model->getSocialData($modules[0]['module_id'], $content[0]->id);
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-lg-8">
        <div class="program-summary-wrap">
            <h3>Program Summary</h3>
            <?php if(isset($social_data_individual) && !empty($social_data_individual)) { ?>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_sharing_toolbox share-page"></div>
            <?php } ?>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>Title</h5>
                            <span><?php echo $content[0]->name ?></span>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>Venue</h5>
                            <span><?php echo $content[0]->venue ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>START POINT</h5>
                            <span><?php echo $content[0]->start_point ?></span>
                            <span>LAT:   <?php echo $content[0]->start_point_latitude ?> / LONG:  <?php echo $content[0]->start_point_longitude ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>Finish point</h5>
                            <span><?php echo $content[0]->finish_point ?></span>
                            <span>LAT:  <?php echo $content[0]->finish_point_latitude ?> / LONG:  <?php echo $content[0]->finish_point_longitude ?></span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>DATE</h5>
                            <span><?php echo date('M jS Y', strtotime($content[0]->start_date)) . ' – ' . date('M jS Y', strtotime($content[0]->end_date)) ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>ENTRY FEE</h5>
                            <span>US$ <?php echo $content[0]->entry_fee ?>/-</span>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-lg-12">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>EVENTS</h5>
                            <span><?php echo $content[0]->events ?></span>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-lg-6">
                            <h5>CATEGORIES</h5>
                            <span><?php echo $content[0]->categories ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="event-detail-tab">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a class="introduction" href="#introduction" data-toggle="tab">Introduction</a></li>
                <li><a class="itinerary" href="#itinerary" data-toggle="tab">Itinerary</a></li>
                <li><a class="contact" href="#contact" data-toggle="tab">Race Contact</a></li>
            </ul>

            <div id="myTabContent" class="event-detail-tab-content tab-content" >
                <div class="tab-pane fade active in" id="introduction">
                    <?php echo $content[0]->description ?>
                    <?php if(isset($race_media) && !empty($race_media['objectives'])) { ?>
                        <div class="race-contents-wrap">
                            <h4>Objectives</h4>
                            <div class="row">
                                <?php
                                $i = 1;
                                $count = count($race_media['objectives']);
                                foreach($race_media['objectives'] as $val) { ?>
                                <div class="col-xs-12 col-sm-6 col-lg-6">
                                    <img src="<?php echo base_url($val->image) ?>" alt=""<?php echo $val->title ?> />
                                    <p><?php echo $val->caption ?></p>
                                </div>
                                <?php
                                    if($i != $count) {
                                        if ($i % 2 == 0) {
                                            echo '</div><div class="row">';
                                        }
                                    } else {
                                        echo '</div>';
                                    }
                                    $i++;
                                }
                                ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="itinerary">
                    <?php echo $content[0]->itinerary ?>
                </div>
                <div class="tab-pane fade" id="contact">
                    <?php echo $content[0]->contact ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-lg-4">
        <aside>
            <div class="row">
                <?php 
                if(isset($content[0]->youtube_link) && !empty($content[0]->youtube_link)) {
                $video_arr = explode('=', $content[0]->youtube_link);
                ?>
                    <div class="col-xs-12 col-sm-6 col-lg-12">
                        <div class="aside-content-wrap">
                            <h3>Video</h3>
                            <iframe width="100%" height="225" src="https://www.youtube.com/embed/<?php echo $video_arr[1] ?>" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                <?php } ?>
                <?php if(isset($race_media) && !empty($race_media['event_organizer'])) { ?>
                <div class="col-xs-12 col-sm-6 col-lg-12">
                    <div class="aside-content-wrap">
                        <h3>Event Organizer</h3>
                        <?php foreach($race_media['event_organizer'] as $val) { ?>
                            <a href=""><img src="<?php echo base_url($val->image) ?>" alt="<?php echo $val->title ?>"></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <?php if(isset($race_media) && !empty($race_media['media_marketing'])) { ?>
                <div class="col-xs-12 col-sm-6 col-lg-12">
                    <div class="aside-content-wrap">
                        <h3>Media marketing</h3>
                        <?php foreach($race_media['media_marketing'] as $val) { ?>
                        <a href=""><img src="<?php echo base_url($val->image) ?>" alt="<?php echo $val->title ?>"></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <?php if(isset($race_media) && !empty($race_media['press_coverage'])) { ?>
                <div class="col-xs-12 col-sm-6 col-lg-12">
                    <div class="aside-content-wrap">
                        <h3>Press coverage</h3>
                        <?php foreach($race_media['press_coverage'] as $val) { ?>
                        <a href=""><img src="<?php echo base_url($val->image) ?>" alt="<?php echo $val->title ?>"></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <?php if(isset($race_media) && !empty($race_media['supporters'])) { ?>
                <div class="col-xs-12 col-sm-6 col-lg-12">
                    <div class="aside-content-wrap">
                        <h3>THE EVENT SUPPORTERS</h3>
                        <?php foreach($race_media['supporters'] as $val) { ?>
                            <a href=""><img src="<?php echo base_url($val->image) ?>" alt="<?php echo $val->title ?>"></a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </aside>
    </div>
</div>
<?php } ?>