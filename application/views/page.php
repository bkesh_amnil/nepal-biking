<?php
$sectionClass = 'inner-page-bg-1';
if($module_name == 'detail') {
	$sectionClass = 'inner-page-bg-2';
	if(isset($background_image) && !empty($background_image)) {
		$img = base_url($background_image->image);
	} else {
		$img = 'img/inner-page-banner-pattern-bg.jpg';
	}
	if(!empty($content->image)) {
		$cover_img = base_url($content->image);
	} else {
		$cover_img = base_url('img/logo.png');
	}
	?>
	<section class="inner-page-banner" style="background-image: url('<?php echo $img ?>')">
		<div class="container">
			<div class="about-logo events-logo">
				<img class="img-responsive" src="<?php echo $cover_img ?>" />
			</div>
		</div>
	</section>
	<?php
} else if($module_name == 'content') {
	$sectionClass = 'inner-page-bg-2';
	if(!empty($content[0]->background_image)) {
		$img = base_url($content[0]->background_image);
	} else {
		$img = 'img/inner-page-banner-pattern-bg.jpg';
	}
	if(!empty($content[0]->image)) {
		$cover_img = base_url($content[0]->image);
	} else {
		$cover_img = base_url('img/logo.png');
	}
	?>
	<section class="inner-page-banner" style="background-image: url('<?php echo $img ?>')">
	    <div class="container">
	        <div class="about-logo">
	            <img class="img-responsive" src="<?php echo $cover_img ?>" />
	        </div>
	    </div>
	</section>
<?php } else if($module_name == 'race') {
	$sectionClass = 'inner-page-bg-2';
	if(!empty($content[0]->backgroud_image)) {
		$img = base_url($content[0]->backgroud_image);
	} else {
		$img = 'img/inner-page-banner-pattern-bg.jpg';
	}
	?>
	<section class="inner-page-banner" style="background-image: url(<?php echo $img ?>)">
		<div class="container">
			<div class="event-logo">
				<img class="img-responsive" src="<?php echo base_url($content[0]->cover_image) ?>" />
			</div>
			<?php if($content[0]->google_form_link != '' && strtotime(date('Y-m-d')) < strtotime($content[0]->start_date)) { ?>
				<a class="btn-event-register" href="<?php echo $content[0]->google_form_link ?>" target="_blank">Register Here +</a>
			<?php } ?>
		</div>
	</section>
<?php } ?>
<section class="<?php echo $sectionClass ?>">
	<?php if(isset($breadcrumb) && !empty($breadcrumb)) { ?>
		<section class="breadcrumb-wrap">
			<?php $this->load->view('common/breadcrumb', $breadcrumb) ?>
		</section>
	<?php } else {
		if($module_name != 'detail') {
			if(isset($content) && !empty($content)) {
			?>
			<div class="details-banner-img">
				<img src="<?php echo base_url($content->image) ?>" alt="<?php echo $content->name ?>" />
			</div>
		<?php } } } ?>
	<div class="container">
		<?php
                 switch($module_name) {
			case "content":
			case "faq":
			case "team":
				$this->load->view('content.php');
				break;
			case "tour":
				$this->load->view('tour/list.php');
				break;
			case "tour_detail":
				$this->load->view('tour/detail.php');
				break;
			case "news":
				$this->load->view('news/list.php');
				break;
			case "detail":
				$this->load->view('detail.php');
				break;
			case "event":
				$this->load->view('events/list.php');
				break;
			case "departure":
				$this->load->view('fixed_departure.php');
				break;
			case "menu":
				$this->load->view('site_map.php');
				break;
			case "gallery":
			case "video":
				$this->load->view('media/list.php');
				break;
			case "product":
				$this->load->view('product/list.php');
				break;
			case "product_detail":
				$this->load->view('product/detail.php');
				break;
			case "bike":
				$this->load->view('bike/list.php');
				break;
			case "bike_detail":
				$this->load->view('bike/detail.php');
				break;
			case "race":
				$this->load->view('race.php');
				break;
			case "book":
				$this->load->view('tour/book_form.php');
				break;
			case "enquire":
				$this->load->view('tour/enquire_form.php');
				break;
                        case "testimonial":
				switch($count) {
					case "2":
						$this->load->view('testimonial/detail.php');
						break;
					case "1":
						$this->load->view('testimonial/list.php');
						break;
				}
				break;
		}
		?>
    </div>
</section>