<section class="inner-page-banner" style="background-image: url(img/inner-page-banner-pattern-bg.jpg)">
    <div class="container">
        <div id="map-canvas"></div>
    </div>
</section>
<section class="inner-page-bg-2">
	<?php if(isset($breadcrumb) && !empty($breadcrumb)) { ?>
		<section class="breadcrumb-wrap">
			<?php $this->load->view('common/breadcrumb', $breadcrumb) ?>
		</section>
	<?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6  col-lg-6">
				<div class="contact-contents-wrap">
					<h3><?php echo $content[0]->name ?></h3>
                    <?php echo $content[0]->long_description ?>

                    <h3>Follow Us on</h3>
                    <div class="connect-us">
                        <span>CONNECT WITH US</span>
                        <?php if(isset($facebook) && !empty($facebook)) { ?>
                        	<a class="facebook-big" href="<?php echo $facebook ?>" target="_blank"></a>
                    	<?php } ?>
                    	<?php if(isset($twitter) && !empty($twitter)) { ?>
                        	<a class="twitter-big" href="<?php echo $twitter ?>" target="_blank"></a>
                    	<?php } ?>
                    	<?php if(isset($youtube) && !empty($youtube)) { ?>
                        	<a class="youtube-big" href="<?php echo $youtube ?>" target="_blank"></a>
                    	<?php } ?>
						<?php if(isset($instagram) && !empty($instagram)) { ?>
							<a class="instagram-big" href="<?php echo $instagram ?>" target="_blank"></a>
						<?php } ?>
						<?php if(isset($pinterest) && !empty($pinterest)) { ?>
							<a class="pinterest-big" href="<?php echo $pinterest ?>" target="_blank"></a>
						<?php } ?>
                    </div>
                </div>
			</div>
			<div class="col-xs-12 col-sm-6 col-lg-6">
                <div class="contact-contents-wrap">
                    <?php $this->load->view('form/inquiry.php'); ?>
                </div>
            </div>
		</div>
	</div>
</section>