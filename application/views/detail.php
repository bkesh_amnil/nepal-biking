<?php if(isset($content) && !empty($content)) { ?>
    <div class="detail-wrap">
        <div class="details">
            <a class="close-btn" href="#"></a>
            <?php if(isset($content->google_form_link) && !empty($content->google_form_link) && strtotime($content->unpublish_date) > strtotime(date('Y-m-d'))) { ?>
                <a class="btn-event-register" href="<?php echo $content->google_form_link ?>" target="_blank">Register Here</a>
            <?php } ?>
            <h3><?php echo $content->name ?></h3>
            <span class="post-by"> Publised On : <?php echo date('d F Y', strtotime($content->publish_date)) ?> </span>
            <?php echo $content->description ?>
            <?php if(isset($social_datas) && !empty($social_datas)) { ?>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_sharing_toolbox"></div>
            <?php } ?>
        </div>
    </div>
<?php } ?>