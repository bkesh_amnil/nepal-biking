<div class="container">
    <h3 class="sections-title-2">WHAT OUR CUSTOMER SAY</h3>
    <div class="row">
        <div class="col-xs-5 col-sm-2 col-lg-2">
            <div class="blockquote-wrap">
                <img src="<?php echo base_url('img/blockquote-left.png') ?>" /> 
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-8 col-lg-8">
            <div id="customer-review" class="owl-carousel">
            	<?php foreach($testimonials as $testimonial) { ?>
	                <div class="customer-comments">
                            <div class="row">
                                <?php if(!empty($testimonial->customer_image)) { ?>
                                <div class="col-lg-2 col-sm-3 col-xs-12">
                                    <img src="<?php echo base_url($testimonial->customer_image) ?>" />
                                </div>
                                <?php } ?>
                                <div class="customer-description col-lg-10 col-sm-9 col-xs-12">
                                    <span><?php echo $testimonial->customer_full_name ?></span> - <span><?php echo $testimonial->iso ?></span> - <span><?php echo date('m-d-Y', strtotime($testimonial->created_date)) ?></span> 
                                    <?php echo $testimonial->customer_testimonial ?>
                                </div>   
                            </div>
	                </div>
                <?php } ?>
            </div> 
        </div>                      
        
        <div class="col-xs-5 col-sm-2 col-lg-2 col-xs-offset-7 col-sm-offset-0">
            <div class="blockquote-wrap blockquote-wrap-btn-review">
                <img src="img/blockquote-right.png" />
                <a class="btn-write-review" href="javascript:void(0);">+ Write your review</a>
            </div>
        </div>
    </div>
</div>