<section class="package-banner">
	<div class="container">
    	<div class="row">
			<?php if(isset($banners) && !empty($banners)) { ?>
			    <?php $this->load->view('banner', $banners); ?>
			<?php } ?>
			<?php if(isset($highlighted_tours) && !empty($highlighted_tours)) { ?>
			    <?php $this->load->view('tour/highlighted_tours', $highlighted_tours); ?>
			<?php } ?>
		</div>
	</div>
</section>
<?php if(isset($latest_tours) && !empty($latest_tours)) { ?>
	<section class="pattern-bg">
        <?php $this->load->view('tour/latest_tours', $latest_tours); ?>
	</section>
<?php } ?>
<?php if(isset($latest_bike_tours) && !empty($latest_bike_tours)) { ?>
	<section class="hire-bike-section">
		<?php $this->load->view('bike/latest_list', $latest_bike_tours); ?>
	</section>
<?php } ?>
<?php if((isset($latest_news) && !empty($latest_news)) || (isset($latest_events) && !empty($latest_events))) { ?>
	<section class="keep-updated-wrap">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-lg-6">
					<?php $this->load->view('news/latest_list', $latest_news); ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-6">
					<?php $this->load->view('news/highlighted_list', $hot_news); ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-6">
					<?php $this->load->view('events/highlighted_list', $hot_events); ?>
				</div>
				<div class="col-xs-12 col-sm-6 col-lg-6">
					<?php $this->load->view('events/latest_list', $latest_events); ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>
<?php if(isset($testimonials) && !empty($testimonials)) { ?>
	<section class="customer-say">
		<?php $this->load->view('testimonial/slider_list', $testimonials); ?>
	</section>
<?php } ?>