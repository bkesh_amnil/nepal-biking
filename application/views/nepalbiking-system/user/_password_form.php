<form action="" method="post">
    <div class="form-group has-feedback">
        <input type="text" name="email" class="form-control" placeholder="Email Address"/>
        <span class="glyphicon glyphicon-envelop form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary btn-block btn-flat">Get Password</button>
        </div>
    </div>
</form>