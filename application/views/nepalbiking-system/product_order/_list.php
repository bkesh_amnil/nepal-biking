<form action="<?php echo base_url(BACKENDFOLDER . '/' . $this->header['page_name'] . '/export') ?>" method="post">
    <div class="searchBySite" style="width:auto;display:inline-block">
        <span style="width:75px;">Export Filter in Field</span>
        <select name="export_filter_id" id="export_filter_id">
            <option value="0">All</option>
            <option value="product_category" data-type="select">Product Category</option>
            <option value="product" data-type="select">Product</option>
            <option value="customer_name" data-type="select">Customer Name</option>
            <option value="booking_status" data-type="select">Booking Status</option>
            <option value="order_date" data-type="input">Order Date</option>
        </select>
        <div class="searchFields" style="float: right;">
            <select name="export_filter_product_category" id="export_filter_product_category">
                <?php
                if(isset($product_order_categories) && !empty($product_order_categories)) {
                    foreach($product_order_categories as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_product" id="export_filter_product">
                <?php
                if(isset($product_orders) && !empty($product_orders)) {
                    foreach($product_orders as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_customer_name" id="export_filter_customer_name">
                <?php
                if(isset($customers) && !empty($customers)) {
                    foreach($customers as $val) {
                        ?>
                        <option value="<?php echo $val->customer_full_name ?>"><?php echo $val->customer_full_name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_booking_status" id="export_filter_booking_status">
                <?php
                if(isset($booking_status) && !empty($booking_status)) {
                    foreach($booking_status as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <input name="export_filter_order_date" id="export_filter_order_date" placeholder="Y-m-d">
        </div>
    </div>
    <input type="submit" name="submit_button" value="Export">
</form>
<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Product Name</th>
            <th></th>
            <th>Customer Name</th>
            <th>Ordered Date</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Order Status</th>
            <th></th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Product Name</th>
            <th>Order Made By</th>
            <th>Customer Name</th>
            <th>Ordered Date</th>
            <th>Quantity</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Order Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($rows) : $serial_number = 1; ?>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                    <td><?php echo $row->product_name ?></td>
                    <td><?php echo ($row->created_by != '0') ? get_userdata('name') : $row->customer_full_name  ?></td>
                    <td><?php echo $row->customer_full_name ?></td>
                    <td><?php echo date('Y-m-d', $row->created_date) ?></td>
                    <td><?php echo $row->product_quantity ?></td>
                    <td><?php echo $row->product_price ?></td>
                    <td><?php echo $row->total_price ?></td>
                    <td><?php echo $row->booking_status ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'product_order',
                            'moduleData' => $row,
                            'options' => 'EDS'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>