<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <?php
    $disable = '';
    if($id != '') {
        $disable = ' disabled="disabled"';
        ?>
        <input type="hidden" name="product_category_id" value="<?php echo $product_order->product_category_id ?>">
        <input type="hidden" name="product_id" value="<?php echo $product_order->product_id ?>">
        <input type="hidden" name="product_quantity" value="<?php echo $product_order_status->product_quantity ?>">
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="product_category_id">Product Category *</label>

                <select name="product_category_id" id="product_category_id" class="form-control"<?php echo $disable ?>>
                    <option>Select</option>
                    <?php
                    if(isset($product_categories) && !empty($product_categories)) {
                        foreach($product_categories as $product_category) {
                            ?>
                            <option disabled><?php echo $product_category->name ?></option>
                            <?php
                            if(!empty($product_category->childs)) {
                                foreach($product_category->childs as $child) {
                                    $selected = '';
                                    if($child->id == $product_order->product_category_id) {
                                        $selected = ' selected="selected"';
                                    }
                                    ?>
                                    <option value="<?php echo $child->id ?>"<?php echo $selected ?>><?php echo '--'.$child->name ?></option>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="code">Product *</label>
                <select name="product_id" id="product_id" class="form-control"<?php echo $disable ?>>
                    <option value="Select">Select</option>
                    <?php
                    if(isset($products) && !empty($products)) {
                        foreach($products as $val) {
                            $selected = '';
                            if($val->id == $product_order->product_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" name="product_series" value="" class="form-control" id="product_series" placeholder="Product Series">
            <div class="form-group">
                <label for="product_model">Product Model *</label>
                <input type="hidden" name="product_model" value="<?php echo $product_order->product_model ?>" class="form-control">
                <input type="text" name="product_model" value="<?php echo $product_order->product_model ?>" class="form-control" id="product_model" placeholder="Product Model" disabled>
            </div>
           <div class="form-group">
                <label for="product_quantity">Product Quantity *</label>
                <select name="product_quantity" id="product_quantity" class="form-control"<?php echo $disable ?>>
                <?php
                $range = range(1, 50);
                foreach($range as $val) {
                    $selected = '';
                    if($val == $product_order_status->product_quantity) {
                        $selected = ' selected="selected"';
                    }
                    ?>
                    <option value="<?php echo $val ?>"<?php echo $selected ?>><?php echo $val ?></option>
                    <?php
                }
                ?>
                </select>
            </div>
            <div class="form-group">
                <label for="product_price">Unit Price (in $) *</label>
                <input type="hidden" name="product_price" value="<?php echo $product_order_status->product_price ?>" class="form-control">
                <input type="text"<?php echo $disable ?> name="product_price" value="<?php echo $product_order_status->product_price ?>" class="form-control" id="product_price" placeholder="Product Price" disabled>
            </div>
            <div class="form-group">
                <label for="total_price">Total Cost (in $) *</label>
                <input type="hidden" name="total_price" value="<?php echo $product_order_status->total_price ?>" class="form-control">
                <input type="text"<?php echo $disable ?> name="total_price" value="<?php echo $product_order_status->total_price ?>" class="form-control" id="total_price" placeholder="Total Cost (in $)" disabled>
            </div>
            <div class="form-group">
                <label for="customer_full_name">Full Name *</label>
                <input type="text" name="customer_full_name" value="<?php echo $product_order_status->customer_full_name ?>" class="form-control" id="customer_full_name" placeholder="Full Name">
            </div>
            <div class="form-group">
                <label for="customer_email_address">Email Address</label>
                <input type="text" name="customer_email_address" value="<?php echo $product_order_status->customer_email_address ?>" class="form-control" id="customer_email_address" placeholder="Email Address">
            </div>
            <div class="form-group">
                <label for="customer_contact_number">Contact Nos. *</label>
                <input type="text" name="customer_contact_number" value="<?php echo $product_order_status->customer_contact_number ?>" class="form-control" id="customer_contact_number" placeholder="Contact Nos.">
            </div>
            <div class="form-group">
                <label for="customer_special_instruction">Special Instruction *</label>
                <textarea rows="7" name="customer_special_instruction" class="form-control" id="customer_special_instruction" placeholder="Special Instruction"><?php echo $product_order_status->customer_special_instruction ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('customer_special_instruction', true)
                    };
                </script>
            </div>
            <div class="form-group">
                <label for="product_order_status_id">Order Status *</label>
                <select name="product_order_status_id" id="product_order_status_id" class="form-control">
                    <option value="1" <?php echo (isset($product_order_status->product_order_status_id) && $product_order_status->product_order_status_id == '1') ? 'selected="selected"' : '' ?>>Pending</option>
                    <option value="2" <?php echo (isset($product_order_status->product_order_status_id) && $product_order_status->product_order_status_id == '2') ? 'selected="selected"' : '' ?>>Confirmed</option>
                    <?php if($id != '') { ?>
                        <option value="3" <?php echo (isset($product_order_status->product_order_status_id) && $product_order_status->product_order_status_id == '3') ? 'selected="selected"' : '' ?>>Cancelled</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="product_note">Note from DTD</label>
                <textarea rows="7" name="product_note" class="form-control" id="product_note" placeholder="Note from DTD"></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('product_note', true)
                    };
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <h3>Booking Status</h3>
            <div class="form-group">
                <label for="product_name">Product Name</label>
                <span id="selected_product_name"><?php echo isset($product_order_status->product_name) ? $product_order_status->product_name : '' ?></span>
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <span id="selected_quantity"><?php echo isset($product_order_status->product_quantity) ? $product_order_status->product_quantity : '' ?></span>
            </div>
            <div class="form-group">
                <label for="unit_price">Unit Price</label>
                <span id="selected_unit_price"><?php echo isset($product_order_status->product_price) ? $product_order_status->product_price : '' ?></span>
            </div>
            <div class="form-group">
                <label for="total_price">Total Price</label>
                <span id="selected_total_price"><?php echo isset($product_order_status->total_price) ? $product_order_status->total_price : '' ?></span>
            </div>
            <?php if($id != '') { ?>
                <h3>Booking Logs</h3>
                <?php if (isset($logs) && !empty($logs)) { $i = 1; ?>
                    <div class="panel-group" id="accordion">
                        <?php foreach ($logs as $log) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="profie-history-title" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $i ?>">
                                            <div class="history-title"><?php echo '<span>Order Date : </span>' . '<span>' . date('Y-m-d H:i:s', $log->created_date) . '</span>' ?></div>
                                            <div class="history-title"><?php echo '<sapn>Order Status : </span>' . '<span>' . $log->booking_status . '</span>' ?></div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="<?php echo $i ?>" class="panel-collapse collapse <?php echo $i == 1 ? 'in' : '' ?>">
                                    <div class="panel-body">
                                        <?php echo (!empty($log->product_note)) ? $log->product_note : 'No Note'; ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; } ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>