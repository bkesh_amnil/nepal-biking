<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="menu_id">Menu *</label>
                <select name="menu_id" id="menu_id" class="form-control">
                    <?php
                    if(isset($menus) && !empty($menus)) {
                        foreach($menus as $menu) {
                            $selected = '';
                            if($menu->id == $image_manager->menu_id) {
                                $selected = ' selected="selected"';
                            }
                        ?>
                        <option value="<?php echo $menu->id ?>"<?php echo $selected ?>><?php echo $menu->name ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $image_manager->status == '1' || $image_manager->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $image_manager->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image (1920 X 1199)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $image_manager->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($image_manager->image != '') { ?>
                    <img src="<?php echo base_url($image_manager->image) ?>" width="25%"/>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>