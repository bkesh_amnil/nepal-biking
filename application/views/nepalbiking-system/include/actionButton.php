<?php
// getting module permission
$ci = &get_instance();
$permission = $ci->checkModulePermission($ci->data['active_module_id']);
?>

<?php if(substr($actionBtnData['options'], 0, 1) == 'E') { ?> <!-- checking to display options -->
    <?php if($actionBtnData['module'] != 'tour_map' && $actionBtnData['module'] != 'subscription' && $activeModulePermission['edit']) {
        $action = true;
        switch($actionBtnData['module']) {
            case 'tour_inclusion_exclusion':
            case 'tour_map':
            case 'tour_price':
            case 'tour_departure':
            case 'tour_gallery':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/create/'.$actionBtnData['moduleData']->tour_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'bike_price':
            case 'bike_gallery':
            case 'bike_rent':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/create/'.$actionBtnData['moduleData']->bike_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'product_gallery';
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/create/'.$actionBtnData['moduleData']->product_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'form_fields':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/create/'.$actionBtnData['moduleData']->form_id.'/'.$actionBtnData['moduleData']->id);
                break;
            default:
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/create/'.$actionBtnData['moduleData']->id);
                break;
        }
        ?>
        <a title="Edit Data" href="<?php echo $hrefUrl ?>" class="btn btn-primary">
            <i class="fa fa-edit fa-fw"></i>
        </a>
    <?php } ?>
<?php } ?>

<?php if(substr($actionBtnData['options'], 1, 1) == 'D') { ?> <!-- checking to display options -->
    <?php if($activeModulePermission['delete'] && $actionBtnData['module'] != 'role' && $actionBtnData['module'] != 'tour_book' && $actionBtnData['module'] != 'bike_book' && $actionBtnData['module'] != 'product_order') {
        if($actionBtnData['module'] == 'module'){
            if($actionBtnData['moduleData']->deletable) {
                $action = true;
                ?>
                <a title="Delete Data" href="<?php echo base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->id) ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                    <i class="fa fa-trash fa-fw"></i>
                </a>
                <?php
            }
        } else {
        $action = true;
        switch($actionBtnData['module']) {
            case 'tour_inclusion_exclusion':
            case 'tour_map':
            case 'tour_price':
            case 'tour_departure':
            case 'tour_gallery':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->tour_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'bike_price':
            case 'bike_gallery':
            case 'bike_rent':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->bike_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'product_gallery';
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->product_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'form_fields':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->form_id.'/'.$actionBtnData['moduleData']->id);
                break;
            default:
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/delete/'.$actionBtnData['moduleData']->id);
                break;
        }
        ?>
        <a title="Delete Data" href="<?php echo $hrefUrl ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">
            <i class="fa fa-trash fa-fw"></i>
        </a>
    <?php } } ?>
<?php } ?>

<?php if(substr($actionBtnData['options'], 2, 1) == 'S') { ?> <!-- checking to display options -->
    <?php if($actionBtnData['module'] != 'role' && $actionBtnData['module'] != 'user' && $actionBtnData['module'] != 'tour_map' && $actionBtnData['module'] != 'tour_book' && $actionBtnData['module'] != 'bike_book' && $actionBtnData['module'] != 'product_order' && $actionBtnData['module'] != 'subscription' && $actionBtnData['module'] != 'form_fields' && $actionBtnData['module'] != 'tour_enquire' && $activeModulePermission['edit']) {
        $action = true;
        switch($actionBtnData['module']) {
            case 'tour_inclusion_exclusion':
            case 'tour_price':
            case 'tour_departure':
            case 'tour_gallery':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/status/'.$actionBtnData['moduleData']->status.'/'.$actionBtnData['moduleData']->tour_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'bike_price':
            case 'bike_gallery':
            case 'bike_rent':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/status/'.$actionBtnData['moduleData']->status.'/'.$actionBtnData['moduleData']->bike_id.'/'.$actionBtnData['moduleData']->id);
                break;
            case 'product_gallery';
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/status/'.$actionBtnData['moduleData']->status.'/'.$actionBtnData['moduleData']->product_id.'/'.$actionBtnData['moduleData']->id);
                break;
            default:
                $hrefUrl = base_url(BACKENDFOLDER.'/'.$actionBtnData['module'].'/status/'.$actionBtnData['moduleData']->status.'/'.$actionBtnData['moduleData']->id);
                break;
        }
        ?>
        <?php
        if($actionBtnData['moduleData']->status == '0') {
            $icon_class = 'fa-circle-o';
            $button_class = 'btn-info';
            $button_text = '';
            $title = 'Click to Active';
        } else {
            $icon_class = 'fa-circle';
            $button_class = 'btn-success';
            $button_text = '';
            $title = 'Click to Inactive';
        }
        ?>
        <a title="<?php echo $title ?>" href="<?php echo $hrefUrl  ?>" class="btn <?php echo $button_class ?>" onclick="return confirm('Are you sure?')">
            <i class="fa <?php echo $icon_class ?> fa-fw"></i><?php echo $button_text ?>
        </a>
    <?php } ?>
<?php } ?>
<?php
if($actionBtnData['module'] == 'event' || $actionBtnData['module'] == 'race') {
    if (!empty($actionBtnData['moduleData']->google_form_link)) {
        ?>
        <a title="Show Form Data"
           href="<?php echo base_url(BACKENDFOLDER . '/import_form_data/formdata/' . $actionBtnData['module'] . '/' . $actionBtnData['moduleData']->id) ?>"
           class="btn btn-primary">
            <i class="fa fa-list fa-fw"></i>
        </a>
        <?php
    }
}
?>

<?php echo !(isset($action)) ? 'No permission granted for other actions' : '' ?>