<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <?php
    $disable = '';
    if($id  != '') {
        $disable = ' disabled="disabled"';
        ?>
        <input type="hidden" name="bike_category_id" value="<?php echo $bike_book->bike_category_id ?>">
        <input type="hidden" name="bike_id" value="<?php echo $bike_book->bike_id ?>">
        <input type="hidden" name="previous_rent_date_id" value="<?php echo $bike_book_status->rent_date_id ?>">
        <input type="hidden" name="previous_book_status" value="<?php echo $bike_book_status->bike_booking_status_id ?>">
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="bike_category_id">Bike Category *</label>
                <select name="bike_category_id" id="bike_category_id" class="form-control" <?php echo $disable ?>>
                    <?php
                    if(isset($bike_categories) && !empty($bike_categories)) {
                        foreach($bike_categories as $bike_category) {
                            $selected = '';
                            if($bike_category->id == $bike_book->bike_category_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $bike_category->id ?>"<?php echo $selected ?>><?php echo $bike_category->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="bike_id">Bike Name *</label>
                <select name="bike_id" id="bike_id" class="form-control"<?php echo $disable ?>>
                    <?php
                    if(isset($bikes) && !empty($bikes)) {
                        foreach($bikes as $val) {
                            $selected = '';
                            if($val->id == $bike_book->bike_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    } else {
                        ?>
                        <option>No Bike Available</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" name="bike_available" value="">
            <div class="form-group">
                <label for="rent_date_id">Rent Date *</label>
                <select name="rent_date_id" id="rent_date_id" class="form-control">
                    <?php
                    $date_arr = array();
                    if(isset($rent_dates) && !empty($rent_dates)) {
                        foreach($rent_dates as $val) {
                            if(in_array($val->id, $date_arr)) {} else {
                                $date_arr[$val->id] = $val;
                            }
                        }
                        if(isset($date_arr) && !empty($date_arr)) {
                            foreach($date_arr as $val) {
                                $selected = '';
                                $disable = '';
                                if(isset($logs[0]->rent_date_id) && $val->id == $logs[0]->rent_date_id) {
                                    $selected = ' selected="selected"';
                                }
                                if($val->all_allocation == $val->booked) {
                                    $disable = ' disabled="disabled"';
                                }
                                ?>
                                <option value="<?php echo $val->id ?>"<?php echo $disable ?> <?php echo $selected ?>><?php echo $val->rent_date ?></option>
                                <?php
                            }
                        }
                    } else {
                        if($id != '') {
                            ?>
                            <option value="<?php echo $logs[0]->rent_date_id ?>" selected="selected"><?php echo $logs[0]->rent_date ?></option>
                            <?php
                        } else {
                            $date = 0;
                            ?>
                            <option>No Date/Price Available</option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="number_of_days">Rent Duration Days *</label>
                <?php
                $range = range(1, 30);
                /*$str = $rent_dates[0]->name;
                $str = preg_replace('/\D/', '', $str);
                $range = range(1, $str);*/
                ?>
                <select name="number_of_days_id" id="number_of_days_id" class="form-control">
                    <?php
                    if(isset($range) && !empty($range)) {
                        foreach($range as $val) {
                            $selected = '';
                            if($val == $bike_book_status->number_of_days_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val ?>"<?php echo $selected ?>><?php echo $val ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="number_of_pax">Number of pax *</label>
                <select name="number_of_pax" id="number_of_pax" class="form-control">
                    <?php
                    $range = range(1, 20);
                    foreach($range as $val) {
                        $selected = '';
                        if($val == $logs[0]->number_of_pax) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $val ?>"<?php echo $selected ?>><?php echo $val ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="price_per_person">Price per Person(in $) *</label>
                <?php if($id == '') { ?>
                <input type="hidden" name="price_per_person" value="<?php echo isset($rent_dates[0]->unit_price) && !empty($rent_dates[0]->unit_price) ? $rent_dates[0]->unit_price : $bike_book_status->price_per_person ?>">
                <input type="text" name="price_per_person" value="<?php echo isset($rent_dates[0]->unit_price) && !empty($rent_dates[0]->unit_price) ? $rent_dates[0]->unit_price : $bike_book_status->price_per_person ?>" class="form-control" id="price_per_person" placeholder="Price per Person(in $)" disabled>
                <?php } else { ?>
                <input type="hidden" name="price_per_person" value="<?php echo $bike_book_status->price_per_person ?>">
                <input type="text" name="price_per_person" value="<?php echo $bike_book_status->price_per_person ?>" class="form-control" id="price_per_person" placeholder="Price per Person(in $)" disabled>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="total_price">Total Cost(in $) *</label>
                <?php if($id == '') { ?>
                <input type="hidden" class="total_price" name="total_price" value="<?php echo isset($rent_dates[0]->unit_price) && !empty($rent_dates[0]->unit_price) ? $rent_dates[0]->unit_price : $bike_book_status->total_price ?>">
                <input type="text" name="total_price" value="<?php echo isset($rent_dates[0]->unit_price) && !empty($rent_dates[0]->unit_price) ? $rent_dates[0]->unit_price : $bike_book_status->total_price ?>" class="form-control total_price" id="total_price" placeholder="Total Cost(in $)" disabled>
                <?php } else { ?>
                <input type="hidden" class="total_price" name="total_price" value="<?php echo $bike_book_status->total_price ?>">
                <input type="text" name="total_price" value="<?php echo $bike_book_status->total_price ?>" class="form-control total_price" id="total_price" placeholder="Total Cost(in $)" disabled>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="customer_full_name">Full Name *</label>
                <input type="text" name="customer_full_name" value="<?php echo $bike_book_status->customer_full_name ?>" class="form-control" id="customer_full_name" placeholder="Full Name">
            </div>
            <div class="form-group">
                <label for="customer_email_address">Email Address *</label>
                <input type="text" name="customer_email_address" value="<?php echo $bike_book_status->customer_email_address ?>" class="form-control" id="customer_email_address" placeholder="Email Address">
            </div>
            <div class="form-group">
                <label for="customer_dob">Date of Birth </label>
                <input type="text" name="customer_dob" value="<?php echo $bike_book_status->customer_dob ?>" class="form-control" id="customer_dob" placeholder="Date of Birth">
            </div>
            <div class="form-group">
                <label for="customer_nationality">Nationality </label>
                <select name="customer_nationality" id="customer_nationality" class="form-control">
                    <?php
                    if(isset($nationalities) && !empty($nationalities)) {
                        foreach($nationalities as $val) {
                            $selected = '';
                            if($val->name == $logs[0]->customer_nationality) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->name ?>" <?php echo $selected ?>><?php echo $val->printable_name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="customer_passport_number">Passport Nos. </label>
                <input type="text" name="customer_passport_number" value="<?php echo $bike_book_status->customer_passport_number ?>" class="form-control" id="customer_passport_number" placeholder="Passpost Nos.">
            </div>
            <div class="form-group">
                <label for="customer_contact_number">Contact Nos. *</label>
                <input type="text" name="customer_contact_number" value="<?php echo $bike_book_status->customer_contact_number ?>" class="form-control" id="customer_contact_number" placeholder="Contact Nos">
            </div>
            <div class="form-group">
                <label for="customer_contact_address">Contact Address *</label>
                <input type="text" name="customer_contact_address" value="<?php echo $bike_book_status->customer_contact_address ?>" class="form-control" id="customer_contact_address" placeholder="Contact Address">
            </div>
            <div class="form-group">
                <label for="customer_room_number">Room Nos </label>
                <input type="text" name="customer_room_number" value="<?php echo $bike_book_status->customer_room_number ?>" class="form-control" id="customer_room_number" placeholder="Room Nos">
            </div>
            <div class="form-group">
                <label for="customer_local_address">Local Address </label>
                <input type="text" name="customer_local_address" value="<?php echo $bike_book_status->customer_local_address ?>" class="form-control" id="customer_local_address" placeholder="Local Address">
            </div>
            <div class="form-group">
                <label for="bike_booking_status_id">Booking Status *</label>
                <select name="bike_booking_status_id" id="bike_booking_status_id" class="form-control">
                    <option value="1" <?php echo (isset($bike_book_status->bike_booking_status_id) && $bike_book_status->bike_booking_status_id == '1') ? 'selected="selected"' : '' ?>>Pending</option>
                    <option value="2" <?php echo (isset($bike_book_status->bike_booking_status_id) && $bike_book_status->bike_booking_status_id == '2') ? 'selected="selected"' : '' ?>>Confirmed</option>
                    <?php if($id != '') { ?>
                        <option value="3" <?php echo (isset($bike_book_status->bike_booking_status_id) && $bike_book_status->bike_booking_status_id == '3') ? 'selected="selected"' : '' ?>>Cancelled</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="bike_note">Note from DTD</label>
                <textarea rows="7" name="bike_note" class="form-control" id="bike_note" placeholder="Note from DTD"><?php //echo $bike_book_status->bike_note ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('bike_note', true)
                    };
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <h3>Booking Status</h3>
            <div class="form-group">
                <label for="bike_name">Bike Name</label>
                <span id="selected_bike_name"><?php echo (isset($bikes) && !empty($bikes)) ? $bikes[0]->name : '' ?></span>
            </div>
            <div class="form-group">
                <label for="rent_date">Rent Date</label>
                <span id="selected_rent_date">
                    <?php
                    if($id != '') {
                        echo $bike_book_status->rent_date;
                    } else {
                        echo (isset($rent_dates) && !empty($rent_dates)) ? $rent_dates[0]->rent_date : '';
                    }
                    ?>
                </span>
            </div>
            <div class="form-group">
                <label for="total_booking_available">Total Booking Available</label>
                <span id="selected_booking_available">
                    <?php
                    if($id != '') {
                        echo $bike_book_status->allocation;
                    } else {
                        echo (isset($rent_dates) && !empty($rent_dates)) ? $rent_dates[0]->all_allocation : '';
                    }
                    ?>
                </span>
            </div>
            <div class="form-group">
                <label for="total_booked">Total Booked</label>
                <span id="selected_total_booked">
                    <?php
                    if($id != '') {
                        echo $bike_book_status->booked;
                    } else {
                        echo (isset($rent_dates) && !empty($rent_dates)) ? $rent_dates[0]->booked : '';
                    }
                    ?>
                </span>
            </div>
            <div class="form-group">
                <label for="total_available">Total Available</label>
                <span id="selected_total_available"><?php echo (isset($rent_dates) && !empty($rent_dates)) ? $rent_dates[0]->available : $bike_book_status->available ?></span>
            </div>
            <?php if($id != '') { ?>
                <h3>Booking Logs</h3>
                <?php if (isset($logs) && !empty($logs)) { $i = 1; ?>
                    <div class="panel-group" id="accordion">
                        <?php foreach ($logs as $log) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="profie-history-title" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $i ?>">
                                            <div class="history-title"><?php echo '<span>Book/Update Date : </span>' . '<span>' . date('Y-m-d H:i:s', $log->created_date) . '</span>' ?></div>
                                            <div class="history-title"><?php echo '<span>Rent Date : </span>' . '<span>' . $log->rent_date . '</span>' ?></span></div>
                                            <div class="history-title"><?php echo '<sapn>Booking Status : </span>' . '<span>' . $log->booking_status . '</span>' ?></div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="<?php echo $i ?>" class="panel-collapse collapse <?php echo $i == 1 ? 'in' : '' ?>">
                                    <div class="panel-body">
                                        <?php echo (!empty($log->bike_note)) ? $log->bike_note : 'No Note'; ?>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } ?>
                    </div>
                <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>