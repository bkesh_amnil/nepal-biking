<form action="<?php echo base_url(BACKENDFOLDER . '/' . $this->header['page_name'] . '/export') ?>" method="post">
    <div class="searchBySite" style="width:auto;display:inline-block">
        <span style="width:75px;">Export Filter in Field</span>
        <select name="export_filter_id" id="export_filter_id">
            <option value="0">All</option>
            <option value="bike_category" data-type="select">Bike Category</option>
            <option value="bike" data-type="select">Bike</option>
            <option value="customer_name" data-type="select">Customer Name</option>
            <option value="rent_status" data-type="select">Rent Status</option>
            <!--<option value="rent_days" data-type="select">Rent Days</option>-->
            <option value="rent_date" data-type="input">Rent Date</option>
        </select>
        <div class="searchFields" style="float: right;">
            <select name="export_filter_bike_category" id="export_filter_bike_category">
                <?php
                if(isset($bike_categories) && !empty($bike_categories)) {
                    foreach($bike_categories as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_bike" id="export_filter_bike">
                <?php
                if(isset($bikes) && !empty($bikes)) {
                    foreach($bikes as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_customer_name" id="export_filter_customer_name">
                <?php
                if(isset($customers) && !empty($customers)) {
                    foreach($customers as $val) {
                        ?>
                        <option value="<?php echo $val->customer_full_name ?>"><?php echo $val->customer_full_name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_rent_status" id="export_filter_rent_status">
                <?php
                if(isset($rent_status) && !empty($rent_status)) {
                    foreach($rent_status as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <!--<select name="export_filter_rent_days" id="export_filter_rent_days">
                <?php
/*                if(isset($rent_days) && !empty($rent_days)) {
                    foreach($rent_days as $val) {
                        */?>
                        <option value="<?php /*echo $val->id */?>"><?php /*echo $val->name */?></option>
                        <?php
/*                    }
                }
                */?>
            </select>-->
            <input name="export_filter_rent_date" id="export_filter_rent_date" placeholder="Y-m-d">
        </div>
    </div>
    <input type="submit" name="submit_button" value="Export">
</form>
<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Bike Name</th>
            <th></th>
            <th>Customer Name</th>
            <th>Rent Date</th>
            <th>Booked Date</th>
            <th>Number of Pax</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Booking Status</th>
            <th></th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Bike Name</th>
            <th>Booked By</th>
            <th>Customer Name</th>
            <th>Rent Date</th>
            <th>Booked Date</th>
            <th>Number of Pax</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Booking Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($bike_bookings) : $serial_number = 1; ?>
            <?php foreach ($bike_bookings as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                    <td><?php echo $row->bike_name ?></td>
                    <td><?php echo ($row->created_by != '0') ? get_userdata('name') : $row->customer_full_name  ?></td>
                    <td><?php echo $row->customer_full_name ?></td>
                    <td><?php echo $row->rent_date ?></td>
                    <td><?php echo date('Y-m-d', $row->created_date) ?></td>
                    <td><?php echo $row->number_of_pax ?></td>
                    <td><?php echo $row->price_per_person ?></td>
                    <td><?php echo $row->total_price ?></td>
                    <td><?php echo $row->booking_status ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'bike_book',
                            'moduleData' => $row,
                            'options' => 'EDS'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>