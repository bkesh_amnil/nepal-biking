<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?php 
            foreach ($fields as $field) {
                $rules = explode('|', $field->validation_rule);
                $field_name = 'field_name[' . $field->id . ']';
                ?>
            <div class="form-group">
                <label for="<?php echo $field->field_name ?>"><?php echo $field->field_label ?><?php echo (in_array('required', $rules)) ? '*' : '' ?></label>
                <?php
                switch($field->field_type) {
                    case 'textarea':
                        ?>
                        <textarea rows="7" name="<?php echo $field_name ?>" class="form-control" id="<?php echo $field->field_name ?>" placeholder="<?php echo $field->field_label ?>"><?php echo $field->default_value ?></textarea>
                        <script>
                            window.onload = function() {
                                load_ckeditor('<?php echo $$field_name ?>', true)
                            };
                        </script>
                        <?php
                        break;
                    case 'upload':
                        ?>
                        <input type="file" name="<?php echo $field_name ?>" value="<?php echo $field->default_value ?>" class="form-control <?php echo $field->field_class ?>" id="<?php echo $field->field_name ?>" placeholder="<?php echo $field->field_label ?>">
                        <?php
                        break;
                    case 'password':
                        ?>
                        <input type="password" name="<?php echo $field_name ?>" value="<?php echo $field->default_value ?>" class="form-control <?php echo $field->field_class ?>" id="<?php echo $field->field_name ?>" placeholder="<?php echo $field->field_label ?>">
                        <?php
                        break;
                    case 'hidden':
                        ?>
                        <input type="hidden" name="<?php echo $field_name ?>" value="<?php echo $field->default_value ?>" class="form-control <?php echo $field->field_class ?>" id="<?php echo $field->field_name ?>" placeholder="<?php echo $field->field_label ?>">
                        <?php
                        break;
                    case 'select':
                        $result = $this->form_model->getFormFieldValues($field->id);
                        if(isset($result) && !empty($result)) {
                        ?>
                        <select name="<?php echo $field_name; ?>" class="form-control">
                            <?php foreach($result as $val) { ?>
                            <option value="<?php echo $val->display_text; ?>"><?php echo $val->display_text; ?></option>
                            <?php } ?>
                        </select>
                        <?php
                        }
                        break;
                    case 'radio':
                        $result = Model_FormField::getFormFieldValues($field->id);
                        $attribute = array('class' => $field->field_class, 'id' => 'dynamic_field_'.$field->id);
                        if(isset($result) && !empty($result)) {
                            foreach($result as $row) {
                                $checked = ($row->display_text == $field->default_value)?TRUE:FALSE;
                                echo Form::radio($field_name, $row->display_text, $checked, $attribute) . $row->display_text . ' &nbsp; ';
                            }
                        }
                        break;
                    case 'multiple_upload':
                        break;
                    default:
                        ?>
                        <input type="text" name="<?php echo $field_name ?>" value="<?php echo $field->default_value ?>" class="form-control <?php echo $field->field_class ?>" id="<?php echo $field->field_name ?>" placeholder="<?php echo $field->field_label ?>">
                        <?php
                        break;
                }
                ?>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <?php if($form_action == 'both') { ?>
                <button type="submit" class="btn btn-primary">Save</button>
                <?php } ?>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $form_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>