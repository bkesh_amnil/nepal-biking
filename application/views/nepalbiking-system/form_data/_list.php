<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tr>
            <th></th>
            <?php foreach($fields as $field): ?>
            <th><?php echo $field->field_label ?></th>
            <?php endforeach; ?>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($submitted_forms) : $serial_number = 1; ?>
            <?php foreach ($submitted_forms as $submitted_form_id => $submitted_form) : ?>
                <tr class="row_<?php echo $submitted_form_id; ?>" id="<?php echo $submitted_form_id; ?>">
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <?php foreach($fields as $field) : ?>
                    <td rel="<?php echo $submitted_form_id ?>">&nbsp;<?php echo $submitted_form[$field->id] ?></td>
                    <?php endforeach; ?>
                    <td>
                        <a title="Edit Data" href="<?php echo base_url(BACKENDFOLDER.'/form_data/create/'.$form_id.'/'.$submitted_form_id) ?>" class="btn btn-primary">
                            <i class="fa fa-edit fa-fw"></i>
                        </a>
                        <a title="Delete Data" href="<?php echo base_url(BACKENDFOLDER.'/form_data/delete/'.$form_id.'/'.$submitted_form_id) ?>" class="btn btn-danger" onclick="return confirm('Are you sure?')">
                    <i class="fa fa-trash fa-fw"></i>
                </a>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <!-- <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr> -->
        <?php endif; ?>
        </tbody>
    </table>
</form>