<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <?php
    $disable = '';
    if($id  != '') {
        $disable = ' disabled="disabled"';
        ?>
        <input type="hidden" name="tour_category_id" value="<?php echo $tour_book->tour_category_id ?>">
        <input type="hidden" name="tour_id" value="<?php echo $tour_book->tour_id ?>">
        <input type="hidden" name="previous_departure_date_id" value="<?php echo $tour_book_status->departure_date_id ?>">
        <input type="hidden" name="previous_book_status" value="<?php echo $tour_book_status->tour_booking_status_id ?>">
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="tour_category_id">Tour Category *</label>
                <select name="tour_category_id" id="tour_category_id" class="form-control" <?php echo $disable ?>>
                    <?php
                    if(isset($tour_categories) && !empty($tour_categories)) {
                        foreach($tour_categories as $tour_category) {
                            $selected = '';
                            if($tour_category->id == $tour_book->tour_category_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $tour_category->id ?>"<?php echo $selected ?>><?php echo $tour_category->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="tour_id">Tour Name *</label>
                <select name="tour_id" id="tour_id" class="form-control"<?php echo $disable ?>>
                    <?php
                    if(isset($tours) && !empty($tours)) {
                        foreach($tours as $val) {
                            $selected = '';
                            if($val->id == $tour_book->tour_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    } else {
                        ?>
                        <option>No Tour Available</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="departure_date_id">Departure Date *</label>
                <select name="departure_date_id" id="departure_date_id" class="form-control">
                    <?php
                    if(isset($departure_dates) && !empty($departure_dates)) {
                        foreach($departure_dates as $val) {
                            $selected = '';
                            $disable = '';
                            if($val->id == $tour_book_status->departure_date_id) {
                                $selected = ' selected="selected"';
                            }
                            if($val->tour_all_allocation == $val->tour_booked) {
                                $disable = ' disabled="disabled"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?><?php echo $disable ?>><?php echo $val->tour_departure_date ?></option>
                            <?php
                        }
                    } else {
                        if($id != '') {
                            ?>
                            <option value="<?php echo $logs[0]->departure_date_id ?>" selected="selected"><?php echo $logs[0]->tour_departure_date ?></option>
                            <?php
                        } else {
                            $date = 0;
                            ?>
                            <option>No Date/Price Available</option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="number_of_pax">Number of pax *</label>
                <select name="number_of_pax" id="number_of_pax" class="form-control">
                    <option value="Select">Select</option>
                    <?php
                    $range = range(1, 20);
                    foreach($range as $val) {
                        $selected = '';
                        if($val == $tour_book_status->number_of_pax) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $val ?>"<?php echo $selected ?>><?php echo $val ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="price_per_person">Price per Person(in $) *</label>
                <input type="hidden" name="price_per_person" value="<?php echo $tour_book_status->price_per_person ?>">
                <input type="text" name="price_per_person" value="<?php echo $tour_book_status->price_per_person ?>" class="form-control" id="price_per_person" placeholder="Price per Person(in $)" disabled>
            </div>
            <div class="form-group">
                <label for="total_price">Total Cost(in $) *</label>
                <input type="hidden" name="total_price" value="<?php echo $tour_book_status->total_price ?>">
                <input type="text" name="total_price" value="<?php echo $tour_book_status->total_price ?>" class="form-control" id="total_price" placeholder="Total Cost(in $)" disabled>
            </div>
            <div class="form-group">
                <label for="customer_full_name">Full Name *</label>
                <input type="text" name="customer_full_name" value="<?php echo $tour_book_status->customer_full_name ?>" class="form-control" id="customer_full_name" placeholder="Full Name">
            </div>
            <div class="form-group">
                <label for="customer_email_address">Email Address *</label>
                <input type="text" name="customer_email_address" value="<?php echo $tour_book_status->customer_email_address ?>" class="form-control" id="customer_email_address" placeholder="Email Address">
            </div>
            <div class="form-group">
                <label for="customer_contact_address">Contact Address *</label>
                <input type="text" name="customer_contact_address" value="<?php echo $tour_book_status->customer_contact_address ?>" class="form-control" id="customer_contact_address" placeholder="Contact Address">
            </div>
            <div class="form-group">
                <label for="customer_contact_number">Contact Nos *</label>
                <input type="text" name="customer_contact_number" value="<?php echo $tour_book_status->customer_contact_number ?>" class="form-control" id="customer_contact_number" placeholder="Contact Nos">
            </div>
            <div class="form-group">
                <label for="tour_booking_status_id">Booking Status *</label>
                <select name="tour_booking_status_id" id="tour_booking_status_id" class="form-control">
                    <option value="1" <?php echo (isset($tour_book_status->tour_booking_status_id) && $tour_book_status->tour_booking_status_id == '1') ? 'selected="selected"' : '' ?>>Pending</option>
                    <option value="2" <?php echo (isset($tour_book_status->tour_booking_status_id) && $tour_book_status->tour_booking_status_id == '2') ? 'selected="selected"' : '' ?>>Confirmed</option>
                    <?php if($id != '') { ?>
                    <option value="3" <?php echo (isset($tour_book_status->tour_booking_status_id) && $tour_book_status->tour_booking_status_id == '3') ? 'selected="selected"' : '' ?>>Cancelled</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="tour_note">Note from DTD</label>
                <textarea rows="7" name="tour_note" class="form-control" id="tour_note" placeholder="Note from DTD"><?php //echo $tour_book_status->tour_note ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('tour_note', true)
                    };
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <h3>Booking Status</h3>
            <?php
            $tour_name = (isset($tours) && !empty($tours)) ? $tours[0]->name : '';
            $departure_date = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->tour_departure_date : '';
            $allocated_booking = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->tour_all_allocation : '';
            $total_booked = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->tour_booked : '';
            $available_booking = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->tour_available : '';
            if($id != '') {
                $tour_name = $tour_book_status->tour_name;
                $departure_date = $tour_book_status->departure_date;
                $allocated_booking = $tour_book_status->allocation;
                $total_booked = $tour_book_status->booked;
                $available_booking = $tour_book_status->available;
            }
            ?>
            <div class="form-group">
                <label for="tour_name">Tour Name</label>
                <span id="selected_tour_name"><?php echo $tour_name ?></span>
            </div>
            <div class="form-group">
                <label for="departure_date">Departure Date</label>
                <span id="selected_departure_date"><?php echo $departure_date ?></span>
            </div>
            <div class="form-group">
                <label for="total_booking_available">Total Booking Available</label>
                <span id="selected_booking_available"><?php echo $allocated_booking ?></span>
            </div>
            <div class="form-group">
                <label for="total_booked">Total Booked</label>
                <span id="selected_total_booked"><?php echo $total_booked ?></span>
            </div>
            <div class="form-group">
                <label for="total_available">Total Available</label>
                <span id="selected_total_available"><?php echo $available_booking ?></span>
            </div>
            <?php if($id != '') { ?>
                <h3>Booking Logs</h3>
                <?php if (isset($logs) && !empty($logs)) { $i = 1; ?>
                    <div class="panel-group" id="accordion">
                        <?php foreach ($logs as $log) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="profie-history-title" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $i ?>">
                                            <div class="history-title"><?php echo '<span>Book/Update Date : </span>' . '<span>' . date('Y-m-d H:i:s', $log->created_date) . '</span>' ?></div>
                                            <div class="history-title"><?php echo '<span>Departure Date : </span>' . '<span>' . $log->tour_departure_date . '</span>' ?></span></div>
                                            <div class="history-title"><?php echo '<sapn>Booking Status : </span>' . '<span>' . $log->booking_status . '</span>' ?></div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="<?php echo $i ?>" class="panel-collapse collapse <?php echo $i == 1 ? 'in' : '' ?>">
                                    <div class="panel-body">
                                        <?php echo (!empty($log->tour_note)) ? $log->tour_note : 'No Note'; ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; } ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>