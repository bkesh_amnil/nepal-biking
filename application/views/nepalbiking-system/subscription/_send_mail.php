<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="to_emails">To Emails *</label>
                <input type="text" name="to_emails" value="<?php echo implode(',', $subcriptions->to_emails) ?>" class="form-control"
                       id="name" placeholder="To Emails">
            </div>
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" name="subject" value="<?php echo $subcriptions->subject ?>" class="form-control" id="subjcet" placeholder="Subject">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
        	<div class="form-group">
        		<label for="File">File</label>
        		<input type="text" onclick="BrowseServer1(this)" data-resource-type="Files" data-multiple="false" name="file" value="" class="form-control" id="files" placeholder="File">
        	</div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="message">Message *</label>
                <textarea rows="7" name="message" class="form-control" id="message"
                          placeholder="Message"><?php echo $subcriptions->message ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('message');
    };
</script>