<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="name" value="<?php echo $logo->name ?>" class="form-control" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="url">Url</label>
                <input type="text" name="url" value="<?php echo $logo->url ?>" class="form-control" id="url" placeholder="Url">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $logo->status == '1' || $logo->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $logo->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image(770 X 509) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $logo->image ?>" class="form-control" id="image" placeholder="Banner Image">
                <?php if($logo->image != '') { ?>
                    <img src="<?php echo base_url($logo->image) ?>" width="25%"/>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>