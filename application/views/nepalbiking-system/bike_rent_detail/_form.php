<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="rent_date">Rent Date *</label>
                <input type="text" name="rent_date" value="<?php echo isset($bike_rent_detail->rent_date) ? $bike_rent_detail->rent_date : '' ?>" class="form-control" id="rent_date" placeholder="Rent Date">
            </div>
            <div class="form-group">
                <label for="all_allocation">All Allocation *</label>
                <input type="text" name="all_allocation" value="<?php echo isset($bike_rent_detail->all_allocation) ? $bike_rent_detail->all_allocation : '' ?>" class="form-control" id="all_allocation" placeholder="All Allocation">
            </div>
            <div class="form-group">
                <label for="available">Available *</label>
                <input type="text" name="available" value="<?php echo isset($bike_rent_detail->available) ? $bike_rent_detail->available : '' ?>" class="form-control" id="available" placeholder="Available">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="booked">Booked *</label>
                <input type="text" name="booked" value="<?php echo isset($bike_rent_detail->booked) ? $bike_rent_detail->booked : '' ?>" class="form-control" id="booked" placeholder="Booked">
            </div>
<input type="hidden" name="is_fixed_departure" value="1">
            <!--<div class="form-group">
                <label for="is_fixed_departure">Is Fixed Departure *</label>
                <select name="is_fixed_departure" id="is_fixed_departure" class="form-control">
                    <option value="1" <?php echo isset($bike_rent_detail->is_fixed_departure) && ($bike_rent_detail->is_fixed_departure == '1' || $bike_rent_detail->is_fixed_departure == '') ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo isset($bike_rent_detail->is_fixed_departure) && $bike_rent_detail->is_fixed_departure == '0'? 'selected' : '' ?>>No</option>
                </select>
            </div>-->
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo isset($bike_rent_detail->status) && ($bike_rent_detail->status == '1' || $bike_rent_detail->status == '') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo isset($bike_rent_detail->status) && $bike_rent_detail->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $bike_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>