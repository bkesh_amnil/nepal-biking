<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="product_category_id">Product Category *</label>
                <select name="product_category_id" id="product_category_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(isset($product_categories) && !empty($product_categories)) {
                        foreach($product_categories as $product_category) {
                            ?>
                            <option disabled><?php echo $product_category->name ?></option>
                            <?php
                            if(!empty($product_category->childs)) {
                                foreach($product_category->childs as $child) {
                                    $selected = '';
                                    if($child->id == $product->product_category_id) {
                                        $selected = ' selected="selected"';
                                    }
                                    ?>
                                    <option value="<?php echo $child->id ?>"<?php echo $selected ?>><?php echo '--'.$child->name ?></option>
                                    <?php
                                }
                            }
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="code">Code *</label>
                <input type="text" name="code" value="<?php echo $product->code ?>" class="form-control" id="name" placeholder="Code">
            </div>
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $product->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Slug *</label>
                <input type="text" name="slug" value="<?php echo $product->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="product_model">Model *</label>
                <input type="text" name="product_model" value="<?php echo $product->product_model ?>" class="form-control" id="product_slug" placeholder="Model">
            </div>
            <div class="form-group">
                <label for="normal_price">Normal Price (in Rs.) *</label>
                <input type="text" name="normal_price" value="<?php echo $product->normal_price ?>" class="form-control" id="normal_price" placeholder="Normal Price">
            </div>
            <div class="form-group">
                <label for="promotional_price">Promotional Price (in Rs.) *</label>
                <input type="text" name="promotional_price" value="<?php echo $product->promotional_price ?>" class="form-control" id="promotional_price" placeholder="Promotional Price">
            </div>
            <div class="form-group">
                <label for="in_stock">In Stock</label>
                <select name="in_stock" id="in_stock" class="form-control">
                    <option value="1" <?php echo $product->in_stock == '1'? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo $product->in_stock == '0' || $product->in_stock == '' ? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="is_new">Is New *</label>
                <select name="is_new" id="is_new" class="form-control">
                    <option value="1" <?php echo $product->is_new == '1' ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo $product->is_new == '0' || $product->is_new == '' ? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="is_special_offer">Is Special Offer *</label>
                <select name="is_special_offer" id="is_special_offer" class="form-control">
                    <option value="1" <?php echo $product->is_special_offer == '1' ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo $product->is_special_offer == '1' ? 'selected' : '' ?>>No</option>
                </select>
            </div>

        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="cover_image">Cover Image (606x350)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover_image" value="<?php echo $product->cover_image ?>" class="form-control" id="cover_image" placeholder="Cover Image">
            </div>
            <?php if($product->cover_image != '') { ?>
                <img src="<?php echo base_url($product->cover_image) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group">
                <label for="short_description">Short Description *</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description" placeholder="Short Description"><?php echo $product->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $product->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $product->meta_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $product->status == '1' || $product->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $product->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Description"><?php echo $product->description ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description');
    };
</script>