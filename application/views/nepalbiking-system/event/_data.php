<form action="" method="post" id="gridForm" autocomplete="off">
    <?php if(isset($columns) && !empty($columns)) { ?>
        <table class="table table-bordered table-hover list-datatable">
            <thead>
            <tr>
                <?php foreach($columns as $column) { ?>
                    <th><?php echo ucwords($column->column_name) ?></th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
                <?php foreach($rows as $row) { ?>
                    <tr>
                        <?php foreach($row as $val) { ?>
                        <td><?php echo $val ?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php } else { ?>
        <p>No Excel Data uploaded as of Yet.<a href="<?php echo base_url(BACKENDFOLDER.'/import_form_data') ?>">Upload</a> </p>
    <?php } ?>
</form>