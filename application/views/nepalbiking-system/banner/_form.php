<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="name" value="<?php echo $banner->name ?>" class="form-control" id="name" placeholder="Title">
            </div>
            <!--<div class="form-group">
                <label for="category_id">Category *</label>
                <select name="category_id" id="category_id" class="form-control">
                    <?php /*if($categories) : */?>
                        <?php /*foreach($categories as $category) : */?>
                            <option value="<?php /*echo $category->id */?>" <?php /*echo ($banner->category_id == $category->id) ? 'selected' : '' */?>>
                                <?php /*echo $category->name */?>
                            </option>
                        <?php /*endforeach */?>
                    <?php /*endif */?>
                </select>
            </div>-->
            <div class="form-group">
                <label for="image">Image(770 X 509) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $banner->image ?>" class="form-control" id="image" placeholder="Banner Image">
                <?php if($banner->image != '') { ?>
                    <img src="<?php echo base_url($banner->image) ?>" width="25%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $banner->status == '1' || $banner->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $banner->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="link_type">Link Type</label>
                <select name="link_type" id="link_type" class="form-control">
                    <option value="none" <?php echo $banner->link_type == 'none' ? 'selected="selected"' : '' ?>>None</option>
                    <option value="menu" <?php echo $banner->link_type == 'menu' ? 'selected="selected"' : '' ?>>Menu</option>
                    <option value="tour" <?php echo $banner->link_type == 'tour' ? 'selected="selected"' : '' ?>>Tour</option>
                    <option value="bike" <?php echo $banner->link_type == 'bike' ? 'selected="selected"' : '' ?>>Bike</option>
                    <option value="accessories" <?php echo $banner->link_type == 'accessories' ? 'selected="selected"' : '' ?>>Accessories</option>
                    <option value="url" <?php echo $banner->link_type == 'url' ? 'selected="selected"' : '' ?>>Url</option>
                </select>
            </div>
            <?php
            $menuDisplay = $tourDisplay = $bikeDisplay = $accessoriesDisplay = $urlDisplaby = $linkOpens = 'display:none;';
            if($banner->link_type == 'menu') {
                $menuDisplay = $linkOpens = 'display:block;';
            } else if($banner->link_type == 'tour') {
                $tourDisplay = $linkOpens = 'display:block;';
            } else if($banner->link_type == 'bike') {
                $bikeDisplay = $linkOpens = 'display:block;';
            } else if($banner->link_type == 'accessories') {
                $accessoriesDisplay = $linkOpens = 'display:block;';
            } else if($banner->link_type == 'url') {
                $urlDisplaby = $linkOpens = 'display:block;';
            }
            ?>
            <div class="form-group" id="menuId" style="<?php echo $menuDisplay ?>">
                <label class="form-label">Menus</label>
                <select name="menu_link_id" class="form-control">
                    <option value="0">Select Menu</option>
                    <?php
                    if(isset($menus) && !empty($menus)) {
                        foreach ($menus as $menu) {
                            $selected = '';
                            if($menu->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                         ?>
                            <option value="<?php echo $menu->id ?>"<?php echo $selected ?>><?php echo $menu->name ?></option>
                         <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="tourId" style="<?php echo $tourDisplay ?>">
                <label class="form-label">Tours</label>
                <select name="tour_link_id" class="form-control">
                    <option value="0">Select Tour</option>
                    <?php
                    if(isset($tours) & !empty($tours)) {
                        foreach($tours as $tour) {
                            $selected = '';
                            if($tour->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                        ?>
                            <option value="<?php echo $tour->id ?>"<?php echo $selected ?>><?php echo $tour->name ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="bikeId" style="<?php echo $bikeDisplay ?>">
                <label class="form-label">Bikes</label>
                <select name="bike_link_id" class="form-control">
                    <option value="0">Select Bike</option>
                    <?php
                    if(isset($bikes) && !empty($bikes)) {
                        foreach($bikes as $bike) {
                            $selected = '';
                            if($bike->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $bike->id ?>"<?php echo $selected ?>><?php echo $bike->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="accessoriesId" style="<?php echo $accessoriesDisplay ?>">
                <label class="form-label">Accessories</label>
                <select name="product_link_id" class="form-control">
                    <option value="0">Select Accessories</option>
                    <?php
                    if(isset($products) && !empty($products)) {
                        foreach($products as $product) {
                            $selected = '';
                            if($product->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $product->id ?>"<?php echo $selected ?>><?php echo $product->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="urlId" style="<?php echo $urlDisplaby ?>">
                <label class="form-label">Url</label>
                <input type="text" name="link_url" value="<?php echo $banner->link_url ?>" class="form-control" id="link_url" placeholder="Url">
            </div>
            <div class="form-group" id="linkOpens" style="<?php echo $linkOpens ?>">
                <label class="form-label">Link Opens</label>
                <select name="opens" id="opens" class="form-control">
                    <option value="same">Same Window</option>
                    <option value="new">New Tab</option>
                </select>
            </div>
            <div class="form-group">
                <label for="description">Banner Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Banner Description"><?php echo $banner->description ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('description', true)
                    };
                </script>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>