<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $race->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Slug *</label>
                <input type="text" name="slug" value="<?php echo $race->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="venue">Venue *</label>
                <input type="text" name="venue" value="<?php echo $race->venue ?>" class="form-control" id="product_slug" placeholder="Venue">
            </div>
            <div class="form-group">
                <label for="start_point">Start Point *</label>
                <input type="text" name="start_point" value="<?php echo $race->start_point ?>" class="form-control" id="start_point" placeholder="Start Point">
            </div>
            <div class="form-group">
                <label for="start_point_latitude">Start Point Latitude *</label>
                <input type="text" name="start_point_latitude" value="<?php echo $race->start_point_latitude ?>" class="form-control" id="start_point_latitude" placeholder="Start Point Latitude">
            </div>
            <div class="form-group">
                <label for="start_point_longitude">Start Point Longitude *</label>
                <input type="text" name="start_point_longitude" value="<?php echo $race->start_point_longitude ?>" class="form-control" id="start_point_longitude" placeholder="Start Point Longitude">
            </div>
            <div class="form-group">
                <label for="finish_point">Finish Point *</label>
                <input type="text" name="finish_point" value="<?php echo $race->finish_point ?>" class="form-control" id="finish_point" placeholder="Finish Point">
            </div>
            <div class="form-group">
                <label for="finish_point_latitude">Finish Point Latitude *</label>
                <input type="text" name="finish_point_latitude" value="<?php echo $race->finish_point_latitude ?>" class="form-control" id="finish_point_latitude" placeholder="Finish Point Latitude">
            </div>
            <div class="form-group">
                <label for="finish_point_longitude">Finish Point Longitude *</label>
                <input type="text" name="finish_point_longitude" value="<?php echo $race->finish_point_longitude ?>" class="form-control" id="finish_point_longitude" placeholder="Finish Point Longitude">
            </div>
            <div class="form-group">
                <label for="entry_fee">Entry Fee (in $) *</label>
                <input type="text" name="entry_fee" value="<?php echo $race->entry_fee ?>" class="form-control" id="entry_fee" placeholder="Entry Fee">
            </div>
            <div class="form-group">
                <label for="start_date">Start Date *</label>
                <input type="text" name="start_date" value="<?php echo $race->start_date ?>" class="form-control" id="valid_from" placeholder="Start Date">
            </div>
            <div class="form-group">
                <label for="end_date">End Date *</label>
                <input type="text" name="end_date" value="<?php echo $race->end_date ?>" class="form-control" id="valid_to" placeholder="End Date">
            </div>
            <div class="form-group">
                <label for="youtube_link">Youtube Link *</label>
                <input type="text" name="youtube_link" value="<?php echo $race->youtube_link ?>" class="form-control" id="youtube_link" placeholder="Youtube Link">
            </div>
            <div class="form-group">
                <label for="google_form_link">Google Form Link</label>
                <input type="text" name="google_form_link" value="<?php echo (isset($race->google_form_link) && !empty($race->google_form_link)) ? $race->google_form_link : '' ?>" class="form-control" id="google_form_link" placeholder="Google Form Link" <?php echo (/*!empty($race->google_form_link*/$id ? 'disabled="disabled"' : '') ?>>
            </div>
            <?php if(/*!empty($race->google_form_link)*/ $id != '') { ?>
                <input type="hidden" name="google_form_link_edit" value="<?php echo $race->google_form_link ?>" class="form-control">
                <input type="checkbox" name="edit_google_form" id="edit_google_form" value="1">Edit Google Form
            <?php } ?>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="cover_image">Cover Image (188X188)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover_image" value="<?php echo $race->cover_image ?>" class="form-control" id="cover" placeholder="Cover Image">
            </div>
            <?php if($race->cover_image != '') { ?>
                <img src="<?php echo base_url($race->cover_image) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group">
                <label for="backgroud_image">Background Image (1920X1199)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="backgroud_image" value="<?php echo $race->backgroud_image ?>" class="form-control" id="backgroud_image" placeholder="Background Image">
            </div>
            <?php if($race->backgroud_image != '') { ?>
                <img src="<?php echo base_url($race->backgroud_image) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group">
                <label for="categories">Categories *</label>
                <input type="text" name="categories" value="<?php echo $race->categories ?>" class="form-control" id="categories" placeholder="Categories">
            </div>
            <div class="form-group">
                <label for="events">Events *</label>
                <input type="text" name="events" value="<?php echo $race->events ?>" class="form-control" id="events" placeholder="Events">
            </div>
            <div class="form-group">
                <label for="contact">Contact *</label>
                <textarea rows="7" name="contact" class="form-control" id="contact" placeholder="Contact"><?php echo $race->contact ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $race->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $race->meta_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $race->status == '1' || $race->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $race->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Description"><?php echo $race->description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="itinerary">Itinerary *</label>
                <textarea rows="7" name="itinerary" class="form-control" id="itinerary" placeholder="Itinerary"><?php echo $race->itinerary ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group" id="objectives">
                <label for="objectives">Objectives *</label>
                <input type="text" onclick="BrowseServerRace(this)" data-resource-type="image" data-multiple="true"
                       data-show-detail="true" class="form-control" id="objectives" placeholder="Objectives">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia['objectives'])) {
                foreach($savedMedia['objectives'] as $val) {
                    ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($val->image) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $val->image ?>" name="media[objectives][]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $val->title ?>" name="title[objectives][]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="caption[objectives][]"><?php echo $val->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href='javascript:void(0)' class='deleteMedia' title='Click To Cancel'>Remove</a>
                <?php
                }
            }
            ?>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group" id="event_organizer">
                <label for="event_organizer">Event Organizer</label>
                <input type="text" onclick="BrowseServerRace(this)" data-resource-type="image" data-multiple="true"
                       data-show-detail="true" class="form-control" id="event_organizer" placeholder="Event Organizer">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia['event_organizer'])) {
                foreach($savedMedia['event_organizer'] as $val) {
                    ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($val->image) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $val->image ?>" name="media[event_organizer][]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $val->title ?>" name="title[event_organizer][]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="caption[event_organizer][]"><?php echo $val->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                        <a href='javascript:void(0)' class='deleteMedia' title='Click To Cancel'>Remove</a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group" id="media_marketing">
                <label for="media_marketing">Media Marketing</label>
                <input type="text" onclick="BrowseServerRace(this)" data-resource-type="image" data-multiple="true"
                       data-show-detail="true" class="form-control" id="media_marketing" placeholder="Media Marketing">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia['media_marketing'])) {
                foreach($savedMedia['media_marketing'] as $val) {
                    ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($val->image) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $val->image ?>" name="media[media_marketing][]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $val->title ?>" name="title[media_marketing][]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="caption[media_marketing][]"><?php echo $val->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                        <a href='javascript:void(0)' class='deleteMedia' title='Click To Cancel'>Remove</a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group" id="press_coverage">
                <label for="press_coverage">Press Coverage</label>
                <input type="text" onclick="BrowseServerRace(this)" data-resource-type="image" data-multiple="true"
                       data-show-detail="true" class="form-control" id="press_coverage" placeholder="Press Coverage">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia['press_coverage'])) {
                foreach($savedMedia['press_coverage'] as $val) {
                    ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($val->image) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $val->image ?>" name="media[press_coverage][]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $val->title ?>" name="title[press_coverage][]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="caption[press_coverage][]"><?php echo $val->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                        <a href='javascript:void(0)' class='deleteMedia' title='Click To Cancel'>Remove</a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group" id="supporters">
                <label for="supporters">Event Supporters</label>
                <input type="text" onclick="BrowseServerRace(this)" data-resource-type="image" data-multiple="true"
                       data-show-detail="true" class="form-control" id="supporters" placeholder="Event Supporters">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia['supporters'])) {
                foreach($savedMedia['supporters'] as $val) {
                    ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($val->image) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $val->image ?>" name="media[supporters][]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $val->title ?>" name="title[supporters][]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="caption[supporters][]"><?php echo $val->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                        <a href='javascript:void(0)' class='deleteMedia' title='Click To Cancel'>Remove</a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('contact', true);
        load_ckeditor('description');
        load_ckeditor('itinerary');
    };
</script>