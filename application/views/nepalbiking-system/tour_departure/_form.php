<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="tour_departure_date">Departure Date *</label>
                <input type="text" name="tour_departure_date" value="<?php echo isset($tour_departure->tour_departure_date) ? $tour_departure->tour_departure_date : '' ?>" class="form-control" id="tour_departure_date" placeholder="Departure Date">
            </div>
            <div class="form-group">
                <label for="tour_all_allocation">All Allocation *</label>
                <input type="text" name="tour_all_allocation" value="<?php echo isset($tour_departure->tour_all_allocation) ? $tour_departure->tour_all_allocation : '' ?>" class="form-control" id="tour_all_allocation" placeholder="Tour All Allocation">
            </div>
            <div class="form-group">
                <label for="tour_available">Available *</label>
                <input type="text" name="tour_available" value="<?php echo isset($tour_departure->tour_available) ? $tour_departure->tour_available : '' ?>" class="form-control" id="tour_available" placeholder="Available">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="tour_booked">Booked *</label>
                <input type="text" name="tour_booked" value="<?php echo isset($tour_departure->tour_booked) ? $tour_departure->tour_booked : '' ?>" class="form-control" id="tour_booked" placeholder="Booked">
            </div>
            <div class="form-group">
                <label for="is_fixed_departure">Is Fixed Departure *</label>
                <select name="is_fixed_departure" id="is_fixed_departure" class="form-control">
                    <option value="1" <?php echo isset($tour_departure->is_fixed_departure) && ($tour_departure->is_fixed_departure == '1' || $tour_departure->is_fixed_departure == '') ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo isset($tour_departure->is_fixed_departure) && $tour_departure->is_fixed_departure == '0'? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo isset($tour_departure->status) && ($tour_departure->status == '1' || $tour_departure->status == '') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo isset($tour_departure->status) && $tour_departure->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $tour_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>