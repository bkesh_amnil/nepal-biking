<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <input type="hidden" name="customer_email_address" value="<?php echo $tour_enquire->customer_email_address ?>"/>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="tour_category_id">Tour Category *</label>
                <select name="tour_category_id" id="tour_category_id" class="form-control" disabled>
                    <?php
                    if(isset($tour_categories) && !empty($tour_categories)) {
                        foreach($tour_categories as $tour_category) {
                            $selected = '';
                            if($tour_category->id == $tour_enquire->tour_category_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $tour_category->id ?>"<?php echo $selected ?>><?php echo $tour_category->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="tour_id">Tour Name *</label>
                <select name="tour_id" id="tour_id" class="form-control" disabled>
                    <?php
                    if(isset($tours) && !empty($tours)) {
                        foreach($tours as $val) {
                            $selected = '';
                            if($val->id == $tour_enquire->tour_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="departure_date_id">Departure Date *</label>
                <input type="text" name="departure_date" value="<?php echo $tour_enquire->departure_date ?>" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label for="number_of_pax">Number of pax *</label>
                <input type="text" name="number_of_pax" value="<?php echo $tour_enquire->number_of_pax ?>" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label for="customer_full_name">Full Name *</label>
                <input type="text" name="customer_full_name" value="<?php echo $tour_enquire->customer_full_name ?>" class="form-control" id="customer_full_name" placeholder="Full Name" disabled>
            </div>
            <div class="form-group">
                <label for="enquire_note">Note from DTD</label>
                <textarea rows="7" name="enquire_note" class="form-control" id="enquire_note" placeholder="Note from DTD"><?php echo $tour_enquire->enquire_note ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('enquire_note', true)
                    };
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="customer_email_address">Email Address *</label>
                <input type="text" name="customer_email_address" value="<?php echo $tour_enquire->customer_email_address ?>" class="form-control" id="customer_email_address" placeholder="Email Address" disabled>
            </div>
            <div class="form-group">
                <label for="customer_contact_address">Contact Address *</label>
                <input type="text" name="customer_contact_address" value="<?php echo $tour_enquire->customer_contact_address ?>" class="form-control" id="customer_contact_address" placeholder="Contact Address" disabled>
            </div>
            <div class="form-group">
                <label for="customer_contact_number">Contact Nos *</label>
                <input type="text" name="customer_contact_number" value="<?php echo $tour_enquire->customer_contact_number ?>" class="form-control" id="customer_contact_number" placeholder="Contact Nos" disabled>
            </div>
            <div class="form-group">
                <label for="customer_enquiry">Customer Enquiry *</label>
                <textarea rows="7" name="customer_enquiry" class="form-control" id="customer_enquiry" placeholder="Customer Enquiry" disabled><?php echo $tour_enquire->customer_enquiry ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <?php if(isset($tour_enquire->enquire_note) && empty($tour_enquire->enquire_note)) {?>
                    <button type="submit" class="btn btn-primary">Save</button>
                <?php } ?>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>