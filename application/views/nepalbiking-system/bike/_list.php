<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th></th>
        <th>Bike Name</th>
        <th>Bike Code</th>
        <th></th>
    </tr>
    </tfoot>
    <thead>
    <tr>
        <th>SN</th>
        <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
        <th>Bike Name</th>
        <th>Bike Code</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($bikes) : $serial_number = 1; ?>
        <?php foreach ($bikes as $row) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                <td><?php echo $row->name ?></td>
                <td><?php echo $row->code ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'bike',
                        'moduleData' => $row,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</form>