<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="bike_category_id">Tour Category *</label>
                <select name="bike_category_id" id="bike_category_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(!empty($bike_categories)) {
                        foreach($bike_categories as $val) {
                            $selected = '';
                            if($val->id == $bike->bike_category_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="code">Code *</label>
                <input type="text" name="code" value="<?php echo $bike->code ?>" class="form-control" id="name" placeholder="Code">
            </div>
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $bike->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Slug *</label>
                <input type="text" name="slug" value="<?php echo $bike->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="bike_model">Bike Model *</label>
                <input type="text" name="bike_model" value="<?php echo $bike->bike_model ?>" class="form-control" id="bike_model" placeholder="Bike Model">
            </div>
            <div class="form-group">
                <label for="size">Size *</label>
                <input type="text" name="size" value="<?php echo $bike->size ?>" class="form-control" id="size" placeholder="Size">
            </div>
            <div class="form-group">
                <label for="cost">Cost (in $) *</label>
                <input type="text" name="cost" value="<?php echo $bike->cost ?>" class="form-control" id="cost" placeholder="Cost">
            </div>
            <div class="form-group">
                <label for="charge_per_day">Charger per Day (in $) *</label>
                <input type="text" name="charge_per_day" value="<?php echo $bike->charge_per_day ?>" class="form-control" id="charge_per_day" placeholder="Charger per Day (in $)">
            </div>
            <div class="form-group">
                <label for="available_quantity">Available Quantity *</label>
                <input type="text" name="available_quantity" value="<?php echo $bike->available_quantity ?>" class="form-control" id="available_quantity" placeholder="Available Quantity">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $bike->status == '1' || $bike->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $bike->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="cover_image">Cover Image (606x350)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover_image" value="<?php echo $bike->cover_image ?>" class="form-control" id="cover_image" placeholder="Cover Image">
            </div>
            <?php if($bike->cover_image != '') { ?>
                <img src="<?php echo base_url($bike->cover_image) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group" id="selectImages">
                <label for="file">File (pdf only)*</label>
                <input type="text" onclick="BrowseServer1(this)" data-resource-type="Files" data-multiple="false" name="file" value="<?php echo $bike->file ?>" class="form-control" id="files" placeholder="File">
            </div>
            <div class="form-group">
                <label for="rent_type">Rent Type *</label>
                <?php
                $disable = '';
                if(isset($bike->rent_type) && !empty($bike->rent_type) && $bike->rent_type != 'Select') {
                    $disable = ' disabled="disabled"';
                    ?>
                    <input type="hidden" name="rent_type" value="<?php echo $bike->rent_type ?>">
                    <?php
                }
                ?>
                <select name="rent_type" id="rent_type" class="form-control"<?php echo $disable ?>>
                    <?php
                    if(!empty($rent_types)) {
                        foreach($rent_types as $val) {
                            $selected = '';
                            if($val->code == $bike->rent_type) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->code ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="rent_year">Rent Year *</label>
                <?php
                if(isset($bike->rent_year) && !empty($bike->rent_year) && !is_array($bike->rent_year)) {
                    $saved_years = explode(',', $bike->rent_year);
                    ?>
                    <input type="hidden" name="saved_rent_year" value="<?php echo $bike->rent_year ?>">
                    <?php
                }
                $years = range(2015, 2035);
                if(isset($years) && !empty($years)) {
                    ?>
                    <div>
                        <?php
                        foreach($years as $year) {
                            $disable = '';
                            if(isset($saved_years) && in_array($year, $saved_years)) {
                                $disable = ' disabled="disabled"';
                            }
                            ?>
                            <span>
                        <input type="checkbox" name="rent_year[]" value="<?php echo $year ?>"<?php echo $disable ?>>
                                <?php echo $year; ?>
                        </span>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="form-group">
                <label for="short_description">Short Description *</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description" placeholder="Short Description"><?php echo $bike->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $bike->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $bike->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Description"><?php echo $bike->description ?></textarea>
            </div>
            <div class="form-group">
                <label for="terms">Terms *</label>
                <textarea rows="7" name="terms" class="form-control" id="terms" placeholder="Terms"><?php echo $bike->terms ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description');
        load_ckeditor('terms');
    };
</script>