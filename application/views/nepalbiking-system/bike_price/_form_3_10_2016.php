<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="valid_from">Valid From *</label>
                <input type="text" name="valid_from" value="<?php echo isset($bike_price->valid_from) ? $bike_price->valid_from : '' ?>" class="form-control" id="valid_from" placeholder="Valid From">
            </div>
            <div class="form-group">
                <label for="valid_to">Valid To *</label>
                <input type="text" name="valid_to" value="<?php echo isset($bike_price->valid_to) ? $bike_price->valid_to : '' ?>" class="form-control" id="valid_to" placeholder="Valid To">
            </div>
            <div class="form-group">
                <label for="number_of_days">Number of Days *</label>
                <select name="number_of_days" id="number_of_days" class="form-control">
                    <?php
                    if(isset($bike_rent_days) && !empty($bike_rent_days)) {
                        foreach($bike_rent_days as $val) {
                            $selected = '';
                            if($val->id == $ike_price->number_of_days_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="unit_price">Unit Price (in $) *</label>
                <input type="text" name="unit_price" value="<?php echo isset($bike_price->unit_price) ? $bike_price->unit_price : '' ?>" class="form-control" id="unit_price" placeholder="Unit Price">
            </div>
            <div class="form-group">
                <label for="is_outside_valley">Is Outside Valley</label>
                <select name="is_outside_valley" id="is_outside_valley" class="form-control">
                    <option value="1" <?php echo (isset($bike_price->is_outside_valley) && $bike_price->is_outside_valley == '1') ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo (isset($bike_price->is_outside_valley) && $bike_price->is_outside_valley == '0') ? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo (isset($bike_price->status) && $bike_price->status == '1') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo (isset($bike_price->status) && $bike_price->status == '0') ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <?php if(empty($bike_price->id)) { ?>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group"><span class="add-info"> [ + Add ]</span></div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <table class="table table-bordered multi-table" id="dynamic_table" width="100%">
                        <thead>
                        <th>From</th>
                        <th>To</th>
                        <th>Number of Days</th>
                        <th>Unit Price(in $)</th>
                        <th>Is Outside Valley</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary" id="action">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/'. $bike_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>