<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="tour_category_id">Tour Category *</label>
                <select name="tour_category_id" id="tour_category_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(!empty($tour_categories)) {
                      foreach($tour_categories as $val) {
                          $selected = '';
                          if($val->id == $tour->tour_category_id) {
                              $selected = ' selected="selected"';
                          }
                        ?>
                          <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                        <?php
                      }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="code">Code *</label>
                <input type="text" name="code" value="<?php echo $tour->code ?>" class="form-control" id="name" placeholder="Code">
            </div>
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $tour->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Slug *</label>
                <input type="text" name="slug" value="<?php echo $tour->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="duration">Duration *</label>
                <input type="text" name="duration" value="<?php echo $tour->duration ?>" class="form-control" id="slug" placeholder="Duration">
            </div>
            <div class="form-group">
                <label for="minimum_group_size">Minimum Group Size *</label>
                <input type="text" name="minimum_group_size" value="<?php echo $tour->minimum_group_size ?>" class="form-control" id="slug" placeholder="Minimum Group Size">
            </div>
            <div class="form-group">
                <label for="best_season">Best Season *</label>
                <input type="text" name="best_season" value="<?php echo $tour->best_season ?>" class="form-control" id="slug" placeholder="Best Season">
            </div>
            <div class="form-group">
                <label for="altitude">Altitude (in mts.) *</label>
                <input type="text" name="altitude" value="<?php echo $tour->altitude ?>" class="form-control" id="slug" placeholder="Altitude (in mts.)">
            </div>
            <div class="form-group">
                <label for="difficulty_id">Difficulty *</label>
                <select name="difficulty_id" id="difficulty_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(!empty($tour_difficulty)) {
                        foreach($tour_difficulty as $val) {
                            $selected = '';
                            if($val->id == $tour->difficulty_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>" <?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="is_highlighted">Highlight Tour</label>
                <select name="is_highlighted" id="is_highlighted" class="form-control">
                    <option value="1" <?php echo $tour->is_highlighted == '1'? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo $tour->is_highlighted == '0' || $tour->is_highlighted == '' ? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="support">Support *</label>
                <input type="radio" value="1"  name="support" <?php echo isset($tour->support) && $tour->support == '1' ? 'checked="checked"' : '' ?>>Yes
                <input type="radio" value="0"  name="support" <?php echo isset($tour->support) && $tour->support == '0' ? 'checked="checked"' : '' ?>>No
            </div>
            <div class="form-group">
                <label for="is_recommended">Is Recommended *</label>
                <input type="radio" value="1"  name="is_recommended" <?php echo (isset($tour->is_recommended) && $tour->is_recommended == 1) ? 'checked' : '' ?>>Yes
                <input type="radio" value="0"  name="is_recommended" <?php echo (isset($tour->is_recommended) && $tour->is_recommended == 0) ? 'checked' : '' ?>>No
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $tour->status == '1' || $tour->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $tour->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="cover_image">Cover Image (1920x1323)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover_image" value="<?php echo $tour->cover_image ?>" class="form-control" id="cover_image" placeholder="Cover Image">
            </div>
            <?php if($tour->cover_image != '') { ?>
                <img src="<?php echo base_url($tour->cover_image) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group" id="selectImages">
                <label for="file">File (pdf only)*</label>
                <input type="text" onclick="BrowseServer1(this)" data-resource-type="Files" data-multiple="false" name="file" value="<?php echo $tour->file ?>" class="form-control" id="files" placeholder="File">
            </div>
            <div class="form-group">
                <label for="map_file">Map File (10MB size kml only)</label>
                <input type="text" onclick="BrowseServer1(this)" data-resource-type="Files" data-multiple="false" name="map_file" value="<?php echo $tour->map_file ?>" class="form-control" id="map_files" placeholder="Map File">
            </div>
            <div class="form-group">
                <label for="departure_type_id">Departure Type *</label>
                <?php
                $disable = '';
                if(isset($tour->departure_type) && !empty($tour->departure_type) && $tour->departure_type != 'Select') {
                    $disable = ' disabled="disabled"';
                    ?>
                    <input type="hidden" name="departure_type" value="<?php echo $tour->departure_type ?>">
                    <?php
                }
                ?>
                <select name="departure_type" id="departure_type" class="form-control"<?php echo $disable ?>>
                   <?php
                    if(!empty($tour_departure)) {
                        foreach($tour_departure as $val) {
                            $selected = '';
                            if($val->code == $tour->departure_type) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->code ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="departure_year">Departure Year *</label>
                <?php
                if(isset($tour->departure_year) && !empty($tour->departure_year) && !is_array($tour->departure_year)) {
                    $saved_years = explode(',', $tour->departure_year);
                    //if($id != 0) {
                        ?>
                        <input type="hidden" name="saved_departure_year" value="<?php echo $tour->departure_year ?>">
                        <?php
                    //}
                }
                $years = range(2015, 2035);
                if(isset($years) && !empty($years)) {
                    ?>
                    <div>
                    <?php
                    foreach($years as $year) {
                        $disable = '';
                        if(isset($saved_years) && in_array($year, $saved_years)) {
                            $disable = ' disabled="disabled"';
                        }
                        ?>
                        <span>
                        <input type="checkbox" name="departure_year[]" value="<?php echo $year ?>"<?php echo $disable ?>>
                        <?php echo $year; ?>
                        </span>
                        <?php
                    }
                    ?>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="form-group">
                <label for="short_description">Short Description *</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description" placeholder="Short Description"><?php echo $tour->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $tour->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $tour->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Description"><?php echo $tour->description ?></textarea>
            </div>
            <div class="form-group">
                <label for="itinerary">Itinerary *</label>
                <textarea rows="7" name="itinerary" class="form-control" id="itinerary" placeholder="Itinerary"><?php echo $tour->itinerary ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description');
        load_ckeditor('itinerary');
    };
</script>