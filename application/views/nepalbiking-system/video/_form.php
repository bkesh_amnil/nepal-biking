<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $video->name ?>" class="form-control title"
                       id="name" placeholder="Name">
            </div>
            <div class="form-group">
                <label for="slug">Alias</label>
                    <input type="text" name="slug" value="<?php echo $video->slug ?>" class="form-control slug"
                       id="slug" placeholder="Alias">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="youtube_link">Youtube Link *</label>
                <input type="text" name="youtube_link" value="http://www.youtube.com/watch?v=<?php echo (isset($video->video_id)) ? $video->video_id : '' ?>" class="form-control"
                       id="youtube_link" placeholder="Youtube Link">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $video->status == '1' || $video->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $video->status == '0' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Description</label>
                <textarea rows="7" name="description" class="form-control" id="description"
                          placeholder="Testimonial"><?php echo $video->description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('description');
    };
</script>