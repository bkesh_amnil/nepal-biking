<h3 class="sections-title"><?php echo ucfirst($module_name) ?></h3>
<div class="row media-listing" id="news-events-listing">
	<?php
        foreach($content as $media) {
             if(isset($modules)) {
			$social_data_individual = $this->public_model->getSocialData($modules[0]['module_id'], $media->id);
		}
        ?>
	<div class="col-xs-6 col-sm-3 col-lg-3">
		<?php
		if($count == 1) {
			$aClass = '';
			$attr = '';
			$aUrl = site_url($module_name . '/' . $media->slug);
                        $atarget = '';
			if($module_name == 'video') {
				$imgUrl = 'http://i4.ytimg.com/vi/'.$media->video_id.'/0.jpg';
				$aUrl = $media->youtube_link;
				$aClass = ' fancybox-media';
				$attr = ' rel="media-gallery"';
                                $atarget = '_blank';
			} else {
				$imgUrl = image_thumb($media->cover, 270, 270, '', true);
			}
			?>
                        <?php if(isset($social_data_individual) && !empty($social_data_individual)) { ?>
				<!-- Go to www.addthis.com/dashboard to customize your tools -->
				<a class="addthis_button btn-share share-page-gallery" addthis:url="<?php echo !empty($social_data_individual['Facebook']->link) ? $social_data_individual['Facebook']->link : current_url() . '/' . $media->slug ?>" addthis:title="<?php echo (isset($social_data_individual['Facebook']->title) && !empty($social_data_individual['Facebook']->title)) ? $social_data_individual['Facebook']->title : $media->name ?>" addthis:description="<?php echo (isset($social_data_individual['Facebook']->description) && !empty($social_data_individual['Facebook']->description)) ? $social_data_individual['Facebook']->description : '' ?>"href="javascript:void(0)"><img src="<?php echo base_url('img/icon/icon-addthis-share.png') ?>" alt=""/></a>
			<?php } ?>
			<a href="<?php echo $aUrl ?>" target="<?php echo $atarget ?>" class="bikes-wrap<?php echo $aClass ?>"<?php echo $attr ?> data-caption="<?php echo (isset($media->description) && !empty($media->description)) ? '<h2>'.$media->name . '</h2><p>' . $media->description .'</p>' : '<h2>'.$media->name.'</h2>' ?>">
				<?php //if(!empty($media->cover)) { ?>
					<div class="trip-img-wrap">
						<img class="img-responsive" src="<?php echo $imgUrl ?>" alt="<?php echo $media->name ?>" />
					</div>
				<?php //} ?>
				<h2><?php echo (strlen($media->name) > 56) ? $media->name . ' ...' : $media->name ?></h2>
			</a>
		<?php } else { ?>
			<?php if(!empty($media->media)) { ?>
				<span class='st_sharethis_hcount share-page-gallery' st_title="<?php echo $media->title ?>" st_url="<?php echo current_url() ?>" st_image="<?php echo base_url($media->media) ?>" st_summary="<?php echo $media->caption ?>"></span>
				<a class="bikes-wrap gallery-link fancybox" href="<?php echo base_url($media->media) ?>" data-fancybox-group="gallery" title="<?php echo $media->title ?>" data-caption="<?php echo '<h2>'.$media->title . '</h2><p>' . $media->caption .'</p>' ?>">
					<div class="trip-img-wrap">
					<img class="img-responsive" src="<?php echo image_thumb($media->media, 270, 270, '', true) ?>" alt="<?php echo $media->title ?>" />
					</div>
					<h2><?php echo $media->title ?></h2>
				</a>
			<?php } ?>

		<?php } ?>
	</div>
	<?php } ?>
</div>