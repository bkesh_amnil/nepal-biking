<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
    <div class="row">
    	<?php foreach($highlighted_tours as $highlighted_tour) { ?>
    	<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
            <div class="aside-package-wrap">
                <a href="<?php echo site_url('tour/' . $highlighted_tour->slug) ?>"><img class="img-responsive" src="<?php echo base_url($highlighted_tour->cover_image) ?>" alt="<?php echo $highlighted_tour->name ?>" /></a>
                <div class="about-package-small">
                    <div class="banner-description">
                       <p><?php echo $highlighted_tour->short_description ?></p>
                    </div>
                    <div class="banner-title">
                        <a href="<?php echo site_url('tour/' . $highlighted_tour->slug) ?>"><h3><?php echo $highlighted_tour->name ?></h3></a>
                        <a class="btn-detail" href="<?php echo site_url('tour/' . $highlighted_tour->slug) ?>">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    	<?php } ?>
    </div>
</div>