<div class="container">
    <h3 class="sections-title">Book your tour</h3>
                
    <form class="filter-tour-wrap">
        <div class="row">
            <div class="col-sm-12 col-lg-3">
                <h3>FILTER YOUR TOURS</h3>
            </div>
            
            <?php $this->load->view('form/filter_tour.php'); ?>
        </div>
    </form>
    
    <div class="row" id="tour-listing">
        <?php 
         if(isset($content) && !empty($content)) {
            foreach($content as $ind => $val) {
            	$ids[] = $val->id;
                $name = $val->name;
                if(strlen($val->name) > 27) {
                      $name = substr($val->name, 0, 27) . ' ...';
                }
                $frequency = $this->db->select('name')->where('code', $val->departure_type)->get('tbl_departure_type')->row();
            ?>
            <div class="col-xs-6 col-sm-3 col-lg-3">
                <a href="<?php echo site_url('tour/' . $val->slug) ?>" class="trip-wrap inner-trip-wrap">
                    <div class="trip-img-wrap">
                        <img class="img-responsive" src="<?php echo image_thumb($val->cover_image, 270, 270, '', true) ?>" alt="<?php echo $val->name ?>" />
                    </div>
                    <h2 title="<?php echo $val->name ?>"><?php echo $name ?></h2>
                    <span>Tour Code : <span><?php echo $val->code ?></span></span>
                    <span>Frequency : <span><?php echo $frequency->name ?></span></span>
                    <span>Duration : <span></span><?php echo $val->duration ?></span>
                </a>
            </div>
            <?php
            }
        }
        ?>
    </div>
    <?php if($count_rows > $item_per_page) { ?>
        <div class="processing load-more-processing" style="display:none;"></div>
        <button class="btn-more" id="load_more_button">Load More...</button>
    <?php } ?>
</div>
<input type="hidden" id="hidden_type" value="<?php echo $active_menu ?>" />
<input type="hidden" id="hidden_ids" value="<?php echo implode(',', $ids) ?>">