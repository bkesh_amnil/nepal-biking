<div class="row">
    <div class="col-sm-12 col-md-9 col-lg-9">
        <div class="tours-status-wrap">
            <div class="bookingform-wrap">
                <?php $this->load->view('form/book_tour.php'); ?>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3 col-lg-3">
        <div class="about-tour">
            <div class="aside-tour-about">
                <h3><?php echo $content['info']->name ?></h3>
                <div class="tour-level tour-code">
                    <h4>Difficulty : <span id="difficulty-text"><?php echo $content['info']->difficulty_name ?></span></h4>
                    <?php
                    $moderate = $easy = $medium = $pro = $expert = '';
                    switch(strtolower(str_replace(' ', '-', $content['info']->difficulty_name))) {
                        case 'easy':
                            $easy = 'level-easy';
                            break;
                        case 'moderate':
                            $moderate = 'level-moderate';
                            break;
                        case 'strenuous':
                            $medium = 'level-medium';
                            break;
                        case 'tough':
                            $pro = 'level-pro';
                            break;
                        default:
                            $expert = 'level-expert';
                            break;
                    }
                    ?>
                    <div class="difficulty-level">
                        <span id="<?php echo $easy ?>" title="Easy">A</span><span id="<?php echo $moderate ?>" title="Moderate">B</span><span id="<?php echo $medium ?>" title="Strenuous">C</span><span id="<?php echo $pro ?>" title="Tough">D</span><span id="<?php echo $expert ?>" title="Very Tough">E</span>
                    </div>
                </div>
                <div class="tour-code">
                    TOUR CODE : <span><?php echo $content['info']->code ?></span>
                </div>
                <div class="tour-code">
                    FREQUENCY: <span><?php echo $content['info']->departure_name ?></span>
                </div>
                <div class="tour-code">
                    Duration: <span><?php echo $content['info']->duration ?></span>
                </div>
                <div class="tour-code">
                    Best Season: <span><?php echo $content['info']->best_season ?></span>
                </div>
                <div class="tour-code">
                    Minimum Group Size: <span><?php echo $content['info']->minimum_group_size ?></span>
                </div>
                <div class="tour-code">
                    Altitude: <span><?php echo $content['info']->altitude . ' mts.' ?></span>
                </div>
                <div class="tour-code">
                    Support: <span><?php echo ($content['info']->support == '1') ? 'Yes' : 'No' ?></span>
                </div>
                <div class="about-text">
                    <p><?php echo $content['info']->short_description ?></p>
                </div>

            </div>
        </div>
    </div>
</div>