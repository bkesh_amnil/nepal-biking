<div class="container">
    <h3 class="sections-title">Book Your Tours</h3>
    <div class="row">
        <?php foreach($latest_tours as $latest_tour) { ?>
        <div class="col-xs-6 col-sm-3 col-lg-3">
            <a href="<?php echo site_url('tour/' . $latest_tour->slug) ?>" class="trip-wrap">
                <div class="trip-img-wrap">
                    <img class="img-responsive" src="<?php echo base_url($latest_tour->cover_image) ?>" />
                </div>
                <h2><?php echo $latest_tour->name ?></h2>
            </a>
        </div>
        <?php } ?>
    </div>
    <?php if($count_tours > 4) { ?>
        <a href="<?php echo site_url('nepalbiketours') ?>" class="btn-more">More Tours</a>
    <?php } ?>
</div>