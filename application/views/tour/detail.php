<!-- <a class="addthis_button_compact"></a> -->
<div class="row">
    <div class="col-sm-12 col-md-3 col-lg-3 col-lg-push-9 col-md-push-9">
        <div class="about-tour">
            <div class="aside-tour-about">
                <h3><?php echo $content['info']->name ?></h3>
                <div class="tour-level tour-code">
                    <h4>Difficulty : <span id="difficulty-text"><?php echo $content['info']->difficulty_name ?></span></h4>
                    <?php
                    $moderate = $easy = $medium = $pro = $expert = '';
                    switch(strtolower(str_replace(' ', '-', $content['info']->difficulty_name))) {
                        case 'easy':
                            $easy = 'level-easy';
                            break;
                        case 'moderate':
                            $moderate = 'level-moderate';
                            break;
                        case 'strenuous':
                            $medium = 'level-medium';
                            break;
                        case 'tough':
                            $pro = 'level-pro';
                            break;
                        default:
                            $expert = 'level-expert';
                            break;
                    }
                    ?>
                    <div class="difficulty-level">
                    <span id="<?php echo $easy ?>" title="Easy">A</span><span id="<?php echo $moderate ?>" title="Moderate">B</span><span id="<?php echo $medium ?>" title="Strenuous">C</span><span id="<?php echo $pro ?>" title="Tough">D</span><span id="<?php echo $expert ?>" title="Very Tough">E</span>
                    </div>
                </div>
                <div class="tour-code">
                    TOUR CODE : <span><?php echo $content['info']->code ?></span>
                </div>
                <div class="tour-code">
                    FREQUENCY: <span><?php echo $content['info']->departure_name ?></span>
                </div>
                <div class="tour-code">
                    Duration: <span><?php echo $content['info']->duration ?></span>
                </div>
                <div class="tour-code">
                    Best Season: <span><?php echo $content['info']->best_season ?></span>
                </div>
                <div class="tour-code">
                    Minimum Group Size: <span><?php echo $content['info']->minimum_group_size ?></span>
                </div>
                <div class="tour-code">
                    Altitude: <span><?php echo $content['info']->altitude . ' mts.' ?></span>
                </div>
                <div class="tour-code">
                    Support: <span><?php echo ($content['info']->support == '1') ? 'Yes' : 'No' ?></span>
                </div>
                <div class="about-text">
                    <p><?php echo $content['info']->short_description ?></p>
                </div>
                
                <!--<a href="<?php echo current_url().'/book/' ?>" class="btn-book-tour">BOOK NOW</a>-->
                <a class="btn-all btn-book-tour book-now" data-date="" rel="<?php echo $tour_slug?>" href="javascript:void(0);">Book now</a>
                <!--<a href="<?php echo current_url().'/enquire/' ?>" class="btn-enquire-tour">ENQUIRE NOW</a>-->
                <a class="btn-enquire btn-enquire-tour btn-all" rel="<?php echo $tour_slug?>" href="javascript:void(0);">Enquire now</a>
                <a class="download-tour-detail" href="javascript:void(0);" file="<?php echo $content['info']->file ?>">Download Pdf</a>
                <!--<a href="javascript:void(0);" rel="<?php echo $content['info']->file ?>" class="download-tour-detail">Download .PDF</a>-->

            </div>
            <!--<form id="download_pdf_file" method="post" action="<?php echo base_url('download/index') ?>">
                <input type="hidden" id="file" name="file" value="">
            </form>-->
        </div>
        
        <!--<div class="share-tour">
            <h3>SHARE</h3>
            <span class='st_sharethis_hcount' st_title="<?php /*echo $content['info']->name */?>" st_url="<?php /*echo current_url() */?>" st_image="<?php /*echo base_url($content['info']->cover_image) */?>" st_summary="<?php /*echo $content['info']->short_description */?>"></span>
        </div>-->
    </div>

    <div class="col-sm-12 col-md-9 col-lg-9 col-lg-pull-3 col-md-pull-3">
        <div class="tours-status-wrap">
            <?php if(isset($social_datas) && !empty($social_datas)) { ?>
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_sharing_toolbox share-page"></div>
            <?php } ?>
            <?php
            $divClass = '';
            if(count($content['gallery']) > 0) {
                $divClass = ' class="owl-carousel tour-detail-slide-owl"';
            }
            ?>
            <div id="tour-detail-slide"<?php echo $divClass ?>>
                <?php if(!empty($content['info']->cover_image)) { ?>
                    <div><img class="img-responsive" src="<?php echo base_url($content['info']->cover_image) ?>" alt="<?php echo $content['info']->name ?>"></div>
                <?php } ?>
                <?php 
                if(isset($content['gallery']) && !empty($content['gallery'])) {
                    foreach($content['gallery'] as $img) {
                    ?>
                    <div>
                        <img class="img-responsive" src="<?php echo base_url($img->image) ?>" alt="<?php echo $img->title ?>">
                    </div>
                    <?php
                    }
                }
                ?>
            </div>
        
            <!--<div class="bookingform-wrap">
                <?php /*$this->load->view('form/book_tour.php'); */?>
            </div>
            <div class="enquireform-wrap">
                <?php /*$this->load->view('form/enquire_tour.php'); */?>
            </div>-->
        </div>
        <div class="tour-detail-tab">
            <div class="row">
                <div class="col-sm-1 col-lg-1">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a class="overview" href="#overview" data-toggle="tab">Overview</a></li>
                        <li><a class="itinerary" href="#itineray" data-toggle="tab">ITINERARY</a></li>
                        <li><a class="costing" href="#costing" data-toggle="tab">COSTING</a></li>
                        <?php
                        $onclick = '';
                        if(!empty($content['info']->map_file)) {
                            $map_type = 'file';
                            $onclick = ' onclick="initMap()"';
                        }
                        if(!empty($content['tour_map'])) {
                            $map_type = 'lat_long';
                            $onclick = '';
                        }
                        if(!empty($content['info']->map_file) && !empty($content['tour_map'])) {
                            $map_type = 'both';
                            $onclick = '';
                        }
                        ?>
                        <li><a class="tab-map" href="#tour-map" data-toggle="tab"<?php echo $onclick ?>>MAP</a></li>
                    </ul>
                </div>

                <div class="col-sm-11 col-lg-11">
                    <div id="myTabContent" class="tab-content" >
                        <div class="tab-pane fade active in" id="overview">
                            <h4 class="tab-title">OVERVIEW</h4>
                            <?php echo $content['info']->description ?>
                        </div>

                        <div class="tab-pane fade" id="itineray">
                            <h4 class="tab-title">ITINERARY</h4>
                            <div class="tour-planning">
                                <?php echo $content['info']->itinerary; ?>
                            </div> 
                            <?php if(isset($content['inexclusions']) && !empty($content['inexclusions'])) { ?>
                            <div class="row">
                                <?php if(isset($content['inexclusions']['inclusion']) && !empty($content['inexclusions']['inclusion'])) { ?>
                                <div class="col-lg-6">
                                    <div class="table-responsive">
                                        <table class="table table-bordered custom-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Cost Includes
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                <?php foreach($content['inexclusions']['inclusion'] as $ind => $val) { ?>
                                                    <td><?php echo $val ?></td>
                                                    <?php 
                                                    if($ind % 2 != 0)  {
                                                        echo '</tr><tr>';
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if(isset($content['inexclusions']['exclusion']) && !empty($content['inexclusions']['exclusion'])) { ?>
                                <div class="col-lg-6">
                                    <div class="table-responsive">
                                        <table class="table table-bordered custom-table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        Cost Excludes
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <?php foreach($content['inexclusions']['exclusion'] as $ind => $val) { ?>
                                                        <td><?php echo $val ?></td>
                                                        <?php
                                                        if($ind % 2 != 0) {
                                                            echo '</tr><tr>';
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>

                        <div class="tab-pane fade" id="costing">
                            <h4 class="tab-title">TOUR PRICE</h4>
                            <?php if(isset($content['tour_price']) && !empty($content['tour_price'])) { ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered custom-table">
                                            <thead>
                                                <tr>
                                                    <th>From</th>
                                                    <th>To</th>
                                                    <th>Nos. of Pax</th>
                                                    <th>Unit Price ($)</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($content['tour_price'] as $val) { ?>
                                                <tr>
                                                    <td><?php echo $val->valid_from ?></td>
                                                    <td><?php echo $val->valid_to ?></td>
                                                    <td><?php echo $val->number_of_pax ?></td>
                                                    <td><?php echo $val->unit_price ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <?php } else { ?>
                                <p>No Price Added as of Yet.</p>
                            <?php } ?>
                        </div>
                        
                        <div class="tab-pane fade" id="tour-map">
                            <?php
                            if((isset($content['tour_map']) && !empty($content['tour_map'])) || (isset($content['info']->map_file) && !empty($content['info']->map_file))) {
                                if(!empty($content['info']->map_file)) {
                                    $map_type = 'file';
                                }
                                if(!empty($content['tour_map'])) {
                                    $map_type = 'lat_long';
                                }
                                if(!empty($content['info']->map_file) && !empty($content['tour_map'])) {
                                    $map_type = 'both';
                                }
                                if($map_type == 'file') {
                                ?>
                                    <div id="map" style="height: 800px;"></div>
                                    <input type="hidden" id="hidden_lat_long" value='0' />
                                    <input type="hidden" id="hidden_map_file" value="<?php echo $content['info']->map_file ?>">
                                    <script>
                                        function initMap() {
                                            setTimeout(function(){
                                                var map = new google.maps.Map(document.getElementById('map'), {
                                                    zoom: 11,
                                                    center: {lat: 27.740132, lng: 85.331406}
                                                });

                                                var kmlUrl = $("#baseUrl").val() + $("#hidden_map_file").val();

                                                var kmlOptions = {
                                                    suppressInfoWindows: true,
                                                    preserveViewport: false,
                                                    map: map
                                                };
                                                var kmlLayer = new google.maps.KmlLayer(kmlUrl, kmlOptions);
                                            }, 200);
                                        }
                                    </script>
                                    <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEVo4KtVSWz6boY2tMwfZojpnoV44fUtY&signed_in=true">
                                    </script>
                                    <input type="hidden" id="hidden_map_type" value="mapfile">
                                <?php
                                } else if($map_type == 'lat_long') { ?>
                                    <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEVo4KtVSWz6boY2tMwfZojpnoV44fUtY&signed_in=true">
                                    </script>
                                    <input type="hidden" id="hidden_lat_long" value='<?php echo $content['tour_map'] ?>' />
                                    <div id="map-canvas"></div>
                                    <input type="hidden" id="hidden_map_type" value="maplatlong">
                                <?php
                                } else { ?>
                                    <a href="javascript:void(0);" class="view-detail-map">View Detail Map</a>
                                    <div id="map" style="display:none"></div>
                                    <input type="hidden" id="hidden_map_file" value="<?php echo $content['info']->map_file ?>">
                                    <script>
                                    function initMap() {
                                        setTimeout(function(){
                                            var map = new google.maps.Map(document.getElementById('map'), {
                                                zoom: 11,
                                                center: {lat: 27.740132, lng: 85.331406}
                                            });

                                            var kmlUrl = $("#baseUrl").val() + $("#hidden_map_file").val();

                                            var kmlOptions = {
                                                suppressInfoWindows: true,
                                                preserveViewport: false,
                                                map: map
                                            };
                                            var kmlLayer = new google.maps.KmlLayer(kmlUrl, kmlOptions);
                                        }, 200);



                                    }
                                </script>
                                    <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEVo4KtVSWz6boY2tMwfZojpnoV44fUtY&signed_in=true">
                                    </script>
                                    <input type="hidden" id="hidden_lat_long" value='<?php echo $content['tour_map'] ?>' />
                                    <div id="map-canvas" style="height:800px"></div>
                                    <input type="hidden" id="hidden_map_type" value="maplatlong">
                                <?php
                                }
                            } else { ?>
                            <input type="hidden" id="hidden_lat_long" value='0' />
                                <p>No Map Available as of Now</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="hidden_tour_bike_id" value="<?php echo $content['info']->id ?>" />
<input type="hidden" id="hidden_tour_bike_type" value="tour" />