<div class="highlighted-post">
    <h3 class="post-wrap-title">Top Story</h3>
    <?php 
    if(isset($hot_news) && !empty($hot_news)) {
    	foreach($hot_news as $news) {
    		?>
		    <a href="<?php echo site_url('news/' . $news->slug) ?>" class="post-wrap">
		    	<?php if(!empty($news->image)) { ?>
			        <div class="post-img-wrap">
			            <img class="" src="<?php echo base_url($news->image) ?>" alt="<?php echo $news->name ?>" />
			        </div>
		        <?php } ?>
		        <h3><?php echo $news->name ?></h3>
		        <!--<span class="post-by"> By Ric McLaughlin on 17 September 2015 </span>-->
		    </a>
    	<?php
    	}
    } else { ?>
    	<p><?php echo 'No News Highlighted as of Yet.' ?></p>
    <?php } ?>
</div>