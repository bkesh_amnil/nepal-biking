<h3 class="post-wrap-title">News</h3>

<div class="row">
	<?php 
	if(!empty($latest_news)) {
		foreach($latest_news as $news) {
		?>
		    <div class="col-xs-12 col-sm-6 col-lg-6">
		        <a href="<?php echo site_url('news/' . $news->slug) ?>" class="post-wrap">
		        	<?php if(!empty($news->image)) { ?>
			            <div class="post-img-wrap">
			                <img class="" src="<?php echo base_url($news->image) ?>" alt="<?php echo $news->name ?>" />
			            </div>
		            <?php } ?>
		            <h3><?php echo $news->name ?></h3>
		            <!--<span class="post-by"> By Ric McLaughlin on 17 September 2015 </span>-->
		        </a>
		    </div>
	    <?php
		}
	} else { ?>
		<p><?php echo 'No News Added as of Yet.' ?></p>
	<?php } ?>
</div>