        <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url() ?>">
        <input type="hidden" id="hidden_total_pages" value="<?php echo (isset($total_pages) && !empty($total_pages)) ? $total_pages : 0 ?>">
        <input type="hidden" id="hidden_current_url" value="<?php echo current_url() ?>">
        <input type="hidden" id="hidden_count" value="<?php echo isset($count) ? $count : '' ?>">

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                         <!--<a href="<?php echo site_url() ?>" class="footer-logo"><img src="<?php echo base_url($site_logo) ?>" alt="<?php echo $site_title ?>"/></a>-->
                         <?php if(isset($footer_logos) && !empty($footer_logos)) { ?>
                        <div class="logoHolder">
                            <span>Recommended By</span>
                            <?php
                            foreach($footer_logos as $footer_logo) {
                                if(!empty($footer_logo->url)) {
                                    ?>
                                    <a href="<?php echo $footer_logo->url ?>" target="_blank">
                                    <?php } ?>
                                        <img src="<?php echo base_url($footer_logo->image) ?>" alt="<?php echo $footer_logo->name ?>" />
                                    <?php if(!empty($footer_logo->url)) { ?>
                                    </a>
                                    <?php } ?>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    
                    <!--<div class="col-xs-12 col-sm-6 col-lg-4">
                        <?php $this->load->view('form/subscribe.php') ?>
                        
                    </div>-->
                    
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="follow-subscribe clearfix">
                            <div class="follow-dawn">
                                <span>Follow Dawn Till Dusk</span>
                                <?php if(isset($facebook) && !empty($facebook)) { ?>
                                    <a class="facebook" href="<?php echo $facebook ?>" target="_blank"></a>
                                <?php } ?>
                                <?php if(isset($twitter) && !empty($twitter)) { ?>
                                    <a class="twitter" href="<?php echo $twitter ?>" target="_blank"></a>
                                <?php } ?>
                                <?php if(isset($youtube) && !empty($youtube)) { ?>
                                    <a class="youtube" href="<?php echo $youtube ?>" target="_blank"></a>
                                <?php } ?>
                                <?php if(isset($instagram) && !empty($instagram)) { ?>
                                    <a class="instagram" href="<?php echo $instagram ?>" target="_blank"></a>
                                <?php } ?>
                                <?php if(isset($pinterest) && !empty($pinterest)) { ?>
                                    <a class="pinterest" href="<?php echo $pinterest ?>" target="_blank"></a>
                                <?php } ?>
                            </div>
                            <?php $this->load->view('form/subscribe.php') ?>
                        </div>
                    </div>

                    <div class="col-xs-12 visible-xs footer-navbar-nav">
                    <?php if(isset($top_menus) && !empty($top_menus)) { ?>
                        <ul class="nav navbar-nav">
                            <?php 
                            foreach($top_menus as $top_menu) {
                                $id = '';
                                $slug = site_url($top_menu['slug']);
                                if($top_menu['slug'] == '#') {
                                    $slug = "javascript:void(0);";
                                    $id = 'testimonial_popup';
                                }
                            ?>
                                <li><a href="<?php echo $slug ?>" id="<?php echo $id ?>">
                                    <?php echo $top_menu['name'] ?>
                                </a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                </div>
            </div>
            
            <div class="footer-nav-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-lg-6">
                            <nav>
                                <?php if(isset($bottom_menus) && !empty($bottom_menus)) { ?>
                                <ul class="nav navbar-nav footer-nav">
                                    <?php
                                    foreach($bottom_menus as $bottom_menu) {
                                        $target = '';
                                        if($bottom_menu['menu_opens'] == 'new') {
                                            $target = ' target="_blank"';
                                        }
                                        ?>
                                    <li><a href="<?php echo site_url($bottom_menu['slug']) ?>"<?php echo $target ?>><?php echo ucfirst($bottom_menu['name']) ?></a></li>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                            </nav>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-lg-6">
                            <div class="copy-right">
                                ©<?php echo date('Y') ?> Dawn Till Dusk . All Rights Reserved. Powered by <a class="amnil" href="http://www.candidservice.com/" target="_blank">Candid Service</a>
                            </div>
                        </div>
                    </div>
                     
                </div>
            </div>
        </footer>

        <a href="#0" class="cd-top">Top</a>
        <?php $this->load->view('form/download_pdf.php') ?>
        <?php $this->load->view('form/testimonial_form.php') ?>

        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <span class="success-msg"></span>
                        <button class="btn-ok" type="submit" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="messageModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel"></h4>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
        <?php $this->load->view('form/book_tour_modal.php'); ?>
        <?php $this->load->view('form/enquire_tour_modal.php'); ?>
        <script src="<?php echo base_url('js/vendor/smart-menu/jquery.smartmenus.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/vendor/smart-menu/jquery.smartmenus.bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/jquery.validate.min.js') ?>" type="text/javascript" ></script>
        <script src="<?php echo base_url('js/jquery.autocomplete.js') ?> " type="text/javascript"></script>
        <script src="<?php echo base_url('assets/ckeditor/ckeditor.js') ?>" type="text/javascript"></script>
        <?php if($active_menu == 'team' || $active_menu == 'home' || ($active_menu == 'tour' && $count > 1) || ($active_menu == 'nepalbiketours' && $count > 1) || ($active_menu == 'bike-rent' && $count > 1) || ($active_menu == 'bike-accessories' && $count > 1)) { ?>
        <script src="<?php echo base_url('js/vendor/owl-carasouel/owl.carousel.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/owl-custom.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php /*if(($active_menu == 'tour' && $count == 1) || ($module_name == 'news' && $count == 1) || ($module_name == 'events' && $count == 1)) { ?>
        <script src="<?php echo base_url('js/jquery.autocomplete.js') ?> " type="text/javascript"></script>
        <?php }*/ ?>
        <?php if(($active_menu == 'tour' && $count == 1) || ($active_menu == 'nepalbiketours' && $count == 1)) { ?>
        <script src="<?php echo base_url('js/tour-filter.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'bike-accessories' && $count == 1) { ?>
        <script src="<?php echo base_url('js/product-filter.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'bike-accessories' && $count > 1) { ?>
            <script src="<?php echo base_url('js/product-order.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'bike-rent' && $count == 1) { ?>
        <script src="<?php echo base_url('js/bike-rent-filter.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if(($module_name == 'news' && $count == 1) || ($module_name == 'event' && $count == 1)) { ?>
        <script src="<?php echo base_url('js/news-events-filter.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if(($active_menu == 'tour' && $count == 1) || ($active_menu == 'nepalbiketours' && $count == 1) || ($module_name == 'news' && $count == 1) || ($module_name == 'event' && $count == 1) || ($active_menu == 'bike-rent' && $count == 1) || ($active_menu == 'bike-accessories' && $count == 1)) { ?>
        <script src="<?php echo base_url('js/load-more.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if(($module_name == 'news' && $count > 1) || ($module_name == 'event' && $count > 1) || ($module_name == 'detail' && $count > 1)) { ?>
        <script src="<?php echo base_url('js/news-events.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'about-us' || $active_menu == 'about-dawn-till-dusk' || $active_menu == 'faq' || ($active_menu == 'tour' && $count > 1 && $count < 3) || ($active_menu == 'nepalbiketours' && $count > 1 && $count < 3) || $active_menu == 'race') { ?>
        <script src="<?php echo base_url('js/bootstrap-tabcollapse.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/custom-tab-collapse.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'about-us' || $active_menu == 'about-dawn-till-dusk' || $active_menu == 'faq' || $active_menu == 'team') { ?>
        <script src="<?php echo base_url('js/faq-about.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <script src="<?php echo base_url('js/custom_1.js') ?>" type="text/javascript"></script>
        <?php if(/*$active_menu == 'contact-us' || */($active_menu == 'tour' || $active_menu == 'nepalbiketours') && $count > 1) { ?>
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <?php } ?>
        <?php if($active_menu == 'contact-us') { ?>
        <script src="<?php echo base_url('js/contact-us.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/form.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php  /*if(($active_menu == 'bike-rent' && $count > 1) || ($active_menu == 'tour' && $count > 1)) { */?><!--
        <script src="<?php /*echo base_url('assets/datepicker/bootstrap-datepicker.js') */?>" type="text/javascript"></script>
        --><?php /*}*/ ?>
        <?php if(($active_menu == 'tour' && $count == 3) || ($active_menu == 'nepalbiketours' && $count == 3)) { ?>
        <script src="<?php echo base_url('js/datepicker.js') ?>"></script>
        <script src="<?php echo base_url('js/tour.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'bike-rent' && $count > 1) { ?>
        <script src="<?php echo base_url('js/datepicker.js') ?>"></script>
        <script src="<?php echo base_url('js/bike-rent.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if(($active_menu == 'tour' && $count > 1 && $count < 3) || ($active_menu == 'nepalbiketours' && $count > 1 && $count < 3)) { ?>
        <script src="<?php echo base_url('js/maps.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/datepicker.js') ?>"></script>
        <script src="<?php echo base_url('js/tour.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'departure') { ?>
        <script src="<?php echo base_url('js/fixed-departure.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if(($active_menu == 'gallery' && $count > 1) || $active_menu == 'video' || $active_menu == 'bikinginnepal-Video') { ?>
        <script src="<?php echo base_url('js/jquery.fancybox.pack.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/jquery.fancybox-media.js') ?>"></script>
        <script src="<?php echo base_url('js/gallery.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'about-us' || $active_menu == 'about-dawn-till-dusk' || $active_menu == 'faq' || $active_menu == 'team') { ?>
        <script src="<?php echo base_url('js/content.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if(($active_menu == 'tour' && $count > 1 && $count < 3) || ($active_menu == 'nepalbiketours' && $count > 1 && $count < 3) || ($active_menu == 'bike-rent' && $count > 1) || $active_menu = 'home') { ?>
        <script src="<?php echo base_url('js/download.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($module_name == 'testimonial' && $count > 1) { ?>
            <script src="<?php echo base_url('js/testimonial.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'tour' || $active_menu == 'nepalbiketours') { ?>
            <script type="text/javascript">
                $(document).on("click", "a.view-detail-map", function() {

                    $("a.view-detail-map").text('View Lat-Long Map');
                    $("a.view-detail-map").addClass("view-latlong-map");
                    $("a.view-detail-map").removeClass("view-detail-map");

                    $("div#map").css("display", "block");
                    $("div#map").css("height", "800px");
                    $("div#map-canvas").css("display", "none");
                    $("div#map-canvas").css("height", "0px");
                    initMap();
                })

                $(document).on("click", "a.view-latlong-map", function() {

                    $("a.view-latlong-map").text('View Detail Map');
                    $("a.view-latlong-map").addClass("view-detail-map");
                    $("a.view-latlong-map").removeClass("view-latlong-map");

                    $("div#map").css("display", "none");
                    $("div#map").css("height", "0px");
                    $("div#map-canvas").css("display", "block");
                    $("div#map-canvas").css("height", "800px");
                })
            </script>
        <?php } ?>
<!-- Google Tracking Code -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30339249-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- Google Tracking Code -->
    </body>
</html>