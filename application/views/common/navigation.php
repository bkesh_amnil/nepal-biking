<body>
    <header>
        <nav class="navbar top-header-nav-wrap">
            <div class="container">
                <div class="navbar-collapse collapse navbar-left">
                    <?php if(isset($top_menus) && !empty($top_menus)) { ?>
                        <ul class="nav navbar-nav">
                            <?php 
                            foreach($top_menus as $top_menu) {
                                $target = '';
                                $id = '';
                                $slug = site_url($top_menu['slug']);
                                if($top_menu['slug'] == '#') {
                                    $slug = "javascript:void(0);";
                                    $id = 'testimonial_popup';
                                }
                                if($top_menu['menu_opens'] == 'new') {
                                    $target = ' target="_blank"';
                                }
                            ?>
                                <li><a href="<?php echo $slug ?>" id="<?php echo $id ?>"<?php echo $target ?>>
                                    <?php echo $top_menu['name'] ?>
                                </a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                
                <div class="header-top-content">
                    <div class="connect-us">
                        <span>CONNECT WITH US</span>
                        <?php if(isset($facebook) && !empty($facebook)) { ?>
                            <a class="facebook" href="<?php echo $facebook ?>" target="_blank"></a>
                        <?php } ?>
                        <?php if(isset($twitter) && !empty($twitter)) { ?>
                            <a class="twitter" href="<?php echo $twitter ?>" target="_blank"></a>
                        <?php } ?>
                        <?php if(isset($youtube) && !empty($youtube)) { ?>
                            <a class="youtube" href="<?php echo $youtube ?>" target="_blank"></a>
                        <?php } ?>
                        <?php if(isset($instagram) && !empty($instagram)) { ?>
                            <a class="instagram" href="<?php echo $instagram ?>" target="_blank"></a>
                        <?php } ?>
                        <?php if(isset($pinterest) && !empty($pinterest)) { ?>
                            <a class="pinterest" href="<?php echo $pinterest ?>" target="_blank"></a>
                        <?php } ?>
                    </div>       
                </div>
            </div>
        </nav>

        <nav class="navbar header-nav-wrap">
            <div class="container">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>               
                    <h1><a href="<?php echo site_url() ?>" class="navbar-brand header-logo"><img src="<?php echo base_url($site_logo) ?>" alt="<?php echo $site_name ?>"/></a></h1>
                </div>

                <div class="navbar-collapse collapse header-nav" id="navbar">
                    <?php if(isset($main_menus) && !empty($main_menus)) { ?>
                    <ul class="nav navbar-nav">
                        <?php 
                        foreach($main_menus as $main_menu) {
                            $target = '';
                            $class = $dropdownClass = '';
                            if($active_menu == $main_menu['slug']) {
                                $class= 'active';
                            }
                            if(isset($main_menu['childs']) && !empty($main_menu['childs'])) {
                                $dropdownClass = ' dropdown';
                            }
                            $url = site_url($main_menu['slug']);
                            if($main_menu['slug'] == 'home') {
                                $url = site_url();
                            }
                            if($main_menu['menu_opens'] == 'new') {
                                $target = ' target="_blank"';
                            }
                            ?>
                            <li class="<?php echo $class.$dropdownClass ?>">
                                <?php if(isset($main_menu['childs']) && !empty($main_menu['childs'])) { ?>
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"<?php echo $target ?>>
                                        <?php echo $main_menu['name'] ?>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach($main_menu['childs'] as $child) {
                                            $target1 = '';
                                            if($child['menu_opens'] == 'new') {
                                                $target1 = ' target="_blank"';
                                            }
                                            ?>
                                            <li><a class="hvr-sweep-to-right" href="<?php echo site_url($child['slug']) ?>"<?php echo $target1 ?>><?php echo $child['name'] ?></a></li> 
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <a href="<?php echo $url ?>"<?php echo $target ?>><?php echo $main_menu['name'] ?></a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php } ?>

                    <?php $this->load->view('common/search') ?>
                </div>
            </div>
        </nav>
    </header>