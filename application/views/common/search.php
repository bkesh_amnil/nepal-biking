<form class="navbar-form search-wrap" method="post" action="<?php echo base_url('search') ?>">
    <input name="search_text" class="form-control" id="search" type="text" placeholder="<?php echo (isset($search_text) && !empty($search_text) ? $search_text: 'SEARCH') ?>">
    <i class="glyphicon glyphicon-search"></i>
</form>