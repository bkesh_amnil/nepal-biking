<?php

class MY_Form_validation extends CI_Form_validation {

	function unique($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('unique',	'The %s is already being used.');

		list($table, $field, $cur_id) = explode(".", $params, 3);
	
		$cur_id = (int)$cur_id;
		if($cur_id)
			$CI->db->where('id != ', $cur_id);
			
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();

		if ($query->row()) {
			return false;
		} else {
			return true;
		}
	}

	function valid_google_url($value) {
		$CI =& get_instance();

		$CI->form_validation->set_message('valid_google_url',	'The %s is not valid.');

		if (strpos($value,'http://goo.gl/forms/') !== false) {
			return true;
		} else {
			return false;
		}
	}

	function valid_size($value, $params) {
		$CI =& get_instance();

		list($table, $field) = explode(".", $params, 2);

		switch($table) {
			case "tbl_banner":
				$width = '770';
				$height = '509';
				break;
			case "tbl_news":
			case "tbl_event":
				switch ($field) {
					case "image":
						$width = '573';
						$height = '416';
						break;
					case "background_image":
						$width = '1920';
						$height = '1199';
						break;
				}
				break;
			case "tbl_race":
				switch($field){
					case "cover_image":
						$width = '188';
						$height = '188';
						break;
					case "backgroud_image":
						$width = '1920';
						$height = '1199';
						break;
				}
				break;
			case "tbl_content":
				switch($field){
					case "image":
						$width = '188';
						$height = '188';
						break;
					case "background_image":
						$width = '1920';
						$height = '1199';
						break;
				}
				break;
			case "tbl_team":
				$width = '690';
				$height = '350';
				break;
			case "tbl_gallery":
				$width = '1920';
				$height = '1323';
				break;
			case "tbl_testimonial":
				$width = '215';
				$height = '200';
				break;
			case "tbl_tour":
				$width = '1920';
				$height = '1323';
				break;
			case "tbl_bike":
				$width = '606';
				$height = '350';
				break;
			case "tbl_product":
				$width = '606';
				$height = '350';
				break;
			case "tbl_background_image":
				$width = "1920";
				$height = "1199";
				break;

		}
		
		$img_size = getimagesize(base_url($value));
		$img_width = $img_size[0];
		$img_height = $img_size[1];

		if($img_width > $width || $img_height > $height) {
			$CI->form_validation->set_message('valid_size', 'The %s size is bigger');
			return false;
		} else if($img_width < $width || $img_height < $height) {
			$CI->form_validation->set_message('valid_size', 'The %s size is smaller');
			return false;
		} else {
			return true;
		}
	}

	function valid_file($value, $params) {
		$CI =& get_instance();

		list($table, $field) = explode(".", $params, 2);

		$file_ext = pathinfo(base_url($value), PATHINFO_EXTENSION);

		if($file_ext != 'pdf') {
			$CI->form_validation->set_message('valid_file', 'File isn\'t pdf');
			return false;
		} else {
			return true;
		}
	}
	
	function exists($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('exists',	'The %s does not exist.');

		list($table, $field) = explode(".", $params, 2);
	
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();
	
		if ( ! $query->row()) {
			return false;
		} else {
			return true;
		}
	}
	
	//calling:: fixed_values[Active,Inactive]
	function fixed_values($value, $valid_values = '')
	{
		$this->CI->form_validation->set_message('fixed_values',	'The %s is invalid.');
		
		if($valid_values == '') {		
			$array = array('Yes', 'No');
		} else {
			$array = explode(',', $valid_values);
		}
		
		return (in_array(trim($value), $array)) ? TRUE : FALSE;
	}
	
	function url($value)
	{
		
	}
}
?>