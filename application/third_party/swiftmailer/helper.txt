function swiftsend($params)
{
    require_once APPPATH.'third_party/swiftmailer/swift_required.php';

    try {
        if($_SERVER['SERVER_NAME'] === 'localhost') {
            // Create the Transport
            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername('satish.localmail@gmail.com')
                ->setPassword('neversaydie');
        } else {
            $transport = Swift_SendmailTransport::newInstance();
        }

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        // Create a message
        $message = Swift_Message::newInstance($params['subject'])
            ->setFrom(array($params['from'] => $params['fromname']))
            ->setTo(array($params['to'] => $params['toname']))
            ->setBody($params['message'], 'text/html');

        // Send the message
        $result = $mailer->send($message);

        return $result;
    } catch(Exception $e) {
        if(ENVIRONMENT == 'development') {
            debug($e);
        } else {
            redirect();
        }
    }
}